#ifndef POKERSTARSHOOK_H_
#define POKERSTARSHOOK_H_

#if defined DLL_EXPORT
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif

#include "EasyHook.h"

extern "C"
{
   DECLDIR void __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO* InRemoteInfo);
}


#endif