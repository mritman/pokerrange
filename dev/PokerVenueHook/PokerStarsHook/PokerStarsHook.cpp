#define DLL_EXPORT

#include <Windows.h>
#include <iostream>
#include "PokerStarsHook.h"
#include "EasyHook.h"
#include "UnicodeStringSender.h"
//#include "ChatWindow.h"

TRACED_HOOK_HANDLE hHook = NULL;
UnicodeStringSender* sender = NULL;
boolean attached = false;

// This function is used in a loop that iterates over the chat line 
// and is called with a pointer to the next character in the line on each loop iteration.
typedef void (__thiscall * Function1_t)(void *__this, wchar_t* line, int size);
Function1_t Real_Function1 = (Function1_t) 0x0086B440; // Outdated.

// This function receives the chat lines. It can receives the same chat line multiple times.
typedef int (__thiscall * Real_Function2_t)(void *__this, wchar_t* line, int size, int, int, int, int);

//Previous: 0x007BF690
Real_Function2_t Real_Function2 = (Real_Function2_t) 0x007FED16; // Outdated.

void DetachHook(TRACED_HOOK_HANDLE hHook);

void __fastcall Mine_Function1(void *__this, int edx, wchar_t* line, int size)
{
	//AppendLine((LPCWSTR)line);
	sender->sendString(line);
    Real_Function1(__this, line, size);
}

void __fastcall Mine_Function2(void *__this, int edx, wchar_t* line, int size, int a, int b, int c, int d)
{
	//AppendLine((LPCWSTR)line);

	boolean success = sender->sendString(line);
	if (!success) {
		DetachHook(hHook);
	}
	
	Real_Function2(__this, line, size, a, b, c, d);
}

void AttachHook(TRACED_HOOK_HANDLE hHook, int port)
{
	if (!attached) {
		//initChatWindow(hInstDll);

		sender = new UnicodeStringSender(port);
		sender->init();

		hHook = new HOOK_TRACE_INFO();

		LhInstallHook(
        Real_Function2,
        Mine_Function2,
        NULL,
        hHook);
	
		ULONG threadList[1] = { (ULONG)-1 };
		LhSetExclusiveACL(threadList, 1, hHook);

		RhWakeUpProcess();

		attached = true;
	}
}

void DetachHook(TRACED_HOOK_HANDLE hHook) 
{
	if (attached) {
		sender->close();
		delete sender;
		sender = NULL;
		LhUninstallHook(hHook);
		delete hHook;
		hHook = NULL;
		LhWaitForPendingRemovals();
		attached = false;
	}
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD dwReason, LPVOID lpvReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH) {
		
    } else if (dwReason == DLL_PROCESS_DETACH) {
		DetachHook(hHook);
    }

    return TRUE;
}

void __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO* InRemoteInfo) 
{
	int* port = (int*) InRemoteInfo->UserData;
	AttachHook(hHook, *port);
}