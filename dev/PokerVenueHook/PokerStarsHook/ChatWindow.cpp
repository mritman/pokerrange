#include <Windows.h>
#include <iostream>

#define WM_APPEND_LINE	401

#include "ChatWindow.h"

HINSTANCE inj_hInstance;
HWND hwnd;
HWND edit_hWnd;

void AppendLineInt(LPCWSTR line) {
	std::wstring newLine;

	newLine = (std::wstring)line;
	newLine += TEXT("\n");

	int iLen = GetWindowTextLength(edit_hWnd);
	int start = iLen;

	if (iLen > 10000) {
		start = 0;
	}

	SendMessage(edit_hWnd, EM_SETSEL, start, iLen);
	SendMessage(edit_hWnd, EM_REPLACESEL, true, (WPARAM)newLine.c_str());
}

void AppendLine(LPCWSTR line) {
	COPYDATASTRUCT cds;

	cds.dwData = WM_APPEND_LINE;
	cds.cbData = (wcslen(line)*sizeof(wchar_t))+2;
	cds.lpData = (PVOID) line;

	SendMessage(hwnd, WM_COPYDATA, 0, (LPARAM)(LPVOID) &cds);
}

LRESULT CALLBACK DLLWindowProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		case WM_COPYDATA:
			{
			PCOPYDATASTRUCT pCds = (PCOPYDATASTRUCT) lParam;

			switch(pCds->dwData)
			{
				case WM_APPEND_LINE:
					AppendLineInt((LPCWSTR) pCds->lpData);
					break;
			}

			break;
			}
		case WM_DESTROY:
			PostQuitMessage (0);
			break;
		default:
			return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

//Register our windows Class
BOOL RegisterDLLWindowClass(wchar_t szClassName[])
{
    WNDCLASSEX wc;

    wc.hInstance =  inj_hInstance;
    wc.lpszClassName = (LPCWSTR)szClassName;
    wc.lpfnWndProc = DLLWindowProc;
    wc.style = CS_DBLCLKS;
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.lpszMenuName = NULL;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    if (!RegisterClassEx(&wc))
		return 0;

	return 1;
}

//The new thread
DWORD WINAPI ThreadProc(LPVOID lpParam)
{
    MSG messages;
	wchar_t *pString = reinterpret_cast<wchar_t * > (lpParam);

	RegisterDLLWindowClass(L"InjectedDLLWindowClass");

	hwnd = CreateWindowEx(0, TEXT("InjectedDLLWindowClass"), pString, WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT, 400, 300, NULL, NULL, inj_hInstance, NULL );

	RECT clientRect;
	GetClientRect(hwnd, &clientRect);

	edit_hWnd = CreateWindow(TEXT("edit"), TEXT(""), WS_VISIBLE|WS_CHILD|WS_BORDER|WS_VSCROLL|WS_HSCROLL|ES_MULTILINE|ES_WANTRETURN|ES_AUTOHSCROLL|ES_AUTOVSCROLL, 0, 0, clientRect.right, clientRect.bottom, hwnd, NULL, inj_hInstance, NULL);

	ShowWindow(hwnd, SW_SHOWNORMAL);
	UpdateWindow(hwnd);

    while (GetMessage(&messages, NULL, 0, 0))
    {
		TranslateMessage(&messages);
        DispatchMessage(&messages);
    }

    return 1;
}

void initChatWindow(HINSTANCE hInstance) {
	inj_hInstance = hInstance;
	CreateThread(0, NULL, ThreadProc, (LPVOID)TEXT("Chat Log"), NULL, NULL);
}