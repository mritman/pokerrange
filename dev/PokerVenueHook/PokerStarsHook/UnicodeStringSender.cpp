#include <Windows.h>
#include <iostream>
#include <stdint.h>
#include "UnicodeStringSender.h"

UnicodeStringSender::UnicodeStringSender(int port) {
	serverPort = port;
	ConnectSocket = INVALID_SOCKET;
	initialized = false;
	closed = false;
}

boolean UnicodeStringSender::sendString(wchar_t* string) {
	if (!initialized || closed) return false; 

	std::string utf8String = toUtf8(string);
	utf8String += "\n";

	int iResult = send(ConnectSocket, utf8String.c_str(), utf8String.size(), 0);
    if (iResult == SOCKET_ERROR) {
        close();
        return false;
    }

	return true;
}

// Convert a wide Unicode string to a UTF8 string
std::string UnicodeStringSender::toUtf8(const std::wstring &wstr)
{
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo(size_needed, 0);
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
}

boolean UnicodeStringSender::init() {
	if (initialized || closed) return false;

	//----------------------
    // Declare and initialize variables.
    int iResult;
    WSADATA wsaData;

    struct sockaddr_in clientService; 

	//----------------------
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != NO_ERROR) {
		closed = true;
        return false;
    }

	//----------------------
    // Create a SOCKET for connecting to server
    ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (ConnectSocket == INVALID_SOCKET) {
        WSACleanup();
		closed = true;
        return false;
    }

	//----------------------
    // The sockaddr_in structure specifies the address family,
    // IP address, and port of the server to be connected to.
    clientService.sin_family = AF_INET;
    clientService.sin_addr.s_addr = inet_addr("127.0.0.1");
    clientService.sin_port = htons(serverPort);

    //----------------------
    // Connect to server.
    iResult = connect( ConnectSocket, (SOCKADDR*) &clientService, sizeof(clientService) );
    if (iResult == SOCKET_ERROR) {
        close();
        return false;
	}

	initialized = true;

	return true;
}

void UnicodeStringSender::close() {
	if (!closed) {
		closesocket(ConnectSocket);
		WSACleanup();
		closed = true;
	}
}