#include <Windows.h>

#ifndef CHATWINDOW_H
#define CHATWINDOW_H

void initChatWindow(HINSTANCE hInstance);
void AppendLine(LPCWSTR line);

#endif