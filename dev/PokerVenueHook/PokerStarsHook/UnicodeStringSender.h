#ifndef UNICODESTRINGSENDER_H
#define UNICODESTRINGSENDER_H

class UnicodeStringSender {
  public:
    UnicodeStringSender(int port);
    boolean sendString(wchar_t* string);
	boolean UnicodeStringSender::init();
	void close();
  private:
	int serverPort;
	SOCKET ConnectSocket;
	std::string toUtf8(const std::wstring &wstr);
	boolean initialized;
	boolean closed;
};

#endif