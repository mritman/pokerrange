#include <iostream>
#include <sstream>
#include "EasyHook.h"
using namespace std;

#define FORCE(expr) {if(!NT_SUCCESS(NtStatus = (expr))) goto ERROR_ABORT;}

int main(int argc, char **argv) {
	NTSTATUS NtStatus;

	if (argc < 3) {
		cout << "Injects hooks into poker venue processes.\n Usage: PokerVenueHookInstaller.exe <port> <processid>\n";
		return 0;
	}

	istringstream ss1(argv[1]);
	int port;
	if (!(ss1 >> port)) {
		cerr << "Invalid port number: " << argv[1] << '\n';
		return 1;
	}

	istringstream ss2(argv[2]);
	int pid;
	if (!(ss2 >> pid)) {
		cerr << "Invalid process id: " << argv[2] << '\n';
		return 1;
	}

	FORCE(RhInjectLibrary(pid,
		0,
		EASYHOOK_INJECT_DEFAULT,
		L"PokerStarsHook.dll",
		NULL,
		&port,
		sizeof(port)));

	cout << "Hook installed.";
	return 0;

	ERROR_ABORT:
		cerr << RtlGetLastErrorString();
		return NtStatus;
}