package pr.domain.poker;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.Rank;
import pr.domain.cards.Suit;
import pr.domain.gamestate.GameState;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class HoleCards implements Comparable<HoleCards> {

	public static final int NR_HOLE_CARD_COMBINATIONS = 1326;
	
	public static final int MAX_HOLECARDS = 2;
	
	private final Card card1;
	private final Card card2;
	
	@Getter 
	private final boolean folded;
	
	public HoleCards(Card card1, Card card2) {
		this(card1, card2, false);
	}
	
	public HoleCards(Card card1, Card card2, boolean folded) {
		Preconditions.checkNotNull("card1", card1);
		Preconditions.checkNotNull("card2", card2);
		
		if(Card.isHigher(card1, card2)) {
			this.card1 = card1;
			this.card2 = card2;
		} else {
			this.card1 = card2;
			this.card2 = card1;
		}
		
		this.folded = folded;
	}
	
	public Card card1() {
		return card1;
	}
	
	public Card card2() {
		return card2;
	}
	
	public HoleCards fold() {
		return new HoleCards(card1, card2, true);
	}
	
	public HoleCards unfold() {
		return new HoleCards(card1, card2);
	}
	
	public Card highestCard() {
		if (card1.rank().intValue() > card2.rank().intValue()) {
			return card1;
		} else {
			return card2;
		}
	}
	
	public boolean isPair() {
		if (card1.rank() == card2.rank()) {
			return true;
		}
		
		return false;
	}
	
	public boolean isSuited() {
		if (card1.suit() == card2.suit()) {
			return true;
		}
		
		return false;
	}
	
	public boolean isConnected() {
		if (Math.abs(card1.rank().intValue() - card2.rank().intValue()) == 1) {
			return true;
		}
		
		return false;
	}
	
	public boolean isGapConnected() {
		if (Math.abs(card1.rank().intValue() - card2.rank().intValue()) == 2) {
			return true;
		}
		
		return false;
	}
	
	public HoleCards cloneHoleCards() {
		return new HoleCards(card1, card2);
	}
	
	public String toString() {
		return card2.toString() + " " + card1.toString();
	}
	
	public static HoleCards fromString(String string) throws IllegalArgumentException {
		String[] split = string.split(" ");
		
		if (split.length != 2) {
			throw new IllegalArgumentException(string);
		}
		
		Card card1 = Card.fromString(split[0]);
		Card card2 = Card.fromString(split[1]);
		
		return new HoleCards(card1, card2);		
	}

	public int compareTo(HoleCards other) {
		if (card1.rank().intValue() > other.card1().rank().intValue()) {
			return 1;
		} else if (card1.rank().intValue() < other.card1.rank().intValue()) {
			return -1;
		} else if (card2.rank().intValue() > other.card2.rank().intValue()) { //The ranks of the highcards of each hand are the same.
			return 1;
		} else if (card2.rank().intValue() < other.card2.rank().intValue()) {
			return -1;
		} else if (card1.suit().intValue() > other.card1.suit().intValue()) { //The ranks of both cards of both hands are the same.
			return 1;
		} else if (card1.suit().intValue() < other.card1.suit().intValue()) {
			return -1;
		} else if (card2.suit().intValue() > other.card2.suit().intValue()) { //The suits of the highcard of each hand are the same.
			return 1;
		} else if (card2.suit().intValue() < other.card2.suit().intValue()) {
			return -1;
		} else { //Both hands are the same.
			return 0;
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

	    if (!(o instanceof HoleCards)) return false;
	    
	    HoleCards other = (HoleCards) o;
	    
		return Objects.equal(card1(), other.card1())
				&& Objects.equal(card2(), other.card2());
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(card1(), card2());
	}

	public List<Card> asList() {
		return Arrays.asList(card1, card2);
	}
	
	public boolean contains(Card card) {
		if (card1 == card || card2 == card) {
			return true;
		}
		
		return false;
	}
	
	public static Set<HoleCards> createAllHoleCards() {
		TreeSet<HoleCards> allPossibleHoleCards = new TreeSet<HoleCards>();
		
		for (Rank rank1 : Rank.values()) {
			for (Suit suit1 : Suit.values()) {
				Card card1 = Card.createCard(rank1, suit1);
			
				for(Suit suit2 : Suit.values()) {
					for(Rank rank2 : Rank.values()) {
						if (suit1 == suit2 && rank1 == rank2) continue;		/* If card1 equals card2 continue. (Two of the same card is not possible.) */
						
						Card card2 = Card.createCard(rank2, suit2);
						
						if (Card.isHigher(card1, card2)) continue;			/* If card1 is a higher ranked card than card2, then all possible combinations with card2 have already been added to the set. */
						
						HoleCards newHand = new HoleCards(card1, card2);
						
						if (!allPossibleHoleCards.contains(newHand)) {
							allPossibleHoleCards.add(newHand);
						}
					}
				}	
			}
		}
		
		return allPossibleHoleCards;
	}
	
	public static Set<HoleCards> createAllPossibleHoleCards(GameState gameState) {
		Set<HoleCards> possibleHoleCards = createAllHoleCards();
		
		removeHandsFromSet(possibleHoleCards, gameState);		
		
		return possibleHoleCards;
	}
	
	public static void removeHandsFromSet(Set<HoleCards> set, GameState gameState) {
		if (gameState.getHoleCards() != null) {
			removeHandsFromSet(set, gameState.getHoleCards().card1());
			removeHandsFromSet(set, gameState.getHoleCards().card2());
		}
		if (gameState.getBoardCards().getFlop() != null) {
			removeHandsFromSet(set, gameState.getBoardCards().getFlop().getCardOne());
			removeHandsFromSet(set, gameState.getBoardCards().getFlop().getCardTwo());
			removeHandsFromSet(set, gameState.getBoardCards().getFlop().getCardThree());
		}
		
		if (gameState.getBoardCards().getTurn() != null) {
			removeHandsFromSet(set, gameState.getBoardCards().getTurn());
		}
		if (gameState.getBoardCards().getRiver() != null) {
			removeHandsFromSet(set, gameState.getBoardCards().getRiver());
		}
	}
	
	private static void removeHandsFromSet(Set<HoleCards> set, Card card) {
		Iterator<HoleCards> itr = set.iterator();
		HoleCards currentHand;
		
		while(itr.hasNext()) {
			currentHand = itr.next();
			
			if (currentHand.card1() == card || currentHand.card2() == card) {
				itr.remove();
			}
		}
	}
	
}
