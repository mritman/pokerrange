package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.CardSuitComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class Flush extends Hand {
	
	public enum Type {
		BOARD,
		PLAYER;
	}
	
	@Getter	private List<Card> flush;
	@Getter private Type type;

	protected Flush(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);

		calculateHand(holeCards, boardCards);
	}
	
	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newFlush = new LinkedList<>();
		
		containsFlush(cards, newFlush);
		
		Collections.sort(newFlush, CardComparator.getInstance());
		flush = Collections.unmodifiableList(newFlush);
		type = calculateType(newFlush, boardCards);
		
		cards.removeAll(newFlush);
		
		init(HandValue.FLUSH, newFlush, cards);
	}

	protected static void containsFlush(List<Card> cards, List<Card> flush) throws DoesNotMakeHandException {
		if (cards == null || cards.size() < 5 || cards.size() > 7) {
			throw new DoesNotMakeHandException(HandValue.FLUSH);
		}

		containsSuits(cards, flush, 5);
	}

	protected static void containsSuits(List<Card> cards, List<Card> suited, int count) throws DoesNotMakeHandException {
		if (count != 4 && count != 5) {
			throw new IllegalArgumentException("Argument 'count' should be 4 or 5 but was: " + count + ".");
		}
		
		List<Card> suitSorted = new LinkedList<>();
		suitSorted.addAll(cards);
		Collections.sort(suitSorted, CardSuitComparator.getInstance());
		Collections.reverse(suitSorted);
		
		Card lastCard = null;
		
		for (Card card : suitSorted) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}
			
			if (lastCard.suit() == card.suit()) {
				if (suited.isEmpty()) {
					suited.add(card);
					suited.add(lastCard);
				} else if (suited.get(0).suit() == card.suit()) {
					suited.add(card);
				} else if (suited.size() < count) {
					suited.clear();
					suited.add(card);
					suited.add(lastCard);
				}
				
				if (suited.size() == 5) {
					break;
				}
			}
			
			lastCard = card;
		}
		
		if (doesNotMakeFlushDraw(suited, count)) {
			throw new DoesNotMakeHandException(HandValue.FLUSH_DRAW);
		} else if (doesNotMakeFlush(suited, count)) {
			throw new DoesNotMakeHandException(HandValue.FLUSH);
		}
	}

	/**
	 * @return true if a flush draw was requested but the hand did not make one, false otherwise
	 */
	private static boolean doesNotMakeFlush(List<Card> suited, int count) {
		return count == 5 && suited.size() < 5;
	}

	/**
	 * @return true if a flush was requested but the hand did not make one, false otherwise
	 */
	private static boolean doesNotMakeFlushDraw(List<Card> suited, int count) {
		return count == 4 && suited.size() != count;
	}
	
	private static Type calculateType(List<Card> flush, BoardCards boardCards) {
		for (Card card : flush) {
			if (!boardCards.contains(card)) {
				return Type.PLAYER;
			}
		}
		
		return Type.BOARD;
	}

}
