package pr.domain.poker.event.seat;

import pr.domain.poker.event.PokerEvent;

public interface SeatEvent extends PokerEvent {}
