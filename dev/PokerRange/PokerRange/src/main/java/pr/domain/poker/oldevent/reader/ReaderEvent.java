package pr.domain.poker.oldevent.reader;

import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.table.PokerTable;

public abstract class ReaderEvent extends PokerTableEvent {

	protected ReaderEvent(PokerTable table) {
		super(table);
	}

}
