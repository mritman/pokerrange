package pr.domain.poker.range.typed;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.cards.Rank;

@Embeddable
public class HandTypeRange {
	
	@Getter
	@Enumerated(EnumType.STRING)
	private Rank lower;
	
	@Getter
	@Enumerated(EnumType.STRING)
	private Rank upper;
	
	HandTypeRange() {}
	
	public HandTypeRange(Rank lower, Rank upper) {
		this.lower = lower;
		this.upper = upper;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		
		if (!(object instanceof HandTypeRange)) return false;
		
		HandTypeRange other = (HandTypeRange) object;
		
		return lower == other.lower && upper == other.upper;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(lower, upper);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass())
				.add("lower", lower)
				.add("upper", upper).toString();
	}

}
