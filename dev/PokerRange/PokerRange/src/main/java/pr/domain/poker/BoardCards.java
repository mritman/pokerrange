package pr.domain.poker;

import java.util.LinkedList;
import java.util.List;

import pr.domain.cards.Card;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

public class BoardCards implements Comparable<BoardCards> {
	private static final String THE_TURN_IS_NULL_BUT_THE_RIVER_IS_NOT_NULL = "The turn is null but the river is not null.";

	public static final BoardCards PREFLOP = new BoardCards();
	
	public static final int MAX_BOARDCARDS = 5;
	
	private final Flop flop;
	private final Card turn;
	private final Card river;
	
	public BoardCards() {
		flop = null;
		turn = null;
		river = null;
	}
	
	public BoardCards(Card flop1, Card flop2, Card flop3, Card turn, Card river) {
		if (flop1 == null || flop2 == null || flop3 == null) {
			throw new IllegalArgumentException("The flop has to consist of three cards.");
		}
		
		flop = new Flop(flop1, flop2, flop3);
		
		this.turn = turn;
		
		if (turn == null && river != null) {
			throw new IllegalArgumentException(THE_TURN_IS_NULL_BUT_THE_RIVER_IS_NOT_NULL);
		}
		
		this.river = river;
		
	}
	
	public BoardCards(Card[] cards) {
		if (cards.length < Flop.SIZE || cards.length > MAX_BOARDCARDS) {
			throw new IllegalArgumentException();
		}
		
		flop = new Flop(cards[0], cards[1], cards[2]);
		
		turn = (cards.length > 3) ? cards[3] : null;

		if (cards.length > 4 && turn == null && cards[4] != null) {
			throw new IllegalArgumentException(THE_TURN_IS_NULL_BUT_THE_RIVER_IS_NOT_NULL);				
		}
		
		river = (cards.length > 4) ? cards[4] : null;
	}

	public BoardCards(List<Card> cards) {
		if (cards.size() < Flop.SIZE || cards.size() > MAX_BOARDCARDS) {
			throw new IllegalArgumentException();
		}
		
		flop = new Flop(cards.get(0), cards.get(1), cards.get(2));
		
		turn = (cards.size() > 3) ? cards.get(3) : null;
		
		if (cards.size() > 4 && turn == null && cards.get(4) != null) {
			throw new IllegalArgumentException(THE_TURN_IS_NULL_BUT_THE_RIVER_IS_NOT_NULL);
		}
		
		river =  (cards.size() > 4) ? cards.get(4) : null;
	}
	
	public Flop getFlop() {
		return flop;
	}
	
	public Card getTurn() {
		return turn;
	}
	
	public Card getRiver() {
		return river;
	}
	
	public Street getStreet() {
		if (river != null) {
			return Street.RIVER;
		} else if (turn != null) {
			return Street.TURN;
		} else if (flop != null) {
			return Street.FLOP;
		} else {
			return Street.PREFLOP;
		}
	}
	
	/**
	 * @return a list containing all board cards in the order of: flop, turn, river.
	 */
	public List<Card> asList() {
		LinkedList<Card> cards = new LinkedList<Card>();
		
		if (flop != null) {
			for (Card card : flop.getCards()) {
				cards.add(card);
			}
		}
		
		if (turn != null) cards.add(turn);
		if (river != null) cards.add(river);
		
		return cards;
	}
	
	public int count() {
		int nrOfCards = 0;
		
		if (flop == null) {
			nrOfCards += Flop.SIZE;
			
			if (turn != null) {
				nrOfCards++;
				
				if (river != null) {
					nrOfCards++;
				}
			}
		}
		
		return nrOfCards;
	}
	
	public boolean contains(Card card) {
		if (flop.getCards().contains(card)) {
			return true;
		}
		
		if (turn == card) return true;
		if (river == card) return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		if (getStreet() == Street.PREFLOP) {
			return "[preflop]";
		}
		
		StringBuilder sb = new StringBuilder();

		if (flop != null) {
			sb.append(flop.getCards().get(0).toString());
			sb.append(" ");
			sb.append(flop.getCards().get(1).toString());
			sb.append(" ");
			sb.append(flop.getCards().get(2).toString());
		}
		
		if (turn != null) {
			sb.append(" ");
			sb.append(turn.toString());
			
			if (river != null) {
				sb.append(" ");
				sb.append(river.toString());
			}
		}
		
		return sb.toString();
	}

	public static BoardCards fromString(String boardCardString) {
		String[] split = boardCardString.split(" ");
		List<Card> cards = new LinkedList<>();
		
		if ("".equals(boardCardString)) {
			return new BoardCards();
		}
		
		if (split.length < 3 || split.length > 5) {
			throw new IllegalArgumentException();
		}
		
		for (String string : split) {
			Card card = Card.fromString(string);
			cards.add(card);
		}
		
		return new BoardCards(cards); 
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		final BoardCards other = (BoardCards) obj;
		
		return isFlopEqual(other.getFlop()) 
				&& getTurn() == other.getTurn()
				&& getRiver() == other.getRiver();
	}
	
	private boolean isFlopEqual(Flop otherFlop) {
		return Objects.equal(flop, otherFlop);
	}
	
	@Override
	public int hashCode() {
		Card flop1 = null;
		Card flop2 = null;
		Card flop3 = null;
		
		if (flop != null) {
			flop1 = getFlop().getCardOne();
			flop2 = getFlop().getCardTwo();
			flop3 = getFlop().getCardThree();
		}
		
		return Objects.hashCode(flop1, flop2, flop3, getTurn(), getRiver());
	}

	@Override
	public int compareTo(BoardCards other) {
		return ComparisonChain.start()
				.compare(flop, other.getFlop(), Ordering.natural().nullsFirst())
				.compare(turn, other.getTurn(), Ordering.natural().nullsFirst())
				.compare(river, other.getRiver(), Ordering.natural().nullsFirst())
				.result();
	}

	public boolean hasFlop() {
		return flop != null; 
	}

	public boolean hasTurn() {
		return turn != null;
	}

	public boolean hasRiver() {
		return river != null;
	}

	public boolean isPreflop() {
		return getStreet() == Street.PREFLOP;
	}
}