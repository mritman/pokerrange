package pr.domain.handbasket.handcollection;

import java.util.Comparator;

import pr.domain.handbasket.BasketElement;

public class BasketElementComparator implements Comparator<BasketElement> {

	private static final BasketElementComparator basketElementComparator = new BasketElementComparator();
	
	public static BasketElementComparator getInstance() {
		return basketElementComparator;
	}
	
	@Override
	public int compare(BasketElement elementOne, BasketElement elementTwo) {
		if (elementOne instanceof HandCollection && elementTwo instanceof HandCollection) {
			return HandCollectionComparator.compareHandCollections((HandCollection) elementOne, (HandCollection) elementTwo) * -1;
		} else if (elementOne instanceof HandCollection && elementTwo instanceof HandCollectionGroup) {
			return -1;
		} else if (elementOne instanceof HandCollectionGroup && elementTwo instanceof HandCollection) {
			return 1;
		} else if (elementOne instanceof HandCollectionGroup && elementTwo instanceof HandCollectionGroup) {
			return HandCollectionGroupComparator.getInstance().compare((HandCollectionGroup) elementOne, (HandCollectionGroup) elementTwo);
		} else {
			throw new IllegalArgumentException("Unsupported subclass of BasketElement.");
		}
	}

}
