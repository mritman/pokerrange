package pr.domain.handbasket;


import com.google.common.base.Predicate;

public interface HandBasketSubsetKey {

	String getName();
	
	Predicate<BasketElement> getPredicate();
	
}
