package pr.domain.poker.game;

public class GameException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public static final String CAN_ONLY_BET_PREFLOP = "Can only bet after the flop.";

	public static final String INCORRECT_ACTION_ORDER = "Next seat expected to act is %s but encountered: %s, %s";

	public GameException(String message) {
		super(message);
	}
	
	public GameException(String format, Object... args) {
		super(String.format(format, args));
	}

}