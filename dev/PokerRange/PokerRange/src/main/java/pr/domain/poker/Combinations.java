package pr.domain.poker;

import static pr.domain.poker.hand.HandValue.HIGH_CARD;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

import lombok.Getter;
import pr.domain.poker.hand.Hand;
import pr.domain.poker.hand.HandCalculator;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.comparator.HandComparator;

public class Combinations implements Comparable<Combinations> {
	
	@Getter	private final HoleCards holeCards;
	@Getter private final BoardCards boardCards;
	
	/**
	 * The hand (and any draws) that the hole cards make with a given board.
	 */
	private final LinkedList<Hand> hands;
	
	private Combinations(HoleCards holeCards, BoardCards boardCards, LinkedList<Hand> hands) {
		this.holeCards = holeCards;
		this.boardCards = boardCards;
		this.hands = new LinkedList<>(hands);
	}
	
	public Combinations(HoleCards holeCards, BoardCards boardCards) {
		Preconditions.checkNotNull("boardCards", boardCards);
		this.boardCards = boardCards;
		this.holeCards = holeCards;
		
		List<Hand> calculatedHands = HandCalculator.calculateHands(holeCards, boardCards);
		hands = new LinkedList<>(calculatedHands);
		Collections.sort(hands, HandComparator.getInstance());
		Collections.reverse(hands);
	}
	
	/**
	 * @return the highest valued card combination this hand makes.
	 */
	public Hand getHighestHand() {
		if (hands.isEmpty()) {
			return null;
		} else {
			return hands.getFirst();
		}
	}
	
	public Hand getHighestNonDrawHand() {
		for (Hand hand : hands) {
			if (!hand.getValue().isDraw()) {
				return hand;
			}
		}
		
		return null;
	}
	
	public Hand getHand(HandValue handValue) {
		for (Hand hand : hands) {
			if (hand.getValue() == handValue) {
				return hand;
			}
		}
		
		return null;
	}
	
	public List<Hand> getHands() {
		return Collections.unmodifiableList(hands);
	}
	
	public Combinations fold() {
		return new Combinations(holeCards.fold(), boardCards, hands);
	}
	
	public Combinations unfold() {
		return new Combinations(holeCards.unfold(), boardCards, hands);
	}
	
	public boolean isComboDraw() {
		int minimumSize = 2;
		
		if (getHand(HIGH_CARD) != null) {
			minimumSize = 3;
		}
		
		if (hands.size() >= minimumSize) {
			return true;
		}
		
		return false;
	}

	@Override
	public int compareTo(Combinations other) {
		return ComparisonChain.start()
				.compare(holeCards, other.getHoleCards())
				.compare(boardCards, other.getBoardCards())
				.result();
	}

}
