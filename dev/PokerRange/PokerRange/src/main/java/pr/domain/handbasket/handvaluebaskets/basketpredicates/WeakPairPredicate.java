package pr.domain.handbasket.handvaluebaskets.basketpredicates;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.Pair;
import pr.domain.poker.hand.StraightDraw;
import pr.domain.poker.hand.TwoPair;
import pr.domain.poker.hand.Pair.RelativeRank;

import com.google.common.base.Predicate;

public class WeakPairPredicate implements Predicate<Combinations> {

	public boolean apply(Combinations combinations) {
		HandValue value = combinations.getHighestHand().getValue();

		if (value == HandValue.PAIR) {
			Pair pair = (Pair) combinations.getHighestHand();

			if (pair.getRelativeRank().intValue() < Pair.RelativeRank.SECOND_PAIR.intValue()) {
				return true;
			}

		} else if (value == HandValue.STRAIGHT_DRAW) {
			StraightDraw straightDraw = (StraightDraw) combinations.getHighestHand();

			if (straightDraw.getSide() == StraightDraw.Side.INSIDE) {
				return true;
			}
		} else if (value == HandValue.TWO_PAIR) {
			TwoPair twoPair = (TwoPair) combinations.getHighestHand();
			
			if (twoPair.getType() == TwoPair.Type.COMBINED_TWO_PAIR) {
				RelativeRank relativeRank = twoPair.getFirstPairType() != Pair.Type.BOARD_PAIR ? twoPair
						.getFirstPairRelativeRank() : twoPair
						.getSecondPairRelativeRank();
				
				if (relativeRank.intValue() < Pair.RelativeRank.SECOND_PAIR.intValue()) {
					return true;
				}
				
			}
		}

		return false;
	}

}
