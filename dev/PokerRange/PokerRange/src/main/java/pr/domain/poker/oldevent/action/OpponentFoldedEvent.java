package pr.domain.poker.oldevent.action;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.player.Opponent;

public class OpponentFoldedEvent extends ActionEvent {

	@Getter
	private final Opponent opponent;
	
	public OpponentFoldedEvent(Opponent opponent) {
		super(opponent.getTable());
		this.opponent = opponent;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass()).add("opponent", opponent.getName()).toString();
	}

}
