package pr.domain.poker;

import java.util.Comparator;

import com.google.common.collect.ComparisonChain;

import pr.domain.poker.hand.comparator.HandComparator;

public class CombinationsComparator implements Comparator<Combinations> {

	private static final CombinationsComparator combinationsComparator = new CombinationsComparator();
	
	@Override
	public int compare(Combinations combinations1, Combinations combinations2) {
		return ComparisonChain
				.start()
				.compare(combinations1.getHighestHand(),
						combinations2.getHighestHand(),
						HandComparator.getInstance())
				.compare(combinations1.getHoleCards(), 
						combinations2.getHoleCards(),
						HoleCardsRankComparator.getInstance())
				.compareFalseFirst(combinations1.isComboDraw(),
						combinations2.isComboDraw())
				.result();
	}
	
	public static CombinationsComparator getInstance() {
		return combinationsComparator;
	}

}
