package pr.domain.handbasket.handcollection;

import java.util.TreeSet;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

public class HandCollection extends BasketElement implements Comparable<HandCollection> {

	@Getter	private TreeSet<Combinations> hands;
	
	public HandCollection(TreeSet<Combinations> hands) {
		if (hands == null) {
			this.hands = new TreeSet<Combinations>();
		} else {
			this.hands = new TreeSet<>(hands);
		}
	}
	
	@Override
	public boolean containsHandFoldedByUser() {
		for (Combinations hand : hands) {
			if (hand.getHoleCards().isFolded()) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public int getHandCount() {
		return hands.size();
	}
	
	@Override
	public Combinations getHighestHoleCards() {
		return hands.first();
	}

	@Override
	public HandValue getHandValue() {
		return getHighestHoleCards().getHighestHand().getValue();
	}

	@Override
	public String getName() {
		return hands.first().getHoleCards().card1().rank().strValue() + hands.first().getHoleCards().card2().rank().strValue();
	}

	public boolean isSuited() {
		for (Combinations hand : hands) {
			if (!hand.getHoleCards().isSuited()) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean hasSuited() {
		for (Combinations hand : hands) {
			if (hand.getHoleCards().isSuited()) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isComboDraw() {
		return hands.first().isComboDraw();
	}
	
	@Override
	public void fold() {
		TreeSet<Combinations> foldedCombinations = new TreeSet<>();
		
		for (Combinations combination : hands) {
			foldedCombinations.add(combination.fold());
		}
		
		hands = foldedCombinations;
	}

	@Override
	public void unfold() {
		TreeSet<Combinations> unfoldedCombinations = new TreeSet<>();
		
		for (Combinations combination : hands) {
			unfoldedCombinations.add(combination.unfold());
		}
		
		hands = unfoldedCombinations;
	}

	@Override
	public TreeSet<Combinations> getCombinations() {
		return new TreeSet<>(hands);
	}
	
	@Override
	public int compareTo(HandCollection other) {
		return HandCollectionComparator.compareHandCollections(this, other);
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		
		if (!(other instanceof HandCollection)) return false; 
		
		HandCollection otherHandCollection = (HandCollection) other;
		
		return compareTo(otherHandCollection) == 0;
	}
	
	@Override
	public int hashCode() {
		return HandCollectionComparator.hashCode(this);
	}
	
}
