package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class FourOfAKind extends Hand {

	public enum Type {
		BOARD,
		PLAYER;
	}
	
	@Getter private List<Card> fourOfAKind;
	@Getter	private Type type;
	
	protected FourOfAKind(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHand(holeCards, boardCards);
	}
	
	public Rank getRank() {
		return fourOfAKind.get(0).rank();
	}
	
	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newFourOfAKind = new LinkedList<>();
		
		calculateFourOfAKind(cards, newFourOfAKind);
		
		Collections.sort(newFourOfAKind, CardComparator.getInstance());
		fourOfAKind = Collections.unmodifiableList(newFourOfAKind);
		type = calculateType(newFourOfAKind, boardCards);
		
		cards.removeAll(newFourOfAKind);
		
		init(HandValue.FOUR_OF_A_KIND, newFourOfAKind, cards);
	}

	private void calculateFourOfAKind(List<Card> cards, List<Card> fourOfAKind) throws DoesNotMakeHandException {
		if (cards == null || cards.size() < 4 || cards.size() > 7) {
			throw new DoesNotMakeHandException(HandValue.FOUR_OF_A_KIND);
		}
		
		ListIterator<Card> iterator = cards.listIterator(cards.size());
		Card lastCard = null;
		Card card = null;
		
		while (iterator.hasPrevious()) {
			if (lastCard == null) {
				lastCard = iterator.previous();
				continue;
			}
			
			card = iterator.previous();
		
			if (card.rank() == lastCard.rank()) {
				if (fourOfAKind.isEmpty()) {
					fourOfAKind.add(card);
					fourOfAKind.add(lastCard);
				} else if (fourOfAKind.size() < 4 && fourOfAKind.get(0).rank() == card.rank()) {
					fourOfAKind.add(card);
					if (fourOfAKind.size() == 4) break; 
				} else {
					fourOfAKind.clear();
					fourOfAKind.add(card);
					fourOfAKind.add(lastCard);
				}
			}
			
			lastCard = card;
		}
		
		if (fourOfAKind.size() != 4) {
			throw new DoesNotMakeHandException(HandValue.FOUR_OF_A_KIND); 
		}
	}
	
	private Type calculateType(List<Card> fourOfAKind, BoardCards boardCards) {
		for (Card card : fourOfAKind) {
			if (!boardCards.contains(card)) {
				return Type.PLAYER;
			}
		}
		
		return Type.BOARD;
	}
	
}
