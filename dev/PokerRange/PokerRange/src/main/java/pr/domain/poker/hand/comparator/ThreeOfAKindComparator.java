package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.ThreeOfAKind;

public class ThreeOfAKindComparator implements Comparator<ThreeOfAKind> {

	private static final ThreeOfAKindComparator threeOfAKindComparator = new ThreeOfAKindComparator();
	
	public static ThreeOfAKindComparator getInstance() {
		return threeOfAKindComparator;
	}
	
	@Override
	public int compare(ThreeOfAKind h1, ThreeOfAKind h2) {
		
		int cmp = Integer.compare(h1.getRank().intValue(), h2.getRank().intValue());
		
		if (cmp != 0) {
			return cmp;
		}

		return KickerComparator.compareKickers(h1.getKickers(), h2.getKickers());
	}

	public int hashCode(ThreeOfAKind hand) {
		return Objects.hash(hand.getRank(), KickerComparator.getInstance().hashCode(hand.getKickers()));
	}

}
