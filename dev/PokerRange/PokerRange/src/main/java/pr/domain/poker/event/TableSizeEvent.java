package pr.domain.poker.event;

import lombok.Getter;
import pr.domain.poker.table.TableSize;

import com.google.common.base.Objects;

public class TableSizeEvent implements PokerEvent {

	@Getter
	private final TableSize size;
	
	public TableSizeEvent(TableSize size) {
		this.size = size;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("size", size)
				.toString();
	}
}
