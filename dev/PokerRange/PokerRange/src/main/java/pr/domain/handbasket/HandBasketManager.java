package pr.domain.handbasket;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import lombok.Setter;
import pr.domain.gamestate.GameState;
import pr.domain.poker.Combinations;
import pr.domain.poker.Street;

public class HandBasketManager {

	private HandBasketFactory handBasketFactory;
	
	private Set<Combinations> combinations;
	
	/**
	 * Any hand in this list will not be added to any basket.
	 */
	private Set<Combinations> foldedHoleCards;

	/**
	 * This map contains the hand baskets.
	 */
	private Map<HandBasketKey, HandBasket> handBaskets;
	
	@Getter
	private GameState gameState;
	
	@Getter
	@Setter
	private boolean autoFold;

	private HandBasketKey willFoldKey;
	
	public HandBasketManager(HandBasketFactory handBasketFactory, HandBasketKey willFoldKey) {
		this.handBasketFactory = handBasketFactory;
		combinations = Collections.<Combinations> emptySet();
		foldedHoleCards = new TreeSet<>();
		handBaskets = Collections.<HandBasketKey, HandBasket> emptyMap();
		gameState = new GameState();
		
		this.willFoldKey = willFoldKey;
	}
	
	public void update(Set<Combinations> newHoleCards, GameState gameState) {
		updateFoldedHoleCards(gameState.getStreet());
		
		this.combinations = Collections.unmodifiableSet(new TreeSet<>(newHoleCards));
		this.gameState = gameState;
		
		handBasketFactory.setFoldedHoleCards(foldedHoleCards);
		handBaskets = handBasketFactory.createHandBaskets(combinations);
	}
	
	public Map<HandBasketKey, HandBasket> getHandBasketMap() {
		return Collections.unmodifiableMap(handBaskets);
	}
	
	public HandBasketKey[] getHandBasketKeys() {
		return handBasketFactory.getBasketKeys();
	}

	public HandBasketKey getWillFoldKey() {
		return willFoldKey;
	}
	
	public Set<Combinations> getHoleCards() {
		return combinations;
	}
	
	public void reset() {
		combinations = Collections.<Combinations> emptySet();
		foldedHoleCards.clear();
		handBaskets = null;
		gameState = null;
	}

	/**
	 * Adds the hands to be folded from the previous street to {@link #foldedHoleCards}.
	 * 
	 * @param street the next street
	 */
	private void updateFoldedHoleCards(Street street) {
		if (street == Street.PREFLOP) {
			foldedHoleCards.clear();
		}
		
		if (street.ordinal() < Street.TURN.ordinal() || street.ordinal() <= gameState.getStreet().ordinal()) {
			return;
		}
		
		if (handBaskets == null || handBaskets.get(willFoldKey) == null	|| handBaskets.get(willFoldKey).getHandsCount() == 0) {
			return;
		}
		
		foldedHoleCards.addAll(getHoleCardsToFold());
	}
	
	/**
	 * Returns a collection of folded {@link Combinations} that are in the WILL_FOLD
	 * basket and are not contained in any other basket.
	 */
	private Collection<Combinations> getHoleCardsToFold() {
		if (!autoFold) return Collections.<Combinations>emptyList();
		
		List<Combinations> foldedHandList = new LinkedList<Combinations>();
		
		// handsInOtherBaskets is used to check that hole cards in the fold basket
		// are not contained in HandCollections in any other baskets.
		List<Combinations> handsInOtherBaskets = new LinkedList<Combinations>();
		
		for (HandBasketKey key : handBasketFactory.getBasketKeys()) {
			if (key == willFoldKey) continue;
			handsInOtherBaskets.addAll(handBaskets.get(key).getHands());
		}
		
		List<Combinations> willBeFoldedHands = handBaskets.get(willFoldKey).getHands();
		for (Combinations hand : willBeFoldedHands) {
			if (!handsInOtherBaskets.contains(hand)) {
				foldedHandList.add(hand.fold());
			}
		}
		
		return foldedHandList;
	}
	
}
