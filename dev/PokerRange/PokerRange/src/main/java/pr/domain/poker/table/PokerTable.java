package pr.domain.poker.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import lombok.Setter;
import pr.domain.gamestate.GameState;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.player.Opponent;
import pr.util.Utils;

import com.google.common.base.Objects;

public class PokerTable {
	
	@Getter
	private final String name;
	
	@Getter
	private final PokerVenue venue;
	
	@Getter
	private final PokerTableKey key;
	
	@Getter
	private GameState gameState;
	
	@Getter
	@Setter
	private String handId;
	
	private boolean manualInput;
	
	@Getter
	private TableSize size;

	/**
	 * The number of the seat with seat 1 being to the right of the top
	 * center of the table.
	 */
	@Getter
	private Seat playerSeat;
	
	private Set<Opponent> opponents;
	
	protected PokerTable(String name, PokerVenue venue) {
		if (name == null || "".equals(name)) {
			throw new IllegalArgumentException("A table name may not be null or an empty string.");
		}

		this.venue = venue;
		this.name = name;
		this.key = new PokerTableKey(name, venue);
		
		gameState = new GameState();
		
		manualInput = false;
		
		playerSeat = Seat.SEAT1;
		
		size = TableSize.SIX;
		
		opponents = new TreeSet<>(new Comparator<Opponent>() {

			@Override
			public int compare(Opponent o1, Opponent o2) {
				return Integer.compare(o1.getSeat().toInt(), o2.getSeat().toInt());
			}

		});
	}
	
	/**
	 * Creates an {@link Opponent} with the given name and adds it to this table.
	 * 
	 * @param name the name of the opponent
	 * @return the opponent if it was added to the table, null otherwise
	 */
	public Opponent addOpponent(String name, Seat seat) {
		Opponent opponent = new Opponent(name, this, seat);
		boolean wasAdded = opponents.add(opponent);
		
		if (wasAdded) {
			return opponent;
		}
		
		return null;
	}
	

	public boolean addOpponent(Opponent opponent) {
		return opponents.add(opponent);
	}

	public boolean removeOpponent(Opponent opponent) {
		return opponents.remove(opponent);
	}
	
	/**
	 * Returns the unmodifiable set of opponents at this table.
	 * 
	 * @return the unmodifiable set of opponents at this table.
	 */
	public Set<Opponent> getOpponents() {
		return Collections.unmodifiableSet(opponents);
	}
	
	public Opponent getOpponent(Seat seat) {
		for (Opponent opponent : opponents) {
			if (opponent.getSeat() == seat) {
				return opponent;
			}
		}
		
		return null;
	}

	public Opponent getOpponent(String opponentName) {
		for (Opponent opponent : opponents) {
			if (opponent.getName().equals(opponentName)) {
				return opponent;
			}
		}
		
		return null;
	}
	
	public void setSize(TableSize size) {
		if (size.size() < playerSeat.toInt()) {
			setPlayerSeat(Seat.fromInt(size.size()));
		}
		
		this.size = size;
	}
	
	public void setPlayerSeat(Seat seat) {
		if (seat == null) {
			throw new IllegalArgumentException("The specified argument 'seat' may not be null.");
		} else if (seat.toInt() > size.size()) {
			throw new IllegalArgumentException("Can not select seat " + seat.toInt() + "on a table with " + size.size() + "seats.");
		}
		
		playerSeat = seat;
	}
	
	public void setGameState(GameState gameState) {
		if (gameState == null) {
			throw new IllegalArgumentException("The specified argument 'gameState' may not be null.");
		}
		
		this.gameState = gameState;
	}
	
	public boolean isManualInput() {
		return manualInput;
	}
	
	public void setManualInput(boolean value) {
		manualInput = value;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == null || getClass() != object.getClass()) {
			return false;
		}
		
		final PokerTable other = (PokerTable) object;
		
		return Objects.equal(name, other.getName()) && Objects.equal(venue, other.getVenue());
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(name, venue);
	}
	
	@Override
	public String toString() {
		return com.google.common.base.Objects.toStringHelper(getClass())
				.add("name", getName())
				.add("venue", getVenue())
				.add("numberOfSeats", getSize())
				.add("playerSeat", getPlayerSeat())
				.toString();
	}

	public void newGame() {
		for (Opponent opponent : opponents) {
			opponent.getActionHistory().reset();
		}
	}
	
	/**
	 * @return the first seat in the list of <code>seats</code> after the
	 *         specified <code>seat</code>.
	 */
	public static Seat getFirstSeatAfter(Collection<Seat> seats, Seat seat) {
		List<Seat> list = new ArrayList<>(seats);
		if (!list.contains(seat)) {
			list.add(seat);
		}
		
		Collections.sort(list);
		Utils.rotatePast(list, seat);
		
		return list.get(0);
	}

}
