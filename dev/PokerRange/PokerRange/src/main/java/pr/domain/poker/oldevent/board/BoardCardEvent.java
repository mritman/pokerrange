package pr.domain.poker.oldevent.board;

import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.table.PokerTable;

public abstract class BoardCardEvent extends PokerTableEvent {

	protected BoardCardEvent(PokerTable table) {
		super(table);
	}

}
