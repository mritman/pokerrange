package pr.domain.handbasket.handvaluebaskets.subsets.willfold;

import pr.domain.handbasket.BasketElement;

import com.google.common.base.Predicate;

public class OtherPredicate implements Predicate<BasketElement> {

	@Override
	public boolean apply(BasketElement basketElement) {
		HighCardPredicate highCardPredicate = new HighCardPredicate();
		
		return !highCardPredicate.apply(basketElement);
	}

}
