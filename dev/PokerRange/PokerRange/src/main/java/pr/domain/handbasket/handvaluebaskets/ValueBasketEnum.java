package pr.domain.handbasket.handvaluebaskets;

import lombok.Getter;
import pr.domain.handbasket.FoldedPredicate;
import pr.domain.handbasket.HandBasketKey;
import pr.domain.handbasket.handvaluebaskets.basketpredicates.DrawPredicate;
import pr.domain.handbasket.handvaluebaskets.basketpredicates.PairPredicate;
import pr.domain.handbasket.handvaluebaskets.basketpredicates.TwoPairPlusPredicate;
import pr.domain.handbasket.handvaluebaskets.basketpredicates.WeakPairPredicate;
import pr.domain.handbasket.handvaluebaskets.basketpredicates.WillFoldPredicate;
import pr.domain.poker.Combinations;

import com.google.common.base.Predicate;

public enum ValueBasketEnum implements HandBasketKey {
	FOLDED("F", new FoldedPredicate()),
	WILL_FOLD("WF", new WillFoldPredicate()), 
	DRAW("D", new DrawPredicate()),
	WEAK_PAIR("WP", new WeakPairPredicate()),
	PAIR("P", new PairPredicate()),
	TWO_PAIR_PLUS("2P+", new TwoPairPlusPredicate());
	
	@Getter
	private String shortName;
	
	@Getter
	private Predicate<Combinations> predicate;
	
	private ValueBasketEnum(String shortName, Predicate<Combinations> predicate) {
		this.shortName = shortName;
		this.predicate = predicate;
	}

	@Override
	public String getName() {
		return toString();
	}

}
