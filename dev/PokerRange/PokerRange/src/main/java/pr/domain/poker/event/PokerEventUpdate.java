package pr.domain.poker.event;

import java.util.Collections;
import java.util.List;

import pr.domain.poker.table.PokerTableKey;

import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;
import com.google.common.base.Preconditions;

public class PokerEventUpdate {

	private final PokerTableKey tableKey;
	private final List<PokerEvent> events;
	
	public PokerEventUpdate(PokerTableKey tableKey, List<PokerEvent> events) {
		Preconditions.checkNotNull("tableKey", tableKey);
		Preconditions.checkNotNull("events", events);
		Preconditions.checkArgument(!events.isEmpty(), "Argument 'events' may not be null or empty.");
		
		this.tableKey = tableKey;
		this.events = events;
	}
	
	public List<PokerEvent> getEvents() {
		return Collections.unmodifiableList(events);
	}
	
	@Override
	public String toString() {
		ToStringHelper toStringHelper = Objects.toStringHelper(getClass());
		int i = 0;
		
		toStringHelper.add("tableKey", tableKey);
		for (PokerEvent tableEvent : events) {
			toStringHelper.add("Event " + ++i, tableEvent);
		}
		
		return toStringHelper.toString();
	}
	
}
