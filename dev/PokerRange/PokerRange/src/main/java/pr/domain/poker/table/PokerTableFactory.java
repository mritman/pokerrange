package pr.domain.poker.table;

import pr.domain.poker.PokerVenue;

public final class PokerTableFactory {
	
	private PokerTableFactory() {};
	
	public static PokerTable createTable(String name, PokerVenue venue) {
		switch (venue) {
		case POKERSTARS:
			return new PokerStarsTable(name, venue);
		case FULL_TILT_POKER:
			throw new UnsupportedOperationException("Not yet implemented");
		case MANUAL:
			return new PokerTable(name, venue);
		default:
			throw new IllegalStateException("This should never happen.");	
		}
	}

}
