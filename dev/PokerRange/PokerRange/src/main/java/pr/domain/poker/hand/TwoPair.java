package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class TwoPair extends Hand {
	
	public enum Type {
		TWO_BOARD_PAIR,
		COMBINED_TWO_PAIR,
		TWO_PLAYER_PAIR;
	}
	
	@Getter
	private Type type;
	
	@Getter
	private List<Card> firstPair;
	@Getter
	private Pair.Type firstPairType;
	@Getter
	private Pair.RelativeRank firstPairRelativeRank;

	@Getter
	private List<Card> secondPair;
	@Getter
	private Pair.Type secondPairType;
	@Getter
	private Pair.RelativeRank secondPairRelativeRank;
	
	@Getter
	private List<Card> thirdPair;
	@Getter
	private Pair.Type thirdPairType;
	@Getter
	private Pair.RelativeRank thirdPairRelativeRank;
	
	protected TwoPair(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);

		calculateHand(holeCards, boardCards);
	}

	public Rank getFirstPairRank() {
		return firstPair.get(0).rank();
	}
	
	public Rank getSecondPairRank() {
		return secondPair.get(0).rank();
	}
	
	public Rank getThirdPairRank() {
		if (thirdPair != null) {
			return thirdPair.get(0).rank();
		}
		
		return null;
	}
	
	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newFirstPair = new LinkedList<>();
		List<Card> newSecondPair = new LinkedList<>();
		List<Card> newThirdPair = new LinkedList<>();
		
		calculatePairs(cards, newFirstPair, newSecondPair, newThirdPair);
		
		firstPair = Collections.unmodifiableList(newFirstPair);
		firstPairType = Pair.calculatePairType(newFirstPair, holeCards, boardCards);
		firstPairRelativeRank = Pair.calculateRelativeRank(newFirstPair, boardCards);
		
		secondPair = Collections.unmodifiableList(newSecondPair);
		secondPairType = Pair.calculatePairType(newSecondPair, holeCards, boardCards);
		secondPairRelativeRank = Pair.calculateRelativeRank(newSecondPair, boardCards);
		
		if (!newThirdPair.isEmpty()) {
			thirdPair = Collections.unmodifiableList(newThirdPair);
			thirdPairType = Pair.calculatePairType(newThirdPair, holeCards, boardCards);
			thirdPairRelativeRank = Pair.calculateRelativeRank(newThirdPair, boardCards);
		}
		
		cards.removeAll(newFirstPair);
		cards.removeAll(newSecondPair);
		
		List<Card> valueCards = new LinkedList<Card>();
		valueCards.addAll(newFirstPair);
		valueCards.addAll(newSecondPair);
		
		type = calculateType();
		
		init(HandValue.TWO_PAIR, valueCards, cards);
	}

	private void calculatePairs(List<Card> cards, List<Card> firstPair,	List<Card> secondPair, List<Card> thirdPair) throws DoesNotMakeHandException {
		ListIterator<Card> iterator = cards.listIterator(cards.size());
		Card lastCard = null;
		Card card = null;
		
		while (iterator.hasPrevious()) {
			if (lastCard == null) {
				lastCard = iterator.previous();
				continue;
			}
			
			card = iterator.previous();
			
			if (card.rank() == lastCard.rank()) {
				if (makesThreeOfAKind(firstPair, card)) {
					// At least three of a kind.
					throw new DoesNotMakeHandException(HandValue.TWO_PAIR);
				}
				
				if (makesFullHouse(firstPair, secondPair, card)) {
					// Full house.
					throw new DoesNotMakeHandException(HandValue.TWO_PAIR);
				}
				
				if (firstPair.isEmpty()) {
					firstPair.add(card);
					firstPair.add(lastCard);
				} else if (secondPair.isEmpty()) {
					secondPair.add(card);
					secondPair.add(lastCard);
				} else if (thirdPair.isEmpty()){
					thirdPair.add(card);
					thirdPair.add(lastCard);
				} else {
					// Full house (e.g. QQQ-KK-AA).
					throw new DoesNotMakeHandException(HandValue.TWO_PAIR);
				}
			}
			
			lastCard = card;
		}
		
		if (firstPair.isEmpty() || secondPair.isEmpty()) {
			throw new DoesNotMakeHandException(HandValue.TWO_PAIR);
		}
	}

	private boolean makesThreeOfAKind(List<Card> firstPair, Card card) {
		return !firstPair.isEmpty() && firstPair.get(0).rank() == card.rank();
	}

	private boolean makesFullHouse(List<Card> firstPair, List<Card> secondPair,
			Card card) {
		return !firstPair.isEmpty() && !secondPair.isEmpty() && secondPair.get(0).rank() == card.rank();
	}

	private Type calculateType() {
		if (getFirstPairType() == Pair.Type.BOARD_PAIR && getSecondPairType() == Pair.Type.BOARD_PAIR) {
			return Type.TWO_BOARD_PAIR;
		} else if (getFirstPairType() == Pair.Type.COMBINED_PAIR && getSecondPairType() == Pair.Type.COMBINED_PAIR) {
			return Type.TWO_PLAYER_PAIR;
		} else {
			return Type.COMBINED_TWO_PAIR;
		}
	}

}
