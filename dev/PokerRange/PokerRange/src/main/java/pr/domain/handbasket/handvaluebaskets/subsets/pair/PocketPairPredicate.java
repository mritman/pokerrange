package pr.domain.handbasket.handvaluebaskets.subsets.pair;

import pr.domain.handbasket.BasketElement;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.Pair;

import com.google.common.base.Predicate;

public class PocketPairPredicate implements Predicate<BasketElement> {

	@Override
	public boolean apply(BasketElement basketElement) {
		Combinations combinations = basketElement.getHighestHoleCards();
		
		if (combinations.getHighestHand().getValue() == HandValue.PAIR) {
			Pair pair = (Pair) combinations.getHighestHand();
			
			if (pair.getRelativeRank() == Pair.RelativeRank.PRE_FLOP_POCKET_PAIR) {
				return true;
			}
		}
		
		return false;
	}

}
