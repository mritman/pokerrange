package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class SuitedGapConnector extends HandType {
	
	public SuitedGapConnector() {
		this(Rank.TWO, Rank.QUEEN);
	}
	
	public SuitedGapConnector(Rank lowestRank, Rank highestRank) {
		super(HandTypeEnum.SUITED_GAP_CONNECTOR, Rank.TWO, Rank.QUEEN, lowestRank, highestRank);
	}
	
	public boolean addHand(HoleCards holeCards) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (holeCards.card2().rank().intValue() >= getLowestRank().intValue() && holeCards.card2().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(holeCards.isGapConnected() && holeCards.isSuited() && inRange) {
			return super.addHand(holeCards);
		}
		
		return false;
	}
}
