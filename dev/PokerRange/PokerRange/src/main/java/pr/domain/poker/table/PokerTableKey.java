package pr.domain.poker.table;

import lombok.Getter;
import pr.domain.poker.PokerVenue;

import com.google.common.base.Objects;

public class PokerTableKey {
	@Getter
	private final String name;
	@Getter
	private final PokerVenue venue;
	
	public PokerTableKey(String name, PokerVenue venue) {
		this.name = name;
		this.venue = venue;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;

		if (!(object instanceof PokerTableKey))	return false;

		PokerTableKey other = (PokerTableKey) object;

		return Objects.equal(name, other.name)
				&& Objects.equal(venue, other.venue);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(name, venue);
	}

}
