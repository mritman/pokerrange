package pr.domain.poker.hand;

import lombok.Getter;


public enum HandValue {
	HIGH_CARD(0),
	STRAIGHT_DRAW(1, true),
	FLUSH_DRAW(2, true),
	PAIR(3),
	TWO_PAIR(4),
	THREE_OF_A_KIND(5),
	STRAIGHT(6),
	FLUSH(7),
	FULL_HOUSE(8),
	FOUR_OF_A_KIND(9),
	STRAIGHT_FLUSH(10);
	
	private int intValue;
	
	@Getter
	private boolean draw;
	
	private HandValue(int intValue) {
		this(intValue, false);
	}
	
	private HandValue(int intValue, boolean draw) {
		this.intValue = intValue;
		this.draw = draw;
	}
	
	public int intValue() {
		return intValue;
	}

}