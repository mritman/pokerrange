package pr.domain.handbasket.handcollection;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.Combinations;
import pr.domain.poker.CombinationsComparator;

public class HandCollectionComparator implements Comparator<HandCollection>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public int compare(HandCollection handCollection1, HandCollection handCollection2) {
		return compareHandCollections(handCollection1, handCollection2);
	}
	
	public static int compareHandCollections(HandCollection handCollection1, HandCollection handCollection2) {
		if (handCollection1.getCombinations() == null || handCollection1.getCombinations().isEmpty()) {
			// If hands is null or empty it is always save to say that this
			// instance is smaller than the other.
			return -1;
		} else if (handCollection2.getCombinations() == null || handCollection2.getCombinations().isEmpty()) {
			// If the other instance's hands is null or empty it is always save to
			// say that this instance is greater than the other instance.
			return 1;
		}

		Combinations holeCards1 = handCollection1.getCombinations().first();
		Combinations holeCards2 = handCollection2.getCombinations().first();
		
		return CombinationsComparator.getInstance().compare(holeCards1, holeCards2);
	}

	public static int hashCode(HandCollection handCollection) {
		if (handCollection.getCombinations() != null && !handCollection.getCombinations().isEmpty()) {
			Combinations combinations = handCollection.getCombinations().first();
			return Objects.hash(combinations.getHighestHand(), combinations.getHoleCards().card1(), combinations.getHoleCards().card2());
		} else {
			return Objects.hash(handCollection.getHandValue());
		}
	}

}
