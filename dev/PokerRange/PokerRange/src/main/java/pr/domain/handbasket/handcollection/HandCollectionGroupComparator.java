package pr.domain.handbasket.handcollection;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.HandValueComparator;

import com.google.common.collect.ComparisonChain;

public class HandCollectionGroupComparator implements Comparator<HandCollectionGroup> {

	private static final HandCollectionGroupComparator handCollectionGroupComparator = new HandCollectionGroupComparator();
	
	public static HandCollectionGroupComparator getInstance() {
		return handCollectionGroupComparator;
	}
	
	@Override
	public int compare(HandCollectionGroup group1, HandCollectionGroup group2) {
		return ComparisonChain
				.start()
				.compare(group1.getHandValue(), group2.getHandValue(),
						HandValueComparator.getInstance())
				.result();
	}

	public static int hashCode(HandCollectionGroup handCollectionGroup) {
		return Objects.hash(handCollectionGroup.getHandValue());
	}

}