package pr.domain.poker.table;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * The number of seats at a poker table.
 */
public enum TableSize {
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	ELEVEN(11);
	
	private final int size;
	
	@Getter
	private final List<Seat> seats;
	
	private TableSize(int value) {
		this.size = value;
		seats = new ArrayList<>();
		for (int i = 1; i <= value; i++) {
			seats.add(Seat.fromInt(i));
		}
	}
	
	public int size() {
		return size;
	}
	
	public static TableSize fromString(String size) {
		int integerSize = Integer.parseInt(size);
		
		for (TableSize tableSize : TableSize.values()) {
			if (tableSize.size() == integerSize) {
				return tableSize;
			}
		}
		
		throw new IllegalArgumentException("Could not parse argument: " + size);
	}
	
	@Override
	public String toString() {
		return String.valueOf(size);
	}
	
}
