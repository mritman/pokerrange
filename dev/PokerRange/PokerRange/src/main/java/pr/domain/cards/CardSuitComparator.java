package pr.domain.cards;

import java.util.Comparator;

import com.google.common.collect.ComparisonChain;

public class CardSuitComparator implements Comparator<Card> {

	private static final CardSuitComparator cardSuitComparator = new CardSuitComparator();
	
	public static CardSuitComparator getInstance() {
		return cardSuitComparator;
	}
	
	@Override
	public int compare(Card card1, Card card2) {
		if (card1 == card2) {
			return 0;
		} else if (card1 == null) {
			return -1;
		} else if (card2 == null) {
			return 1;
		}
		
		return ComparisonChain
				.start()
				.compare(card1.suit(), card2.suit(), SuitComparator.getInstance())
				.compare(card1.rank(), card2.rank(), RankComparator.getInstance())
				.result();
	}

}
