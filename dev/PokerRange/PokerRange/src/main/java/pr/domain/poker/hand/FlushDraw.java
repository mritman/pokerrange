package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.cards.Suit;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class FlushDraw extends Hand {

	public enum Type {
		BOARD,
		PLAYER
	}
	
	@Getter private List<Card> flushDraw;
	@Getter private Type type;
	@Getter	private boolean nutFlushDraw;
	
	public FlushDraw(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHand(holeCards, boardCards);
	}
	
	public final Suit getSuit() {
		return flushDraw.get(0).suit();
	}

	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newFlushDraw = new LinkedList<>();
		
		containsFlushDraw(cards, newFlushDraw);
		
		Collections.sort(newFlushDraw, CardComparator.getInstance());
		flushDraw = Collections.unmodifiableList(newFlushDraw);
		type = calculateType(newFlushDraw, boardCards);
		nutFlushDraw = calculateNutFlush(holeCards, boardCards, getSuit());
		
		cards.removeAll(newFlushDraw);
		
		init(HandValue.FLUSH_DRAW, newFlushDraw, cards);
	}

	protected final static void containsFlushDraw(List<Card> cards, List<Card> flush) throws DoesNotMakeHandException {
		if (cards == null || cards.size() < 4 || cards.size() > 6) {
			throw new DoesNotMakeHandException(HandValue.FLUSH_DRAW);
		}

		Flush.containsSuits(cards, flush, 4);
	}
	
	private Type calculateType(List<Card> flushDraw, BoardCards boardCards) {
		for (Card card : flushDraw) {
			if (!boardCards.contains(card)) {
				return Type.PLAYER;
			}
		}
		
		return Type.BOARD;		
	}

	private boolean calculateNutFlush(HoleCards holeCards, BoardCards boardCards, Suit flushSuit) {
		List<Card> boardCardList = CardUtil.asList(null, boardCards);
		LinkedList<Card> boardSuitedList = new LinkedList<Card>();
		Card highestSuitedCardNotOnBoard = null;
		
		if (holeCards == null) return false;
		
		// Add all board cards that are of the same suit as flushSuit to cardsList.
		for (Card card : boardCardList) {
			if (card.suit() == flushSuit) {
				boardSuitedList.add(card);
			}
		}

		for (int i = Rank.ACE.intValue(); i >= Rank.TWO.intValue(); i--) {
			Rank currentRank = null;
			Card currentCard = null;
			
			currentRank = Rank.parseRank(i);
			currentCard = Card.createCard(currentRank, flushSuit);

			// If currentCard is not on the board it is the highest ranking suited card not on the board.
			if (!boardSuitedList.contains(currentCard)) {
				highestSuitedCardNotOnBoard = currentCard;
				break;
			}
		}
		
		if (holeCards.card1() == highestSuitedCardNotOnBoard || holeCards.card2() == highestSuitedCardNotOnBoard) {
			return true;
		}
		
		return false;
	}

}
