package pr.domain.poker.range.typed;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

import lombok.Getter;
import pr.domain.gamestate.GameState;
import pr.domain.poker.HoleCards;
import pr.domain.poker.range.HandRange;
import pr.domain.poker.range.typed.types.AceX;
import pr.domain.poker.range.typed.types.AceXSuited;
import pr.domain.poker.range.typed.types.HandType;
import pr.domain.poker.range.typed.types.HandTypeEnum;
import pr.domain.poker.range.typed.types.KingX;
import pr.domain.poker.range.typed.types.OtherHands;
import pr.domain.poker.range.typed.types.Pair;
import pr.domain.poker.range.typed.types.SuitedConnector;
import pr.domain.poker.range.typed.types.SuitedGapConnector;

import com.google.common.base.Preconditions;

/**
 * TODO Factor out the transients fields and related logic to a separate class.
 */
@Entity
public class TypedHandRange implements HandRange {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Getter
	private String name;
	
	@ElementCollection
	@CollectionTable(name="typedhandrange_handtyperange")
	@MapKeyEnumerated(EnumType.STRING)
	@MapKeyColumn(name="handtyperange_key")
	private Map<HandTypeEnum, HandTypeRange> range;
	
	@Transient private Pair pair = new Pair();
	@Transient private AceXSuited aceXSuited = new AceXSuited();
	@Transient private AceX aceX = new AceX();
	@Transient private KingX kingX = new KingX();
	@Transient private SuitedConnector suitedConnector = new SuitedConnector();
	@Transient private SuitedGapConnector suitedGapConnector = new SuitedGapConnector();
	@Transient private OtherHands other = new OtherHands();
	@Transient private List<HandType> handTypes = new LinkedList<>();
	
	TypedHandRange() {
		initHandTypesList();
	};
	
	public TypedHandRange(String name) {
		this(name, new HashMap<HandTypeEnum, HandTypeRange>());
	}
	
	public TypedHandRange(String name, Map<HandTypeEnum, HandTypeRange> rangeMap) {
		Preconditions.checkNotNull(rangeMap);
		
		this.name = name;
		range = new HashMap<>(rangeMap);
		
		initHandTypesList();
		
		if (range.isEmpty()) {
			for (HandType handType : handTypes) {
				range.put(handType.getName(), new HandTypeRange(handType.getLowestRank(), handType.getHighestRank()));
			}
		}
		
		setRange(range);
	}

	private void initHandTypesList() {
		handTypes.add(pair);
		handTypes.add(aceXSuited);
		handTypes.add(aceX);
		handTypes.add(kingX);
		handTypes.add(suitedConnector);
		handTypes.add(suitedGapConnector);
		handTypes.add(other);
	}
	
	@Override
	public void calculateRange(GameState gameState) {
		Set<HoleCards> possibleHoleCardCombinations = HoleCards.createAllPossibleHoleCards(gameState); 
		
		Iterator<HoleCards> itr = possibleHoleCardCombinations.iterator();
		HoleCards currentHand;
		
		clear();
		
		while(itr.hasNext()) {
			currentHand = itr.next();
			
			assignHoleCardCombinationToType(currentHand);
			
			itr.remove();
		}
	}

	private void assignHoleCardCombinationToType(HoleCards hand) {
		if (pair.addHand(hand)) {
			return;
		} else if (aceXSuited.addHand(hand)) {
			return;
		} else if (aceX.addHand(hand)) {
			return;
		} else if (kingX.addHand(hand)) {
			return;
		} else if (suitedConnector.addHand(hand)) {
			return;
		} else if (suitedGapConnector.addHand(hand)) {
			return;
		} else {
			other.addHand(hand);
		}
	}
	
	@Override
	public Set<HoleCards> getHandsInRange() {
		TreeSet<HoleCards> handsInRange = new TreeSet<>();
		
		for (HandType handType : handTypes) {
			
			if (handType.getName() == HandTypeEnum.OTHER) continue; 
			
			handsInRange.addAll(handType.getHands());
		}
		
		return Collections.unmodifiableSet(handsInRange);
	}
	
	public void clear() {
		for (HandType handType : handTypes) {
			handType.getHands().clear();
		}
	}
	
	public void resetRange() {
		for (HandType handType : handTypes) {
			handType.resetRange();
		}
	}
	
	public Map<HandTypeEnum, HandTypeRange> getRange() {
		HashMap<HandTypeEnum, HandTypeRange> rangeMap = new HashMap<>();
		
		for (HandType handType : handTypes) {
			if (handType.getName() == HandTypeEnum.OTHER || !handType.isEnabled()) continue; 
			
			HandTypeRange range = new HandTypeRange(handType.getLowestRank(), handType.getHighestRank());
			rangeMap.put(handType.getName(), range);
		}
		
		return rangeMap;
	}
	
	public void setRange(Map<HandTypeEnum, HandTypeRange> rangeMap) {
		for (HandType handType : handTypes) {
			HandTypeRange handTypeRange = rangeMap.get(handType.getName());
			updateHandType(handType, handTypeRange);
		}
	}

	private void updateHandType(HandType handType, HandTypeRange range) {
		if (range != null) {
			handType.setLowestRank(range.getLower());
			handType.setHighestRank(range.getUpper());
			handType.enable();
		} else {
			handType.disable();
		}
	}
	
	public int getSize(HandTypeEnum handTypeName) {
		for (HandType handType : handTypes) {
			if (handType.getName() == handTypeName) {
				return handType.getHands().size();
			}
		}
		
		return 0;
	}
	
	public int getInRangeSize() {
		int size = getTotalSize();
		size -= other.getHands().size();
		return size;
	}
	
	public int getTotalSize() {
		int size = 0;
		
		for (HandType handType : handTypes) {
			size += handType.getHands().size();
		}
		
		return size;
	}

	public double getPercentage(HandTypeEnum handTypeName) {

		for (HandType handType : handTypes) {
			if (handType.getName() == handTypeName) {
				if (getTotalSize() == 0) break;
				return (double) handType.getHands().size() / getTotalSize() * 100;
			}
		}
		
		return 0;
	}

	public double getInRangePercentage() {
		return (double) getInRangeSize() / getTotalSize() * 100;
	}
	
	public HandType findHandType(HandTypeEnum handTypeName) {
		
		for (HandType handType : handTypes) {
			if (handType.getName() == handTypeName) {
				return handType;
			}
		}
		
		return null;
	}

	public void setHandTypeRange(HandTypeEnum handTypeEnum, HandTypeRange typeRange) {
		range.put(handTypeEnum, typeRange);
		HandType handType = findHandType(handTypeEnum);
		handType.setHighestRank(typeRange.getLower());
		handType.setHighestRank(typeRange.getUpper());
		
		updateHandType(handType, typeRange);
	}

	public void removeHandTypeRange(HandTypeEnum handTypeEnum) {
		range.remove(handTypeEnum);
		updateHandType(findHandType(handTypeEnum), null);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, range);
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		
		if (!(object instanceof TypedHandRange)) return false;
		
		TypedHandRange other = (TypedHandRange) object;
		
		return Objects.equals(name, other.name) && Objects.equals(range, other.range);
	}
	
	@PostLoad
	protected void postLoad() {
		setRange(range);
	}
	
	@Override
	public String toString() {
		return com.google.common.base.Objects.toStringHelper(getClass())
				.add("name", name).add("range", range).toString();
	}

}
