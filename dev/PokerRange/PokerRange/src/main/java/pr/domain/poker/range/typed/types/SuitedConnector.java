package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class SuitedConnector extends HandType {
	
	public SuitedConnector() {
		this(Rank.TWO, Rank.KING);
	}
	
	public SuitedConnector(Rank lowestRank, Rank highestRank) {
		super(HandTypeEnum.SUITED_CONNECTOR, Rank.TWO, Rank.KING, lowestRank, highestRank);
	}

	public boolean addHand(HoleCards holeCards) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (holeCards.card2().rank().intValue() >= getLowestRank().intValue() && holeCards.card2().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(holeCards.isConnected() && holeCards.isSuited() && inRange) {
			return super.addHand(holeCards);
		}
		
		return false;
	}
}
