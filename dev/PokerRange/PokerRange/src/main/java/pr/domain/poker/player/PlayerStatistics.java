package pr.domain.poker.player;

import com.google.common.base.Objects;

import lombok.Getter;

public class PlayerStatistics {
	public static final PlayerStatistics DEFAULT = new PlayerStatistics(0, 0, 0, 0, 0);

	/**
	 * The number of hands in the database of this player.
	 */
	@Getter
	private final int numberOfHands;
	
	/**
	 * @return the percentage of hands that player voluntarily put money in the pot if
	 * he had the opportunity to do so
	 */
	@Getter	
	private final double vpip;
	
	/**
	 * @return the percentage of hands that the player raised one or more times preflop
	 */
	@Getter
	private final double preflopRaise;
	
	/**
	 * @return the percentage of hands that the player three bet preflop
	 */
	@Getter	
	private final double preflopThreeBet;

	/**
	 * @return the percentage of times that the player faced a first raise
	 *         preflop and called it
	 */
	@Getter
	private final double preflopColdCallFirstRaise;
	
	public PlayerStatistics(int numberOfHands, double vpip, double preflopRaise, double preflopThreeBet, double preflopColdCallFirstRaise) {
		this.numberOfHands = numberOfHands;
		this.vpip = vpip;
		this.preflopRaise = preflopRaise;
		this.preflopThreeBet = preflopThreeBet;
		this.preflopColdCallFirstRaise = preflopColdCallFirstRaise;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass())
				.add("numberOfHands", numberOfHands)
				.add("vpip", vpip)
				.add("preflopRaise", preflopRaise)
				.add("preflopThreeBet", preflopThreeBet)
				.add("preflopColdCallFirstRaise", preflopColdCallFirstRaise)
				.toString();
	}

}
