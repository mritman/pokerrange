package pr.domain.poker.event;

import com.google.common.base.Objects;

import lombok.Getter;

public class PlayerSitsOutEvent implements PokerEvent {

	@Getter private final String name;

	public PlayerSitsOutEvent(String name) {
		super();
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("name", name).toString();
	}

}
