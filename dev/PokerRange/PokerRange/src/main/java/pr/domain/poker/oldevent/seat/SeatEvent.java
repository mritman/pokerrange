package pr.domain.poker.oldevent.seat;

import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.table.PokerTable;

public abstract class SeatEvent extends PokerTableEvent {

	protected SeatEvent(PokerTable table) {
		super(table);
	}

}
