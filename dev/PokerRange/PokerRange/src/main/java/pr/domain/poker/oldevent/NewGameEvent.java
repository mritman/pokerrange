package pr.domain.poker.oldevent;

import pr.domain.poker.table.PokerTable;

public class NewGameEvent extends PokerTableEvent {

	public NewGameEvent(PokerTable table) {
		super(table);
	}

}
