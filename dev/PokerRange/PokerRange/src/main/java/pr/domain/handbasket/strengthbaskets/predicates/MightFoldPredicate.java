package pr.domain.handbasket.strengthbaskets.predicates;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

import com.google.common.base.Predicate;

public class MightFoldPredicate implements Predicate<Combinations>  {

	private List<HandValue> handValues;
	
	public MightFoldPredicate() {
		handValues = new LinkedList<>();
		
		Collections.addAll(handValues,
				HandValue.STRAIGHT_DRAW, 
				HandValue.FLUSH_DRAW, 
				HandValue.PAIR,
				HandValue.TWO_PAIR); 
	}
	
	@Override
	public boolean apply(Combinations combinations) {
		HandValue value = combinations.getHighestHand().getValue();
		
		if (handValues.contains(value)) {
			return true;
		}
		
		return false;
	}

}
