package pr.domain.handbasket.strengthbaskets.predicates;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Predicate;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

public class WontFoldPredicate implements Predicate<Combinations> {
	private List<HandValue> handValues;
	
	public WontFoldPredicate() {
		handValues = new LinkedList<>();
		
		Collections.addAll(handValues,
				HandValue.THREE_OF_A_KIND,
				HandValue.STRAIGHT,
				HandValue.FLUSH,
				HandValue.FOUR_OF_A_KIND,
				HandValue.FULL_HOUSE,
				HandValue.STRAIGHT_FLUSH); 
	}
	
	@Override
	public boolean apply(Combinations combinations) {
		HandValue value = combinations.getHighestHand().getValue();
		
		if (handValues.contains(value)) {
			return true;
		}
		
		return false;
	}

}
