package pr.domain.cards;

import static pr.domain.cards.Rank.ACE;
import static pr.domain.cards.Rank.EIGHT;
import static pr.domain.cards.Rank.FIVE;
import static pr.domain.cards.Rank.FOUR;
import static pr.domain.cards.Rank.JACK;
import static pr.domain.cards.Rank.KING;
import static pr.domain.cards.Rank.NINE;
import static pr.domain.cards.Rank.QUEEN;
import static pr.domain.cards.Rank.SEVEN;
import static pr.domain.cards.Rank.SIX;
import static pr.domain.cards.Rank.TEN;
import static pr.domain.cards.Rank.THREE;
import static pr.domain.cards.Rank.TWO;
import static pr.domain.cards.Rank.parseRank;
import static pr.domain.cards.Suit.CLUB;
import static pr.domain.cards.Suit.DIAMOND;
import static pr.domain.cards.Suit.HEART;
import static pr.domain.cards.Suit.SPADE;
import static pr.domain.cards.Suit.parseSuit;

import java.util.Comparator;

import com.google.common.base.Preconditions;

public enum Card implements Comparator<Card> {
	CLUB_ACE		(ACE, 	CLUB),
	CLUB_KING		(KING,	CLUB),
	CLUB_QUEEN		(QUEEN,	CLUB),
	CLUB_JACK		(JACK, 	CLUB),
	CLUB_TEN		(TEN, 	CLUB),
	CLUB_NINE		(NINE, 	CLUB),
	CLUB_EIGHT		(EIGHT, CLUB),
	CLUB_SEVEN		(SEVEN, CLUB),
	CLUB_SIX		(SIX, 	CLUB),
	CLUB_FIVE		(FIVE, 	CLUB),
	CLUB_FOUR		(FOUR, 	CLUB),
	CLUB_THREE		(THREE, CLUB),
	CLUB_TWO		(TWO, 	CLUB),
	
	DIAMOND_ACE		(ACE, 	DIAMOND),
	DIAMOND_KING	(KING, 	DIAMOND),
	DIAMOND_QUEEN	(QUEEN, DIAMOND),
	DIAMOND_JACK	(JACK, 	DIAMOND),
	DIAMOND_TEN		(TEN, 	DIAMOND),
	DIAMOND_NINE	(NINE, 	DIAMOND),
	DIAMOND_EIGHT	(EIGHT, DIAMOND),
	DIAMOND_SEVEN	(SEVEN, DIAMOND),
	DIAMOND_SIX		(SIX, 	DIAMOND),
	DIAMOND_FIVE	(FIVE, 	DIAMOND),
	DIAMOND_FOUR	(FOUR, 	DIAMOND),
	DIAMOND_THREE	(THREE, DIAMOND),
	DIAMOND_TWO		(TWO, 	DIAMOND),
	
	SPADE_ACE		(ACE, 	SPADE),
	SPADE_KING		(KING, 	SPADE),
	SPADE_QUEEN		(QUEEN, SPADE),
	SPADE_JACK		(JACK, 	SPADE),
	SPADE_TEN		(TEN, 	SPADE),
	SPADE_NINE		(NINE, 	SPADE),
	SPADE_EIGHT		(EIGHT, SPADE),
	SPADE_SEVEN		(SEVEN, SPADE),
	SPADE_SIX		(SIX, 	SPADE),
	SPADE_FIVE		(FIVE, 	SPADE),
	SPADE_FOUR		(FOUR, 	SPADE),
	SPADE_THREE		(THREE, SPADE),
	SPADE_TWO		(TWO, 	SPADE),

	HEART_ACE		(ACE, 	HEART),
	HEART_KING		(KING, 	HEART),
	HEART_QUEEN		(QUEEN, HEART),
	HEART_JACK		(JACK, 	HEART),
	HEART_TEN		(TEN, 	HEART),
	HEART_NINE		(NINE, 	HEART),
	HEART_EIGHT		(EIGHT, HEART),
	HEART_SEVEN		(SEVEN, HEART),
	HEART_SIX		(SIX, 	HEART),
	HEART_FIVE		(FIVE, 	HEART),
	HEART_FOUR		(FOUR, 	HEART),
	HEART_THREE		(THREE, HEART),
	HEART_TWO		(TWO, 	HEART);
	
	private final Rank rank;
	private final Suit suit;
	
	private Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public Rank rank() {
		return rank;
	}
	
	public Suit suit() {
		return suit;
	}
	
	public String toString() {
		return rank.strValue() + suit.charValue();
	}
	
	public static Card createCard(Rank rank, Suit suit) {
		Preconditions.checkNotNull(rank);
		Preconditions.checkNotNull(suit);
		
		for(Card c : Card.values()) {
			if (c.rank() == rank && c.suit() == suit) {
				return c;
			}
		}
		
		throw new IllegalArgumentException();
    }
	
	public static boolean isCard(String par) {
		char rank;
		char suit;
		
		if (par.length() == 2) {
			rank = par.charAt(0);
			suit = par.charAt(1);
		} else {
			return false;
		}
		
		return Rank.isRank(rank) && Suit.isSuit(suit);
	}
	
	public static Card fromString(String par) {
		Rank rank;
		Suit suit;

		if (par.length() == 2) {
			rank = parseRank(par.charAt(0));
			suit = parseSuit(par.charAt(1));
		} else if (par.length() == 3) {
			rank = parseRank(par.substring(0, 2));
			suit = parseSuit(par.charAt(2));
		} else {
			throw new IllegalArgumentException();
		}
		
		return Card.createCard(rank, suit);
	}
	
	public static boolean isHigher(Card card1, Card card2){
		if(card1.rank().intValue() > card2.rank().intValue()) {
			return true;
		} else if (card1.rank().intValue() < card2.rank().intValue()) {
			return false;
		} else {
			if (card1.suit().intValue() > card2.suit().intValue()) {
				return true;
			} else if (card1.suit().intValue() < card2.suit().intValue()) {
				return false;
			}
		}
		
		return false;
	}

	public int compare(Card card1, Card card2) {
		return CardComparator.getInstance().compare(card1, card2);
	}
	
}
