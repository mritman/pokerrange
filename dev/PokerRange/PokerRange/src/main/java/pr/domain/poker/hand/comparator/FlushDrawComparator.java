package pr.domain.poker.hand.comparator;

import java.util.Comparator;

import pr.domain.poker.hand.FlushDraw;

public class FlushDrawComparator implements Comparator<FlushDraw> {

	private static final FlushDrawComparator flushDrawComparator = new FlushDrawComparator();
	
	public static FlushDrawComparator getInstance() {
		return flushDrawComparator;
	}
	
	@Override
	public int compare(FlushDraw fd1, FlushDraw fd2) {
		return KickerComparator.compareKickers(fd1.getValueCards(), fd2.getValueCards());
	}

	public int hashCode(FlushDraw hand) {
		return KickerComparator.getInstance().hashCode(hand.getValueCards());
	}

}
