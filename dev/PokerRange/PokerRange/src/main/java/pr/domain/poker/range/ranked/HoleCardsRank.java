package pr.domain.poker.range.ranked;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

class HoleCardsRank implements Comparable<HoleCardsRank> {
	
	public static final int MAX_RANK_SIZE = 12;
	
	@Getter private final Rank rank1;
	@Getter private final Rank rank2;
	@Getter private final SuitedRequirement suited;
	@Getter private final int ordinal;
	
	public HoleCardsRank(Rank rank1, Rank rank2, SuitedRequirement suited, int ordinal) {
		this.rank1 = rank1;
		this.rank2 = rank2;
		this.suited = suited;
		this.ordinal = ordinal;
	}

	@Override
	public int compareTo(HoleCardsRank o) {
		return Integer.compare(ordinal, o.getOrdinal());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		
		if (!(obj instanceof HoleCardsRank)) {
			return false;
		}
		
		HoleCardsRank other = (HoleCardsRank) obj;
		
		return Objects.equals(rank1, other.getRank1())
				&& Objects.equals(rank2, other.getRank2())
				&& Objects.equals(suited, other.getSuited());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(rank1, rank2, suited);
	}
	
	@Override
	public String toString() {
		return com.google.common.base.Objects.toStringHelper(getClass().getName())
				.add("rank1", rank1)
				.add("rank2", rank2)
				.add("suited", suited)
				.add("ordinal", ordinal)
				.toString();
	}
	
	public Set<HoleCards> filter(Collection<HoleCards> holeCardsCollection) {
		Set<HoleCards> filteredSet = new TreeSet<>();
		
		for (HoleCards holeCards : holeCardsCollection) {
			if (holeCards.card1().rank() == rank1 && holeCards.card2().rank() == rank2) {
				if (holeCards.isPair() && rank1 == rank2) {
					filteredSet.add(holeCards);
				} else if (suited == SuitedRequirement.SUITED && holeCards.isSuited()) {
					filteredSet.add(holeCards);
				} else if (suited == SuitedRequirement.OFFSUIT && !holeCards.isSuited()) {
					filteredSet.add(holeCards);
				}
			}
		}
		
		return filteredSet;
	}
	
	public enum SuitedRequirement {
		SUITED,
		OFFSUIT,
		BOTH;
		
		public static SuitedRequirement fromChar(char c) {
			switch (c) {
			case 's':
				return SUITED;
			case 'o':
				return OFFSUIT;
			case 'b':
				return BOTH;
			default:
				throw new IllegalArgumentException("Could not convert char: " + c);
			}
		}
	}

}