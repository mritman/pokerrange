package pr.domain.poker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import pr.domain.cards.Card;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

public class Flop implements Comparable<Flop> {
	
	public static final int SIZE = 3;

	private final Stack<Card> flop;
	
	public Flop(Card card1, Card card2, Card card3) {
		Preconditions.checkNotNull(card1);
		Preconditions.checkNotNull(card2);
		Preconditions.checkNotNull(card3);
		
		flop = new Stack<>();
		flop.push(card1);
		flop.push(card2);
		flop.push(card3);
	}
	
	public Flop(List<Card> cards) {
		Preconditions.checkNotNull(cards);
		Preconditions.checkArgument(cards.size() == 3);
		
		flop = new Stack<>();
		flop.push(cards.get(0));
		flop.push(cards.get(1));
		flop.push(cards.get(2));
	}
	
	public List<Card> getCards() {
		return new LinkedList<>(flop);
	}
	
	public Card getCardOne() {
		return flop.get(0);
	}
	
	public Card getCardTwo() {
		return flop.get(1);
	}
	
	public Card getCardThree() {
		return flop.get(2);
	}
	
	@Override	
	public boolean equals(Object object) {
		if (object == null) return false;
		if (!(object instanceof Flop)) return false;
		
		Flop other = (Flop) object;
		
		return getCards().equals(other.getCards());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(flop.get(0), flop.get(1), flop.get(2));
	}
	
	@Override
	public String toString() {
		return flop.get(0).toString() + " " + flop.get(1).toString() + " " + flop.get(2).toString(); 
	}

	@Override
	public int compareTo(Flop other) {
		return ComparisonChain.start()
				.compare(getCardOne(), other.getCardOne())
				.compare(getCardTwo(), other.getCardTwo())
				.compare(getCardThree(), other.getCardThree())
				.result();
	}

	public static Flop fromString(String flop) {
		String[] cardStrings = flop.split(" ");
		List<Card> cards = new ArrayList<>();
		for (String cardString : cardStrings) {
			cards.add(Card.fromString(cardString));
		}
		
		return new Flop(cards);
	}

}
