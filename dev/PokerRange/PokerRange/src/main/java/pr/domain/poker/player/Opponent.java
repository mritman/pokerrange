package pr.domain.poker.player;


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.google.common.base.Preconditions;

import lombok.Getter;
import lombok.Setter;
import pr.domain.poker.range.HandRange;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;

public class Opponent {

	@Getter
	@Setter
	private  String name;

	@Getter
	private final PokerTable table;
	
	@Getter
	private Seat seat;
	
	@Getter
	private ActionHistory actionHistory;
	
	@Getter
	@Setter
	private boolean inHand;
	
	@Getter
	private PlayerStatistics statistics; 

	private final Map<String, HandRange> ranges;
	
	public Opponent(String name, PokerTable table, Seat seat) {
		if (name == null || name.isEmpty() || table == null || seat == null) {
			throw new IllegalArgumentException("Constructor arguments may not be null.");
		}
		
		this.name = name;
		this.table = table;
		this.seat = seat;
		
		actionHistory = new ActionHistory();
		ranges = new HashMap<>();
		
		statistics = PlayerStatistics.DEFAULT;
	}
	
	public void setRanges(Collection<HandRange> ranges) {
		this.ranges.clear();
		for (HandRange range : ranges) {
			this.ranges.put(range.getName(), range);
		}
	}
	
	public Map<String, HandRange> getRanges() {
		return Collections.unmodifiableMap(ranges);
	}

	public void addRange(HandRange newRange) {
		ranges.put(newRange.getName(), newRange);
	}
	
	public void removeRange(HandRange newRange) {
		ranges.remove(newRange.getName());
	}
	
	public void removeRange(String rangeName) {
		ranges.remove(rangeName);
	}
	
	public void setStatistics(PlayerStatistics statistics) {
		if (statistics == null) return; 
		
		this.statistics = statistics;
	}
	
	public void setSeat(Seat seat) {
		Preconditions.checkNotNull(seat);
		this.seat = seat;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == null || getClass() != object.getClass()) {
			return false;
		}
		
		final Opponent other = (Opponent) object;
		
		return Objects.equals(name, other.getName()) && Objects.equals(table, other.getTable()); 
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, table);
	}

	@Override
	public String toString() {
		return com.google.common.base.Objects.toStringHelper(getClass())
				.add("name", name)
				.add("pokerTable", table.getName())
				.add("seat", seat)
				.add("inHand", inHand)
				.toString();
	}

}
