package pr.domain.poker.event.board;

import pr.domain.poker.event.PokerEvent;

public interface BoardCardEvent extends PokerEvent {}
