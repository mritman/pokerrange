package pr.domain.poker.table;


public enum Seat {
	SEAT1(1),
	SEAT2(2),
	SEAT3(3),
	SEAT4(4),
	SEAT5(5),
	SEAT6(6),
	SEAT7(7),
	SEAT8(8),
	SEAT9(9),
	SEAT10(10),
	SEAT11(11);
	
	private int value;
	
	private Seat(int value) {
		this.value = value;
	}
	
	public int toInt() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public static Seat fromInt(int value) {
		for (Seat seat : Seat.values()) {
			if (seat.toInt() == value) {
				return seat;
			}
		}
		
		throw new IllegalArgumentException(
				"No seat with could be found that corresponds to the specified argument: "
						+ value + ".");
	}

	public static Seat parseSeat(String seat) {
		int seatInt = Integer.parseInt(seat);
		return fromInt(seatInt);
	}

}