package pr.domain.poker.action;

import lombok.Getter;
import pr.domain.poker.Street;

import com.google.common.base.Objects;

public class Action {

	@Getter
	private final ActionType type;
	
	@Getter
	private final Street street;
	
	public Action(ActionType type, Street street) {
		this.type = type;
		this.street = street;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("type", type)
				.add("street", street)
				.toString();
	}
	
}
