package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class FullHouse extends Hand {
	
	public enum Type {
		BOARD,
		PLAYER
	};
	
	@Getter
	private Type type;
	
	@Getter	private List<Card> pair;
	@Getter private Pair.Type pairType;
	@Getter private List<Card> threeOfAKind;
	@Getter private ThreeOfAKind.Type threeOfAKindType;

	protected FullHouse(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);

		calculateHand(holeCards, boardCards);
	}
	
	public Rank getPairRank() {
		return pair.get(0).rank();
	}
	
	public Rank getThreeOfAKindRank() {
		return threeOfAKind.get(0).rank();
	}

	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newPair = new LinkedList<>();
		List<Card> newThreeOfAKind = new LinkedList<>();
		List<Card> valueCards = new LinkedList<>();
		
		calculateFullHouse(cards, newPair, newThreeOfAKind);

		Collections.sort(newPair, CardComparator.getInstance());
		Collections.sort(newThreeOfAKind, CardComparator.getInstance());
		pair = Collections.unmodifiableList(newPair);
		threeOfAKind = Collections.unmodifiableList(newThreeOfAKind);
		
		pairType = Pair.calculatePairType(newPair, holeCards, boardCards);
		threeOfAKindType = ThreeOfAKind.calculateThreeOfAkindType(newThreeOfAKind, boardCards);
		
		type = calculateType();
				
		valueCards.addAll(newPair);
		valueCards.addAll(newThreeOfAKind);
		cards.removeAll(valueCards);
		
		init(HandValue.FULL_HOUSE, valueCards, cards);
	}

	private void calculateFullHouse(List<Card> cards, List<Card> pair, List<Card> threeOfAKind) throws DoesNotMakeHandException {
		checkFullHousePreconditions(cards);
		
		ListIterator<Card> iterator = cards.listIterator(cards.size());
		Card lastCard = null;
		Card card = null;
		
		List<Card> equalRanks = new LinkedList<>();
		
		while (iterator.hasPrevious()) {
			if (lastCard == null) {
				lastCard = iterator.previous();
				continue;
			}
			
			card = iterator.previous();
			
			if (card.rank() == lastCard.rank()) {
				processEqualRankedCard(pair, threeOfAKind, lastCard, card, equalRanks);
			}
			
			lastCard = card;
		}
		
		if (pair.isEmpty() && equalRanks.size() == 2) {
			pair.addAll(equalRanks);
		}

		if (noFullHouseFound(pair, threeOfAKind)) {
			throw new DoesNotMakeHandException(HandValue.FULL_HOUSE);
		}
	}

	private void processEqualRankedCard(List<Card> pair,
			List<Card> threeOfAKind, Card lastCard, Card card,
			List<Card> equalRanks) throws DoesNotMakeHandException {
		
		if (equalRanks.isEmpty()) {
			equalRanks.add(card);
			equalRanks.add(lastCard);
		} else if (willMakeThreeOfAKind(card, equalRanks)) {
			if (threeOfAKind.isEmpty()) {
				equalRanks.add(card);
				threeOfAKind.addAll(equalRanks);
			} else if (pair.isEmpty()) {
				Collections.sort(equalRanks, CardComparator.getInstance());
				pair.addAll(equalRanks);
				equalRanks.add(card);
			}
		} else if (equalRanksContainsPair(card, equalRanks)) {
			if (pair.isEmpty()) {
				pair.addAll(equalRanks);
			}
			equalRanks.clear();
			equalRanks.add(card);
			equalRanks.add(lastCard);
		} else if (willMakeFourOfAKind(card, equalRanks)) {
			// Four of a kind.
			throw new DoesNotMakeHandException(HandValue.FULL_HOUSE);
		} else if (equalRanks.size() == 3 && equalRanks.get(0).rank() != card.rank()) {
			equalRanks.clear();
			equalRanks.add(card);
			equalRanks.add(lastCard);
		} else {
			throw new IllegalStateException("This block should never be reached.");
		}
	}

	private void checkFullHousePreconditions(List<Card> cards)
			throws DoesNotMakeHandException {
		if (cards == null || cards.size() < 5 || cards.size() > 7) {
			throw new DoesNotMakeHandException(HandValue.FULL_HOUSE);
		}
	}

	private boolean equalRanksContainsPair(Card card, List<Card> equalRanks) {
		return equalRanks.size() == 2 && equalRanks.get(0).rank() != card.rank();
	}

	private boolean willMakeThreeOfAKind(Card card, List<Card> equalRanks) {
		return equalRanks.size() == 2 && equalRanks.get(0).rank() == card.rank();
	}

	private boolean willMakeFourOfAKind(Card card, List<Card> equalRanks) {
		return equalRanks.size() == 3 && equalRanks.get(0).rank() == card.rank();
	}

	private boolean noFullHouseFound(List<Card> pair, List<Card> threeOfAKind) {
		return pair.isEmpty() || pair.size() != 2 || threeOfAKind.isEmpty() || threeOfAKind.size() != 3;
	}
	
	private Type calculateType() {
		if (pairType == Pair.Type.BOARD_PAIR && threeOfAKindType == ThreeOfAKind.Type.BOARD) {
			return Type.BOARD;
		}
		
		return Type.PLAYER;
	}

}
