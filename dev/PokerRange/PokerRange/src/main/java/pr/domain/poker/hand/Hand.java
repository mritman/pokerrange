package pr.domain.poker.hand;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.hand.comparator.HandComparator;

public abstract class Hand implements Comparable<Hand> {
	
	/**
	 * The maximum number of cards in a hand.
	 */
	protected final static int MAX_HAND_SIZE = 5;
	
	@Getter
	private final HoleCards holeCards;
	
	@Getter
	private final BoardCards boardCards;
	
	/**
	 * It is the responsibility of a concrete subclass to initialize this value.
	 */
	@Getter
	private HandValue value;
	
	private Map<Card, Boolean> valueMap;
	
	/**
	 * The following lists are unmodifiable and sorted by rank.
	 */
	private List<Card> valueCards;
	private List<Card> nonValueCards;
	private List<Card> kickers;
	
	public Hand(HoleCards holeCards, BoardCards boardCards) {
		this.boardCards = boardCards;
		this.holeCards = holeCards;
	}

	protected void init(HandValue value, List<Card> valueCards, List<Card> nonValueCards) {
		Collections.sort(valueCards, CardComparator.getInstance());
		Collections.sort(nonValueCards, CardComparator.getInstance());
		
		this.value = value;
		this.valueCards = Collections.unmodifiableList(valueCards);
		this.nonValueCards = Collections.unmodifiableList(nonValueCards);
		calculateKickers();
		
		valueMap = new HashMap<Card, Boolean>();
		
		for (Card card : valueCards) {
			valueMap.put(card, true);
		}
		
		for (Card card : nonValueCards) {
			valueMap.put(card, false);
		}

		this.valueMap = Collections.unmodifiableMap(valueMap);
	}
	
	private void calculateKickers() {
		int nrOfValueCards = getValueCards().size();
		int nrOfNonValueCards = nonValueCards.size();
		int nrOfKickers = MAX_HAND_SIZE - nrOfValueCards;
		
		nrOfKickers = Math.min(nrOfKickers, nrOfNonValueCards);
		nrOfKickers = Math.max(nrOfKickers, 0); 
		
		List<Card> newKickers = nonValueCards.subList(nrOfNonValueCards - nrOfKickers, nrOfNonValueCards);
		
		this.kickers = Collections.unmodifiableList(newKickers);
	}

	/**
	 * @return an unmodifiable map that maps each card in both the holecards and
	 *         boardcards to a boolean indicating if the card is a value card. A
	 *         value card is a card that contributes to the value of the
	 *         hand (not including kickers).
	 */
	public Map<Card, Boolean> getValueMap() {
		return valueMap;
	}
	
	/**
	 * @return an unmodifiable list containing the cards that make up the hand
	 *         without the kickers sorted by {@link CardComparator}.
	 */
	public List<Card> getValueCards() {
		return valueCards;
	}
	
	/**
	 * This methods returns an unmodifiable list of all cards that do not contribute to the
	 * value of the hand. This method differs from {@link #getKickers()} because it
	 * returns all other cards, {@link #getKickers()} returns a list containing at most 3 cards.
	 * 
	 * @return an unmodifiable list of non value cards sorted by {@link CardComparator}.
	 */
	public List<Card> getNonValueCards() {
		return nonValueCards;
	}
	
	/**
	 * @return an unmodifiable list containing the kicker cards sorted by {@link CardComparator}.
	 */
	public List<Card> getKickers() {
		return kickers;
	}

	public boolean hasPocketPair() {
		return holeCards.isPair();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		for (Card card : getValueCards()) {
			sb.append(card.toString()).append(" ");
		}
		
		if (getNonValueCards().size() > 0) {
			sb.append("( ");
			
			for (Card card : getNonValueCards()) {
				sb.append(card.toString()).append(" ");
			}
			
			sb.append(") ");
		}
		
		return sb.toString();
	}

	@Override
	public final int compareTo(Hand other) {
		return HandComparator.getInstance().compare(this, other);
	}
	
	@Override
	public final boolean equals(Object other) {
		if (this == other) return true;

		if (!(other instanceof Hand)) return false;

		Hand otherHand = (Hand) other;
		
		return compareTo(otherHand) == 0;
	}
	
	@Override
	public final int hashCode() {
		return HandComparator.getInstance().hashCode(this);
	}
	
}
