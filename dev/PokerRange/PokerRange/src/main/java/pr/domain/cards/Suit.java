package pr.domain.cards;


public enum Suit {
	SPADE(1, 's'),
	CLUB(2, 'c'),
	HEART(3, 'h'),
	DIAMOND(4, 'd');
	
	private final int intValue;
	private final char charValue;
	
	Suit(int intValue, char charValue) {
		this.intValue = intValue;
		this.charValue = charValue;
	}
	
	public int intValue() {
		return intValue;
	}
	
	public char charValue() {
		return charValue;
	}
	
	public static Suit parseSuit(int i) {
		for (Suit s : Suit.values()) {
			if (i == s.intValue()) {
				return s;
			}
		}
		
		throw new IllegalArgumentException();
	}
	
	public static Suit parseSuit(char arg) {			
		for (Suit s : Suit.values()) {
			if (arg == s.charValue()) {
				return s;
			}
		}
		
		throw new IllegalArgumentException();
	}
	
	public static boolean isSuit(char arg) {
		return arg == 'c' || arg == 'd' || arg == 's' || arg == 'h';
	}
}