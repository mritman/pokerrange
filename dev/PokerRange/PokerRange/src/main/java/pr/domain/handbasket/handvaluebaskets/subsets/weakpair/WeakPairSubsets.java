package pr.domain.handbasket.handvaluebaskets.subsets.weakpair;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.base.Predicate;

public enum WeakPairSubsets implements HandBasketSubsetKey {
	INSIDE_STRAIGHT_DRAW(0, "Inside Straight Draw", new InsideStraightDrawPredicate()),
	UNDER_PAIR(1, "Under Pair", new UnderPairPredicate()),
	BOTTOM_PAIR(2, "Bottom pair", new BottomPairPredicate()),
	FOURTH_AND_A_HALF_PAIR(3, "4,5th pair", new FourthAndAHalfPairPredicate()),
	FOURTH_PAIR(4, "4th pair", new FourthPairPredicate()),
	THIRD_AND_A_HALF_PAIR(5, "3,5th pair", new ThirdAndAHalfPairPredicate()),
	THIRD_PAIR(6, "3rd pair", new ThirdPairPredicate()),
	SECOND_AND_A_HALF_PAIR(7, "2,5th pair", new SecondAndAHalfPairPredicate());
	
	private int intValue;
	
	@Getter
	private Predicate<BasketElement> predicate;
	
	@Getter
	private String name;
	
	private WeakPairSubsets(int intValue, String name, Predicate<BasketElement> predicate) {
		this.intValue = intValue;
		this.name = name;
		this.predicate = predicate;
	}
	
	public int intValue() {
		return intValue;
	}
	
}
