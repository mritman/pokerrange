package pr.domain.poker.range;

import java.util.Collections;
import java.util.Set;

import pr.domain.gamestate.GameState;
import pr.domain.poker.HoleCards;

public interface HandRange {

	String getName();
	
	void calculateRange(GameState gameState);

	Set<HoleCards> getHandsInRange();

	public static final HandRange EMPTY_HAND_RANGE = new HandRange() {

		@Override
		public String getName() {
			return "EmptyHandRange";
		}

		@Override
		public void calculateRange(GameState gameState) {
			return;
		}

		@Override
		public Set<HoleCards> getHandsInRange() {
			return Collections.emptySet();
		}
		
	};

}
