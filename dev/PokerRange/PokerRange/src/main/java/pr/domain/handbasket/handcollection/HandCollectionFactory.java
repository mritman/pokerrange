package pr.domain.handbasket.handcollection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import pr.domain.poker.Combinations;
import pr.domain.poker.CombinationsComparator;

public final class HandCollectionFactory {
	
	private HandCollectionFactory() {};
	
	public static Set<HandCollection> createHandCollections(List<Combinations> combinations) {
		// This set needs to be constructed with a comparator because
		// BasketElement does not implement the Comparable interface.
		TreeSet<HandCollection> handCollections = new TreeSet<HandCollection>(new HandCollectionComparator());
		
		Collections.sort(combinations, CombinationsComparator.getInstance());
		
		if (combinations.size() > 0) {
			Combinations nextRank = combinations.get(0);
			do {
				nextRank = createHandCollection(nextRank, combinations, handCollections);
			} while (nextRank != null);
		}
		
		return handCollections;
	}
	
	private static Combinations createHandCollection(Combinations referenceHand, List<Combinations> basketList, Set<HandCollection> handCollections) {
		TreeSet<Combinations> subset = new TreeSet<Combinations>();
		
		Combinations currentHand = null;
		Iterator<Combinations> itr = basketList.iterator();
		
		while (itr.hasNext()) {
			currentHand = itr.next();
			if (CombinationsComparator.getInstance().compare(currentHand, referenceHand) != 0) {
				break;
			}
			
			subset.add(currentHand);
			itr.remove();
		}
		
		if (!subset.isEmpty()) {
			HandCollection suitedCollection = new HandCollection(subset);
			handCollections.add(suitedCollection);
		}
		
		if (CombinationsComparator.getInstance().compare(currentHand, referenceHand) != 0) {
			return currentHand;
		} else {
			return null;
		}
	}

}
