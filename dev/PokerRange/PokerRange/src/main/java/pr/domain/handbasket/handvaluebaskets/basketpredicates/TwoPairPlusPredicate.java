package pr.domain.handbasket.handvaluebaskets.basketpredicates;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.TwoPair;

import com.google.common.base.Predicate;

public class TwoPairPlusPredicate implements Predicate<Combinations> {
	
	private List<HandValue> handValues;
	
	public TwoPairPlusPredicate() {
		handValues = new LinkedList<>();
		
		Collections.addAll(handValues,
				HandValue.THREE_OF_A_KIND, 
				HandValue.STRAIGHT, 
				HandValue.FLUSH,
				HandValue.FULL_HOUSE, 
				HandValue.FOUR_OF_A_KIND,
				HandValue.STRAIGHT_FLUSH);
	}
	
	@Override
	public boolean apply(Combinations combinations) {
		HandValue value = combinations.getHighestHand().getValue();
		
		if (handValues.contains(value)) {
			return true;
		} else if (value == HandValue.TWO_PAIR) {
			TwoPair twoPair = (TwoPair) combinations.getHighestHand();
			
			if (twoPair.getType() == TwoPair.Type.TWO_PLAYER_PAIR) {
				return true;
			}
		}
		
		return false;
	}

}
