package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.CardSuitComparator;
import pr.domain.cards.Rank;
import pr.domain.cards.Suit;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class StraightFlush extends Hand {
	
	public enum Type {
		BOARD,
		PLAYER
	}
	
	@Getter	private List<Card> straightFlush;
	@Getter private Type type;
	@Getter private boolean royalFlush;

	public StraightFlush(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);

		calculateHand(holeCards, boardCards);
	}
	
	public Rank getRank() {
		return straightFlush.get(straightFlush.size() - 1).rank();
	}

	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		LinkedList<Card> newStraightFlush = new LinkedList<>();
		
		calculateStraightFlush(holeCards, cards, newStraightFlush);
		
		straightFlush = Collections.unmodifiableList(newStraightFlush);
		royalFlush = newStraightFlush.getLast().rank() == Rank.ACE;
		type = calculateType(newStraightFlush, holeCards);
		
		cards.removeAll(newStraightFlush);
		
		//Copy the list of value cards so that its order will not be changed by #init().
		List<Card> valueCards = new LinkedList<>(newStraightFlush);
		
		init(HandValue.STRAIGHT_FLUSH, valueCards, cards);
	}

	private static void calculateStraightFlush(HoleCards holeCards, List<Card> cards, List<Card> straightFlush) throws DoesNotMakeHandException {
		List<Card> noDuplicates = removeDuplicateRanks(holeCards, cards);
		
		if (noDuplicates.size() < 5) {
			throw new DoesNotMakeHandException(HandValue.STRAIGHT_FLUSH);
		}
		
		// Check if the card array contains a regular straight flush
		for (int i = noDuplicates.size() - 5; i >= 0; i--) {
			List<Card> subList = noDuplicates.subList(i, i + 5);
			
			try {
				List<Card> result = new LinkedList<>();
				
				Flush.containsFlush(subList, result);
				result.clear();
				
				Straight.calculateStraight(subList, result);
				if (result.isEmpty()) {
					continue;
				}
				
				straightFlush.addAll(subList);
				
				return;
			} catch (DoesNotMakeHandException e) {
				// Do nothing.
			}
		}
		
		// If a regular straight flush was not found and if the cards contain an ace, 
		// see if it makes an ace-to-five straight flush.
		if (noDuplicates.get(noDuplicates.size() - 1).rank() == Rank.ACE) {
			List<Card> subList = noDuplicates.subList(0, 4);
			subList.add(noDuplicates.get(noDuplicates.size() - 1));
			
			try {
				List<Card> result = new LinkedList<>();
				
				Flush.containsFlush(subList, result);
				if (result.isEmpty()) {
					throw new DoesNotMakeHandException(HandValue.STRAIGHT_FLUSH);
				}
				result.clear();
				
				Straight.calculateStraight(subList, result);
				if (result.isEmpty()) {
					throw new DoesNotMakeHandException(HandValue.STRAIGHT_FLUSH);
				}
				
				straightFlush.addAll(subList);
				
				return;
			} catch (DoesNotMakeHandException e) {
				// Do nothing.
			}
		}
		
		throw new DoesNotMakeHandException(HandValue.STRAIGHT_FLUSH);
	}
	
	/**
	 * <p>
	 * Creates a new list of cards from the specified list of cards with no
	 * duplicate ranks. The specified list of cards is sorted by rank.
	 * </p>
	 * 
	 * <p>
	 * The list of cards is traversed by rank in ascending order. When multiple
	 * cards with the same rank are encountered, board cards get precedence over
	 * hole cards and card of the most frequent suit get precedence over any
	 * other cards of the same rank. In other cases the last encountered card of
	 * a rank is added to the new list.
	 * </p>
	 * 
	 * @param holeCards
	 * @param cards
	 * 
	 * @return a new list of cards containing no duplicate ranks
	 */
	private static List<Card> removeDuplicateRanks(HoleCards holeCards, List<Card> cards) {
		LinkedList<Card> newList = new LinkedList<Card>();

		if (cards == null || cards.isEmpty()) {
			return newList;
		}

		Suit mostFrequentSuit = getMostFrequentSuit(cards);
		Collections.sort(cards, CardComparator.getInstance());

		for (Card card : cards) {
			if (newList.isEmpty()) {
				newList.add(card);
				continue;
			}

			if (card.rank() != newList.getLast().rank()) {
				newList.add(card);
			} else if (card.suit() == mostFrequentSuit) {
				newList.removeLast();
				newList.add(card);				
			} else if (newList.getLast().suit() != mostFrequentSuit
					&& holeCards != null
					&& (!holeCards.contains(card) || holeCards.contains(newList.getLast()))) {
				newList.removeLast();
				newList.add(card);
			}
		}

		return newList;
	}
	
	/**
	 * NOTE: the specified list of cards will be sorted by suit.
	 * 
	 * @param cards
	 *            the list of cards to check for the most frequently occurring
	 *            suit
	 * 
	 * @return the most frequently occurring suit in the list of cards
	 */
	private static Suit getMostFrequentSuit(List<Card> cards) {
		Card lastCard = null;
		Suit mostFrequentSuit = null;
		int currentCount = 1;
		int maxCount = 1;

		Collections.sort(cards, CardSuitComparator.getInstance());

		for (Card card : cards) {
			if (lastCard == null) {
				mostFrequentSuit = card.suit();
				lastCard = card;
				continue;
			}

			if (card.suit() == lastCard.suit()) {
				currentCount++;
			} else {
				if (currentCount >= maxCount) {
					maxCount = currentCount;
					mostFrequentSuit = lastCard.suit();
				}

				currentCount = 1;
			}
		}

		return mostFrequentSuit;
	}
	
	private static Type calculateType(List<Card> straightFlush, HoleCards holeCards) {
		if (holeCards == null || !straightFlush.contains(holeCards.card1()) && !straightFlush.contains(holeCards.card2())) {
			return Type.BOARD;
		}
		
		return Type.PLAYER;
	}

}
