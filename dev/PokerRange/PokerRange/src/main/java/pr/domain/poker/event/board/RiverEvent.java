package pr.domain.poker.event.board;

import lombok.Getter;
import pr.domain.cards.Card;

import com.google.common.base.Objects;

public class RiverEvent implements BoardCardEvent {

	@Getter private final Card river;
	
	public RiverEvent(Card river) {
		this.river = river;
	}
	
	public RiverEvent(String river) {
		this.river = Card.fromString(river);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("river", river).toString();
	}
	
}
