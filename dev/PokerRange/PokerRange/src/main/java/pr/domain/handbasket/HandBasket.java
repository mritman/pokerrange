package pr.domain.handbasket;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import lombok.Setter;
import pr.domain.handbasket.handcollection.BasketElementComparator;
import pr.domain.poker.Combinations;

public class HandBasket {

	/**
	 * This TreeSet is required to use an instance of
	 * {@link BasketElementComparator} to sort the elements. In other words,
	 * {@link TreeSet#comparator()} must not return null.
	 * 
	 * <p>
	 * Functionality requires that a full ordering of any type of BasketElement
	 * can be made. Because it is bad design to make a superclass aware of its
	 * subclasses this logic is externalized in a comparator (instead of
	 * implementing the Comparable interface).
	 * </p>
	 */
	private TreeSet<BasketElement> basketElements = new TreeSet<BasketElement>(new BasketElementComparator());
	
	/**
	 * The total hands in all baskets. Used to calculate the percentage of all
	 * hands that are in this basket.
	 */
	@Setter
	private int totalSize;
	
	@Getter
	private String name;
	
	public HandBasket() {
		this("", new TreeSet<BasketElement>());
	}
	
	public HandBasket(String name, Set<BasketElement> basketElements) {
		this.basketElements.addAll(basketElements);
		
		for (BasketElement basketElement : this.basketElements) {
			basketElement.setOriginalHandBasket(this);
			basketElement.setCurrentHandBasket(this);
		}
		
		this.name = name;
	}
	
	/**
	 * It is important that {@link HandBasket#add(BasketElement)} and
	 * {@link HandBasket#remove(BasketElement)} are used to modify this
	 * HandBasket's contents so this method returns an unmodifiable set.
	 * 
	 * The set is also a clone so that {@link HandBasket#add(BasketElement)} and
	 * {@link HandBasket#remove(BasketElement)} can be used while iterating over
	 * it.
	 * 
	 * @return an unmodifiable clone of this instance's set of BasketElements
	 */
	public Set<BasketElement> getBasketElements() {
		@SuppressWarnings("unchecked")
		Set<BasketElement> clone = (Set<BasketElement>) basketElements.clone();
		return Collections.unmodifiableSet(clone);
	}
	
	public boolean remove(BasketElement basketElement) {
		basketElement.setCurrentHandBasket(null);
		return basketElements.remove(basketElement);
	}
	
	public boolean add(BasketElement basketElement) {
		basketElement.setCurrentHandBasket(this);
		return basketElements.add(basketElement);
	}
	
	public int getHandsCount() {
		int size = 0;
		
		for (BasketElement basketElement : basketElements) {
			size += basketElement.getHandCount();
		}
		
		return size;
	}
	
	public List<Combinations> getHands() {
		LinkedList<Combinations> hands = new LinkedList<>();
		
		for (BasketElement basketElement : basketElements) {
			hands.addAll(basketElement.getCombinations());
		}
		
		return hands;
	}
	
	public double getPercentage() {
		if (totalSize != 0) { 
			return getHandsCount() / (double) totalSize;
		} else {
			return 0;
		}
	}
	
	/**
	 * Returns a subset of BasketElements in this hand basket ranging
	 * between the specified {@code from} and {@code to} arguments. It does not
	 * matter if {@code from} is higher than {@code to} in the order of the
	 * BasketElements. The subset includes the specified
	 * {@code from} and {@code to} {@link BasketElement}s.
	 * 
	 * @param from indicates the beginning of the range
	 * @param to indicates the end of the range
	 * @return the subset ranging between {@code from} and {@code to} (inclusive)
	 * 
	 * @see TreeSet#subSet(Object, boolean, Object, boolean)
	 */
	public Set<BasketElement> getSubset(BasketElement from, BasketElement to) {
		BasketElement fromElement, toElement;
		int cmp;
		
		// Check which element is higher. TreeSet.subSet() will throw an
		// exception if fromElement is higher than toElement.
		cmp = basketElements.comparator().compare(from, to);
		
		if (cmp < 0) {
			fromElement = from;
			toElement = to;
		} else {
			fromElement = to;
			toElement = from;
		}
		
		return basketElements.subSet(fromElement, true, toElement, true);
	}
	
	public Map<HandBasketSubsetKey, HandBasketSubset> getSubsets() {
		return Collections.emptyMap();
	}
	
	@Override
	public String toString() {
		return name;
	}

}
