package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import pr.domain.cards.Card;

public class KickerComparator implements Comparator<List<Card>> {

	private static final KickerComparator instance = new KickerComparator();
	
	public static KickerComparator getInstance() {
		return instance;
	}
	
	@Override
	public int compare(List<Card> hand1Kickers, List<Card> hand2Kickers) {
		return compareKickers(hand1Kickers, hand2Kickers);
	}
	
	public static int compareKickers(List<Card> hand1Kickers, List<Card> hand2Kickers) {
		if (hand1Kickers.size() != hand2Kickers.size())
			throw new IllegalArgumentException(
					"Can not compare two equal Hands that do not have the same number of kickers.");
		
		ListIterator<Card> itr1 = hand1Kickers.listIterator(hand1Kickers.size());
		ListIterator<Card> itr2 = hand2Kickers.listIterator(hand2Kickers.size());
		
		for (int i = 0; i < hand1Kickers.size(); i ++) {
			int cmp = Integer.compare(itr1.previous().rank().intValue(), itr2.previous().rank().intValue());
			
			if (cmp != 0) return cmp;
		}
		
		// The ranks of the kickers are the same.
		return 0;
	}

	/**
	 * @param hand
	 * @return
	 * 
	 * @see Effective Java 2nd Edition, item 9, page 48.
	 */
	public int hashCode(List<Card> kickers) {
		int hashCode = 17;
		
		for (Card card : kickers) {
			hashCode = 31 + hashCode + card.rank().hashCode();
		}	
		
		return hashCode;
	}

}
