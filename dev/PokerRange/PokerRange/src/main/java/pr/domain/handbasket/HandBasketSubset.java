package pr.domain.handbasket;

import java.util.Set;

import lombok.Getter;

public class HandBasketSubset {
	
	@Getter
	private Set<BasketElement> basketElements;
	
	public HandBasketSubset(Set<BasketElement> basketElements) {
		this.basketElements = basketElements;
	}

	public int size() {
		return basketElements.size();
	}
	
	public int numberOfHands() {
		int result = 0;
		
		for (BasketElement basketElement : basketElements) {
			result += basketElement.getHandCount();
		}
		
		return result;
	}

}
