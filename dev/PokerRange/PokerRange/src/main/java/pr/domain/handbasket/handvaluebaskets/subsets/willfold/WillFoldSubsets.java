package pr.domain.handbasket.handvaluebaskets.subsets.willfold;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.base.Predicate;

public enum WillFoldSubsets implements HandBasketSubsetKey {
	HIGH_CARD(0, "High card", new HighCardPredicate()),
	OTHER(1, "Other", new OtherPredicate());
	
	private int intValue;
	
	@Getter
	private Predicate<BasketElement> predicate;

	@Getter
	private String name;
	
	private WillFoldSubsets(int intValue, String name, Predicate<BasketElement> predicate) {
		this.intValue = intValue;
		this.name = name;
		this.predicate = predicate;
	}
	
	public int intValue() {
		return intValue;
	}
}
