package pr.domain.poker.event.seat;

import pr.domain.poker.table.Seat;

public abstract class SeatPropertyEvent implements SeatEvent {

	public abstract Seat getSeat();
	
	public abstract SeatPropertyEvent setSeat(Seat seat);
	
}
