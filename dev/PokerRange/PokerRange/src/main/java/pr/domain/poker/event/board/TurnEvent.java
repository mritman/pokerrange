package pr.domain.poker.event.board;

import lombok.Getter;
import pr.domain.cards.Card;

import com.google.common.base.Objects;

public class TurnEvent implements BoardCardEvent {

	@Getter private final Card turn;

	public TurnEvent(Card turn) {
		this.turn = turn;
	}
	
	public TurnEvent(String turn) {
		this.turn = Card.fromString(turn);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("turn", turn).toString();
	}

}
