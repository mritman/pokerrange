package pr.domain.poker.oldevent;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import pr.domain.poker.table.PokerTable;

import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;
import com.google.common.base.Preconditions;

public class PokerTableUpdate {

	@Getter
	private final PokerTable table;
	
	private final List<PokerTableEvent> events;
	
	public PokerTableUpdate(PokerTable table, List<PokerTableEvent> events) {
		Preconditions.checkNotNull("table", table);
		Preconditions.checkNotNull("events", events);
		Preconditions.checkArgument(!events.isEmpty(), "Argument 'events' may not be null or empty.");
		
		this.table = table;
		this.events = events;
	}
	
	public List<PokerTableEvent> getEvents() {
		return Collections.unmodifiableList(events);
	}
	
	@Override
	public String toString() {
		ToStringHelper stringHelper = Objects.toStringHelper(getClass());
		int i = 0;
		
		stringHelper.add("tablename", table.getName());
		for (PokerTableEvent tableEvent : events) {
			stringHelper.add("Event " + ++i, tableEvent);
		}
		
		return stringHelper.toString();
	}
	
}
