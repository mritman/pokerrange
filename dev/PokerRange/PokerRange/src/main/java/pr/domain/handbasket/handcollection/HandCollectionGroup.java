package pr.domain.handbasket.handcollection;

import java.util.List;
import java.util.TreeSet;

import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasket;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

public class HandCollectionGroup extends BasketElement implements Comparable<HandCollectionGroup> {

	private List<HandCollection> handCollections;
	
	public HandCollectionGroup(List<HandCollection> handCollections) {
		this.handCollections = handCollections;
	}
	
	@Override
	public void setOriginalHandBasket(HandBasket originalHandBasket) {
		super.setOriginalHandBasket(originalHandBasket);
		
		for (HandCollection handCollection : handCollections) {
			handCollection.setOriginalHandBasket(originalHandBasket);
		}
	}
	
	@Override
	public void setCurrentHandBasket(HandBasket currentHandBasket) {
		super.setCurrentHandBasket(currentHandBasket);
		
		for (HandCollection handCollection : handCollections) {
			handCollection.setCurrentHandBasket(currentHandBasket);
		}
	}
	
	@Override
	public int getHandCount() {
		int handCount = 0;
		
		for (HandCollection handCollection : handCollections) {
			handCount += handCollection.getHandCount();
		}
		
		return handCount;
	}

	@Override
	public TreeSet<Combinations> getCombinations() {
		TreeSet<Combinations> hands = new TreeSet<>();
		
		for (HandCollection handCollection : handCollections) {
			hands.addAll(handCollection.getCombinations());
		}
		
		return hands;
	}

	@Override
	public boolean containsHandFoldedByUser() {
		for (HandCollection handCollection : handCollections) {
			if (handCollection.containsHandFoldedByUser()) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Combinations getHighestHoleCards() {
		return handCollections.get(0).getHighestHoleCards();
	}
	
	public HandValue getHandValue() {
		return handCollections.get(0).getHandValue();
	}

	@Override
	public String getName() {
		return handCollections.get(0).getName();
	}
	
	@Override
	public void fold() {
		for (HandCollection handCollection : handCollections) {
			handCollection.fold();
		}
	}

	@Override
	public void unfold() {
		for (HandCollection handCollection : handCollections) {
			handCollection.unfold();
		}
	}

	@Override
	public int compareTo(HandCollectionGroup other) {
		return HandCollectionGroupComparator.getInstance().compare(this, other);
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		
		if (!(other instanceof HandCollectionGroup)) return false;
		
		HandCollectionGroup otherGroup = (HandCollectionGroup) other;
		
		return compareTo(otherGroup) == 0;
	}
	
	@Override
	public int hashCode() {
		return HandCollectionGroupComparator.hashCode(this);
	}

}
