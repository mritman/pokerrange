package pr.domain.poker.action;

import pr.domain.poker.Street;

public final class ActionFactory {
	
	private ActionFactory() {};

	public static Action createInstance(ActionType type, Street street) {
		return new Action(type, street);
	}
}
