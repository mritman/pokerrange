package pr.domain.poker.hand;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Lists;

public class Pair extends Hand {

	public enum RelativeRank {
		UNDER_PAIR(0),
		BOTTOM_PAIR(1),
		FOURTH_AND_A_HALF_PAIR(2),
		FOURTH_PAIR(3),
		THIRD_AND_A_HALF_PAIR(4),
		THIRD_PAIR(5),
		SECOND_AND_A_HALF_PAIR(6),
		SECOND_PAIR(7),
		FIRST_AND_A_HALF_PAIR(8),
		TOP_PAIR(9),
		OVER_PAIR(10),
		PRE_FLOP_POCKET_PAIR(11);
		
		private int intValue;
		
		private RelativeRank(int value) {
			this.intValue = value;
		}
		
		public int intValue() {
			return intValue;
		}
	}
	
	public enum Type {
		BOARD_PAIR,
		COMBINED_PAIR,
		POCKET_PAIR;
	}

	@Getter
	private List<Card> pair;

	@Getter
	private RelativeRank relativeRank;
	
	@Getter
	private Type type;

	protected Pair(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHand(holeCards, boardCards);
	}

	public Rank getRank() {
		return pair.get(0).rank();
	}

	public boolean isBoardPair() {
		return type == Type.BOARD_PAIR ? true : false;
	}
	
	public boolean isCombinedPair() {
		return type == Type.COMBINED_PAIR ? true : false;
	}
	
	public boolean isPocketPair() {
		return type == Type.POCKET_PAIR ? true : false;
	}

	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newPair = new LinkedList<>();
		
		Card lastCard = null;
		
		for (Card card : cards) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}
			
			if (card.rank() == lastCard.rank()) {
				if (!newPair.isEmpty()) {
					throw new DoesNotMakeHandException(HandValue.PAIR);
				}
				
				newPair.add(lastCard);
				newPair.add(card);
			}
			
			lastCard = card;
		}
		
		if (newPair.isEmpty()) {
			throw new DoesNotMakeHandException(HandValue.PAIR);
		}

		pair = Collections.unmodifiableList(newPair);
		relativeRank = calculateRelativeRank(newPair, boardCards);
		type = calculatePairType(newPair, holeCards, boardCards);
		
		cards.removeAll(newPair);
		
		init(HandValue.PAIR, newPair, cards);
	}
	
	/**
	 * This method assumes that the specified pair makes a pair with the
	 * specified board cards (possibly as hole cards, not necessarily using one
	 * of the board cards), if it does not the return value is unreliable.
	 * 
	 * @param pair
	 * @param holeCards
	 * @param boardCards
	 * 
	 * @return the relative rank of the specified pair
	 */
	protected static RelativeRank calculateRelativeRank(List<Card> pair, BoardCards boardCards) {
		if (pair == null || pair.isEmpty() || boardCards == null) {
			throw new IllegalArgumentException();
		}
		
		if (boardCards.getStreet() == Street.PREFLOP) return RelativeRank.PRE_FLOP_POCKET_PAIR;
		Street street = boardCards.getStreet();
		
		Rank rank = pair.get(0).rank();

		List<Card> cards = boardCards.asList();
		List<Card> noDuplicates = CardUtil.removeDuplicateRanks(null, cards);
		Collections.reverse(noDuplicates);
		
		List<RelativeRank> relativeRanks = Lists.newArrayList(RelativeRank.values());
		relativeRanks.remove(RelativeRank.PRE_FLOP_POCKET_PAIR);
		Collections.reverse(relativeRanks);
		Iterator<RelativeRank> iterator = relativeRanks.iterator();
		
		
		if (isOverPair(rank, noDuplicates)) {
			return RelativeRank.OVER_PAIR;
		}
		
		RelativeRank relativeRank = findRelativeRank(rank, noDuplicates, iterator);
		
		if (relativeRank == null) {
			relativeRank = RelativeRank.UNDER_PAIR;
		} else if (isBottomPair(street, rank, cards, noDuplicates)) {
			relativeRank = RelativeRank.BOTTOM_PAIR;
		}
		
		return relativeRank;
	}

	private static RelativeRank findRelativeRank(Rank rank,	List<Card> noDuplicates, Iterator<RelativeRank> iterator) {
		RelativeRank relativeRank = null;
		Card lastCard = null;
		
		for (Card card : noDuplicates) {
			if (!iterator.hasNext()) {
				break;
			}

			if (lastCard == null) {
				iterator.next();
			}
			
			if (lastCard != null && isRankedBetween(rank, card.rank(), lastCard.rank())) {
				relativeRank = iterator.next();
				break;
			} else if (lastCard != null) {
				iterator.next();
			}
			
			if (card.rank() == rank) {
				relativeRank = iterator.next();
				break;
			}
			
			iterator.next();
			lastCard = card;
		}
		
		return relativeRank;
	}

	/**
	 * @param rank
	 * @param boardCards the board cards sorted by rank in descending order.
	 * @return true if the specified rank is higher than the highest rank on the board, false otherwise
	 */
	private static boolean isOverPair(Rank rank, List<Card> boardCards) {
//		if (boardCards.isEmpty()) {
//			return false;
//		}
		
		return rank.intValue() > boardCards.iterator().next().rank().intValue();
	}

	/**
	 * @param cards the board cards
	 * @param the board cards with duplicate ranks removed
	 * @param noDuplicates the board cards sorted by rank in descending order.
	 * @return true if the specified rank is equal to the lowest rank on the board, false otherwise
	 */
	private static boolean isBottomPair(Street street, Rank rank, List<Card> cards, List<Card> noDuplicates) {
		return rank == cards.get(0).rank() && street.getSize() == noDuplicates.size();
	}
	
	private static boolean isRankedBetween(final Rank r, final Rank a, final Rank b) {
		Rank lowerRank;
		Rank higherRank;
		
		if (a.intValue() <= b.intValue()) {
			lowerRank = a;
			higherRank = b;
		} else {
			lowerRank = b;
			higherRank = a;
		}
		
		return r.intValue() > lowerRank.intValue() && r.intValue() < higherRank.intValue();
	}
	
	/**
	 * This method assumes that the specified hole cards and board cards make a
	 * pair, if they do not the return value is unreliable.
	 * 
	 * @param pair
	 * @param holeCards
	 * @param boardCards
	 * @return
	 */
	protected static Type calculatePairType(List<Card> pair, HoleCards holeCards, BoardCards boardCards) {
		List<Card> holeCardList;
		List<Card> boardCardList;
		Card card1 = pair.get(0);
		Card card2 = pair.get(1);
		
		if (holeCards == null) {
			return Type.BOARD_PAIR;
		} else if (boardCards == null) {
			return Type.POCKET_PAIR;
		}
		
		holeCardList = holeCards.asList();
		boardCardList = boardCards.asList();
		
		if (holeCardList.contains(card1) && holeCardList.contains(card2)) {
			return Type.POCKET_PAIR;
		}
		
		if (boardCardList.contains(card1) && boardCardList.contains(card2)) {
			return Type.BOARD_PAIR;
		}
		
		return Type.COMBINED_PAIR;
	}
}
