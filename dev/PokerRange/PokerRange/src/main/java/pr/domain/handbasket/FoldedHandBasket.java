package pr.domain.handbasket;

import java.util.Set;

public class FoldedHandBasket extends HandBasket {

	public FoldedHandBasket(String name, Set<BasketElement> basketElements) {
		super(name, basketElements);
	}
	
	@Override
	public boolean add(BasketElement basketElement) {
		basketElement.fold();
		return super.add(basketElement);
	}
	
	@Override
	public boolean remove(BasketElement basketElement) {
		basketElement.unfold();
		return super.remove(basketElement);
	}

}
