package pr.domain.poker.event.action;

import lombok.Getter;

import com.google.common.base.Objects;

public class FoldEvent implements ActionEvent {

	@Getter
	private final String name;

	public FoldEvent(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("name", name).toString();
	}

}
