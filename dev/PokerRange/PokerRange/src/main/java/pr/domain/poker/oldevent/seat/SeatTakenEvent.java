package pr.domain.poker.oldevent.seat;

import pr.domain.poker.table.PokerTable;

public abstract class SeatTakenEvent extends SeatEvent {

	protected SeatTakenEvent(PokerTable table) {
		super(table);
	}

}
