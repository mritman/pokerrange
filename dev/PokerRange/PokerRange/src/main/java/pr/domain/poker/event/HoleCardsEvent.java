package pr.domain.poker.event;

import pr.domain.poker.HoleCards;
import pr.domain.poker.table.Seat;

import com.google.common.base.Objects;

public class HoleCardsEvent implements PokerEvent {
	
	private final Seat seat;
	private final HoleCards holeCards;
	
	public HoleCardsEvent(Seat seat, HoleCards holeCards) {
		super();
		this.seat = seat;
		this.holeCards = holeCards;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("seat", seat).add("holeCards", holeCards).toString();
	}
}
