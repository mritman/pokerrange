package pr.domain.poker.util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.Combinations;
import pr.domain.poker.HoleCards;

public final class CardUtil {
	
	private CardUtil() {};

	/**
	 * Creates a list containing the specified hole cards and board cards. Cards
	 * are added to the list in the order of hole cards, flop, turn, river. If a
	 * boardcard on any of the streets is null then any cards from that street
	 * and any subsequent streets are not included in the list. The resulting
	 * list is sorted using RankComparator.
	 * 
	 * @param holeCards
	 *            a set of hole cards
	 * 
	 * @param boardCards
	 *            the cards on the board
	 * 
	 * @return the resulting list of cards, can be empty.
	 * 
	 */
	public static List<Card> asList(HoleCards holeCards, BoardCards boardCards) {
		return asList(holeCards, boardCards, true);
	}

	/**
	 * @see #asList(Combinations, BoardCards)
	 * 
	 * @param holeCards
	 *            a set of hole cards
	 * 
	 * @param boardCards
	 *            the cards on the board
	 * 
	 * @param sort
	 *            if true the returned list will be sorted using
	 *            {@link CardComparator}
	 * 
	 * @return the resulting list of cards, can be empty.
	 */
	public static List<Card> asList(HoleCards holeCards, BoardCards boardCards,
			boolean sort) {
		LinkedList<Card> cards = new LinkedList<Card>();

		if (holeCards != null) {
			cards.add(holeCards.card1());
			cards.add(holeCards.card2());
		}

		if (boardCards.getFlop() != null) {
			cards.addAll(boardCards.getFlop().getCards());

			if (boardCards.getTurn() != null) {
				cards.add(boardCards.getTurn());

				if (boardCards.getRiver() != null) {
					cards.add(boardCards.getRiver());
				}
			}
		}

		if (sort) {
			Collections.sort(cards, CardComparator.getInstance());
		}

		return cards;
	}

	/**
	 * <p>
	 * Creates a new list of cards from the specified list of cards with no
	 * duplicate ranks. The specified list of cards is sorted by rank.
	 * </p>
	 * 
	 * <p>
	 * The list of cards is traversed by rank in ascending order. When multiple
	 * cards with the same rank are encountered, hole cards get precedence over
	 * board cards and over earlier encountered hole cards, in other cases the
	 * last encountered card of a rank is added to the new list.
	 * </p>
	 * 
	 * @param holeCards
	 * @param cards
	 * 
	 * @return a new list of cards containing no duplicate ranks
	 */
	public static List<Card> removeDuplicateRanks(HoleCards holeCards, List<Card> cards) {
		LinkedList<Card> newList = new LinkedList<Card>();

		if (cards == null || cards.isEmpty()) {
			return newList;
		}

		Collections.sort(cards, CardComparator.getInstance());

		for (Card card : cards) {
			if (newList.isEmpty()) {
				newList.add(card);
				continue;
			}

			if (card.rank() != newList.getLast().rank()) {
				newList.add(card);
			} else if (holeCards != null
					&& (holeCards.contains(card) || !holeCards.contains(newList.getLast()))) {
				newList.removeLast();
				newList.add(card);
			}
		}

		return newList;
	}
	
	public static HoleCards createHoleCards(String string) {
		String[] split = string.split(PokerCardStringFormat.HOLE_BOARD_SEPARATOR);

		String holeCardString = split[0];

		return HoleCards.fromString(holeCardString);
	}

	public static BoardCards createBoardCards(String string) {
		String[] split = string.split(PokerCardStringFormat.HOLE_BOARD_SEPARATOR);

		String boardCardString = split.length == 2 ? split[1] : "";

		return BoardCards.fromString(boardCardString);
	}
	
	public static List<Card> createCards(String string) {
		List<Card> cards = new LinkedList<>();
		if ("".equals(string)) return cards; 
		
		String[] split = string.split(PokerCardStringFormat.CARD_SEPARATOR);
		
		for (String cardString : split) {
			Card card = Card.fromString(cardString);
			cards.add(card);
		}
		
		return cards;
	}

	/**
	 * @param cards a list of cards assumed to be sorted by rank
	 * 
	 * @return the highest ace of the list of cards contains one, null otherwise
	 */
	public static Card getAce(List<Card> cards) {
		ListIterator<Card> iterator = cards.listIterator(cards.size());
		
		while (iterator.hasPrevious()) {
			Card card = iterator.previous();
			if (card.rank() == Rank.ACE) {
				return card;
			}
		}
		
		return null;
	}
}
