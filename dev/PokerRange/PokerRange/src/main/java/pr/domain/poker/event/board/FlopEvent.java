package pr.domain.poker.event.board;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.Flop;

public class FlopEvent implements BoardCardEvent {

	@Getter private final Flop flop;
	
	public FlopEvent(Flop flop) {
		this.flop = flop;
	}
	
	public FlopEvent(String flop) {
		this.flop = Flop.fromString(flop);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("flop", flop).toString();
	}

}
