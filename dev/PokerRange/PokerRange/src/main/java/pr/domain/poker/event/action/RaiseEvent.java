package pr.domain.poker.event.action;

import lombok.Getter;

import org.joda.money.Money;

import com.google.common.base.Objects;

public class RaiseEvent implements ActionEvent {
	
	@Getter private final String name;
	@Getter private final Money from;
	@Getter private final Money to;

	public RaiseEvent(String name, Money from, Money to) {
		this.name = name;
		this.from = from;
		this.to = to;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("name", name)
				.add("from", from)
				.add("to", to)
				.toString();
	}
}
