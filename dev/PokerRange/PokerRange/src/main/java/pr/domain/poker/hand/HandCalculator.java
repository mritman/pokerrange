package pr.domain.poker.hand;

import java.util.LinkedList;
import java.util.List;

import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;

public final class HandCalculator {
	
	private HandCalculator() {};
	
	private interface HandCreator {
		boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands);
	}
	
	public static List<Hand> calculateHands(HoleCards holeCards, BoardCards boardCards) {
		List<Hand> hands = new LinkedList<Hand>();
		
		for (HandCreator handCreator : handCreators) {
			if (handCreator.isHand(holeCards, boardCards, hands)) {
				break;
			}
		}
		
		straightDrawCreator.isHand(holeCards, boardCards, hands);
		flushDrawCreator.isHand(holeCards, boardCards, hands);
		
		return hands;
	}
	
	private static final HandCreator highCardCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				HighCard hand = new HighCard(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}

	};
	
	private static final HandCreator straightFlushCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				StraightFlush hand = new StraightFlush(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator fourOfAKindCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				FourOfAKind hand = new FourOfAKind(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator fullHouseCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				FullHouse hand = new FullHouse(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator flushCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				Flush hand = new Flush(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator straightCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				Straight hand = new Straight(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator threeOfAKindCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				ThreeOfAKind hand = new ThreeOfAKind(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator twoPairCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				TwoPair hand = new TwoPair(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator pairCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				Pair hand = new Pair(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator straightDrawCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				StraightDraw hand = new StraightDraw(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator flushDrawCreator = new HandCreator() {
		
		@Override
		public boolean isHand(HoleCards holeCards, BoardCards boardCards, List<Hand> hands) {
			try {
				FlushDraw hand = new FlushDraw(holeCards, boardCards);
				hands.add(hand);
				return true;
			} catch (DoesNotMakeHandException e) {
				return false;
			}
		}
	};
	
	private static final HandCreator[] handCreators = {
			highCardCreator,
			straightFlushCreator,
			fourOfAKindCreator,
			fullHouseCreator,
			flushCreator,
			straightCreator,
			threeOfAKindCreator,
			twoPairCreator,
			pairCreator,
	};
	
}
