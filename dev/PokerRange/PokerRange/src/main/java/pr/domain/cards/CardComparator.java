package pr.domain.cards;

import java.util.Comparator;

import com.google.common.collect.ComparisonChain;

/**
 * Sorts cards by rank then by suit.
 */
public class CardComparator implements Comparator<Card> {
	
	private static final CardComparator cardComparator = new CardComparator();
	
	public static CardComparator getInstance() {
		return cardComparator;
	}
	
	@Override
	public int compare(Card card1, Card card2) {
		if (card1 == card2) {
			return 0;
		} else if (card1 == null) {
			return -1;
		} else if (card2 == null) {
			return 1;
		}
		
		return ComparisonChain
				.start()
				.compare(card1.rank(), card2.rank(), RankComparator.getInstance())
				.compare(card1.suit(), card2.suit(), SuitComparator.getInstance())
				.result();
	}

}
