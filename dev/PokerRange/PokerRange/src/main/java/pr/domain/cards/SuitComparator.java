package pr.domain.cards;

import java.util.Comparator;

public class SuitComparator implements Comparator<Suit> {
	
	private static final SuitComparator suitComparator = new SuitComparator();
	
	public static SuitComparator getInstance() {
		return suitComparator;
	}

	@Override
	public int compare(Suit suit1, Suit suit2) {
		return Integer.compare(suit1.intValue(), suit2.intValue());
	}

}
