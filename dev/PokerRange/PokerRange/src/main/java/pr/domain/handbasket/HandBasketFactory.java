package pr.domain.handbasket;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import pr.domain.handbasket.handcollection.BasketElementComparator;
import pr.domain.handbasket.handcollection.HandCollection;
import pr.domain.handbasket.handcollection.HandCollectionFactory;
import pr.domain.handbasket.handcollection.HandCollectionGroupFactory;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public abstract class HandBasketFactory {

	private HandBasketKey[] basketKeys;
	private HandBasketKey foldedBasketKey;
	
	public HandBasketFactory(HandBasketKey[] basketKeys, HandBasketKey foldedBasketKey) {
		this.basketKeys = basketKeys.clone();
		this.foldedBasketKey = foldedBasketKey;
	}
	
	public void setFoldedHoleCards(Set<Combinations> foldedHoleCards) {
		FoldedPredicate foldedPredicate = (FoldedPredicate) foldedBasketKey.getPredicate();
		foldedPredicate.setFoldedHoleCards(foldedHoleCards);
	}
	
	public Map<HandBasketKey, HandBasket> createHandBaskets(Set<Combinations> combinations) {
		Map<HandBasketKey, HandBasket> map = new HashMap<>();

		Set<BasketElement> foldedBasketElements = createBasketElements(combinations, foldedBasketKey.getPredicate());
		HandBasket foldedHandBasket = new FoldedHandBasket(foldedBasketKey.toString(), foldedBasketElements);
		map.put(foldedBasketKey, foldedHandBasket);
		
		Set<Combinations> nonFoldedHoleCardsList = Sets.newTreeSet(combinations);
		nonFoldedHoleCardsList.removeAll(foldedHandBasket.getHands());
		
		int totalSize = nonFoldedHoleCardsList.size();
		foldedHandBasket.setTotalSize(totalSize);
		
		for (HandBasketKey basketKey : basketKeys) {
			if (basketKey == foldedBasketKey) continue;
			
			Set<BasketElement> basketElements = createBasketElements(nonFoldedHoleCardsList, basketKey.getPredicate());
			HandBasket handBasket = createHandBasket(basketKey.toString(), basketElements);
			handBasket.setTotalSize(totalSize);
			map.put(basketKey, handBasket);
		}

		return map;
	}
	
	public HandBasketKey[] getBasketKeys() {
		return Arrays.copyOf(basketKeys, basketKeys.length);
	}
	
	protected HandBasket createHandBasket(String name, Set<BasketElement> basketElements) {
		return new HandBasket(name, basketElements);
	}
	
	protected Set<BasketElement> createBasketElements(Set<Combinations> combinations, Predicate<Combinations> predicate) {
		List<Combinations> filteredHoleCards = Lists.newArrayList(Collections2.filter(combinations, predicate));
		return createBasketElements(filteredHoleCards);
	}
	
	protected HandBasketKey getFoldedBasketKey() {
		return foldedBasketKey;
	}
	
	private Set<BasketElement> createBasketElements(List<Combinations> combinations) {
		Set<BasketElement> basketElements = new TreeSet<>(new BasketElementComparator());
		Set<HandCollection> handCollections = HandCollectionFactory.createHandCollections(combinations);
		
		basketElements.addAll(handCollections);
		groupBasketElements(basketElements);
		
		return basketElements;
	}
	
	private static void groupBasketElements(Set<BasketElement> basketElements) {
		HandCollectionGroupFactory.groupBasketElements(basketElements, HandValue.FLUSH);
		HandCollectionGroupFactory.groupBasketElements(basketElements, HandValue.FLUSH_DRAW);
	}

}
