package pr.domain.handbasket;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import pr.domain.poker.Combinations;

import com.google.common.base.Predicate;

public class FoldedPredicate implements Predicate<Combinations> {

	private Collection<Combinations> foldedHoleCards;
	
	public FoldedPredicate() {
		foldedHoleCards = new LinkedList<>();
	}
	
	public FoldedPredicate(Collection<Combinations> foldedHoleCards) {
		this.foldedHoleCards = foldedHoleCards;
	}
	
	@Override
	public boolean apply(Combinations combinations) {
		if (foldedHoleCards.contains(combinations)) {
			return true;
		}
		
		return false;
	}

	public void setFoldedHoleCards(Set<Combinations> foldedHoleCards) {
		this.foldedHoleCards = foldedHoleCards;
	}

}
