package pr.domain.cards;

import java.util.Comparator;

public enum Rank implements Comparable<Rank>, Comparator<Rank> {
	TWO(2, '2', "2"),
	THREE(3, '3', "3"),
	FOUR(4, '4', "4"),
	FIVE(5, '5', "5"),
	SIX(6, '6', "6"),
	SEVEN(7, '7', "7"),
	EIGHT(8, '8', "8"),
	NINE(9, '9', "9"),
	TEN(10, 'T', "T"),
	JACK(11, 'J', "J"),
	QUEEN(12, 'Q', "Q"),
	KING(13, 'K', "K"),
	ACE(14, 'A', "A");
	
	private final int intValue;
	private final char charValue;
	private final String strValue;
	
	private Rank(int intValue, char charValue, String strValue) {
		this.intValue = intValue;
		this.charValue = charValue;
		this.strValue = strValue;
	}
	
	public int intValue() {
		return intValue;
	}
	
	public char charValue() {
		return charValue;
	}
	
	public String strValue() {
		return strValue;
	}
	
	public static Rank parseRank(int i) {
		for (Rank r : Rank.values()) {
			if (i == r.intValue()) {
				return r;
			}
		}
		
		throw new IllegalArgumentException();
	}
	
	public static Rank parseRank(final char arg) {
		char rankChar = Character.toUpperCase(arg);
		
		for (Rank r : Rank.values()) {
			if (rankChar == r.charValue()) {
				return r;
			}
		}
		
		throw new IllegalArgumentException(Character.toString(arg));
	}
	
	public static Rank parseRank(final String arg) {
		String rankString = arg.toUpperCase();
		
		for (Rank r : Rank.values()) {
			if (rankString.equals(r.strValue())) {
				return r;
			}
		}
		
		throw new IllegalArgumentException(arg);
	}
	
	public static boolean isRank(char arg) {
		char rankChar = Character.toUpperCase(arg);
		
		for (Rank r : Rank.values()) {
			if (rankChar == r.charValue) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isRank(String arg) {
		if (arg.length() > 1) {
			return false;
		}
		
		return isRank(arg.charAt(0));
	}

	@Override
	public int compare(Rank r1, Rank r2) {
		return Integer.compare(r1.intValue(), r2.intValue());
	}
}
