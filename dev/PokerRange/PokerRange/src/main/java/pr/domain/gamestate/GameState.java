package pr.domain.gamestate;

import lombok.Getter;
import pr.domain.poker.BoardCards;
import pr.domain.poker.Combinations;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;
import pr.util.Optional;

public class GameState {
	
	@Getter private final HoleCards holeCards;
	@Getter private final BoardCards boardCards;
	@Getter private final Optional<Combinations> holeCardCombinations;
	@Getter private final Combinations boardCardCombinations;
	
	public GameState() {
		holeCards = null;
		boardCards = new BoardCards();
		holeCardCombinations = Optional.empty();
		boardCardCombinations = new Combinations(null, boardCards);
	}
	
	public GameState(HoleCards holeCards, BoardCards boardCards) {
		if (boardCards == null) {
			throw new IllegalArgumentException("The argument 'boardCards' may not be null.");
		}
		
		this.holeCards = holeCards;
		this.boardCards = boardCards;
		
		if (holeCards != null) {
			holeCardCombinations = Optional.of(new Combinations(holeCards, boardCards));
		} else {
			holeCardCombinations = Optional.empty();
		}
		
		boardCardCombinations = new Combinations(null, boardCards);
	}

	public Street getStreet() {
		return boardCards.getStreet();
	}
	
	public String toString() {
		return String.format("%s - %s", holeCards, boardCards);
	}
	
}
