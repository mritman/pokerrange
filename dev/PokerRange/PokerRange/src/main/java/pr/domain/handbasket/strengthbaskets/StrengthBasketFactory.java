package pr.domain.handbasket.strengthbaskets;

import pr.domain.handbasket.HandBasketFactory;

public class StrengthBasketFactory extends HandBasketFactory {
	
	public StrengthBasketFactory() {
		super(StrengthBasketEnum.values(), StrengthBasketEnum.FOLDED);
	}

}
