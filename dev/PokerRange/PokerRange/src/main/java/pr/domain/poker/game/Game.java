package pr.domain.poker.game;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import lombok.Getter;
import pr.domain.poker.Street;
import pr.domain.poker.action.Action;
import pr.domain.poker.action.ActionFactory;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;
import pr.util.Optional;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;

public class Game {
	private final ListMultimap<Seat, Action> seatActionMap;
	private final List<Seat> seatsInHand;
	private final Iterator<Seat> seatsInHandCycle;
	private final List<Seat> seatsLeftToActOnCurrentStreet;
	private final Seat dealerPosition;
	private Seat nextSeatToAct;
	private Seat lastToBetCallOrRaise;
	
	@Getter	private Street street;
	@Getter	boolean gameFinished = false;

	public Game(Set<Seat> seatsInHand, Seat dealerPosition) {
		this.seatActionMap = LinkedListMultimap.create();
		this.seatsInHand = Lists.newArrayList(seatsInHand);
		this.dealerPosition = dealerPosition;
		street = Street.PREFLOP;
		
		Collections.sort(this.seatsInHand);

		seatsInHandCycle = Iterables.cycle(this.seatsInHand).iterator();
		seatsLeftToActOnCurrentStreet = Lists.newArrayList(seatsInHand);
		nextSeatToAct = initialize(dealerPosition);
	}
	
	public List<Seat> getSeatsInHand() {
		return ImmutableList.copyOf(seatsInHand);
	}
	
	public List<Action> getActions(Seat seat) {
		return ImmutableList.copyOf(seatActionMap.get(seat));
	}
	
	public Seat getNextSeatToAct() {
		Preconditions.checkState(!gameFinished);
		return nextSeatToAct;
	}
	
	public Optional<Entry<Seat, Action>> getLastAction() {
		Entry<Seat, Action> lastAction = Iterables.getLast(seatActionMap.entries(), null);
		return Optional.ofNullable(lastAction);
	}
	
	public void processEvent(Seat seat, ActionType actionType) throws GameException {
		Preconditions.checkState(!gameFinished);
		
		if (seat != nextSeatToAct) {
			throw new GameException(GameException.INCORRECT_ACTION_ORDER, nextSeatToAct, seat, actionType);
		}

		seatActionMap.put(seat, ActionFactory.createInstance(actionType, street));
		seatsLeftToActOnCurrentStreet.remove(seat);
		
		if (actionType.isFold()) {
			seatsInHandCycle.remove();
		} else if (actionType.isBet()
				|| actionType.isRaise() 
				|| lastToBetCallOrRaise == null) {
			
			if (street.isPreflop() && actionType.isBet()) {
				throw new GameException(GameException.CAN_ONLY_BET_PREFLOP);
			}
			
			lastToBetCallOrRaise = seat;
		}

		if (seatsInHand.size() == 1) {
			gameFinished = true;
			return;
		}
		
		nextSeatToAct = seatsInHandCycle.next();
		if (nextSeatToAct == lastToBetCallOrRaise) {
			toNextStreet();
		}
	}

	private void toNextStreet() {
		if (street.isPreflop()) {
			street = Street.FLOP;
		} else if (street.isFlop()) {
			street = Street.TURN;
		} else if (street.isTurn()) {
			street = Street.RIVER;
		} else {
			gameFinished = true;
		}
		
		Preconditions.checkState(seatsLeftToActOnCurrentStreet.isEmpty());
		seatsLeftToActOnCurrentStreet.addAll(seatsInHand);
		lastToBetCallOrRaise = null;
		
		if (!gameFinished) {
			nextSeatToAct = getFirstSeatAfterDealer();
		}
	}
	
	private Seat initialize(Seat dealerPosition) {
		Preconditions.checkState(street.isPreflop());
		Preconditions.checkState(nextSeatToAct == null);
		Preconditions.checkState(seatsInHand.contains(dealerPosition));
		
		Seat current = null;
		while (current != dealerPosition) {
			current = seatsInHandCycle.next();
		}
		Seat smallBlind = seatsInHandCycle.next();
		Seat bigBlind = seatsInHandCycle.next();
		seatActionMap.put(smallBlind, ActionFactory.createInstance(ActionType.POST_SMALL_BLIND, street));
		seatActionMap.put(bigBlind, ActionFactory.createInstance(ActionType.POST_BIG_BLIND, street));
		
		return seatsInHandCycle.next();
	}

	private Seat getFirstSeatAfterDealer() {
		Preconditions.checkState(!seatsInHand.isEmpty());
		Preconditions.checkState(seatsInHand.size() > 1);
		
		Seat seat = PokerTable.getFirstSeatAfter(seatsInHand, dealerPosition);
		
		while (true) {
			if (seatsInHandCycle.next() == seat) {
				break;
			}
		}
		
		return seat;
	}
	
}
