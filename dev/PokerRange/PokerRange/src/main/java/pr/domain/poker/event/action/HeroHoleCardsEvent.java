package pr.domain.poker.event.action;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.HoleCards;


public class HeroHoleCardsEvent implements ActionEvent {
	
	@Getter private final HoleCards holeCards;
	
	public HeroHoleCardsEvent(HoleCards holeCards) {
		this.holeCards = holeCards;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("holeCards", holeCards).toString();
	}
	
}
