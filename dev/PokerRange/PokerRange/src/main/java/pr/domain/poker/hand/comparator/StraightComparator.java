package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.cards.Rank;
import pr.domain.poker.hand.Straight;

public class StraightComparator implements Comparator<Straight> {

	private static final StraightComparator straightComparator = new StraightComparator();
	
	public static StraightComparator getInstance() {
		return straightComparator;
	}
	
	@Override
	public int compare(Straight straight1, Straight straight2) {
		return Integer.compare(straight1.getRank().intValue(), straight2.getRank().intValue());
	}
	
	public int compare(Rank rank1, Rank rank2) {
		return Integer.compare(rank1.intValue(), rank2.intValue());
	}

	public int hashCode(Straight straight) {
		return Objects.hash(straight.getRank());
	}

}
