package pr.domain.handbasket;

import java.util.TreeSet;

import lombok.Getter;
import lombok.Setter;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

public abstract class BasketElement {
	
	/**
	 * This is the hand basket that an instance of this class was
	 * originally sorted in. It can be placed into another basket by the user
	 * but this field can be used to reset it to its original location.
	 */
	@Getter
	@Setter
	private HandBasket originalHandBasket;
	
	/**
	 * This is the hand basket an instance of this class is currently in.
	 */
	@Getter
	@Setter
	private HandBasket currentHandBasket;
	
	public abstract int getHandCount();
	
	public abstract TreeSet<Combinations> getCombinations();
	
	public abstract boolean containsHandFoldedByUser();
	
	public abstract String getName();
	
	/**
	 * @return the Combinations with the highest hand value
	 */
	public abstract Combinations getHighestHoleCards();
	
	/**
	 * @return the highest hand value of the {@link Combinations} returned by
	 *         {@link #getHighestHoleCards()}
	 */
	public abstract HandValue getHandValue();

	/**
	 * Folds all combinations in the basket.
	 */
	public abstract void fold();

	/**
	 * Unfolds all combinations in the basket.
	 */
	public abstract void unfold();
}