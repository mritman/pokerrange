package pr.domain.poker.player;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.range.typed.TypedHandRange;

@Entity
@NamedQueries({
	@NamedQuery(name = "OpponentAccount.findByNameAndVenue",
				query = "SELECT oa FROM OpponentAccount oa WHERE oa.name = :name AND oa.venue = :venue")
})
@Table(uniqueConstraints = @UniqueConstraint(name = "opponentaccount_name_venue_index", columnNames = {"name", "venue"}))
public class OpponentAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Getter
	private String name;
	
	@Getter
	@Enumerated(EnumType.STRING)
	private PokerVenue venue;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true)
	@JoinTable(
			inverseJoinColumns=@JoinColumn(name="range_id"),
			uniqueConstraints=@UniqueConstraint(name = "opponentaccount_typedhandrange_id_index", columnNames="range_id"))
	@MapKey(name="name")
	private Map<String, TypedHandRange> ranges;
	
	OpponentAccount() {};
	
	public OpponentAccount(String name, PokerVenue venue) {
		this.name = name;
		this.venue = venue;
		this.ranges = new HashMap<>();
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		
		if (!(object instanceof OpponentAccount)) return false;
		
		OpponentAccount other = (OpponentAccount) object;
		
		return Objects.equals(name, other.getName()) && Objects.equals(venue, other.getVenue());
	}
	
	public int hashCode() {
		return Objects.hash(name, venue);
	}
	
	public Collection<TypedHandRange> getRanges() {
		return Collections.unmodifiableCollection(ranges.values());
	}
	
	public void addRange(TypedHandRange range) {
		ranges.put(range.getName(), range);
	}

	public void removeRange(TypedHandRange range) {
		ranges.remove(range.getName());
	}
	
}
