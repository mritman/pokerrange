package pr.domain.poker.hand;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import pr.domain.poker.BoardCards;
import pr.domain.poker.Combinations;
import pr.domain.poker.HoleCards;

public class HandManager {
	
	final static int NR_HAND_TYPES = HandValue.values().length;
	
	private Map<HandValue, TreeSet<Combinations>> handValueMap = new TreeMap<>();
	private BoardCards boardCards = new BoardCards();
	
	public HandManager() {
		for (HandValue handValue : HandValue.values()) {
			handValueMap.put(handValue, new TreeSet<Combinations>());
		}
	}
	
	/**
	 * The <code>Combinations</code> in handsInRange are cloned and sorted into the
	 * <code>Set</code>s of <code>Combinations</code> (one for each
	 * <code>Hand</code>). The boardCards are used to
	 * <code>HandManager</code> the each hand makes.
	 * 
	 * @param handsInRange
	 *            the hands to sort into the Hand Sets
	 * @param boardCards
	 *            the board cards
	 */
	public void calculateHits(Set<HoleCards> handsInRange, BoardCards boardCards) {
		clear();
		this.boardCards = boardCards;
		
		for (HoleCards holeCards : handsInRange) {
			assignHandValue(holeCards);
		}
	}
	
	private void assignHandValue(HoleCards holeCards) {
		Combinations combinations = new Combinations(holeCards, boardCards);
		addToHandValue(combinations);
	}
	
	private void addToHandValue(Combinations combinations) {
		for (Hand hand : combinations.getHands()) {
			Set<Combinations> combinationSet = handValueMap.get(hand.getValue());
			combinationSet.add(combinations);
		}
	}
	
	public Set<Combinations> getHoleCards() {
		TreeSet<Combinations> combinations = new TreeSet<>();
		
		for (TreeSet<Combinations> set : handValueMap.values()) {
			combinations.addAll(set);
		}
		
		return combinations;
	}
	
	/**
	 * Gets all combinations for a given {@link HandValue}.
	 * 
	 * @param handValue the {@link HandValue} for which to get the combinations
	 * @return the unmodifiable set of combinations for the specified hand value
	 */
	public Set<Combinations> getCombinations(HandValue handValue) {
		return Collections.unmodifiableSet(handValueMap.get(handValue));
	}
	
	private void clear() {
		for (TreeSet<Combinations> combinations : handValueMap.values()) {
			combinations.clear();
		}
	}
	
	public int totalSize(HandValue minValue) {
		int size = 0;
		
		for (HandValue handValue : HandValue.values()) {
			if (handValue.intValue() >= minValue.intValue()) {
				size += handValueMap.get(handValue).size();
			}
		}
		
		return size;
	}
	
	public int totalSize() {
		int size = 0;
		
		for (HandValue key : handValueMap.keySet()) {
			if (key != HandValue.STRAIGHT_DRAW && key != HandValue.FLUSH_DRAW) {
				TreeSet<Combinations> combinations = handValueMap.get(key);
				size += combinations.size();
			}
		}
		
		return size;
	}
	
	public double percentage(HandValue handValue) {
		if (totalSize() == 0) return 0;
		
		TreeSet<Combinations> combinations = handValueMap.get(handValue);
		
		return (double) combinations.size() / totalSize() * 100;
	}
}