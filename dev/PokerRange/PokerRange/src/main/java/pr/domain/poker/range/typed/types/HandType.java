package pr.domain.poker.range.typed.types;

import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;
import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public abstract class HandType {
	private final HandTypeEnum typeName;
	
	@Getter private final Rank defaultLowestRank;
	@Getter private final Rank defaultHighestRank;
	
	private final Set<HoleCards> hands = new TreeSet<>();
	private Rank lowestRank;
	private Rank highestRank;
	private boolean enabled = true;
	
	public HandType(HandTypeEnum typeName, Rank defaultLowestRank, Rank defaultHighestRank, Rank lowestRank, Rank highestRank) {
		this.typeName = typeName;
		this.defaultLowestRank = defaultLowestRank;
		this.defaultHighestRank = defaultHighestRank;
		
		setLowestRank(lowestRank);
		setHighestRank(highestRank);
	}
	
	public HandType(HandTypeEnum typeName, Rank lowestRank, Rank highestRank) {
		this(typeName, Rank.TWO, Rank.ACE, lowestRank, highestRank);
	}
	
	public HandTypeEnum getName() {
		return typeName;
	}
	
	public final void setLowestRank(Rank rank) {
		if (rank == null || rank.intValue() < defaultLowestRank.intValue()){
			lowestRank = defaultLowestRank;
		} else if (rank.intValue() > defaultHighestRank.intValue()) {
			lowestRank = defaultHighestRank;
		} else {
			lowestRank = rank;
		}
		
		// The lowest rank can not be higher than the highest rank. If this is
		// the case the highest rank is set equal to the new lowest rank.
		if (highestRank != null && lowestRank.intValue() > highestRank.intValue()) {
			highestRank = lowestRank;
		}
	}
	
	public final void setHighestRank(Rank rank) {
		if (rank == null || rank.intValue() > defaultHighestRank.intValue()) {
			highestRank = defaultHighestRank;
		} else if (rank.intValue() < defaultLowestRank.intValue()) {
			highestRank = defaultLowestRank;
		} else {
			highestRank = rank;
		}
		
		// The highest rank can not be lower than the lowest rank. If this is
		// the case the lowest rank is set equal to the new highest rank.
		if (lowestRank != null && highestRank.intValue() < lowestRank.intValue()) {
			lowestRank = highestRank;  
		}
	}
	
	public void resetRange() {
		lowestRank = defaultLowestRank;
		highestRank = defaultHighestRank;
	}
	
	public Rank getLowestRank() {
		return lowestRank;
	}
	
	public Rank getHighestRank() {
		return highestRank;
	}
	
	public Set<HoleCards> getHands() {
		return hands;
	}
	
	public boolean addHand(HoleCards hand) {
		hands.add(hand.cloneHoleCards());
		
		return true;
	}
	
	public void enable() {
		enabled = true;
	}
	
	public void disable() {
		enabled = false;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public String[] toStringArray() {
		String[] handStrings = new String[hands.size()];
		int i = 0;
		
		for (HoleCards hand : hands) {
			handStrings[i] = hand.toString();
			i++;
		}
		
		return handStrings;
	}

}
