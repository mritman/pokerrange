package pr.domain.poker.util;

public final class PokerCardStringFormat {
	
	private PokerCardStringFormat() {};
	
	public static final String CARD_SEPARATOR = " ";
	public static final String HOLE_BOARD_SEPARATOR = " - ";
	
}
