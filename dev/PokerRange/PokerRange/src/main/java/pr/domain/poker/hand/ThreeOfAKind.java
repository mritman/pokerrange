package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class ThreeOfAKind extends Hand {
	
	public enum Type {
		BOARD,
		PLAYER;
	}
	
	private List<Card> threeOfAKind;
	private Type type; 

	protected ThreeOfAKind(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHands(holeCards, boardCards);
	}
	
	public List<Card> getCards() {
		return threeOfAKind;
	}
	
	public Type getType() {
		return type;
	}
	
	public Rank getRank() {
		return threeOfAKind.get(0).rank();
	}
	
	private void calculateHands(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> newThreeOfAKind = new LinkedList<>();
		
		calculateThreeOfAKind(cards, newThreeOfAKind);
		
		Collections.sort(newThreeOfAKind, CardComparator.getInstance());
		threeOfAKind = Collections.unmodifiableList(newThreeOfAKind);
		type = calculateThreeOfAkindType(newThreeOfAKind, boardCards);

		cards.removeAll(newThreeOfAKind);

		init(HandValue.THREE_OF_A_KIND, newThreeOfAKind, cards);
	}

	private void calculateThreeOfAKind(List<Card> cards, List<Card> threeOfAKind) throws DoesNotMakeHandException {
		ListIterator<Card> iterator = cards.listIterator(cards.size());
		Card lastCard = null;
		Card card = null;
		
		// Used to store a pair that is ranked higher than the possible three of
		// a kind. If a three of a kind is encountered but the pair is not empty
		// this hand does not make a three of a kind but at least a flush.
		List<Card> pair = new LinkedList<>();
		
		while (iterator.hasPrevious()) {
			if (lastCard == null) {
				lastCard = iterator.previous();
				continue;
			}
			
			card = iterator.previous();
			
			if (card.rank() == lastCard.rank()) {
				if (threeOfAKind.isEmpty()) {
					threeOfAKind.add(card);
					threeOfAKind.add(lastCard);
				} else if (willMakeThreeOfAKind(threeOfAKind, card)) {
					threeOfAKind.add(card);

					// Full house.
					if (!pair.isEmpty()) {
						throw new DoesNotMakeHandException(HandValue.THREE_OF_A_KIND);
					}
				} else if (willMakeFourOfAKind(threeOfAKind, card))  {
					// Four of a kind.
					throw new DoesNotMakeHandException(HandValue.THREE_OF_A_KIND);
				} else if (threeOfAKind.size() < 3 && card.rank() != threeOfAKind.get(0).rank())  {
					pair.addAll(threeOfAKind);
					threeOfAKind.clear();
					threeOfAKind.add(card);
					threeOfAKind.add(lastCard);
				} else {
					// Full house.
					throw new DoesNotMakeHandException(HandValue.THREE_OF_A_KIND);
				}
			}
			
			lastCard = card;
		}
		
		if (threeOfAKind.size() != 3) {
			throw new DoesNotMakeHandException(HandValue.THREE_OF_A_KIND);
		}
	}

	private boolean willMakeThreeOfAKind(List<Card> threeOfAKind, Card card) {
		return threeOfAKind.size() == 2 && threeOfAKind.get(0).rank() == card.rank();
	}
	
	private boolean willMakeFourOfAKind(List<Card> threeOfAKind, Card card) {
		return threeOfAKind.size() >= 3 && threeOfAKind.get(0).rank() == card.rank();
	}
	
	protected static Type calculateThreeOfAkindType(List<Card> threeOfAKind, BoardCards boardCards) {
		for (Card card : threeOfAKind) {
			if (!boardCards.contains(card)) {
				return Type.PLAYER;
			}
		}
		
		return Type.BOARD;
	}

}
