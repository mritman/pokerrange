package pr.domain.poker;

import lombok.Getter;

public enum Street {
	PREFLOP(0, 0),
	FLOP(3, 1),
	TURN(4, 2),
	RIVER(5, 3);
	
	@Getter
	private int size;
	
	private int order;
	
	private Street(int size, int order) {
		this.size = size;
		this.order = order;
	}
	
	public boolean isPreflop() {
		return this == PREFLOP;
	}
	
	public boolean isFlop() {
		return this == FLOP;
	}
	
	public boolean isTurn() {
		return this == TURN;
	}
	
	public boolean isRiver() {
		return this == RIVER;
	}
	
	public boolean comesBefore(Street street) {
		return order + 1 == street.order;
	}
	
	public boolean comesAfter(Street street) {
		return order - 1 == street.order;
	}
	
}
