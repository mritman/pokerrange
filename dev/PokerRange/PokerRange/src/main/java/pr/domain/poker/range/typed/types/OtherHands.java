package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;

public class OtherHands extends HandType {
	public OtherHands() {
		super(HandTypeEnum.OTHER, Rank.TWO, Rank.ACE);
	}
	
	public void disable() {
	}
}
