package pr.domain.poker;

import lombok.Getter;

public enum PokerVenue {
	MANUAL("Manual", "Manual"),
	POKERSTARS("PokerStars", "PokerStars.exe"),
	FULL_TILT_POKER("Full Tilt Poker", "FullTiltPoker.exe");
	
	@Getter 
	private final String displayableName;
	
	@Getter
	private final String executableName;
	
	private PokerVenue(String displayableName, String executableName) {
		this.displayableName = displayableName;
		this.executableName = executableName;
	}

}
