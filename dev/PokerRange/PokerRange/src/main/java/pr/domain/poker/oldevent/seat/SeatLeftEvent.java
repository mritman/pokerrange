package pr.domain.poker.oldevent.seat;

import pr.domain.poker.table.PokerTable;

public abstract class SeatLeftEvent extends SeatEvent {

	protected SeatLeftEvent(PokerTable table) {
		super(table);
	}

}
