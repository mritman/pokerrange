package pr.domain.handbasket.handvaluebaskets.subsets.twopairplus;

import pr.domain.handbasket.BasketElement;
import pr.domain.poker.hand.HandValue;

import com.google.common.base.Predicate;

public class ThreeOfKindPredicate implements Predicate<BasketElement> {

	@Override
	public boolean apply(BasketElement basketElement) {
		return basketElement.getHighestHoleCards().getHighestHand().getValue() == HandValue.THREE_OF_A_KIND;
	}

}
