package pr.domain.poker.range.typed.types;

public enum HandTypeEnum {
	PAIR("Pair"),
	ACE_X_SUITED("AXs"),
	ACE_X("AX"),
	KING_X("KX"),
	SUITED_CONNECTOR("SC"),
	SUITED_GAP_CONNECTOR("SGC"),
	OTHER("Other");
	
	private String strValue;
	
	private HandTypeEnum(String strValue) {
		this.strValue = strValue;
	}
	
	public String toString() {
		return strValue;
	}
	
	public static HandTypeEnum parseHandType(String string) {
		if (string.equalsIgnoreCase("Pair")) {
			return PAIR;
		} else if (string.equalsIgnoreCase("AXs")) {
			return ACE_X_SUITED;
		} else if (string.equalsIgnoreCase("AX")) {
			return ACE_X;
		} else if (string.equalsIgnoreCase("KX")) {
			return KING_X;
		} else if (string.equalsIgnoreCase("SC")) {
			return SUITED_CONNECTOR;
		} else if (string.equalsIgnoreCase("SGC")) {
			return SUITED_GAP_CONNECTOR;
		} else if (string.equalsIgnoreCase("Other")) {
			return OTHER;
		} else {
			return null;
		}
	}
}