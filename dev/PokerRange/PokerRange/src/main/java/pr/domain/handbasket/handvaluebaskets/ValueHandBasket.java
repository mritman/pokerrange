package pr.domain.handbasket.handvaluebaskets;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasket;
import pr.domain.handbasket.HandBasketSubset;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.collect.Collections2;

public class ValueHandBasket extends HandBasket {

	private final List<HandBasketSubsetKey> subsetKeys;

	public ValueHandBasket(String name, Set<BasketElement> basketElements, List<HandBasketSubsetKey> subsetKeys) {
		super(name, basketElements);
		
		this.subsetKeys = subsetKeys;
	}
	
	@Override
	public Map<HandBasketSubsetKey, HandBasketSubset> getSubsets() {
		// The fact that this map is a TreeMap is what makes it possible for the
		// client to iterate over the keys in it in their natural order.
		Map<HandBasketSubsetKey, HandBasketSubset> result = new TreeMap<>();
		
		for (HandBasketSubsetKey subsetKey : subsetKeys) {
			Collection<BasketElement> subsetHands = Collections2.filter(getBasketElements(), subsetKey.getPredicate());

			Set<BasketElement> subsetElements = new TreeSet<>(subsetHands);
			result.put(subsetKey, new HandBasketSubset(subsetElements));
		}
		
		return result;
	}

}
