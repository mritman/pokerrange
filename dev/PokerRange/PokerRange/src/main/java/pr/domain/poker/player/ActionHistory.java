package pr.domain.poker.player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.Getter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.Street;
import pr.domain.poker.action.Action;
import pr.domain.poker.action.ActionType;
import pr.util.Optional;

import com.google.common.base.Preconditions;

public class ActionHistory {
	
	private static final Logger logger = LoggerFactory.getLogger(ActionHistory.class);
	
	private Map<Street, List<Action>> actionMap;
	
	@Getter
	private Optional<Action> lastAction;
	
	public ActionHistory() {
		actionMap = new HashMap<>();
		
		for (Street street : Street.values()) {
			actionMap.put(street, new LinkedList<Action>());
		}
		
		reset();
	}

	public final void reset() {
		for (List<Action> actions : actionMap.values()) {
			actions.clear();
		}
		
		lastAction = Optional.empty();
	}
	
	public void actionPerformed(Street street, Action action) {
		Preconditions.checkNotNull(street);
		Preconditions.checkNotNull(action);
		
		if (lastAction.isPresent() && lastAction.get().getType() == ActionType.FOLD) {
			IllegalStateException exception = new IllegalStateException(
					"This player has already folded the current game. " +
					"The reset() method should be called upon a new game and only then can the player perform new actions.");
			logger.error("TODO FIx this", exception);
		}
		
		actionMap.get(street).add(action);
		lastAction = Optional.of(action);
	}
	
	public boolean hasFolded() {
		return lastAction.isPresent() && lastAction.get().getType() == ActionType.FOLD;
	}

}
