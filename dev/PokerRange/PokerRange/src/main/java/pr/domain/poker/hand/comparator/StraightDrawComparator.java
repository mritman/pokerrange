package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.StraightDraw;

public class StraightDrawComparator implements Comparator<StraightDraw> {
	
	private static final StraightDrawComparator straightDrawComparator = new StraightDrawComparator();
	
	public static StraightDrawComparator getInstance() {
		return straightDrawComparator;
	}
	
	@Override
	public int compare(StraightDraw sd1, StraightDraw sd2) {
		int cmp = compareSide(sd1.getSide(), sd2.getSide());
		
		if (cmp != 0) return cmp;
		
		return Integer.compare(sd1.getHighRank().intValue(), sd2.getHighRank().intValue());
	}
	
	public int compareSide(StraightDraw.Side side1, StraightDraw.Side side2) {
		return Integer.compare(side1.getIntValue(), side2.getIntValue());
	}

	public int hashCode(StraightDraw hand) {
		return Objects.hash(hand.getSide(), hand.getHighRank());
	}

}
