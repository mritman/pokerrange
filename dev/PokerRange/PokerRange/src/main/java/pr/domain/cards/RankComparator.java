package pr.domain.cards;

import java.util.Comparator;

public class RankComparator implements Comparator<Rank> {
	
	private static final RankComparator rankComparator = new RankComparator();
	
	public static RankComparator getInstance() {
		return rankComparator;
	}

	@Override
	public int compare(Rank rank1, Rank rank2) {
		return Integer.compare(rank1.intValue(), rank2.intValue());
	}

}
