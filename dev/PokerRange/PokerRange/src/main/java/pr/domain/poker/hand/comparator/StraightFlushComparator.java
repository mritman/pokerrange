package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.StraightFlush;

public class StraightFlushComparator implements Comparator<StraightFlush> {

	private static final StraightFlushComparator straightFlushComparator = new StraightFlushComparator();
	
	public static StraightFlushComparator getInstance() {
		return straightFlushComparator;
	}
	
	@Override
	public int compare(StraightFlush sf1, StraightFlush sf2) {
		return Integer.compare(sf1.getRank().intValue(), sf2.getRank().intValue());
	}

	public int hashCode(StraightFlush hand) {
		return Objects.hash(hand.getRank());
	}
	
}
