package pr.domain.poker.oldevent;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.table.PokerTable;

public class OpponentFoldedHandEvent extends PokerTableEvent {

	@Getter
	private Opponent opponent;

	public OpponentFoldedHandEvent(Opponent opponent, PokerTable table) {
		super(table);
		this.opponent = opponent;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(getClass()).add("opponent", opponent.getName()).toString();
	}

}
