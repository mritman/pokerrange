package pr.domain.poker.range.ranked;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;
import pr.domain.poker.range.ranked.HoleCardsRank.SuitedRequirement;

public class PokerStoveHoleCardsRanks {

	private static Map<HoleCardsRank, Set<HoleCards>> rankedHoleCardsMap = new TreeMap<>(); 
	
	static {
		if (rankedHoleCardsMap.isEmpty()) {
			Set<HoleCards> holeCards = HoleCards.createAllHoleCards();
			Set<HoleCardsRank> holeCardsRanks = getPokerStoveHoleCardsRanks();
			
			for (HoleCardsRank rank : holeCardsRanks) {
				Set<HoleCards> holeCardsSet = rank.filter(holeCards);
				rankedHoleCardsMap.put(rank, holeCardsSet);
			}
		}
	}
	
	/**
	 * @return a map that contains the poker stove hole cards ranks in descending order.
	 */
	public static Map<HoleCardsRank, Set<HoleCards>> getRankedPokerStoveHoleCardsMap() {
		return Collections.unmodifiableMap(rankedHoleCardsMap);
	}
	
	public static Set<HoleCardsRank> getPokerStoveHoleCardsRanks() {
		int ordinal = 0;
		Set<HoleCardsRank> holeCardsRanks = new TreeSet<>();
		
		for (String rankString : getPokerStoveHoleCardsOrder()) {
			Rank rank1 = Rank.parseRank(rankString.charAt(0));
			Rank rank2 = Rank.parseRank(rankString.charAt(1));
			SuitedRequirement suited = SuitedRequirement.BOTH;
			
			if (rankString.length() == 3) {
				suited = SuitedRequirement.fromChar(rankString.charAt(2));
			}
			
			HoleCardsRank holeCardsRank = new HoleCardsRank(rank1, rank2, suited, ordinal);
			ordinal++;
			
			holeCardsRanks.add(holeCardsRank);
		}
		
		return holeCardsRanks;
	}
	
	protected static List<String> getPokerStoveHoleCardsOrder() {
		List<String> set = new LinkedList<>();
		
		Collections.addAll(set,
			"AA",
			"KK",
			"QQ",
			"JJ",
			"TT",
			"99",
			"AKs",
			"AQs",
			"AKo",
			"AJs",
			"KQs",
			"88",
			"ATs",
			"AQo",
			"KJs",
			"KTs",
			"QJs",
			"AJo",
			"KQo",
			"QTs",
			"A9s",
			"77",
			"ATo",
			"JTs",
			"KJo",
			"A8s",
			"K9s",
			"QJo",
			"A7s",
			"KTo",
			"Q9s",
			"A5s",
			"66",
			"A6s",
			"QTo",
			"J9s",
			"A9o",
			"T9s",
			"A4s",
			"K8s",
			"JTo",
			"K7s",
			"A8o",
			"A3s",
			"Q8s",
			"K9o",
			"A2s",
			"K6s",
			"J8s",
			"T8s",
			"A7o",
			"55",
			"Q9o",
			"98s",
			"K5s",
			"Q7s",
			"J9o",
			"A5o",
			"T9o",
			"A6o",
			"K4s",
			"K8o",
			"Q6s",
			"J7s",
			"T7s",
			"A4o",
			"97s",
			"K3s",
			"87s",
			"Q5s",
			"K7o",
			"44",
			"Q8o",
			"A3o",
			"K2s",
			"J8o",
			"Q4s",
			"T8o",
			"J6s",
			"K6o",
			"A2o",
			"T6s",
			"98o",
			"76s",
			"86s",
			"96s",
			"Q3s",
			"J5s",
			"K5o",
			"Q7o",
			"Q2s",
			"J4s",
			"33",
			"65s",
			"J7o",
			"T7o",
			"K4o",
			"75s",
			"T5s",
			"Q6o",
			"J3s",
			"95s",
			"87o",
			"85s",
			"97o",
			"T4s",
			"K3o",
			"J2s",
			"54s",
			"Q5o",
			"64s",
			"T3s",
			"22",
			"K2o",
			"74s",
			"76o",
			"T2s",
			"Q4o",
			"J6o",
			"84s",
			"94s",
			"86o",
			"T6o",
			"96o",
			"53s",
			"93s",
			"Q3o",
			"J5o",
			"63s",
			"43s",
			"92s",
			"73s",
			"65o",
			"Q2o",
			"J4o",
			"83s",
			"75o",
			"52s",
			"85o",
			"82s",
			"T5o",
			"95o",
			"J3o",
			"62s",
			"54o",
			"42s",
			"T4o",
			"J2o",
			"72s",
			"64o",
			"T3o",
			"32s",
			"74o",
			"84o",
			"T2o",
			"94o",
			"53o",
			"93o",
			"63o",
			"43o",
			"92o",
			"73o",
			"83o",
			"52o",
			"82o",
			"42o",
			"62o",
			"72o",
			"32o");
		
		return set;
	}
	
}
