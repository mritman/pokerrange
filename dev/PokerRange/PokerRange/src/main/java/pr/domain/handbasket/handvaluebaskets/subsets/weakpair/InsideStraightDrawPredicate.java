package pr.domain.handbasket.handvaluebaskets.subsets.weakpair;

import pr.domain.handbasket.BasketElement;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.StraightDraw;

import com.google.common.base.Predicate;

public class InsideStraightDrawPredicate implements Predicate<BasketElement> {

	@Override
	public boolean apply(BasketElement basketElement) {
		Combinations combinations = basketElement.getHighestHoleCards();
		
		if (combinations.getHighestHand().getValue() == HandValue.STRAIGHT_DRAW) {
			StraightDraw straightDraw = (StraightDraw) combinations.getHighestHand();
			
			return straightDraw.getSide() == StraightDraw.Side.INSIDE;
		}
		
		return false;
	}
	
}
