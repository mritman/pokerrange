package pr.domain.poker.event.action;

import com.google.common.base.Objects;

import lombok.Getter;

public class CheckEvent implements ActionEvent {
	
	@Getter private final String name;

	public CheckEvent(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("name", name)
				.toString();
	}
	
}
