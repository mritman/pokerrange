package pr.domain.handbasket.handvaluebaskets.subsets.twopairplus;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.base.Predicate;

public enum TwoPairPlusSubsets implements HandBasketSubsetKey {
	TWO_PAIR(0, "Two pair", new TwoPairPredicate()),
	THREE_OF_A_KIND(1, "Three of a kind", new ThreeOfKindPredicate()),
	STRAIGHT(2, "Straight", new StraightPredicate()),
	FLUSH(3, "Flush", new FlushPredicate()),
	FULL_HOUSE_PLUS(4, "Full house +", new FullHousePlusPredicate());

	private int intValue;
	
	@Getter
	private Predicate<BasketElement> predicate;

	@Getter
	private String name;
	
	private TwoPairPlusSubsets(int intValue, String name, Predicate<BasketElement> predicate) {
		this.intValue = intValue;
		this.predicate = predicate;
		this.name = name;
	}
	
	public int intValue() {
		return intValue;
	}
}
