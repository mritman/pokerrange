package pr.domain.handbasket.handvaluebaskets.subsets.pair;

import pr.domain.handbasket.BasketElement;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.Pair;
import pr.domain.poker.hand.TwoPair;
import pr.domain.poker.hand.Pair.RelativeRank;

import com.google.common.base.Predicate;

public class TopPairPredicate implements Predicate<BasketElement> {

	@Override
	public boolean apply(BasketElement basketElement) {
		Combinations combinations = basketElement.getHighestHoleCards();
		
		if (combinations.getHighestHand().getValue() == HandValue.PAIR) {
			Pair pair = (Pair) combinations.getHighestHand();
			
			return pair.getRelativeRank() == Pair.RelativeRank.TOP_PAIR;
		} else if (combinations.getHighestHand().getValue() == HandValue.TWO_PAIR) {
			TwoPair twoPair = (TwoPair) combinations.getHighestHand();
			RelativeRank relativeRank = twoPair.getFirstPairType() != Pair.Type.BOARD_PAIR ? twoPair
					.getFirstPairRelativeRank() : twoPair
					.getSecondPairRelativeRank();
					
			return relativeRank == Pair.RelativeRank.TOP_PAIR;
		}
		
		return false;
	}

}
