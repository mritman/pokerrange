package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.hand.comparator.StraightComparator;
import pr.domain.poker.util.CardUtil;

public class Straight extends Hand {

	public enum Type {
		BOARD,
		PLAYER;
	}
	
	@Getter private List<Card> straight;
	@Getter private Type type; 
	
	@Getter private List<Card> secondStraight;
	@Getter	private Type secondStraightType;
	
	public Straight(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
	
		calculateHand(holeCards, boardCards);
	}
	
	public Rank getRank() {
		return straight.get(straight.size() - 1).rank();
	}
	
	public Rank getSecondRank() {
		if (secondStraight != null) {
			return secondStraight.get(secondStraight.size() - 1).rank();
		}
		
		return null;
	}
	
	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> player = new LinkedList<>();
		List<Card> board = new LinkedList<>();
		List<Card> boardCardList = boardCards.asList();
		
		calculatePlayerStraight(holeCards, boardCards, player);
		
		Collections.sort(boardCardList, CardComparator.getInstance());
		calculateStraight(boardCardList, board);
		
		if (player.isEmpty() && board.isEmpty()) {
			throw new DoesNotMakeHandException(HandValue.STRAIGHT);
		}
		
		assignStraightAndType(player, board);
		
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> valueCards = getType() == Type.PLAYER ? player : board;
		cards.removeAll(valueCards);
		
		//Copy the list of valuecards so that its order will not be changed by #init().
		valueCards = new LinkedList<>(valueCards);
		
		init(HandValue.STRAIGHT, valueCards, cards);
	}
	
	private void calculatePlayerStraight(HoleCards holeCards, BoardCards boardCards, List<Card> straight) {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> noDuplicates = CardUtil.removeDuplicateRanks(holeCards, cards);
		
		if (holeCards == null) return; 
		
		if (noDuplicates.size() < 5) return; 
		
		// Check if the cards make a regular straight.
		for (int i = noDuplicates.size() - 5; i >= 0; i--) {
			List<Card> subList = noDuplicates.subList(i, i+5);
			
			calculateStraight(subList, straight);
			
			if (!straight.isEmpty()
					&& (straight.contains(holeCards.card1()) || straight
							.contains(holeCards.card2()))) {
				return;
			} else {
				straight.clear();
			}
		}
		
		// If a straight was not found and if the cards contain an ace, 
		// see if they makes an ace-to-five straight.
		if (straight.isEmpty()
				&& noDuplicates.get(noDuplicates.size() - 1).rank() == Rank.ACE) {
			
			List<Card> subList = noDuplicates.subList(0, 4);
			subList.add(0, noDuplicates.get(noDuplicates.size() - 1));
			
			calculateStraight(subList, straight);
			
			if (!straight.isEmpty()
					&& (straight.contains(holeCards.card1()) || straight
							.contains(holeCards.card2()))) {
				return;
			} else {
				straight.clear();
			}
		}
	}

	/**
	 * If the specified cards contain a two and an ace then the ace will be
	 * placed at the beginning of the specified list.
	 * 
	 * @param cards
	 *            a list of five cards assumed to be sorted by rank.
	 * @param straight
	 *            the list of cards that will contain the straight if the cards
	 *            make a straight, empty otherwise.
	 */
	protected static void calculateStraight(List<Card> cards, List<Card> straight) {
		if (cards.size() != 5) {
			return;
		}
		
		Card lastCard = null;
		
		// If the lowest card is a 2 and the highest an ace then place the ace at the beginning of the list.
		if (cards.get(0).rank() == Rank.TWO && cards.get(cards.size() -1).rank() == Rank.ACE) {
			Card ace = cards.remove(cards.size() - 1);
			cards.add(0, ace);
		}
		
		for (Card card : cards) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}
			
			if (card.rank().intValue() == lastCard.rank().intValue() + 1 
					|| cards.indexOf(lastCard) == 0 && lastCard.rank() == Rank.ACE && card.rank() == Rank.TWO) {
				lastCard = card;
			} else {
				return;
			}			
		}
		
		straight.addAll(cards);
	}
	
	private void assignStraightAndType(List<Card> player, List<Card> board) {
		if (!player.isEmpty() && !board.isEmpty()) {
			int cmp = StraightComparator.getInstance().compare(player.get(player.size() -1).rank(), board.get(board.size() - 1).rank());
			
			if (cmp <= 0) {
				straight = Collections.unmodifiableList(board);
				type = Type.BOARD;
				secondStraight = Collections.unmodifiableList(player);
				secondStraightType = Type.PLAYER;				
			} else {
				straight = Collections.unmodifiableList(player);
				type = Type.PLAYER;
				secondStraight = Collections.unmodifiableList(board);
				secondStraightType = Type.BOARD;				
			}
		} else if (!player.isEmpty()) {
			straight = Collections.unmodifiableList(player);
			type = Type.PLAYER;
		} else {
			straight = Collections.unmodifiableList(board);
			type = Type.BOARD;
		}
	}

}
