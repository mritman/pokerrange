package pr.domain.poker.player;

import java.util.Collection;

import pr.domain.poker.PokerVenue;
import pr.domain.poker.range.typed.TypedHandRange;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;

public interface OpponentAccountRepository {
	
	/**
	 * The global ranges account is used to store ranges that should be
	 * available to all opponent accounts.
	 */
	static final String GLOBAL_RANGES_ACCOUNT_NAME = "GLOBAL_RANGES_ACCOUNT";
	static final PokerVenue GLOBAL_RANGES_ACCOUNT_VENUE = PokerVenue.MANUAL;

	OpponentAccount getOpponentAccount(String name, PokerVenue venue);
	
	void saveOpponentAccount(OpponentAccount opponentAccount);

	void addRange(Opponent opponent, TypedHandRange range);
	
	void deleteRange(Opponent opponent, TypedHandRange ranges);

	void addGlobalRange(TypedHandRange range);

	Collection<TypedHandRange> getGlobalRanges();
	
	void deleteGlobalRange(TypedHandRange range);

	void refreshRanges(Opponent opponent);

	Opponent getOpponent(String name, PokerTable table, Seat seat);

}
