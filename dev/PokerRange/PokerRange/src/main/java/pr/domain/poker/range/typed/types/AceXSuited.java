package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class AceXSuited extends AceX { 
	
	public AceXSuited() {
		this(Rank.TWO, Rank.KING);
	}
	
	public AceXSuited(Rank lowestRank, Rank highestRank) {
		super(HandTypeEnum.ACE_X_SUITED, lowestRank, highestRank);
	}
	
	public boolean addHand(HoleCards hand) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (hand.card2().rank().intValue() >= getLowestRank().intValue() && hand.card2().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(hand.card1().rank() == Rank.ACE && inRange && hand.isSuited()) {
			return super.addHand(hand);
		}
		
		return false;
	}
}
