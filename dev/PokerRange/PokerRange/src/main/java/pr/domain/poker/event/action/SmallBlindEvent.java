package pr.domain.poker.event.action;

import lombok.Getter;

import org.joda.money.Money;

import com.google.common.base.Objects;

public class SmallBlindEvent implements ActionEvent {
	
	@Getter private final String name;
	@Getter private final Money amount;

	public SmallBlindEvent(String name, Money amount) {
		this.name = name;
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("name", name)
				.add("amount", amount)
				.toString();
	}
	
}
