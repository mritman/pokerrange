package pr.domain.poker.hand.comparator;

import java.util.Comparator;

import pr.domain.poker.hand.Flush;

public class FlushComparator implements Comparator<Flush> {

	private static final FlushComparator flushComparator = new FlushComparator();
	
	public static FlushComparator getInstance() {
		return flushComparator;
	}
	
	@Override
	public int compare(Flush f1, Flush f2) {
		return KickerComparator.compareKickers(f1.getValueCards(), f2.getValueCards());
	}

	public int hashCode(Flush hand) {
		return KickerComparator.getInstance().hashCode(hand.getValueCards());
	}

}
