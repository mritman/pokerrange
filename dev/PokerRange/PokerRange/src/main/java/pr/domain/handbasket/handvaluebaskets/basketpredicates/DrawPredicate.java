package pr.domain.handbasket.handvaluebaskets.basketpredicates;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.StraightDraw;

import com.google.common.base.Predicate;

public class DrawPredicate implements Predicate<Combinations> {

	@Override
	public boolean apply(Combinations combinations) {
		HandValue value = combinations.getHighestHand().getValue();
		if (value == HandValue.FLUSH_DRAW) {
			return true;
		} else if (value == HandValue.STRAIGHT_DRAW) {
			StraightDraw straightDraw = (StraightDraw) combinations.getHighestHand();

			if (straightDraw.getSide() != StraightDraw.Side.INSIDE) {
				return true;
			}
		}
		
		return false;
	}

}
