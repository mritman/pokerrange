package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.Pair;

public class PairComparator implements Comparator<Pair> {

	private static final PairComparator pairComparator = new PairComparator();
	
	public static PairComparator getInstance() {
		return pairComparator;
	}
	
	@Override
	public int compare(Pair p1, Pair p2) {
		int cmp = Integer.compare(p1.getRank().intValue(), p2.getRank().intValue());
		
		if (cmp != 0) {
			return cmp;
		}
		
		return KickerComparator.compareKickers(p1.getKickers(), p2.getKickers());
	}

	public int hashCode(Pair pair) {
		return Objects.hash(pair.getRank(), KickerComparator.getInstance().hashCode(pair.getKickers()));
	}
	
}
