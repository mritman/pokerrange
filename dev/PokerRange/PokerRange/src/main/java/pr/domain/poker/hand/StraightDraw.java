package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;
import pr.domain.poker.hand.comparator.StraightDrawComparator;
import pr.domain.poker.util.CardUtil;

public class StraightDraw extends Hand {
	
	public enum Type {
		BOARD,
		PLAYER
	}
	
	public enum Side {
		INSIDE(0),
		DOUBLE_INSIDE(1),
		OUTSIDE(1);
		
		@Getter
		private int intValue;
		
		private Side(int value) {
			intValue = value;
		}
	}
	
	@Getter private List<Card> straightDraw;
	@Getter private Side side;
	@Getter	private Type type;
	@Getter private Rank highRank;
	@Getter private Rank lowRank;
	
	@Getter private List<Card> secondStraightDraw;
	@Getter private Side secondSide;
	@Getter private Type secondType;
	@Getter private Rank secondHighRank;
	@Getter private Rank secondLowRank;

	public StraightDraw(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHand(holeCards, boardCards);
	}

	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> player = new LinkedList<>();
		List<Card> board = new LinkedList<>();
		Card playerGapCard;
		Card boardGapCard;
		
		if (boardCards.getStreet() == Street.PREFLOP) throw new DoesNotMakeHandException(); 
		
		assertNotStraight(holeCards, boardCards);
		playerGapCard = calculatePlayerStraightDraw(holeCards, boardCards, player);
		boardGapCard = calculateBoardStraightDraw(boardCards, board);
		
		if (player.isEmpty() && board.isEmpty()) {
			throw new DoesNotMakeHandException(HandValue.STRAIGHT_DRAW);
		}
		
		assignFields(player, playerGapCard, board, boardGapCard);
		
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		List<Card> valueCards = getType() == Type.PLAYER ? player : board;
		cards.removeAll(valueCards);
		
		//Copy the list of valuecards so that its order will not be changed by #init().
		valueCards = new LinkedList<>(valueCards);
		
		init(HandValue.STRAIGHT_DRAW, valueCards, cards);
	}

	private Card calculatePlayerStraightDraw(HoleCards holeCards, BoardCards boardCards, List<Card> player) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		cards = CardUtil.removeDuplicateRanks(holeCards, cards);
		Card gapCard = null;
		
		if (holeCards == null) return null;
		
		if (cards.size() < 4) throw new DoesNotMakeHandException(); 
		
		// Check if the cards make a regular straight draw.
		for (int i = cards.size() - 4; i >= 0; i--) {
			List<Card> subList = cards.subList(i, i+4);
			
			gapCard = calculateStraightDraw(subList, cards, player);
			
			if (!player.isEmpty()
					&& (player.contains(holeCards.card1()) || player.contains(holeCards.card2()))) {
				return gapCard;
			} else {
				player.clear();
			}
		}
		
		// If a straight draw was not found and if the cards contain an ace, 
		// see if they make an ace-to-four or -five straight draw.
		if (player.isEmpty()
				&& cards.get(cards.size() - 1).rank() == Rank.ACE) {
			
			List<Card> subList = cards.subList(0, 3);
			subList.add(0, cards.get(cards.size() - 1));
			
			gapCard = calculateStraightDraw(subList, cards, player);
			
			if (!player.isEmpty()
					&& (player.contains(holeCards.card1()) || player
							.contains(holeCards.card2()))) {
				return gapCard;
			} else {
				player.clear();
			}
		}
		
		return null;
	}
	
	private Card calculateBoardStraightDraw(BoardCards boardCards, List<Card> board) {
		if (boardCards.getStreet() != Street.TURN) return null; 
		
		List<Card> boardCardList = boardCards.asList();
		boardCardList = CardUtil.removeDuplicateRanks(null, boardCardList);
		if (boardCardList.size() != 4) return null; 
		
		board.clear();

		return calculateStraightDraw(boardCardList, null, board);
	}

	/**
	 * If the specified cards contain a two and an ace then the ace will be
	 * placed at the beginning of the specified list.
	 * 
	 * @param cards
	 *            a list of four cards assumed to be sorted by rank.
	 * @param allCards
	 * 			  the hole- and board cards
	 * @param straightDraw
	 *            the list of cards that will contain the straight draw if the
	 *            cards make one, empty otherwise.
	 * 
	 * @return the the card on the left side of the highest gap in the
	 *         straight draw, if any, or null if the cards do not make a straight
	 *         draw or an outside straight draw
	 */
	private Card calculateStraightDraw(List<Card> cards, List<Card> allCards, List<Card> straightDraw) {
		Card gapCard = null;
		
		if (cards.size() != 4) {
			return null;
		}
		
		Card lastCard = null;
		int gapCount = 0;
		
		// If the lowest card is a 2 and the highest an ace then place the ace at the beginning of the list.
		if (isAceLow(cards)) {
			Card ace = cards.remove(cards.size() - 1);
			cards.add(0, ace);
		}
		
		for (Card card : cards) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}

			if (isOneRankGap(cards, lastCard, card)) { 
				gapCount++;
				if (gapCount > 1) return null;
				gapCard = lastCard;
				lastCard = card;
			} else if (isNextRank(card, lastCard, cards)) {
				lastCard = card;
			} else {
				return null;
			}
		}
		
		straightDraw.addAll(cards);
		if (gapCount > 0 && allCards != null) {
			paddBottom(gapCard, allCards, straightDraw);
		}
		
		return gapCard;
	}

	private boolean isNextRank(Card card, Card lastCard, List<Card> cards) {
		return card.rank().intValue() == lastCard.rank().intValue() + 1 || cards.indexOf(lastCard) == 0 && lastCard.rank() == Rank.ACE && card.rank() == Rank.TWO;
	}

	private boolean isOneRankGap(List<Card> cards, Card lastCard, Card card) {
		return card.rank().intValue() == lastCard.rank().intValue() + 2 || cards.indexOf(lastCard) == 0 && lastCard.rank() == Rank.ACE && card.rank() == Rank.THREE;
	}

	/**
	 * @param cards list of cards sorted by rank in ascending order.
	 * @return returns true if the specified list of cards starts with a 2 and contains an ace, false otherwise
	 */
	private boolean isAceLow(List<Card> cards) {
		return cards.get(0).rank() == Rank.TWO && cards.get(cards.size() -1).rank() == Rank.ACE;
	}
	
	/**
	 * <p>
	 * This method adds any cards to the straight draw in any of the following
	 * scenarios:
	 * </p>
	 * 
	 * <ul>
	 * <li>XXXX_X</li>
	 * <li>X_XXX_X</li>
	 * <li>XXXX_XX</li>
	 * </ul>
	 * 
	 * <p>
	 * In the above scenarios (double draws) a fifth and possibly sixth card can
	 * be added to the bottom end of the straight draw. The number of cards to
	 * add will be a minimum of 0 and a maximum of 2 cards, and will be on the
	 * bottom end of the straight because the top four cards will already have
	 * been recognized as a straight by
	 * {@link StraightDraw#calculateStraightDraw}.
	 * </p>
	 * 
	 * @param gapPosition
	 *            the index of the card that is on the right side of the first
	 *            gap in the straight.
	 * @param allCards
	 *            the hole- and board cards assumed to be sorted by rank
	 * @param straightDraw
	 *            the cards that form the straight draw
	 */
	private void paddBottom(Card gapCard, List<Card> allCards, List<Card> straightDraw) {
		int gapPosition = straightDraw.indexOf(gapCard);
		
		// XXXX_XX
		if (gapPosition == 1) {
			Card bottomCard = straightDraw.get(0);
			int index = allCards.indexOf(bottomCard);
			
			Card card1 = null;
			Card card2 = null;
			
			if (index - 1 >= 0) card1 = allCards.get(index - 1);
			if (index - 2 >= 0) card2 = allCards.get(index - 2);
			
			if (twoCardsCanBeAddedToBottom(bottomCard, card1, card2)) {
				straightDraw.add(0, card1);
				straightDraw.add(0, card2);
			}
		}
		
		// XXXX_X
		// X_XXX_X
		if (gapPosition == 2) {
			Card bottomCard = straightDraw.get(0);
			int index = allCards.indexOf(bottomCard);
			
			Card card = null;
			
			if (index - 1 >= 0) {
				card = allCards.get(index - 1);
			} else {
				card = CardUtil.getAce(allCards);
			}
			
			if (card == null) return;
			
			if (cardCanBeAddedToBottom(bottomCard, card)) {
				straightDraw.add(0, card);
			}
		}

	}

	private boolean twoCardsCanBeAddedToBottom(Card bottomCard, Card card1,
			Card card2) {
		return card1 != null
				&& card2 != null 
				&& card1.rank().intValue() == bottomCard.rank().intValue() - 1 
				&& card2.rank().intValue() == card1.rank().intValue() - 1;
	}
	
	private boolean cardCanBeAddedToBottom(Card bottomCard, Card card) {
		return card.rank().intValue() == bottomCard.rank().intValue() - 1 
				|| card.rank().intValue() == bottomCard.rank().intValue() - 2
				|| card.rank() == Rank.ACE && bottomCard.rank() == Rank.THREE;
	}

	private void assignFields(List<Card> player, Card playerGapCard, List<Card> board, Card boardGapCard) {
		Side boardSide = null;
		Rank boardRank = null;
		Rank lowBoardRank = null;
		
		Side playerSide = null;
		Rank playerRank = null;
		Rank lowPlayerRank = null;
		
		if (!player.isEmpty()) {
			playerSide = calculateSide(player, playerGapCard);
			List<Rank> ranks = calculateRanks(player, playerGapCard, playerSide);
			playerRank = ranks.get(0);
			if (ranks.size() > 1) {
				lowPlayerRank = ranks.get(1);
			}
		}
		
		if (!board.isEmpty()) {
			boardSide = calculateSide(board, boardGapCard);
			List<Rank> ranks = calculateRanks(board, boardGapCard, boardSide);
			boardRank = ranks.get(0);
			if (ranks.size() > 1) {
				lowBoardRank = ranks.get(1);
			}
		}
		
		if (!player.isEmpty() && !board.isEmpty()) {
			int cmp = StraightDrawComparator.getInstance().compareSide(playerSide, boardSide);
			if (cmp == 0) {
				playerRank = player.get(player.size() - 1).rank();
				boardRank = board.get(board.size() - 1).rank();
				cmp = Integer.compare(playerRank.intValue(), boardRank.intValue());
			}
			
			if (cmp <= 0) {
				assignFirst(board, Type.BOARD, boardSide, boardRank, lowBoardRank);
				assignSecond(player, Type.PLAYER, playerSide, playerRank, lowPlayerRank);
			} else {
				assignFirst(player, Type.PLAYER, playerSide, playerRank, lowPlayerRank);			
				assignSecond(board, Type.BOARD, boardSide, boardRank, lowBoardRank);
			}
		} else if (!player.isEmpty()) {
			assignFirst(player, Type.PLAYER, playerSide, playerRank, lowPlayerRank);
		} else {
			assignFirst(board, Type.BOARD, boardSide, boardRank, lowBoardRank);			
		}
	}
	
	private void assignFirst(List<Card> straightDraw, Type type, Side side, Rank highRank, Rank lowRank) {
		this.straightDraw = Collections.unmodifiableList(straightDraw);
		this.type = type;
		this.side = side;
		this.highRank = highRank;
		this.lowRank = lowRank;
	}
	
	private void assignSecond(List<Card> straightDraw, Type type, Side side, Rank highRank, Rank lowRank) {
		this.secondStraightDraw = Collections.unmodifiableList(straightDraw);
		this.secondType = type;
		this.secondSide = side;
		this.secondHighRank = highRank;
		this.secondLowRank = lowRank;
	}
	
	private static Side calculateSide(List<Card> straightDraw, Card gapCard) {
		boolean hasGap = gapCard != null;
		Side side;

		if (hasGap && straightDraw.size() > 4) {
			side = Side.DOUBLE_INSIDE;
		} else if (straightDraw.get(0).rank() == Rank.ACE
				|| straightDraw.get(straightDraw.size() - 1).rank() == Rank.ACE
				|| hasGap && straightDraw.size() == 4) {
			side = Side.INSIDE;
		} else {
			side = Side.OUTSIDE;
		}

		return side;
	}
	
	/**
	 * @param gapCard
	 *            the card on the left side of the gap, if any, or null
	 */
	private List<Rank> calculateRanks(List<Card> straightDraw, Card gapCard, Side side) {
		List<Rank> ranks = new LinkedList<Rank>();
		
		if (side == Side.INSIDE) {
			Rank rank = straightDraw.get(straightDraw.size() - 1).rank();
			
			if (straightDraw.get(0).rank() == Rank.ACE) {
				rank = Rank.parseRank(rank.intValue() + 1);
			}
			
			ranks.add(rank);
		} else if (side == Side.OUTSIDE) {
			Rank low = straightDraw.get(straightDraw.size() - 1).rank();
			Rank high = Rank.parseRank(low.intValue() + 1);
			Collections.addAll(ranks, high, low);
		} else {
			Rank high = straightDraw.get(straightDraw.size() - 1).rank();
			Rank low = gapCard.rank();
			
			Collections.addAll(ranks, high, low);
		}
		
		return ranks;
	}

	private void assertNotStraight(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);
		cards = CardUtil.removeDuplicateRanks(null, cards);
		
		if (boardCards.getStreet() == Street.RIVER) {
			throw new DoesNotMakeHandException(HandValue.STRAIGHT_DRAW);
		}
		
		// If the lowest card is a 2 and the highest an ace then copy the ace to the beginning of the list.
		if (isAceLow(cards)) {
			Card ace = cards.get(cards.size() - 1);
			cards.add(0, ace);
		}
		
		for (int i = cards.size() - 5; i >= 0; i--) {
			List<Card> subList = cards.subList(i, i+5);
			
			LinkedList<Card> result = new LinkedList<>();
			Straight.calculateStraight(subList, result);
			if (!result.isEmpty()) {
				throw new DoesNotMakeHandException(HandValue.STRAIGHT_DRAW);
			}
		}
	}

}
