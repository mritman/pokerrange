package pr.domain.poker.hand.comparator;

import java.util.Comparator;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.Flush;
import pr.domain.poker.hand.FlushDraw;
import pr.domain.poker.hand.FourOfAKind;
import pr.domain.poker.hand.FullHouse;
import pr.domain.poker.hand.Hand;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.HandValueComparator;
import pr.domain.poker.hand.HighCard;
import pr.domain.poker.hand.Pair;
import pr.domain.poker.hand.Straight;
import pr.domain.poker.hand.StraightDraw;
import pr.domain.poker.hand.StraightFlush;
import pr.domain.poker.hand.ThreeOfAKind;
import pr.domain.poker.hand.TwoPair;

public class HandComparator implements Comparator<Hand> {
	
	private static final HandComparator handComparator = new HandComparator();
	
	public static HandComparator getInstance() {
		return handComparator;
	}
	
	public static int compareByHand(Combinations hand1, Combinations hand2) {
		return getInstance().compare(hand1.getHighestHand(), hand2.getHighestHand());
	}
	
	public static int compareByNonDrawHand(Combinations hand1, Combinations hand2) {
		return getInstance().compare(hand1.getHighestNonDrawHand(), hand2.getHighestNonDrawHand());
	}
	
	@Override
	public int compare(Hand hand1, Hand hand2) {
		int cmp = HandValueComparator.getInstance().compare(hand1.getValue(), hand2.getValue());
		if (cmp != 0)  return cmp; 
		
		return compareEqualValuedHands(hand1, hand2, hand1.getValue());
	}

	private static int compareEqualValuedHands(Hand hand1, Hand hand2, HandValue value) {
		switch (value) {
			case HIGH_CARD:
				return HighCardComparator.getInstance().compare((HighCard) hand1, (HighCard) hand2);
			case PAIR:
				return PairComparator.getInstance().compare((Pair) hand1, (Pair) hand2);
			case TWO_PAIR:
				return TwoPairComparator.getInstance().compare((TwoPair) hand1, (TwoPair) hand2);
			case THREE_OF_A_KIND:
				return ThreeOfAKindComparator.getInstance().compare((ThreeOfAKind) hand1, (ThreeOfAKind) hand2);
			case STRAIGHT:
				return StraightComparator.getInstance().compare((Straight) hand1, (Straight) hand2);
			case FLUSH:
				return FlushComparator.getInstance().compare((Flush) hand1, (Flush) hand2);
			case FULL_HOUSE:
				return FullHouseComparator.getInstance().compare((FullHouse) hand1, (FullHouse) hand2);
			case FOUR_OF_A_KIND:
				return FourOfAKindComparator.getInstance().compare((FourOfAKind) hand1, (FourOfAKind) hand2);
			case STRAIGHT_FLUSH:
				return StraightFlushComparator.getInstance().compare((StraightFlush) hand1, (StraightFlush) hand2);
			case STRAIGHT_DRAW:
				return StraightDrawComparator.getInstance().compare((StraightDraw) hand1, (StraightDraw) hand2);
			case FLUSH_DRAW:
				return FlushDrawComparator.getInstance().compare((FlushDraw) hand1, (FlushDraw) hand2);
			default:
				// This should never be reached. If it is a case statement might be missing for one of the HandValue values.
				throw new IllegalStateException();
		}
	}


	public int hashCode(Hand hand) {
		switch (hand.getValue()) {
		case HIGH_CARD:
			return HighCardComparator.getInstance().hashCode((HighCard) hand);
		case PAIR:
			return PairComparator.getInstance().hashCode((Pair) hand);
		case TWO_PAIR:
			return TwoPairComparator.getInstance().hashCode((TwoPair) hand);
		case THREE_OF_A_KIND:
			return ThreeOfAKindComparator.getInstance().hashCode((ThreeOfAKind) hand);
		case STRAIGHT:
			return StraightComparator.getInstance().hashCode((Straight) hand);
		case FLUSH:
			return FlushComparator.getInstance().hashCode((Flush) hand);
		case FULL_HOUSE:
			return FullHouseComparator.getInstance().hashCode((FullHouse) hand);
		case FOUR_OF_A_KIND:
			return FourOfAKindComparator.getInstance().hashCode((FourOfAKind) hand);
		case STRAIGHT_FLUSH:
			return StraightFlushComparator.getInstance().hashCode((StraightFlush) hand);
		case STRAIGHT_DRAW:
			return StraightDrawComparator.getInstance().hashCode((StraightDraw) hand);
		case FLUSH_DRAW:
			return FlushDrawComparator.getInstance().hashCode((FlushDraw) hand);
		default:
			// This should never be reached. If it is a case statement might be missing for one of the HandValue values.
			throw new IllegalStateException();
		}
	}
	
}
