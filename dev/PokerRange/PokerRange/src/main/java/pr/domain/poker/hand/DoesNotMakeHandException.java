package pr.domain.poker.hand;

import lombok.Getter;

public class DoesNotMakeHandException extends Exception {

	private static final long serialVersionUID = 1L;

	@Getter
	private HandValue handValue;

	public DoesNotMakeHandException() {}
	
	public DoesNotMakeHandException(HandValue handValue) {
		this.handValue = handValue;
	}

}
