package pr.domain.poker.event.seat;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.table.Seat;

public class SeatOccupiedEvent extends SeatPropertyEvent {
	@Getter private final Seat seat;
	@Getter private final String name;
	
	public SeatOccupiedEvent(Seat seat, String name) {
		super();
		this.seat = seat;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("seat", seat).add("name", name).toString();
	}

	@Override
	public SeatPropertyEvent setSeat(Seat seat) {
		return new SeatOccupiedEvent(seat, name);
	}
}
