package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class AceX extends HandType {
	
	public AceX() {
		this(Rank.TWO, Rank.KING); 
	}
	
	public AceX(Rank lowestRank, Rank highestRank) {
		this(HandTypeEnum.ACE_X, lowestRank, highestRank);
	}
	
	protected AceX(HandTypeEnum typeName, Rank lowestRank, Rank highestRank) {
		super(typeName, Rank.TWO, Rank.KING, lowestRank, highestRank);
	}
	
	public boolean addHand(HoleCards hand) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (hand.card2().rank().intValue() >= getLowestRank().intValue() && hand.card2().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(hand.card1().rank() == Rank.ACE && inRange) {
			return super.addHand(hand);
		}
		
		return false;
	}
}
