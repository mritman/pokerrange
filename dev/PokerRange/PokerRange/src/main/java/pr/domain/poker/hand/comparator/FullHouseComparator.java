package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.FullHouse;

public class FullHouseComparator implements Comparator<FullHouse> {

	private static final FullHouseComparator fullHouseComparator = new FullHouseComparator();
	
	public static FullHouseComparator getInstance() {
		return fullHouseComparator;
	}
	
	@Override
	public int compare(FullHouse fh1, FullHouse fh2) {
		int cmp = Integer.compare(fh1.getThreeOfAKindRank().intValue(), fh2.getThreeOfAKindRank().intValue());
		
		if (cmp != 0) {
			return cmp;
		} 
			
		return Integer.compare(fh1.getPairRank().intValue(), fh2.getPairRank().intValue());
	}

	public int hashCode(FullHouse hand) {
		return Objects.hash(hand.getThreeOfAKindRank(), hand.getPairRank());
	}

}
