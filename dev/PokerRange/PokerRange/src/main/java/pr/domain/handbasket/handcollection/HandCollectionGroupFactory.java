package pr.domain.handbasket.handcollection;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pr.domain.handbasket.BasketElement;
import pr.domain.poker.hand.HandValue;

public final class HandCollectionGroupFactory {
	
	private HandCollectionGroupFactory() {};
	
	public static void groupBasketElements(Set<BasketElement> basketElements, HandValue handValue) {
		List<HandCollection> handCollections = new LinkedList<HandCollection>();
		
		Iterator<BasketElement> itr = basketElements.iterator();
		
		while (itr.hasNext()) {
			BasketElement basketElement = itr.next();
			
			if (basketElement instanceof HandCollection) {
				HandCollection handCollection = (HandCollection) basketElement;
				
				if (handCollection.getHandValue() == handValue) {
					itr.remove();
					handCollections.add(handCollection);
				}
			}
		}
		
		if (!handCollections.isEmpty()) {
			HandCollectionGroup handCollectionGroup = new HandCollectionGroup(handCollections);
			basketElements.add(handCollectionGroup);
		}
	}

}
