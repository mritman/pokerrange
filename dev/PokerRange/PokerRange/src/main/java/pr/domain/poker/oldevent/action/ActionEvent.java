package pr.domain.poker.oldevent.action;

import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.table.PokerTable;


public abstract class ActionEvent extends PokerTableEvent {

	protected ActionEvent(PokerTable table) {
		super(table);
	}

}
