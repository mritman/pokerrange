package pr.domain.poker.oldevent;

import lombok.Getter;
import pr.domain.poker.table.PokerTable;

import com.google.common.base.Objects;

public abstract class PokerTableEvent {
	
	@Getter
	private final PokerTable table;
	
	protected PokerTableEvent(PokerTable table) {
		this.table = table;
	}
	
	public String toString() {
		return Objects.toStringHelper(getClass()).toString();
	}

}
