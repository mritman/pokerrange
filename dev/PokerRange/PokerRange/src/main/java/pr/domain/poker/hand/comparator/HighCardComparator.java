package pr.domain.poker.hand.comparator;

import java.util.Comparator;

import pr.domain.poker.hand.HighCard;

public class HighCardComparator implements Comparator<HighCard> {

	private static final HighCardComparator highCardComparator = new HighCardComparator();
	
	public static HighCardComparator getInstance() {
		return highCardComparator;
	}
	
	@Override
	public int compare(HighCard h1, HighCard h2) {
		return KickerComparator.compareKickers(h1.getNonValueCards(), h2.getNonValueCards());
	}

	public int hashCode(HighCard highCard) {
		return KickerComparator.getInstance().hashCode(highCard.getNonValueCards());
	}

}
