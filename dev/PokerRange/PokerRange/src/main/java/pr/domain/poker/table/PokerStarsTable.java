package pr.domain.poker.table;

import pr.domain.poker.PokerVenue;

public class PokerStarsTable extends PokerTable {
	
	PokerStarsTable(String name, PokerVenue venue) {
		super(name, venue);
	}
	
}
