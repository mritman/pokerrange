package pr.domain.poker.hand;

import java.util.Comparator;


public class HandValueComparator implements Comparator<HandValue> {

	private static final HandValueComparator handValueComparator = new HandValueComparator();
	
	public static HandValueComparator getInstance() {
		return handValueComparator;
	}
	
	@Override
	public int compare(HandValue value1, HandValue value2) {
		return Integer.compare(value1.intValue(), value2.intValue());
	}

}
