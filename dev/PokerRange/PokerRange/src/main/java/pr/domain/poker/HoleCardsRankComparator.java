package pr.domain.poker;

import java.util.Comparator;

import pr.domain.cards.RankComparator;

import com.google.common.collect.ComparisonChain;

public class HoleCardsRankComparator implements Comparator<HoleCards> {

	private static final HoleCardsRankComparator holeCardsRankComparator = new HoleCardsRankComparator();
	
	public static HoleCardsRankComparator getInstance() {
		return holeCardsRankComparator;
	}
	
	@Override
	public int compare(HoleCards hand1, HoleCards hand2) {
		return ComparisonChain
				.start()
				.compare(hand1.card1().rank(), hand2.card1().rank(),
						RankComparator.getInstance())
				.compare(hand1.card2().rank(), hand2.card2().rank(),
						RankComparator.getInstance()).result();
	}

}
