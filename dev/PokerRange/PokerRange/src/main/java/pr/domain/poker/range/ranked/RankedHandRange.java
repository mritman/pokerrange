package pr.domain.poker.range.ranked;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import pr.domain.gamestate.GameState;
import pr.domain.poker.HoleCards;
import pr.domain.poker.range.HandRange;

public class RankedHandRange implements HandRange {

	public static final int NR_OF_RANKS = 169;
	
	@Getter
	private String name;
	
	private final double rangePercentage;
	private final Map<HoleCardsRank, Set<HoleCards>> rankedHoleCardsMap;
	private final Set<HoleCards> handsInRange;
	
	public RankedHandRange(String name, double rangePercentage, Map<HoleCardsRank, Set<HoleCards>> rankedHoleCardsMap) {
		this.name = name;
		this.rangePercentage = rangePercentage;
		this.rankedHoleCardsMap = rankedHoleCardsMap;
		handsInRange = new HashSet<>();
	}
	
	@Override
	public void calculateRange(GameState gameState) {
		double rangeSize = HoleCards.NR_HOLE_CARD_COMBINATIONS * rangePercentage;
		
		handsInRange.clear();
		
		for (Set<HoleCards> holeCards : rankedHoleCardsMap.values()) {
			handsInRange.addAll(holeCards);
			
			if (handsInRange.size() >= rangeSize) {
				break;
			}
		}
		
		HoleCards.removeHandsFromSet(handsInRange, gameState);
	}
	
	@Override
	public Set<HoleCards> getHandsInRange() {
		return Collections.unmodifiableSet(handsInRange);
	}
	
}
