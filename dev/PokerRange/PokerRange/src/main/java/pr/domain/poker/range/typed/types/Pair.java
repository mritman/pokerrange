package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class Pair extends HandType {
	
	public Pair() {
		this(Rank.TWO, Rank.ACE);
	}
	
	public Pair(Rank lowestRank, Rank highestRank) {
		super(HandTypeEnum.PAIR, lowestRank, highestRank);
	}
	
	public boolean addHand(HoleCards holeCards) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (holeCards.card1().rank().intValue() >= getLowestRank().intValue() && holeCards.card1().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(holeCards.isPair() && inRange) {
			return super.addHand(holeCards);
		}
		
		return false;
	}

}
