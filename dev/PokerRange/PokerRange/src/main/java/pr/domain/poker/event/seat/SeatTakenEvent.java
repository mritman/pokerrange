package pr.domain.poker.event.seat;

import lombok.Getter;
import pr.domain.poker.table.Seat;

import com.google.common.base.Objects;

public class SeatTakenEvent extends SeatPropertyEvent {

	@Getter private final Seat seat;
	@Getter private final String name;
	
	public SeatTakenEvent(Seat seat, String name) {
		this.seat = seat;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass())
				.add("seat", seat)
				.add("name", name)
				.toString();
	}

	@Override
	public SeatPropertyEvent setSeat(Seat seat) {
		return new SeatTakenEvent(seat, name);
	}

}
