package pr.domain.handbasket.handvaluebaskets.basketpredicates;

import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandValue;

import com.google.common.base.Predicate;

public class WillFoldPredicate implements Predicate<Combinations> {

	@Override
	public boolean apply(Combinations combinations) {
		
		if (combinations.getHands().size() == 1 && combinations.getHighestHand().getValue() == HandValue.HIGH_CARD) {
			return true;
		}
		
		return false;
	}

}
