package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import pr.domain.cards.Card;
import pr.domain.cards.CardSuitComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

public class HighCard extends Hand {
	
	protected HighCard(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		super(holeCards, boardCards);
		
		calculateHand(holeCards, boardCards);
	}
	
	public Card getHighCard() {
		return getNonValueCards().get(getNonValueCards().size() -1);
	}
	
	private void calculateHand(HoleCards holeCards, BoardCards boardCards) throws DoesNotMakeHandException {
		List<Card> cards = CardUtil.asList(holeCards, boardCards);

		if (hasDuplicatesOrIsStraight(cards) || makesFlush(cards)) {
			throw new DoesNotMakeHandException(HandValue.HIGH_CARD);
		}

		init(HandValue.HIGH_CARD, Collections.<Card>emptyList(), cards);
	}
	
	private boolean hasDuplicatesOrIsStraight(List<Card> cards) {
		Card lastCard = null;
		int consecutiveRanks = 1;
		
		for (Card card : cards) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}
			
			if (card.rank() == lastCard.rank()) {
				return true;
			}
			
			if (card.rank().intValue() == lastCard.rank().intValue() + 1 ||
					cards.indexOf(card) == 1 && card.rank() == Rank.TWO && lastCard.rank() == Rank.ACE) {
				
				consecutiveRanks++;
				
				if (consecutiveRanks >= 5) {
					return true;
				}
			} else {
				consecutiveRanks = 1;
			}
			
			lastCard = card;
		}
		
		return false;
	}
	
	private boolean makesFlush(List<Card> cards) {
		Collections.sort(cards, CardSuitComparator.getInstance());
		
		Card lastCard = null;
		int consecutiveSuits = 1;
		
		for (Card card : cards) {
			if (lastCard == null) {
				lastCard = card;
				continue;
			}
			
			if (card.suit() == lastCard.suit()) {
				consecutiveSuits++;
				if (consecutiveSuits >= 5) {
					return true;
				}
			} else {
				consecutiveSuits = 1;
			}
			
			lastCard = card;
		}
		
		return false;
	}

}
