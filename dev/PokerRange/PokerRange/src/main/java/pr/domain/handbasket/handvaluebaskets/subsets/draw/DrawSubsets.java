package pr.domain.handbasket.handvaluebaskets.subsets.draw;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.base.Predicate;

public enum DrawSubsets implements HandBasketSubsetKey {
	OUTSIDE_STRAIGHT_DRAW(0, "Outside straight draw", new OutsideStraightDrawPredicate()),
	FLUSH_DRAW(1, "Flush Draw", new FlushDrawPredicate());
	
	private int intValue;

	@Getter
	private Predicate<BasketElement> predicate;
	
	@Getter
	private String name; 
	
	private DrawSubsets(int intValue, String name, Predicate<BasketElement> predicate) {
		this.intValue = intValue;
		this.predicate = predicate;
		this.name = name;
	}
	
	public int intValue() {
		return intValue;
	}
	
}
