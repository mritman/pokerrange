package pr.domain.poker.action;

import lombok.Getter;

public enum ActionType {
	POST_SMALL_BLIND(0),
	POST_BIG_BLIND(1),
	FOLD(2),
	CHECK(3),
	CALL(4),
	BET(5),
	RAISE(6);
	
	@Getter
	private int intValue;
	
	private ActionType(int intValue) {
		this.intValue = intValue;
	}
	
	public boolean isBet() {
		return this == BET;
	}
	
	public boolean isPostSmallBlind() {
		return this == POST_SMALL_BLIND;
	}
	
	public boolean isPostBigBlind() {
		return this == POST_BIG_BLIND;
	}
	
	public boolean isFold() {
		return this == FOLD;
	}
	
	public boolean isCheck() {
		return this == CHECK;
	}
	
	public boolean isCall() {
		return this == CALL;
	}
	
	public boolean isRaise() {
		return this == RAISE;
	}
	
}
