package pr.domain.poker.oldevent.seat;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.table.PokerTable;

public class SeatLeftByOpponentEvent extends SeatLeftEvent {

	@Getter
	private final Opponent opponent;
	
	public SeatLeftByOpponentEvent(PokerTable table, Opponent opponent) {
		super(table);
		this.opponent = opponent;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass()).add("opponent", opponent.getName()).toString();
	}

}
