package pr.domain.handbasket.strengthbaskets;

import com.google.common.base.Predicate;

import pr.domain.handbasket.FoldedPredicate;
import pr.domain.handbasket.HandBasketKey;
import pr.domain.handbasket.strengthbaskets.predicates.MightFoldPredicate;
import pr.domain.handbasket.strengthbaskets.predicates.WillFoldPredicate;
import pr.domain.handbasket.strengthbaskets.predicates.WontFoldPredicate;
import pr.domain.poker.Combinations;
import lombok.Getter;

public enum StrengthBasketEnum implements HandBasketKey {
	FOLDED("F", new FoldedPredicate()),
	WILL_FOLD("WF", new WillFoldPredicate()),
	MIGHT_FOLD("MF", new MightFoldPredicate()),
	WONT_FOLD("NF", new WontFoldPredicate());
	
	@Getter
	private String shortName;
	
	@Getter
	private Predicate<Combinations> predicate;
	
	private StrengthBasketEnum(String shortName, Predicate<Combinations> predicate) {
		this.shortName = shortName;
		this.predicate = predicate;
	}

	@Override
	public String getName() {
		return toString();
	}

}
