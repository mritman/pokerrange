package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.TwoPair;

public class TwoPairComparator implements Comparator<TwoPair> {

	private static final TwoPairComparator twoPairComparator = new TwoPairComparator();
	
	public static TwoPairComparator getInstance() {
		return twoPairComparator;
	}
	
	@Override
	public int compare(TwoPair p1, TwoPair p2) {
		// Compare high pairs.
		int cmp = Integer.compare(p1.getFirstPairRank().intValue(), p2.getFirstPairRank().intValue());
		if (cmp != 0) return cmp;
		
		cmp = Integer.compare(p1.getSecondPairRank().intValue(), p2.getSecondPairRank().intValue());
		if (cmp != 0) return cmp;
		
		return KickerComparator.compareKickers(p1.getKickers(), p2.getKickers());
	}

	public int hashCode(TwoPair twoPair) {
		return Objects.hash(twoPair.getFirstPairRank(), 
				twoPair.getSecondPairRank(),
				KickerComparator.getInstance().hashCode(twoPair.getKickers()));
	}

}
