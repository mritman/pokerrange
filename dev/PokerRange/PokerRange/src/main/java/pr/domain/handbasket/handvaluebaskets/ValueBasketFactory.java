package pr.domain.handbasket.handvaluebaskets;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasket;
import pr.domain.handbasket.HandBasketFactory;
import pr.domain.handbasket.HandBasketSubsetKey;
import pr.domain.handbasket.handvaluebaskets.subsets.draw.DrawSubsets;
import pr.domain.handbasket.handvaluebaskets.subsets.pair.PairSubsets;
import pr.domain.handbasket.handvaluebaskets.subsets.twopairplus.TwoPairPlusSubsets;
import pr.domain.handbasket.handvaluebaskets.subsets.weakpair.WeakPairSubsets;
import pr.domain.handbasket.handvaluebaskets.subsets.willfold.WillFoldSubsets;

public class ValueBasketFactory extends HandBasketFactory {
	
	public ValueBasketFactory() {
		super(ValueBasketEnum.values(), ValueBasketEnum.FOLDED);
	}

	@Override
	protected HandBasket createHandBasket(String name, Set<BasketElement> basketElements) {
		List<HandBasketSubsetKey> subsets = null;
		
		switch (ValueBasketEnum.valueOf(name)) {
		case WEAK_PAIR:
			subsets = Arrays.<HandBasketSubsetKey>asList(WeakPairSubsets.values());
			break;
		case PAIR:
			subsets = Arrays.<HandBasketSubsetKey>asList(PairSubsets.values());
			break;
		case TWO_PAIR_PLUS:
			subsets = Arrays.<HandBasketSubsetKey>asList(TwoPairPlusSubsets.values());
			break;
		case DRAW:
			subsets = Arrays.<HandBasketSubsetKey>asList(DrawSubsets.values());
			break;
		case WILL_FOLD:
			subsets = Arrays.<HandBasketSubsetKey>asList(WillFoldSubsets.values());
			break;
		default:
			break;
		}
		
		return new ValueHandBasket(name, basketElements, subsets);
	}

}
