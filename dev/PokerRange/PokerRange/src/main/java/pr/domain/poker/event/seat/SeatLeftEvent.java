package pr.domain.poker.event.seat;

import lombok.Getter;

import com.google.common.base.Objects;

public class SeatLeftEvent implements SeatEvent {

	@Getter private final String name;
	
	public SeatLeftEvent(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass()).add("name", name).toString();
	}

}
