package pr.domain.poker.range.typed.types;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;

public class KingX extends HandType {

	public KingX() {
		this(Rank.TWO, Rank.QUEEN);
	}
	
	public KingX(Rank lowestRank, Rank highestRank) {
		super(HandTypeEnum.KING_X, Rank.TWO, Rank.QUEEN, lowestRank, highestRank);
	}
	
	public boolean addHand(HoleCards holeCards) {
		boolean inRange = false;
		
		if (!isEnabled()) return false;
		
		if (holeCards.card2().rank().intValue() >= getLowestRank().intValue() && holeCards.card2().rank().intValue() <= getHighestRank().intValue()) {
			inRange = true;
		}
		
		if(holeCards.card1().rank() == Rank.KING && inRange) {
			return super.addHand(holeCards);
		}
		
		return false;
	}

}
