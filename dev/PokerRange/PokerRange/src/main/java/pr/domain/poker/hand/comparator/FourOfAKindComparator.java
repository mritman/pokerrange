package pr.domain.poker.hand.comparator;

import java.util.Comparator;
import java.util.Objects;

import pr.domain.poker.hand.FourOfAKind;

public class FourOfAKindComparator implements Comparator<FourOfAKind> {

	private static final FourOfAKindComparator fourOfAKindComparator = new FourOfAKindComparator();
	
	public static FourOfAKindComparator getInstance() {
		return fourOfAKindComparator;
	}
	
	@Override
	public int compare(FourOfAKind hand1, FourOfAKind hand2) {
		int cmp = Integer.compare(hand1.getRank().intValue(), hand2.getRank().intValue());
		
		if (cmp != 0) {
			return cmp;
		} else {
			return KickerComparator.compareKickers(hand1.getKickers(), hand2.getKickers());
		}
	}

	public int hashCode(FourOfAKind hand) {
		return Objects.hash(hand.getRank(), KickerComparator.getInstance().hashCode(hand.getKickers()));
	}

}
