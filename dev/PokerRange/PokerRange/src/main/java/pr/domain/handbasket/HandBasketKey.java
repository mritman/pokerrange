package pr.domain.handbasket;

import pr.domain.poker.Combinations;

import com.google.common.base.Predicate;

public interface HandBasketKey {

	String getName();

	Predicate<Combinations> getPredicate();

}
