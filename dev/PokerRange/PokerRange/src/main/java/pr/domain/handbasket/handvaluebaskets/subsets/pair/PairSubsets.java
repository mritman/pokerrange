package pr.domain.handbasket.handvaluebaskets.subsets.pair;

import lombok.Getter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasketSubsetKey;

import com.google.common.base.Predicate;

public enum PairSubsets implements HandBasketSubsetKey  {
	SECOND_PAIR(0, "2nd pair", new SecondPairPredicate()),
	FIRST_AND_A_HALF_PAIR(1, "1,5th pair", new FirstAndAHalfPairPredicate()),
	TOP_PAIR(2, "Top pair", new TopPairPredicate()),
	OVER_PAIR(3, "Over pair", new OverPairPredicate()),
	POCKET_PAIR(4, "Pocket pair", new PocketPairPredicate());
	
	private int intValue;
	
	@Getter
	private Predicate<BasketElement> predicate;
	
	@Getter
	private String name;

	private PairSubsets(int intValue, String name, Predicate<BasketElement> predicate) {
		this.intValue = intValue;
		this.predicate = predicate;
		this.name = name;
	}
	
	public int intValue() {
		return intValue;
	}
	
}
