package pr.infrastructure.tableobserver.event;

import java.util.Set;

import lombok.Getter;
import pr.domain.poker.table.Seat;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;

public class SeatsInHandObserverEvent implements ObserverEvent {

	@Getter
	private final Set<Seat> seats;
	
	public SeatsInHandObserverEvent(Set<Seat> seats) {
		this.seats = ImmutableSet.copyOf(seats);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seats", seats)
				.toString();
	}
	
}
