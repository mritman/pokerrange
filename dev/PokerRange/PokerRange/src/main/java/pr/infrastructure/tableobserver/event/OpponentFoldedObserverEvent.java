package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.table.Seat;

public class OpponentFoldedObserverEvent implements ObserverEvent {

	@Getter
	private final Seat seat;
	
	public OpponentFoldedObserverEvent(Seat seat) {
		this.seat = seat;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("seat", seat).toString();
	}
	
}
