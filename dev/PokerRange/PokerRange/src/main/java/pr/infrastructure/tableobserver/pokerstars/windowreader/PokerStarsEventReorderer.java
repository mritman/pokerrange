package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import pr.domain.poker.action.Action;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.game.Game;
import pr.domain.poker.game.GameException;
import pr.domain.poker.table.PokerTableKey;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.event.ActionObserverEvent;
import pr.infrastructure.tableobserver.event.BoardCardsObserverEvent;
import pr.infrastructure.tableobserver.event.NewDealerObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.event.SeatsInHandObserverEvent;
import pr.util.Optional;

public class PokerStarsEventReorderer {

	private static final Logger logger = LoggerFactory.getLogger(PokerStarsEventReorderer.class);
	
	private final Map<PokerTableKey, Game> gameMap;
	
	public PokerStarsEventReorderer() {
		gameMap = new HashMap<>();
	}
	
	public Game newGame(PokerTableKey tableKey) {
		return gameMap.remove(tableKey);
	}
	
	public Optional<Seat> getNextSeatToAct(PokerTableKey tableKey) {
		Game game = gameMap.get(tableKey);
		
		if (game == null || game.isGameFinished()) return Optional.empty(); 
		
		return Optional.of(gameMap.get(tableKey).getNextSeatToAct());
	}
	
	public Result reorderIfNecessary(PokerTableKey tableKey, Seat currentDealer, List<ObserverEvent> events) {
		try {
			return reorder(tableKey, currentDealer, events);
		} catch (GameException e) {
//			throw new RuntimeException(e);
			logger.warn("Failed to reorder events: " + e.getMessage());
			return failure(tableKey, events);
		}
	}
	
	private Result reorder(PokerTableKey tableKey, Seat currentDealer, List<ObserverEvent> events) throws GameException {
		List<ObserverEvent> reorderedEvents = new ArrayList<>();
		ResultType resultType = ResultType.NO_CHANGE_REQUIRED;
		Game game = gameMap.get(tableKey);
		
		Iterator<ObserverEvent> iterator = events.iterator();
		
		if (game == null || game.isGameFinished()) {
			Optional<Integer> index = getSeatsInHandObserverEvent(events);
			
			if (!index.isPresent()) {
				return new Result(ResultType.WAITING_FOR_NEW_GAME, events);
			}
			
			reorderedEvents.addAll(events.subList(0, index.get()));
			iterator = events.subList(index.get(), events.size()).iterator();
			SeatsInHandObserverEvent seatsInHandEvent = (SeatsInHandObserverEvent) iterator.next();
			reorderedEvents.add(seatsInHandEvent);
			
			if (!seatsInHandEvent.getSeats().contains(currentDealer)) {
				String message = String.format("Seats %s do not contain dealer %s", seatsInHandEvent.getSeats(), currentDealer);
				logger.debug(message);
				return failure(tableKey, events);
			}
			
			game = new Game(seatsInHandEvent.getSeats(), currentDealer);
			gameMap.put(tableKey, game);
		}
		
		while (iterator.hasNext()) {
			ObserverEvent event = iterator.next();
			
			if (event instanceof ActionObserverEvent) {
				ActionObserverEvent actionEvent = (ActionObserverEvent) event;
				
				Seat nextSeatToAct = game.getNextSeatToAct();
				if (!nextSeatToAct.equals(actionEvent.getSeat())) {
					Optional<Entry<Seat,Action>> lastActionEntry = game.getLastAction();
					if (lastActionEntry.isPresent()
							&& ActionType.CHECK.equals(lastActionEntry.get().getValue().getType())
							&& lastActionEntry.get().getValue().getStreet().comesBefore(game.getStreet())
							&& lastActionEntry.get().getKey().equals(nextSeatToAct)) {
						
						game.processEvent(nextSeatToAct, ActionType.CHECK);
						reorderedEvents.add(new ActionObserverEvent(nextSeatToAct, ActionType.CHECK));
					}
				}
				
				game.processEvent(actionEvent.getSeat(), actionEvent.getActionType());
				reorderedEvents.add(actionEvent);
			} else if (event instanceof BoardCardsObserverEvent) {
				BoardCardsObserverEvent boardCardsEvent = (BoardCardsObserverEvent) event;
				
				if (game.getStreet() != boardCardsEvent.getBoardCards().getStreet()) {
					while (game.getStreet() != boardCardsEvent.getBoardCards().getStreet() && iterator.hasNext()) {
						ObserverEvent secondNext = iterator.next();
						if (!(secondNext instanceof ActionObserverEvent)) {
							return failure(tableKey, events);
						}
						
						ActionObserverEvent actionEvent = (ActionObserverEvent) secondNext;
						if (actionEvent.getSeat() != game.getNextSeatToAct()) {
							return failure(tableKey, events);
						}
						
						game.processEvent(actionEvent.getSeat(), actionEvent.getActionType());

						reorderedEvents.add(actionEvent);						
					}
					
					reorderedEvents.add(boardCardsEvent);
					
					resultType = ResultType.REORDERED;
				} else {
					reorderedEvents.add(event);	
				}
			} else if (event instanceof NewDealerObserverEvent) {
				if (!game.isGameFinished()) {
					return failure(tableKey, events);
				}
				
				NewDealerObserverEvent dealerEvent = (NewDealerObserverEvent) event;
				reorderedEvents.add(dealerEvent);
				
				while (iterator.hasNext()) {
					ObserverEvent next = iterator.next();
					if (next instanceof ActionObserverEvent) {
						return failure(tableKey, events);
					} else if (next instanceof SeatsInHandObserverEvent) {
						SeatsInHandObserverEvent seatsInHandObserverEvent = (SeatsInHandObserverEvent) next;
						game = new Game(seatsInHandObserverEvent.getSeats(), dealerEvent.getSeat());
						gameMap.put(tableKey, game);
						reorderedEvents.add(seatsInHandObserverEvent);
						break;
					} else {
						reorderedEvents.add(next);
					}
				}
			} else {
				reorderedEvents.add(event);
			}
		}
		
		return new Result(resultType, reorderedEvents);
	}

	private Result failure(PokerTableKey tableKey, List<ObserverEvent> events) {
		gameMap.remove(tableKey);
		return new Result(ResultType.FAILED_TO_REORDER, events);
	}

	private Optional<Integer> getSeatsInHandObserverEvent(List<ObserverEvent> events) {
		for (ObserverEvent event : events) {
			if (event instanceof SeatsInHandObserverEvent) {
				return Optional.of(events.indexOf(event));
			}
		}
		
		return Optional.empty();
	}
	
	public enum ResultType {
		WAITING_FOR_NEW_GAME,
		NO_CHANGE_REQUIRED,
		REORDERED,
		FAILED_TO_REORDER
	}
	
	public static class Result {
		@Getter private final ResultType type;
		@Getter private final List<ObserverEvent> events;
		
		public Result(ResultType type, List<ObserverEvent> events) {
			this.type = type;
			this.events = events;
		}
	}
}
