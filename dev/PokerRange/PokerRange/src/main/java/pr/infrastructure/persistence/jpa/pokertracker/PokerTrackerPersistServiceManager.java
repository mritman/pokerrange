package pr.infrastructure.persistence.jpa.pokertracker;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.util.Service;

import com.google.inject.persist.PersistService;

public class PokerTrackerPersistServiceManager implements Service {

	private final Logger logger = LoggerFactory.getLogger(PokerTrackerPersistServiceManager.class);
	
	private final PersistService persistService;
	
	private final boolean started;
	
	@Inject
	public PokerTrackerPersistServiceManager(PersistService persistService) {
		this.persistService = persistService;
		persistService.start();
		started = true;
		logger.debug("PokerTracker persist service started.");
	}
	
	@Override
	public void startService() {
		// The persist service is already initialized in this class' constructor
		// to ensure it is started before any EntityManagers
		// need to be injected. Nothing is here done at startup. This
		// service instance is only used to stop the persist service at
		// shutdown.
	}

	@Override
	public void stopService() {
		persistService.stop();
		logger.debug("PokerTracker persist service stopped.");
	}

	@Override
	public boolean isStarted() {
		return started;
	}

}
