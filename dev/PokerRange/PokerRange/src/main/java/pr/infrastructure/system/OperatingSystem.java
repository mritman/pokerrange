package pr.infrastructure.system;

import lombok.Getter;

public enum OperatingSystem {
	WINDOWS("Windows"),
	LINUX("Linux"),
	MAC("Mac");
	
	private OperatingSystem(String namePattern) {
		this.name = namePattern;
	}
	
	private static String OS = System.getProperty("os.name");

	@Getter
	private String name;
	
	public static OperatingSystem get() {
		if (isWindows()) return OperatingSystem.WINDOWS;
		if (isLinux()) return OperatingSystem.LINUX;
		if (isMac()) return OperatingSystem.MAC;
		
		throw new UnsupportedOperationException(
				"This method is not supported on the current host operating system: "
						+ OS);
	}
	
	public static boolean isWindows() {
		return OS.contains(OperatingSystem.WINDOWS.getName());
	}
	
	public static boolean isLinux() {
		return OS.contains(OperatingSystem.LINUX.getName());
	}
	
	public static boolean isMac() {
		return OS.contains(OperatingSystem.MAC.getName());
	}
	
}
