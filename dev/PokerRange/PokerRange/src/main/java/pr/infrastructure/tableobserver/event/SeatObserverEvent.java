package pr.infrastructure.tableobserver.event;

import pr.domain.poker.table.Seat;

public interface SeatObserverEvent extends ObserverEvent {

	public abstract Seat getSeat();

	public abstract SeatObserverEvent setSeat(Seat seat);
	
}
