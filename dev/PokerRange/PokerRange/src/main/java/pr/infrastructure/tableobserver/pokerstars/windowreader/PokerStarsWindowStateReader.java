package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.Image;

import javax.inject.Inject;

import pr.domain.poker.BoardCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.model.ObservedPlayer;
import pr.infrastructure.tableobserver.model.ObservedPlayerBuilder;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.infrastructure.tableobserver.model.ObservedPokerTableBuilder;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsActionReader.ActionResult;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsHoleAndBoardCardsReader.HoleCardsResult;
import pr.util.Optional;

public class PokerStarsWindowStateReader {

	private final PokerStarsHoleAndBoardCardsReader holeAndBoardCardsReader;
	private final PokerStarsActionReader actionReader;
	private final PokerStarsOpponentHoleCardsReader opponentHoleCardsReader;
	private final PokerStarsDealerButtonReader dealerButtonReader;

	@Inject
	public PokerStarsWindowStateReader(
			PokerStarsHoleAndBoardCardsReader holeAndBoardCardsReader,
			PokerStarsActionReader actionReader,
			PokerStarsOpponentHoleCardsReader opponentHoleCardsReader,
			PokerStarsDealerButtonReader dealerButtonReader) {
		
		this.holeAndBoardCardsReader = holeAndBoardCardsReader;
		this.actionReader = actionReader;
		this.opponentHoleCardsReader = opponentHoleCardsReader;
		this.dealerButtonReader = dealerButtonReader;
	}
	
	/**
	 * Tries to extract table information from the specified image and returns
	 * a optional {@link ObservedPokerTable} that includes
	 * the extracted information.
	 * 
	 * @param image
	 *            a poker table window image
	 * @param table
	 *            the current table state
	 * @return an observed table if reading was successful, empty otherwise
	 */
	public Optional<ObservedPokerTable> read(Image image, TableSize size, String name, PokerVenue venue) {
		List<ObservedPlayer> players = getPlayers(image, size);
		Optional<BoardCards> boardCards = holeAndBoardCardsReader.readBoardCards(image);
		Optional<Seat> dealerPosition = dealerButtonReader.readDealerPosition(image, size);
		
		if (!dealerPosition.isPresent() || !boardCards.isPresent()) {
			return Optional.empty();
		}

		ObservedPokerTable table = new ObservedPokerTableBuilder()
				.setName(name)
				.setSize(size)
				.setVenue(venue)
				.setPlayers(players)
				.setBoardCards(boardCards.get())
				.setDealerPosition(dealerPosition.get())
				.build();
		
		return Optional.of(table);
	}

	private List<ObservedPlayer> getPlayers(Image image, TableSize tableSize) {
		List<ObservedPlayer> players = new ArrayList<>();
		Map<Seat, ObservedPlayerBuilder> playerBuilderMap = new HashMap<>();
		
		readOpponentsInHand(image, tableSize, playerBuilderMap);
		readActions(image, tableSize, playerBuilderMap);
		readHoleCards(image, tableSize, playerBuilderMap);
		
		for (ObservedPlayerBuilder builder : playerBuilderMap.values()) {
			players.add(builder.build());
		}
		
		return players;
	}

	private void readOpponentsInHand(Image image, TableSize tableSize, Map<Seat, ObservedPlayerBuilder> playerBuilderMap) {
		List<Seat> opponentsInHand = opponentHoleCardsReader.readOpponentsInHand(image, tableSize);

		for (Seat seat : opponentsInHand) {
			ObservedPlayerBuilder builder = playerBuilderMap.get(seat);
			if (builder == null) {
				playerBuilderMap.put(seat, new ObservedPlayerBuilder().setSeat(seat));
			}
		}
	}
	
	private void readActions(Image image, TableSize tableSize, Map<Seat, ObservedPlayerBuilder> playerBuilderMap) {
		List<ActionResult> actions = actionReader.readActions(image, tableSize);
		
		for (ActionResult actionResult : actions) {
			ObservedPlayerBuilder builder = playerBuilderMap.get(actionResult.getSeat());
			if (builder == null) {
				builder = new ObservedPlayerBuilder().setSeat(actionResult.getSeat());
				playerBuilderMap.put(actionResult.getSeat(), builder);
			}
			
			builder.setLastAction(actionResult.getActionType());
		}
	}
	
	private void readHoleCards(Image image, TableSize tableSize, Map<Seat, ObservedPlayerBuilder> playerBuilderMap) {
		List<HoleCardsResult> holeCardsResults = holeAndBoardCardsReader.readHoleCards(image, tableSize);
		
		for (HoleCardsResult holeCardsResult : holeCardsResults) {
			ObservedPlayerBuilder builder = playerBuilderMap.get(holeCardsResult.getSeat());
			if (builder == null) {
				builder = new ObservedPlayerBuilder().setSeat(holeCardsResult.getSeat());
				playerBuilderMap.put(holeCardsResult.getSeat(), builder);
			}
			
			builder.setHoleCards(holeCardsResult.getHoleCards());
		}
	}
	
}
