package pr.infrastructure.tableobserver.event;


public abstract class PlayerSeatObserverEvent extends PlayerObserverEvent implements SeatObserverEvent {

	protected PlayerSeatObserverEvent(String name) {
		super(name);
	}
	
}
