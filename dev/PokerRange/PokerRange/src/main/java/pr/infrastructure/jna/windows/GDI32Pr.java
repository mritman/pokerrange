package pr.infrastructure.jna.windows;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.GDI32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HBITMAP;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinGDI.BITMAPINFO;
import com.sun.jna.win32.W32APIOptions;

public interface GDI32Pr extends GDI32 {
	
    GDI32Pr INSTANCE = (GDI32Pr) Native.loadLibrary("gdi32", GDI32Pr.class, W32APIOptions.DEFAULT_OPTIONS);
    
	boolean BitBlt(HDC hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, HDC hdcSrc, int nXSrc, int nYSrc, DWORD dwRop);
	
	int GetDIBits(HDC hdc, HBITMAP hbmp, int uStartScan, int cScanLines, int[] lpvBits, BITMAPINFO lpbi, int uUsage);

}
