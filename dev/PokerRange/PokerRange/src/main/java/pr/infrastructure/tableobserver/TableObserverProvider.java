package pr.infrastructure.tableobserver;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import pr.application.pokervenues.pokerstars.PokerStars;
import pr.domain.poker.PokerVenue;

public class TableObserverProvider {
	
	private static final Map<PokerVenue, TableObserver> readerMap = new HashMap<>();
	
	@Inject
	TableObserverProvider(@PokerStars TableObserver pokerStarsTableReader) {
		readerMap.put(PokerVenue.POKERSTARS, pokerStarsTableReader);
	}
	
	/**
	 * Returns the {@link TableObserver} for the specified poker venue or null if no such reader exists.
	 *  
	 * @param venue the poker venue to get the {@link TableObserver} instance for
	 * @return the {@link TableObserver} for the specified poker venue or null if it does not exist
	 */
	public TableObserver getReader(PokerVenue venue) {
		return readerMap.get(venue);
	}

}
