package pr.infrastructure.jna.windows;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.win32.W32APIOptions;

public interface User32Pr extends User32 {
	
	User32Pr INSTANCE = (User32Pr) Native.loadLibrary("user32", User32Pr.class, W32APIOptions.DEFAULT_OPTIONS);
	
	HDC GetWindowDC(HWND hWnd);
	
	boolean GetClientRect(HWND hWnd, RECT rect);
	
	boolean ClientToScreen(HWND hWnd, POINT point);
	
}
