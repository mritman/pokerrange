package pr.infrastructure.tableobserver.event;

import pr.domain.poker.BoardCards;

public class TurnObserverEvent extends BoardCardsObserverEvent {

	public TurnObserverEvent(BoardCards boardCards) {
		super(boardCards);
	}

}
