package pr.infrastructure.tableobserver.event;

import java.util.UUID;

import lombok.Getter;

import com.google.common.base.Objects;

public class NewGameObserverEvent implements ObserverEvent {

	@Getter	private final String tableName;
	@Getter private final UUID id;
	
	public NewGameObserverEvent(String tableName) {
		this.tableName = tableName;
		id = UUID.randomUUID();
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("table", tableName)
				.add("id", id)
				.toString();
	}

}
