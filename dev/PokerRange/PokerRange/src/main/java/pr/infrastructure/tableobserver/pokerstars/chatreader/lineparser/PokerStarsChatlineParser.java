package pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.joda.money.Money;

import pr.domain.poker.event.NewGameEvent;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.action.BetEvent;
import pr.domain.poker.event.action.BigBlindEvent;
import pr.domain.poker.event.action.CallEvent;
import pr.domain.poker.event.action.CheckEvent;
import pr.domain.poker.event.action.FoldEvent;
import pr.domain.poker.event.action.RaiseEvent;
import pr.domain.poker.event.action.SmallBlindEvent;
import pr.domain.poker.event.board.FlopEvent;
import pr.domain.poker.event.board.RiverEvent;
import pr.domain.poker.event.board.TurnEvent;
import pr.util.Optional;

import com.google.common.collect.ImmutableMap;

public class PokerStarsChatlineParser {
	
	private static final LineTemplate NEW_HAND = new LineTemplate(
			NewGameEvent.class, 
			"Dealer: Starting new hand: #", 
			Collections.<String, Class<?>>emptyMap());
	
	private static final LineTemplate SMALL_BLIND = new LineTemplate(
			SmallBlindEvent.class, 
			"Dealer: .* posts small blind .*", 
			ImmutableMap.<String, Class<?>>of(
					"(?<=^Dealer: )(.*)(?= posts)", String.class,
					"(?<=posts small blind )(.*)(?=$)", Money.class));

	private static final LineTemplate BIG_BLIND = new LineTemplate(
			BigBlindEvent.class, 
			"Dealer: .* posts big blind .*", 
			ImmutableMap.<String, Class<?>>of(
					"(?<=^Dealer: )(.*)(?= posts)", String.class,
					"(?<=posts big blind )(.*)(?=$)", Money.class));

	private static final LineTemplate FOLD = new LineTemplate(
			FoldEvent.class, 
			"Dealer: .* folds", 
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: )(.*)(?= folds)", String.class));

	private static final LineTemplate RAISE = new LineTemplate(
			RaiseEvent.class, 
			"Dealer: .* raises .* to .*", 
			ImmutableMap.<String, Class<?>>of(
					"(?<=^Dealer: )(.*)(?= raises)", String.class,
					"(?<=raises )(.*)(?= to)", Money.class,
					"(?<=to )(\\S*)(?=.*)", Money.class));

	private static final LineTemplate CALL = new LineTemplate(
			CallEvent.class, 
			"Dealer: .* calls .*", 
			ImmutableMap.<String, Class<?>>of(
					"(?<=^Dealer: )(.*)(?= calls)", String.class,
					"(?<=calls )(\\S*)(?=.*)", Money.class));
	
	private static final LineTemplate CHECK = new LineTemplate(
			CheckEvent.class,
			"Dealer: .* checks",
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: )(.*)(?= checks)", String.class));

	private static final LineTemplate BET = new LineTemplate(
			BetEvent.class,
			"Dealer: .* bets .*",
			ImmutableMap.<String, Class<?>>of(
					"(?<=^Dealer: )(.*)(?= bets)", String.class,
					"(?<=bets )(\\S*)(?=.*)", Money.class));
	
	private static final LineTemplate FLOP = new LineTemplate(
			FlopEvent.class,
			"Dealer: Dealing Flop: .*",
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: Dealing Flop: \\[)(.*)(?=\\])", String.class));

	private static final LineTemplate TURN = new LineTemplate(
			TurnEvent.class,
			"Dealer: Dealing Turn: .*",
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: Dealing Turn: \\[)(.*)(?=\\])", String.class));

	private static final LineTemplate RIVER = new LineTemplate(
			RiverEvent.class,
			"Dealer: Dealing River: .*",
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: Dealing River: \\[)(.*)(?=\\])", String.class));

	private static final LineTemplate TIMEOUT = new LineTemplate(
			FoldEvent.class, 
			"Dealer: .* has timed out - hand is folded", 
			ImmutableMap.<String, Class<?>>of("(?<=^Dealer: )(.*)(?= has timed out)", String.class));
	
	private static final List<LineTemplate> lineTemplates = Arrays.asList(
			NEW_HAND, SMALL_BLIND, BIG_BLIND, FOLD, CALL, RAISE, CHECK, BET,
			FLOP, TURN, RIVER, TIMEOUT);
	
	public List<PokerEvent> parse(List<String> lines) {
		List<PokerEvent> events = new ArrayList<>();
		for (String line : lines) {
			Optional<PokerEvent> event = parse(line);
			if (event.isPresent()) events.add(event.get());
		}
		return events;
	}
	
	public Optional<PokerEvent> parse(String line) {
		for (LineTemplate lineTemplate : lineTemplates) {
			Optional<PokerEvent> event = ReflectionLineParser.parse(line, lineTemplate);
			if (event.isPresent()) return event; 
		}

		return Optional.empty();
	}

}
