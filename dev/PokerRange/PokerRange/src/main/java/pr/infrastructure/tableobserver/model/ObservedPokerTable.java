package pr.infrastructure.tableobserver.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import lombok.Getter;
import pr.domain.poker.BoardCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.table.PokerTableKey;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.util.Optional;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class ObservedPokerTable {
	
	@Getter	
	private final String name;
	
	@Getter
	private final PokerVenue venue;
	
	@Getter
	private final PokerTableKey key;
	
	/**
	 * The players sorted by seat in ascending order.
	 */
	private final List<ObservedPlayer> players;
	
	@Getter
	private final BoardCards boardCards;
	
	@Getter
	private final UUID handId;

	@Getter
	private final TableSize size;
	
	@Getter
	private final Seat dealer;
	
	protected ObservedPokerTable(String name, PokerVenue venue, TableSize size,
			List<ObservedPlayer> players, UUID handId, BoardCards boardCards, Seat dealer) {
		
		Preconditions.checkNotNull("name", name);
		Preconditions.checkNotNull("handId", handId);
		Preconditions.checkNotNull("boardCards", boardCards);
		Preconditions.checkNotNull("size", size);
		Preconditions.checkNotNull("venue", venue);
		Preconditions.checkNotNull("dealerPosition", dealer);
		
		this.name = name;
		this.venue = venue;
		this.key = new PokerTableKey(name, venue);
		this.size = size;
		this.handId = handId;
		this.boardCards = boardCards;
		this.dealer = dealer;
		
		if (players == null) {
			this.players = Collections.emptyList();
		} else {
			this.players = Collections.unmodifiableList(new ArrayList<>(players));
		}
	}
	
	public List<ObservedPlayer> getPlayers() {
		return Collections.unmodifiableList(players);
	}
	
	public Optional<ObservedPlayer> getPlayer(Seat seat) {
		for (ObservedPlayer player : players) {
			if (player.getSeat() == seat) {
				return Optional.of(player);
			}
		}
		
		return Optional.empty();
	}
	
	public ObservedPokerTable setHandId(UUID handId) {
		return new ObservedPokerTableBuilder(this).setHandId(handId).build();
	}
	
	public ObservedPokerTable setPlayers(List<ObservedPlayer> players) {
		return new ObservedPokerTableBuilder(this).setPlayers(players).build();
	}
	
	public ObservedPokerTable setBoardCards(BoardCards boardCards) {
		return new ObservedPokerTableBuilder(this).setBoardCards(boardCards).build();
	}
	
	public ObservedPokerTable setDealerPosition(Seat dealerPosition) {
		return new ObservedPokerTableBuilder(this).setDealerPosition(dealerPosition).build();
	}

	public Set<Seat> getSeatsInHand() {
		Set<Seat> seatsInHand = new TreeSet<>();
		for (ObservedPlayer player : players) {
			seatsInHand.add(player.getSeat());
		}
		
		return seatsInHand;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("name", name)
				.add("size", size)
				.add("handId", handId)
				.add("boardCards", boardCards)
				.add("players", players)
				.toString();
	}

}
