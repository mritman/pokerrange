package pr.infrastructure.tableobserver.pokerstars;

import java.security.MessageDigest;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;

import javax.inject.Inject;

import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.TableCardReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsActionReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsOpponentHoleCardsReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.HoleCardsPosition;
import pr.util.MessageDigestFactory;
import pr.util.MessageDigestFactory.MessageDigestAlgorithm;

public class OldPokerStarsWindowReader {
	
	private final TableCardReader tableCardReader;
	private final PokerStarsOpponentHoleCardsReader opponentHoleCardsReader;
	
	@Inject
	public OldPokerStarsWindowReader(TableCardReader tableCardReader, PokerStarsActionReader actionReader, PokerStarsOpponentHoleCardsReader opponentHoleCardsReader) {
		this.tableCardReader = tableCardReader;
		this.opponentHoleCardsReader = opponentHoleCardsReader;
	}
	
	protected static class ReadResult<T> {
		@Getter private T result = null;
		@Getter private boolean updated;
		
		public ReadResult(T newValue, boolean updated) {
			this.result = newValue;
			this.updated = updated;
		}
	}
	
	protected boolean isNewHand(Image image, PokerTable table) {
		int nrOfPixels = PokerStarsTableData.HAND_ID_WIDTH * PokerStarsTableData.HAND_ID_HEIGHT;
		
		// Number of pixel times the number of bytes (alpha, blue, green, red) needed per pixel.
		byte[] pixelBytes = new byte[nrOfPixels * 4];
		
		image.getPixelReader().getPixels(
				PokerStarsTableData.HAND_ID_X,
				PokerStarsTableData.HAND_ID_Y,
				PokerStarsTableData.HAND_ID_WIDTH,
				PokerStarsTableData.HAND_ID_HEIGHT,
				WritablePixelFormat.getByteBgraInstance(), pixelBytes, 0,
				(int) PokerStarsTableData.HAND_ID_WIDTH * 4);
		
		MessageDigest md = MessageDigestFactory.getMessageDigest(MessageDigestAlgorithm.MD5);
		md.update(pixelBytes);
		byte[] hashBytes = md.digest();
	
		String handId = bytesArrayToHexString(hashBytes);
		
	    boolean newHand = table.getHandId() == null || !table.getHandId().equals(handId);
	    
	    table.setHandId(handId);
	    
	    return newHand;
	}

	protected ReadResult<HoleCards> readHoleCards(Image image, PokerTable table) {
		if (table.getSize() == null) {
			return new ReadResult<>(table.getGameState().getHoleCards(), false);
		}
		
		HoleCardsPosition position = PokerStarsTableData.getHoleCardsPosition(table.getSize(), table.getPlayerSeat());
		
		Card card1 = readCard(image, position.getCard1().getX(), position.getCard1().getY());
		Card card2 = readCard(image, position.getCard2().getX(), position.getCard2().getY());
		
		if (card1 == null && card2 == null && table.getGameState().getHoleCards() != null) {
			return new ReadResult<>(null, true);
		} else if (card1 != null && card2 != null) {
			HoleCards holeCards = new HoleCards(card1, card2);
			
			if (!holeCards.equals(table.getGameState().getHoleCards())) {
				return new ReadResult<>(holeCards, true);
			}
		}
		
		return new ReadResult<>(table.getGameState().getHoleCards(), false);
	}

	protected ReadResult<BoardCards> readBoardCards(Image image, BoardCards currentBoardCards) {
		Card flop1, flop2, flop3, turn, river;
		turn = null;
		river = null;
		
		Street currentStreet = currentBoardCards.getStreet();
		
		Street street = readStreet(image);
		
		if (street == Street.PREFLOP) {
			if (currentStreet != Street.PREFLOP) {
				return new ReadResult<>(new BoardCards(), true);
			}
			
			return new ReadResult<>(currentBoardCards, false);
		}
		
		flop1 = readCard(image, PokerStarsTableData.CARD1_LOCATION.getX(), PokerStarsTableData.CARD1_LOCATION.getY());
		if (flop1 == null) return new ReadResult<>(currentBoardCards, false);
		
		flop2 = readCard(image, PokerStarsTableData.CARD2_LOCATION.getX(), PokerStarsTableData.CARD2_LOCATION.getY());
		if (flop2 == null) return new ReadResult<>(currentBoardCards, false);
		
		flop3 = readCard(image, PokerStarsTableData.CARD3_LOCATION.getX(), PokerStarsTableData.CARD3_LOCATION.getY());
		if (flop3 == null) return new ReadResult<>(currentBoardCards, false);
		
		turn = readCard(image, PokerStarsTableData.CARD4_LOCATION.getX(), PokerStarsTableData.CARD4_LOCATION.getY());
		
		if (turn != null) {
			river = readCard(image, PokerStarsTableData.CARD5_LOCATION.getX(), PokerStarsTableData.CARD5_LOCATION.getY());	
		}
		
		BoardCards boardCards = new BoardCards(flop1, flop2, flop3, turn, river);
		
		if (boardCards.equals(currentBoardCards) || street != boardCards.getStreet()) {
			return new ReadResult<>(currentBoardCards, false);
		}
		
		return new ReadResult<>(boardCards, true);
	}

	protected List<Seat> readOpponentsInHand(Image image, PokerTable table) {
		return opponentHoleCardsReader.readOpponentsInHand(image, table.getSize());
	}
	
	private String bytesArrayToHexString(byte[] hashBytes) {
		// http://www.rgagnon.com/javadetails/java-0596.html
		String hexes = "0123456789ABCDEF";
		
	    final StringBuilder hex = new StringBuilder( 2 * hashBytes.length );
	    for ( final byte b : hashBytes ) {
	      hex.append(hexes.charAt((b & 0xF0) >> 4)).append(hexes.charAt((b & 0x0F)));
	    }
	    
		return hex.toString();
	}

	private Street readStreet(Image image) {
		Color card1 = image.getPixelReader().getColor(PokerStarsTableData.CARD1_PIXEL.getX(), PokerStarsTableData.CARD1_PIXEL.getY());
		Color card2 = image.getPixelReader().getColor(PokerStarsTableData.CARD2_PIXEL.getX(), PokerStarsTableData.CARD2_PIXEL.getY());
		Color card3 = image.getPixelReader().getColor(PokerStarsTableData.CARD3_PIXEL.getX(), PokerStarsTableData.CARD3_PIXEL.getY());
		Color card4 = image.getPixelReader().getColor(PokerStarsTableData.CARD4_PIXEL.getX(), PokerStarsTableData.CARD4_PIXEL.getY());
		Color card5 = image.getPixelReader().getColor(PokerStarsTableData.CARD5_PIXEL.getX(), PokerStarsTableData.CARD5_PIXEL.getY());
		
		if (PokerStarsTableData.CARD_COLOR.equals(card5)) {
			return Street.RIVER;
		} else if (PokerStarsTableData.CARD_COLOR.equals(card4)) {
			return Street.TURN;
		} else if (PokerStarsTableData.CARD_COLOR.equals(card1)
				|| PokerStarsTableData.CARD_COLOR.equals(card2)
				|| PokerStarsTableData.CARD_COLOR.equals(card3)) {
			
			return Street.FLOP;
		} else {
			return Street.PREFLOP;
		}
	}

	private Card readCard(Image image, int x, int y) {
		Image cardImage = new WritableImage(image.getPixelReader(), x, y, tableCardReader.getWidth(), tableCardReader.getHeight());
		
		return tableCardReader.readCard(cardImage);
	}

}
