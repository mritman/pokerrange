package pr.infrastructure.tableobserver.event;

import pr.domain.poker.BoardCards;

public class FlopObserverEvent extends BoardCardsObserverEvent {

	public FlopObserverEvent(BoardCards boardCards) {
		super(boardCards);
	}
	
}
