package pr.infrastructure.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import pr.util.AutoclosingDaemonThread;

public class SocketStringReader extends AutoclosingDaemonThread {

	private static final int SOCKET_READ_TIMEOUT = (int) TimeUnit.MILLISECONDS.toMillis(100);
	private static final int MAX_LINE_BATCH_SIZE = 1000;
	private final ConcurrentLinkedQueue<List<String>> lines;
	private BufferedReader bufferedReader;
	private ServerSocket serverSocket;
	
	private AtomicInteger port;

	public SocketStringReader(String name) {
		this(name, 0);
	}
	
	public SocketStringReader(String name, int port) {
		super(name);
		lines = new ConcurrentLinkedQueue<List<String>>();
		this.port = new AtomicInteger(port);
	}
	
	public List<List<String>> getLines() {
		List<List<String>> result = new LinkedList<>();
		List<String> line = null;
		
		do {
			line = lines.poll();
			if (line != null) {
				result.add(line);
			}
		} while (line != null && result.size() < MAX_LINE_BATCH_SIZE);
		
		return result;
	}
	
	@Override
	public synchronized void start() {
		super.start();
		while (port.get() == 0);
	}
	
	@Override
	public void run() {
		try {
			bufferedReader = getBufferedSocketReader();
			List<String> newLines = new LinkedList<>();
			while (true) {
				try {
					String line = bufferedReader.readLine();
					if (line == null) break;
					newLines.add(line);
				} catch (SocketTimeoutException e) {
					if (interrupted()) break;
					
					if (!newLines.isEmpty()) {
						lines.add(newLines);
						newLines = new LinkedList<>();
					}
				}
			}
		} catch (IOException e) {
			log.info(e.getMessage());
		} finally {
			close();
		}
		
		log.debug("{} thread will now exit.", Thread.currentThread().getName());
	}
	
	public int getPort() {
		if (port.get() == 0) {
			throw new IllegalStateException("This methods should not be called before initialization.");
		}
		
		return port.get();
	}
	
	public void close() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				log.warn("Exception occurred while closing server socket.", e);
			}
		}
		
		if (bufferedReader != null) {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				log.warn("Exception occurred while closing buffered reader.", e);
			}
		}
	}
	
	private BufferedReader getBufferedSocketReader() throws IOException {
		serverSocket = new ServerSocket(port.get());
		port.set(serverSocket.getLocalPort());
		Socket clientSocket = serverSocket.accept();
		clientSocket.setSoTimeout(SOCKET_READ_TIMEOUT);
		return new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	}

}
