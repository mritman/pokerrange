package pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser;

import java.util.Map;

import lombok.Getter;
import pr.domain.poker.event.PokerEvent;

class LineTemplate {

	@Getter private final Class<? extends PokerEvent> eventClass;
	@Getter private final String linePattern;
	@Getter private final Map<String, Class<?>> patternMap;
	
	public LineTemplate(Class<? extends PokerEvent> eventClass, String linePattern, Map<String, Class<?>> patternMap) {
		this.eventClass = eventClass;
		this.linePattern = linePattern;
		this.patternMap = patternMap;
	}
	
}
