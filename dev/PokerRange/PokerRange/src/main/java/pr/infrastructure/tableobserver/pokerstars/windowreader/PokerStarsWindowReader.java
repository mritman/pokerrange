package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.Image;

import javax.inject.Inject;

import pr.domain.poker.PokerVenue;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.event.NewGameObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.util.Optional;

public class PokerStarsWindowReader {

	private final PokerStarsWindowStateReader windowStateReader;
	private final PokerStarsWindowChangeAnalyzer changeAnalyzer;

	private final Map<String, ObservedPokerTable> previousTableState;
	
	@Inject
	public PokerStarsWindowReader(
			PokerStarsWindowStateReader windowStateReader,
			PokerStarsWindowChangeAnalyzer changeAnalyzer) {
		
		this.windowStateReader = windowStateReader;
		this.changeAnalyzer = changeAnalyzer;
		previousTableState = new HashMap<>();
	}
	
	public List<ObserverEvent> getEvents(Image image, String tableName, TableSize size) {
		ObservedPokerTable previousState = previousTableState.get(tableName);
		Optional<ObservedPokerTable> newState = windowStateReader.read(image, size, tableName, PokerVenue.POKERSTARS);
		
		if (!newState.isPresent()) {
			return Collections.emptyList();
		}
		
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.ofNullable(previousState), newState.get());
		
		Optional<NewGameObserverEvent> newGameObserverEvent = getNewGameEvent(events);
		if (newGameObserverEvent.isPresent()) {
			newState = Optional.of(newState.get().setHandId(newGameObserverEvent.get().getId()));
		}
		
		previousTableState.put(tableName, newState.get());
		
		return events;
	}

	private Optional<NewGameObserverEvent> getNewGameEvent(List<ObserverEvent> events) {
		for (ObserverEvent event : events) {
			if  (event instanceof NewGameObserverEvent) {
				return Optional.of((NewGameObserverEvent) event);
			}
		}
		
		return Optional.empty();
	}

}
