package pr.infrastructure.tableobserver.model;

import lombok.Getter;
import pr.domain.poker.HoleCards;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;

public class ObservedPlayerBuilder {
	@Getter	private Seat seat;
	@Getter	private ActionType lastAction;
	@Getter private HoleCards holeCards;

	public ObservedPlayerBuilder() {}
	
	public ObservedPlayerBuilder(ObservedPlayer player) {
		this.seat = player.getSeat();
		this.lastAction = player.getLastAction().orElse(null);
		this.holeCards = player.getHoleCards().orElse(null);
	}
	
	public ObservedPlayerBuilder setSeat(Seat seat) {
		this.seat = seat;
		return this;
	}
	
	public ObservedPlayerBuilder setLastAction(ActionType lastAction) {
		this.lastAction = lastAction;
		return this;
	}
	
	public ObservedPlayerBuilder setHoleCards(HoleCards holeCards) {
		this.holeCards = holeCards;
		return this;
	}
	
	public ObservedPlayer build() {
		return new ObservedPlayer(seat, lastAction, holeCards);
	}
	
}
