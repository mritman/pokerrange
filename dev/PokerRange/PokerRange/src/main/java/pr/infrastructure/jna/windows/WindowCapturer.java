package pr.infrastructure.jna.windows;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.platform.win32.GDI32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HBITMAP;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.RECT;
import com.sun.jna.platform.win32.WinGDI.BITMAPINFO;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinUser.POINT;

public class WindowCapturer {
	
	private static final Logger logger = LoggerFactory.getLogger(WindowCapturer.class);

	public Image capture(HWND hWnd) {
		// Will hold the screen coordinates of the top left pixel of the client area of the window.
		POINT point = new POINT(0, 0);
		RECT windowRect = new RECT();
		RECT clientRect = new RECT();
		int offsetLeft;
		int offsetTop;
		boolean success;

		success = User32Pr.INSTANCE.GetClientRect(hWnd, clientRect);
		if (!success) {
			logger.debug("Failed to get client area size.");
			return null;
		}
		
		success = User32Pr.INSTANCE.GetWindowRect(hWnd, windowRect);
		if (!success) {
			logger.debug("Failed to get window area size.");
			return null;
		}
		
		success = User32Pr.INSTANCE.ClientToScreen(hWnd, point);
		if (!success) {
			logger.debug("Failed to get screen coordinates of client area.");
			return null;
		}
		
		offsetLeft = point.x - windowRect.left;
		offsetTop = point.y - windowRect.top;
		
		return captureImage(hWnd, offsetLeft, offsetTop, clientRect.right, clientRect.bottom);
	}
	
	private static Image captureImage(HWND hWnd, int x, int y, int width, int height) {
		HDC targetWndHDC = User32Pr.INSTANCE.GetWindowDC(hWnd);
		HDC hCaptureDc = GDI32.INSTANCE.CreateCompatibleDC(targetWndHDC);
		HBITMAP hCaptureBitmap = GDI32Pr.INSTANCE.CreateCompatibleBitmap(targetWndHDC, width, height);
		HANDLE originalObject = GDI32Pr.INSTANCE.SelectObject(hCaptureDc, hCaptureBitmap);
		
		GDI32Pr.INSTANCE.BitBlt(hCaptureDc, 0, 0, width, height, targetWndHDC, x, y, WinGDIPr.SRCCOPY);
		
		Image image = createImageFromHBitmap(width, height, hCaptureDc, hCaptureBitmap);
		
		GDI32Pr.INSTANCE.SelectObject(hCaptureDc, originalObject);
		User32.INSTANCE.ReleaseDC(hWnd, targetWndHDC);
		GDI32.INSTANCE.DeleteDC(hCaptureDc);
		GDI32.INSTANCE.DeleteObject(hCaptureBitmap);
		
		return image;
	}

	private static Image createImageFromHBitmap(int width, int height, HDC hdcMemory, HBITMAP hBitmapMemory) {
		if (width <= 0 || height <= 0) {
			// This can happen if the window is minimized.
			logger.trace("The width or height of the client area of the window to capture is zero. This can happen if the window is minimized.");
			return null;
		}
		
		int[] pixels = new int[width * height];
		int[] bandMasks = new int[] { 0x00ff0000, 0x0000ff00, 0x000000ff };
		DirectColorModel colorModel = new DirectColorModel(32, bandMasks[0], bandMasks[1], bandMasks[2]);
		DataBuffer dataBuffer = new DataBufferInt(pixels, width * height);
		WritableRaster writableRaster = Raster.createPackedRaster(dataBuffer, width, height, width, bandMasks, null);
		
		BufferedImage bufferImage = new BufferedImage(colorModel, writableRaster,	true, null);

		BITMAPINFO info = new BITMAPINFO();
		info.bmiHeader.biSize = 40;
		info.bmiHeader.biWidth = width;
		info.bmiHeader.biHeight = -height;
		info.bmiHeader.biPlanes = 1;
		info.bmiHeader.biBitCount = 32;
		info.bmiHeader.biCompression = WinGDIPr.BI_RGB;
		info.bmiHeader.biSizeImage = 0;
		info.bmiHeader.biClrUsed = 0;
		
		GDI32Pr.INSTANCE.GetDIBits(hdcMemory, hBitmapMemory, 0, height, pixels, info, WinGDIPr.DIB_RGB_COLORS);
		
		WritableImage image = new WritableImage(bufferImage.getWidth(), bufferImage.getHeight());
		SwingFXUtils.toFXImage(bufferImage, image);
		
		return image;
	}

}
