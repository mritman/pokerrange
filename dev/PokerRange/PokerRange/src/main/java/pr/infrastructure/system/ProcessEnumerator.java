package pr.infrastructure.system;

import java.util.List;

import pr.util.Optional;

public interface ProcessEnumerator {

	List<ProcessInfo> getProcesses();

	Optional<ProcessInfo> getProcess(String name);

}