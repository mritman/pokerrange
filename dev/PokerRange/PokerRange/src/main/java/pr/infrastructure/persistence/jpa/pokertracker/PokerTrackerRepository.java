package pr.infrastructure.persistence.jpa.pokertracker;

import pr.domain.poker.player.PlayerStatistics;


public interface PokerTrackerRepository {

	String PREFLOP_RAISE_POKER_STOVE_RANGE = "Preflop-raise PokerStove Range";
	String PREFLOP_3BET_POKER_STOVE_RANGE = "Preflop-3bet PokerStove Range";
	String PREFLOP_COLD_CALL_FIRST_RAISE_POKER_STOVE_RANGE = "Preflop-cold-call-first-raise PokerStove Range";
	
	PlayerStatistics getOpponentStatistics(String name);

}