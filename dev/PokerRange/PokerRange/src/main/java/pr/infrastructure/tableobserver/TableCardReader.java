package pr.infrastructure.tableobserver;

import javafx.scene.image.Image;
import pr.domain.cards.Card;

public interface TableCardReader {
	
	/**
	 * Tries to find the card that the specified image represents.
	 * 
	 * @param image the image to read
	 * @return the {@link Card} that the image represents or null if the image was not recognized.
	 */
	public Card readCard(Image image);
	
	/**
	 * @return the width in pixels the image should be that is passed to {@link #readCard(Image)}
	 */
	public int getWidth();

	/**
	 * @return the height in pixels the image should be that is passed to {@link #readCard(Image)}
	 */
	public int getHeight();

}
