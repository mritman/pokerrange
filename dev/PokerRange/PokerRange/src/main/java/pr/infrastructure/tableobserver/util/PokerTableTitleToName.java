package pr.infrastructure.tableobserver.util;

import pr.domain.poker.PokerVenue;

public class PokerTableTitleToName {

	private static final String POKERSTARS_NAME_SEPERATOR = "-";
	
	public static String getName(String title, PokerVenue venue) {
		switch(venue) {
		case POKERSTARS: return getPokersStarsName(title);
		default: throw new IllegalStateException("This should never be reached");
		}
	}

	private static String getPokersStarsName(String title) {
		int index = title.indexOf(POKERSTARS_NAME_SEPERATOR);
		return title.substring(0, index).trim();
	}

}
