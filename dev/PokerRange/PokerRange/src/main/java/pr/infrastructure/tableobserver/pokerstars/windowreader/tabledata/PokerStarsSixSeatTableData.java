package pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata;

import lombok.Getter;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.HoleCardsPosition;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;

public class PokerStarsSixSeatTableData {
	
	/**
	 * Offset from top left corner of the square that contains the dealer button:
	 * x: 9
	 * y: 9 
	 */
	public enum SixSeatDealerPosition {
		POS1(483, 112),
		POS2(644, 197),
		POS3(575, 337),
		POS4(320, 344),
		POS5(149, 271),
		POS6(230, 116);
		
		@Getter
		private final Position position;
		
		private SixSeatDealerPosition(int x, int y) {
			position = new Position(x, y);
		}
		
		protected static Position getPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1.getPosition();
			case SEAT2: return POS2.getPosition();
			case SEAT3: return POS3.getPosition();
			case SEAT4: return POS4.getPosition();
			case SEAT5: return POS5.getPosition();
			case SEAT6: return POS6.getPosition();
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
	public enum SixSeatHoleCardsPosition implements HoleCardsPosition {
		POS1(492, 31),   		//(500, 61)
		POS2(677, 176),  		//(685, 206)
		POS3(494, 345),  		//(502, 375)
		POS4(233, 344),  		//(241, 374)
		POS5(51, 175),   		//(59, 205)
		POS6(234, 31);   		//(242, 61)
		
		@Getter private Position card1;
		@Getter private Position card2;
		
		private SixSeatHoleCardsPosition(int x, int y) {
			card1 = new Position(x + PokerStarsTableData.CARD_INSET_LEFT, y + PokerStarsTableData.CARD_INSET_TOP);
			card2 = new Position(card1.getX() + PokerStarsTableData.HOLECARDS_CARD2_OFFSET_LEFT, card1.getY() + PokerStarsTableData.HOLECARDS_CARD2_OFFSET_TOP);
		}
		
		protected static HoleCardsPosition getHoleCardsPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1;
			case SEAT2: return POS2;
			case SEAT3: return POS3;
			case SEAT4: return POS4;
			case SEAT5: return POS5;
			case SEAT6: return POS6;
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
	/**
	 * Offsets from topleft corner of the square that contains the avatar circles:
	 * x: 29
	 * y: 40
	 */
	public enum SixSeatActionPosition {
		POS1(519, 68),
		POS2(702, 217),
		POS3(519, 382),
		POS4(248, 382),
		POS5(75, 217),
		POS6(257, 68);
		
		@Getter
		private Position position;
		
		private SixSeatActionPosition(int x, int y) {
			position = new Position(x, y);
		}

		public static Position getPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1.getPosition();
			case SEAT2: return POS2.getPosition();
			case SEAT3: return POS3.getPosition();
			case SEAT4: return POS4.getPosition();
			case SEAT5: return POS5.getPosition();
			case SEAT6: return POS6.getPosition();
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
	public enum SixSeatOpponentHoleCardsPosition {
		POS1(527, 125),
		POS2(632, 222),
		POS3(526, 315),
		POS4(265, 315),
		POS5(155, 222),
		POS6(265, 125);
		
		@Getter
		private Position position;
		
		private SixSeatOpponentHoleCardsPosition(int x, int y) {
			position = new Position(x, y);
		}
		
		public static Position getPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1.getPosition();
			case SEAT2: return POS2.getPosition();
			case SEAT3: return POS3.getPosition();
			case SEAT4: return POS4.getPosition();
			case SEAT5: return POS5.getPosition();
			case SEAT6: return POS6.getPosition();
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
}
