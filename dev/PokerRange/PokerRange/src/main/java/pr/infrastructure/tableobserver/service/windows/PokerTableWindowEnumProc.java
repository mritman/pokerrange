package pr.infrastructure.tableobserver.service.windows;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.PokerVenue;

import lombok.Getter;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;

public class PokerTableWindowEnumProc implements WNDENUMPROC {
	
	public static final Logger logger = LoggerFactory.getLogger(PokerTableWindowEnumProc.class);
	
	public static final String POKER_STARS_TABLE_FRAME_CLASS = "PokerStarsTableFrameClass";
	public static final int MAX_WINDOW_CLASS_NAME_SIZE = 256;
	
	public static class TableWindowResult {
		@Getter private final HWND handle;
		@Getter private final String title;
		@Getter private final PokerVenue venue;
		
		public TableWindowResult(HWND handle, PokerVenue venue, String title) {
			this.handle = handle;
			this.venue = venue;
			this.title = title;
		}
		
		@Override
		public boolean equals(Object object) {
			if (object == null || getClass() != object.getClass()) {
				return false;
			}
			
			final TableWindowResult other = (TableWindowResult) object;
			
			return Objects.equals(title, other.getTitle());
		}
		
		@Override
		public int hashCode() {
			return getWindowId();
		}
		
		public int getWindowId() {
			return handle.hashCode();
		}
	}
	
	@Getter
	private final Set<TableWindowResult> handles = new HashSet<>();
	
	public void reset() {
		handles.clear();
	}

	/**
	 * TODO add more poker venue's here. Currently this only checks for poker
	 * stars table windows.
	 */
	@Override
	public boolean callback(HWND hWnd, Pointer lParam) {
		if (hWnd == null) return true; 
		
		boolean windowVisible = User32.INSTANCE.IsWindowVisible(hWnd);
		int windowStyle = User32.INSTANCE.GetWindowLong(hWnd, WinUser.GWL_STYLE);
		boolean hasChildStyle = (windowStyle & WinUser.WS_CHILD) == 0 ? false : true;
		HWND owner = User32.INSTANCE.GetWindow(hWnd, new DWORD(Long.valueOf(WinUser.GW_OWNER)));
		
		if (hasChildStyle || !windowVisible || owner != null) {
			logger.trace("Retuning because the window is not a top level window, is not visible or has an owner.");
			return true;
		}
		
		char[] className = new char[MAX_WINDOW_CLASS_NAME_SIZE]; 
		
		int result = User32.INSTANCE.GetClassName(hWnd, className, MAX_WINDOW_CLASS_NAME_SIZE);
		if (result == 0) {
			logger.trace("Failed to get the window class name.");
		}
		
		String windowClassName = new String(className).trim();
		
		if (!POKER_STARS_TABLE_FRAME_CLASS.equals(windowClassName)) {
			logger.trace("{} does not equal {}, returning.", windowClassName, POKER_STARS_TABLE_FRAME_CLASS);
			return true;
		}
		
		int windowTextLength = User32.INSTANCE.GetWindowTextLength(hWnd);

		// +1 to include room for the terminating character.
		char[] windowText = new char[windowTextLength+1];
		
		result = User32.INSTANCE.GetWindowText(hWnd, windowText, windowTextLength+1);
		if (result == 0) {
			logger.trace("Failed to get window text, returning.");
			return true;
		}
		
		String title = new String(windowText);
		
		if (!"".equals(title)) {
			logger.trace("Added {} to window handles.", title);
			handles.add(new TableWindowResult(hWnd, PokerVenue.POKERSTARS, title));
		}

		return true;
	}
	
}
