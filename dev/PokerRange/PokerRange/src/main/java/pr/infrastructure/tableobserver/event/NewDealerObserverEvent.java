package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import pr.domain.poker.table.Seat;

public class NewDealerObserverEvent implements SeatObserverEvent {

	private final Seat seat;
	
	public NewDealerObserverEvent(Seat seat) {
		this.seat = seat;
	}
	
	@Override
	public Seat getSeat() {
		return seat;
	}

	@Override
	public SeatObserverEvent setSeat(Seat seat) {
		return new NewDealerObserverEvent(seat);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seat", seat)
				.toString();
	}

}
