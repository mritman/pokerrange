package pr.infrastructure.tableobserver.pokerstars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.Image;

import javax.inject.Inject;

import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.TableSizeEvent;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.TableObserver;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.pokerstars.chatreader.PokerStarsChatReader;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowReader;


public class PokerStarsTableObserver implements TableObserver {

	private final PokerStarsLogReader logReader;
	private final PokerStarsWindowReader windowReader;
	private final PokerStarsLogSeatAdjuster logSeatAdjuster;
	private final PokerStarsChatReader chatReader;
	
	/**
	 * Maps a PokerTable name to the table's size.
	 */
	private final Map<String, TableSize> tableSizes;
	
	@Inject
	public PokerStarsTableObserver(PokerStarsLogReader logReader,
			PokerStarsWindowReader windowReader,
			PokerStarsLogSeatAdjuster logSeatAdjuster,
			PokerStarsChatReader chatReader) {
		
		this.logReader = logReader;
		this.windowReader = windowReader;
		this.logSeatAdjuster = logSeatAdjuster;
		this.chatReader = chatReader;
		tableSizes = new HashMap<>();
	}

	@Override
	public List<PokerEvent> readTable(Image image, String tableName) {
		// TODO add new game event recognition to logReader and 
		// deal with timing issues between the log and the window.
		List<PokerEvent> logEvents = getLogEvents(tableName);
		List<PokerEvent> windowEvents = getWindowEvents(image, tableName);
		List<PokerEvent> chatEvents = chatReader.getEvents();
		
		List<PokerEvent> events = new ArrayList<>();
		events.addAll(logEvents);
		events.addAll(windowEvents);
		events.addAll(chatEvents);
		return events;
	}

	private List<PokerEvent> getLogEvents(String tableName) {
		List<PokerEvent> logEvents = logReader.getEvents(tableName);
		
		TableSize size = getTableSize(tableName, logEvents);
		
		if (size == null) return Collections.emptyList();
		
		return logSeatAdjuster.adjust(logEvents, size);
	}
	
	private List<PokerEvent> getWindowEvents(Image image, String tableName) {
		TableSize size = tableSizes.get(tableName);
		
		if (size == null) return Collections.emptyList();
		
		List<ObserverEvent> windowEvents = windowReader.getEvents(image, tableName, size);
		
		return PokerStarsWindowEventConverter.convert(windowEvents);
	}

	private TableSize getTableSize(String tableName, List<PokerEvent> logEvents) {
		TableSize size = tableSizes.get(tableName);
		
		if (size == null) {
			size = getSizeFromEvents(logEvents);
		}
		
		if (size != null) {
			tableSizes.put(tableName, size);
		}
	
		return size;
	}

	private TableSize getSizeFromEvents(List<PokerEvent> logEvents) {
		for (PokerEvent event : logEvents) {
			if (event instanceof TableSizeEvent) {
				TableSizeEvent tableSizeEvent = (TableSizeEvent) event;
				return tableSizeEvent.getSize();
			}
		}
		
		return null;
	}

}
