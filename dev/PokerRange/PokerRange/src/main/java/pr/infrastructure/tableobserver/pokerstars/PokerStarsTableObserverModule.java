package pr.infrastructure.tableobserver.pokerstars;

import pr.application.pokervenues.pokerstars.PokerStars;
import pr.infrastructure.system.ProcessEnumerator;
import pr.infrastructure.system.WindowsProcessEnumerator;
import pr.infrastructure.tableobserver.PokerVenueHookService;
import pr.infrastructure.tableobserver.TableCardReader;
import pr.infrastructure.tableobserver.TableObserver;
import pr.infrastructure.tableobserver.pokerstars.chatreader.PokerStarsChatReader;
import pr.infrastructure.tableobserver.pokerstars.chatreader.PokerStarsDuplicateChatlineRemover;
import pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser.PokerStarsChatlineParser;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogParser;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogReader;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogReaderThread;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsActionReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsCardReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsDealerButtonReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsEventReorderer;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsHoleAndBoardCardsReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsOpponentHoleCardsReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowChangeAnalyzer;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowStateReader;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class PokerStarsTableObserverModule extends AbstractModule {
	
	@Override
	protected void configure() {
		binder().requireExplicitBindings();
		
		bind(TableCardReader.class).to(PokerStarsCardReader.class).in(Singleton.class);
		bind(PokerStarsActionReader.class).in(Singleton.class);
		bind(PokerStarsOpponentHoleCardsReader.class).in(Singleton.class);
		bind(PokerStarsHoleAndBoardCardsReader.class).in(Singleton.class);
		
		bind(ProcessEnumerator.class).to(WindowsProcessEnumerator.class);
		bind(PokerVenueHookService.class).in(Singleton.class);
		bind(PokerStarsDuplicateChatlineRemover.class);
		bind(PokerStarsChatlineParser.class);
		bind(PokerStarsChatReader.class);
		
		bind(PokerStarsLogReaderThread.class);
		bind(PokerStarsLogParser.class);
		bind(PokerStarsLogReader.class);
		bind(PokerStarsLogSeatAdjuster.class);
		
		bind(PokerStarsDealerButtonReader.class);
		bind(PokerStarsWindowStateReader.class);
		bind(PokerStarsWindowChangeAnalyzer.class);
		bind(PokerStarsEventReorderer.class);
		bind(PokerStarsWindowReader.class);
		
		bind(TableObserver.class).annotatedWith(PokerStars.class).to(PokerStarsTableObserver.class);
	}

}
