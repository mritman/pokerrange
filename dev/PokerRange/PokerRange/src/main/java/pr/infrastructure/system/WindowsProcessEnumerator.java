package pr.infrastructure.system;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pr.infrastructure.jna.windows.Psapi;
import pr.util.Optional;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;

public class WindowsProcessEnumerator implements ProcessEnumerator {
	
	private static final int PROCESS_QUERY_INFORMATION = 0x0400;
	private static final int PROCESS_VM_READ = 0x0010;
	
	@Override
	public List<ProcessInfo> getProcesses() {
		List<Integer> processIds = getProcessIds();
		return getProcessInfo(processIds);
	};
	
	@Override
	public Optional<ProcessInfo> getProcess(final String name) {
		com.google.common.base.Optional<ProcessInfo> element = Iterables.tryFind(getProcesses(), new Predicate<ProcessInfo>() {
			public boolean apply(ProcessInfo processInfo) {
				return processInfo.getName().equals(name);
			}
		});
		
		return Optional.ofNullable(element.orNull());
	}
	
	private List<ProcessInfo> getProcessInfo(List<Integer> processIds) {
		ArrayList<ProcessInfo> processInfoList = new ArrayList<>();
		
		for (int processId : processIds) {
			HANDLE process = Kernel32.INSTANCE.OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, false, processId);
			char[] imageFileNameBuffer = new char[Psapi.MAX_IMAGE_FILE_NAME_LENGTH.intValue()];
			int length = Psapi.INSTANCE.GetModuleBaseName(process, null, imageFileNameBuffer, Psapi.MAX_IMAGE_FILE_NAME_LENGTH.intValue());
			Kernel32.INSTANCE.CloseHandle(process);
			
			if (length == 0) {
				continue;
			}
			
			String processName = Native.toString(imageFileNameBuffer);
			processInfoList.add(new ProcessInfo(processId, processName));
		}

		return processInfoList;
	}

	private List<Integer> getProcessIds() {
		DWORD[] pProcessIdBuffer = new DWORD[Psapi.MAX_RUNNING_PROCESSES];
		DWORD bufferSize = new DWORD(Psapi.MAX_RUNNING_PROCESSES);
		DWORD[] pBytesReturned = new DWORD[1];
		
		boolean success = Psapi.INSTANCE.EnumProcesses(pProcessIdBuffer, bufferSize, pBytesReturned);
		if (!success) {
			throw new RuntimeException("Could not retrieve process ids.");
		}
		
		int numberOfProcessIds = pBytesReturned[0].intValue() / DWORD.SIZE;
		
		DWORD[] processIdDwords = Arrays.copyOfRange(pProcessIdBuffer, 0, numberOfProcessIds);
		List<DWORD> processIds = Arrays.asList(processIdDwords);
		
		return Lists.transform(processIds, new Function<DWORD, Integer>() {
			public Integer apply(DWORD element) {
				return element.intValue();
			}
		});
	}
	
}
