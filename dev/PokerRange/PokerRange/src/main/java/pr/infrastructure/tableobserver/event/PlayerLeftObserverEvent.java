package pr.infrastructure.tableobserver.event;


public class PlayerLeftObserverEvent extends PlayerObserverEvent  {

	public PlayerLeftObserverEvent(String name) {
		super(name);
	}

}
