package pr.infrastructure.tableobserver.pokerstars.logreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import lombok.Setter;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;
import pr.application.preferences.PreferencesListener;
import pr.util.AutoclosingDaemonThread;
import pr.util.BoundedFifoList;

/**
 * <p>
 * This class monitors the PokerStars hand history log directory as set in the
 * {@link Preferences} for any changes to the poker table log files.
 * </p>
 * 
 * <p>
 * Before starting this thread client code has to register a callback to be
 * called when a log has been updated. The callback will receive the table
 * name and a collection of log lines that were read from that table's log.
 * </p>
 * 
 * @TODO Close buffered readers that have not been updated for a certain period
 * of time.
 * 
 * @author M.Ritman
 * 
 */
public class PokerStarsLogReaderThread extends AutoclosingDaemonThread {

	private static final String LOGFILENAME_REGEX = "HH.*.txt";
	
	private static final int TAIL_LENGTH = 100;
	
	private final Preferences preferences;
	
	private final Map<String, BufferedReader> fileReaders = new HashMap<>();
	
	private String logDir;
	
	private boolean logDirChanged;
	
	@Setter
	private LogReaderCallBack logReaderCallBack;
	
	@Inject
	public PokerStarsLogReaderThread(Preferences preferences) {
		super("PokerStars Log Reader");
		this.preferences = preferences;
	}
	
	@Override
	public void run() {
		log.debug("{} thread started.", Thread.currentThread().getName());
		
		if (logReaderCallBack == null) {
			throw new IllegalArgumentException(
					"No callback was set to call when the log is updated. Call the setLogReaderCallBack() before starting this thread.");
		}
		
		observeHandHistoryDirectoryPreference(preferences);
		
		logDir = preferences.get(PreferenceName.POKERSTARS_HAND_HISTORY_DIRECTORY);
		logDirChanged = false;
		
		try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
			monitorLogs(watchService);
		} catch (IOException e) {
			log.error("Exception while monitoring logs", e);
		} catch (InterruptedException e) {
			log.debug("{} thread interrupted.", Thread.currentThread().getName());
		}
		
		cleanup();
		
		log.debug("{} thread will now exit.", Thread.currentThread().getName());
	}
	
	private void observeHandHistoryDirectoryPreference(Preferences preferences) {
		preferences.addListener(PreferenceName.POKERSTARS_HAND_HISTORY_DIRECTORY, new PreferencesListener() {
			
			@Override
			public void preferenceChanged(PreferenceName name, String value) {
				logDir = value;
				logDirChanged = true;
				interrupt();
			}
			
		});
	}
	
	private void monitorLogs(WatchService watchService) throws IOException, InterruptedException {
		Path logDirPath = registerLogDirectoryWithWatchService(watchService);
		
		while (!Thread.interrupted()) {
			try {
				WatchKey key = watchService.take();
				
				processPollEvents(logDirPath, key);
				
				if (!key.reset()) {
					log.warn("WatchKey no longer valid. Pausing log reader until the log directory is updated.");
					registerLogDirectoryWithWatchService(watchService);
					continue;
				}
			} catch (InterruptedException e) {
				if (logDirChanged) {
					logDirPath = registerLogDirectoryWithWatchService(watchService);
					
					// Clear the interrupted status
					Thread.interrupted();
					
					continue;
				}
				
				throw e;
			}
		}
	}
	
	private void processPollEvents(Path logDirPath, WatchKey key) {
		for (WatchEvent<?> event: key.pollEvents()) {
		    WatchEvent.Kind<?> eventKind = event.kind();
		    
		    if (eventKind == StandardWatchEventKinds.OVERFLOW) {
		        continue;
		    }
		    
		    @SuppressWarnings("unchecked")
			WatchEvent<Path> ev = (WatchEvent<Path>)event;
		    Path relativeLogFilePath = ev.context();
		    
		    Path logFile = logDirPath.resolve(relativeLogFilePath);

		    if (!isTextFile(logFile)) continue;
		    
		    if (eventKind == StandardWatchEventKinds.ENTRY_CREATE || eventKind == StandardWatchEventKinds.ENTRY_MODIFY) {
		    	try {
		    		processLog(logFile);
		    	} catch (IOException | NumberFormatException e ) {
					log.debug("Exception while processing log", e);
				}
		    }
		}
	}

	private Path registerLogDirectoryWithWatchService(WatchService watchService) throws IOException, InterruptedException {
		waitForValidLogDir();
		
		Path logDirPath = Paths.get(logDir);
		
		logDirPath.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY);
		
		return logDirPath;
	}
	
	private void waitForValidLogDir() throws InterruptedException {
		logDirChanged = false;
		
		while (logDir == null || !Files.isDirectory(Paths.get(logDir))) {
			try {
				Thread.sleep(TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS));
			} catch (InterruptedException e) {
				if (logDirChanged) {
					// Clear the interrupted status
					Thread.interrupted();
					continue;
				} else {
					throw e;
				}
			}
		}
	}

	private boolean isTextFile(Path file) {
        try {
        	return Files.probeContentType(file) != null && Files.probeContentType(file).equals("text/plain");
        } catch (IOException e) {
            log.error("Failed to check for file type", e);
        }
        
        return false;
	}

	private void processLog(Path fileName) throws IOException {
		String tableName = getTableNameFromFileName(fileName.toString());
		
		BufferedReader reader = fileReaders.get(tableName);
		if (reader == null) {
        	reader = addReader(fileName);
		}
		
		BoundedFifoList<String> logLines = new BoundedFifoList<>(TAIL_LENGTH);
		
		while (reader.ready()) {
			String line = reader.readLine();
			logLines.add(line);
		}
		
		logReaderCallBack.call(tableName, logLines);
	}
	
	private BufferedReader addReader(Path fileName) throws IOException {
		BufferedReader reader = Files.newBufferedReader(fileName, StandardCharsets.UTF_8);
		
		String tableName = getTableNameFromFileName(fileName.toString());
		
		fileReaders.put(tableName,  reader);	
		
		return reader;
	}
	
	private static String getTableNameFromFileName(String filePath) {
		String filename = getLogFileNameFromPath(filePath);
		
		String[] split = filename.split(" ");
		StringBuilder tableName;

    	if (split.length < 2) {
    		throw new IllegalArgumentException("The specified filename does not have the expected format.");
    	}
    	
    	tableName = new StringBuilder(split[1]);
    	
    	if (split.length > 2) {
    		for (int i = 2 ; i < split.length ; i++) {
    			if (!split[i].equals("-")) {
    				tableName.append(" ").append(split[i]);
    			} else {
    				break;
    			}
    		}
    	}
    	
    	return tableName.toString();
	}
	
	private static String getLogFileNameFromPath(String filePath) {
		return find(LOGFILENAME_REGEX, filePath);
	}
	
	private static String find(String regex, String line) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		
		if (matcher.find()) {
			return matcher.group();
		}
		
		return null;
	}
	
	private void cleanup() {
		for (BufferedReader reader : fileReaders.values()) {
			try {
				reader.close();
			} catch (IOException e) {
				log.error("Exception closing log reader", e);
			}
		}
	}
	
	interface LogReaderCallBack {
		
		void call(String tableName, List<String> logLines);
		
	}

}
