package pr.infrastructure.tableobserver.service;

import java.util.Set;

import lombok.Getter;
import pr.domain.poker.table.PokerTableKey;

import com.google.common.collect.ImmutableSet;

public class TablesUpdate {

	@Getter private final Set<PokerTableKey> tablesFound;
	@Getter private final Set<PokerTableKey> tablesClosed;
	
	public TablesUpdate(Set<PokerTableKey> tablesFound,	Set<PokerTableKey> tablesClosed) {
		this.tablesFound= ImmutableSet.copyOf(tablesFound);
		this.tablesClosed = ImmutableSet.copyOf(tablesClosed);
	}
	
}
