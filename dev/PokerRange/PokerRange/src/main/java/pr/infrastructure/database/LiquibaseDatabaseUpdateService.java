package pr.infrastructure.database;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.inject.Inject;

import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;

import com.google.common.base.Strings;

public class LiquibaseDatabaseUpdateService implements DatabaseUpdateService, Closeable {
	
	private static final String DEFAULT_DATABASE_URL = "jdbc:postgresql://localhost:5432/pokerrange";
	private static final String DEFAULT_DATABASE_USER = "postgres";
	private static final String DEFAULT_DATABASE_PASSWORD = "dbpass";
	private static final String LIQUIBASE_CHANGELOG_FILENAME = "/pr/infrastructure/database/changelog/db-changelog-master.xml";
	
	private final Preferences preferences;
	
	private final Liquibase liquibase;

	@Inject
	public LiquibaseDatabaseUpdateService(Preferences preferences) throws DatabaseUpdateServiceException {
		this.preferences = preferences;
		
		Connection connection = getDatabaseConnection();
		
		ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor() {
			@Override
			public InputStream getResourceAsStream(String file)	throws IOException {
				return LiquibaseDatabaseUpdateService.class.getResourceAsStream(file);
			}
			@Override
			public Enumeration<URL> getResources(String packageName) throws IOException {
				return LiquibaseDatabaseUpdateService.class.getClassLoader().getResources(packageName);
			}
		};
		
		DatabaseConnection databaseConnection = new JdbcConnection(connection);
		
		try {
			liquibase = new Liquibase(LIQUIBASE_CHANGELOG_FILENAME, resourceAccessor, databaseConnection);
		} catch (LiquibaseException e) {
			throw new DatabaseUpdateServiceException("Failed to initialize Liquibase.", e);
		}
	}

	@Override
	public boolean isUpdateRequired() throws DatabaseUpdateServiceException {
		try {
			return !liquibase.listUnrunChangeSets(null).isEmpty();
		} catch (LiquibaseException e) {
			throw new DatabaseUpdateServiceException("Could not verify if a database update was required.", e);
		}
	}

	@Override
	public void update() throws DatabaseUpdateServiceException {
		try {
			if (isUpdateRequired()) {
				liquibase.validate();
				liquibase.update(null);
			}
		} catch (LiquibaseException e) {
			throw new DatabaseUpdateServiceException("Database update failed.", e);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			liquibase.forceReleaseLocks();
			if (liquibase.getDatabase().getConnection() != null) {
				liquibase.getDatabase().getConnection().close();
			}
		} catch (LiquibaseException e) {
			throw new IOException("Failed to close LiquibaseUpdateService.", e);
		}
	}
	
	private Connection getDatabaseConnection() throws DatabaseUpdateServiceException {
		String url = DEFAULT_DATABASE_URL;
		String user = DEFAULT_DATABASE_USER;
		String password = DEFAULT_DATABASE_PASSWORD;
		
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_URL))) {
			url = preferences.get(PreferenceName.DATABASE_URL);
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_USER))) {
			user = preferences.get(PreferenceName.DATABASE_USER);
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_PASSWORD))) {
			password = preferences.get(PreferenceName.DATABASE_PASSWORD);
		}
		
		try {
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			throw new DatabaseUpdateServiceException("Failed to connect to the database.", e);
		}
	}

}
