package pr.infrastructure.tableobserver.pokerstars;

import java.util.LinkedList;
import java.util.List;

import javafx.scene.image.Image;

import javax.inject.Inject;

import pr.domain.gamestate.GameState;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.oldevent.NewGameEvent;
import pr.domain.poker.oldevent.OpponentFoldedHandEvent;
import pr.domain.poker.oldevent.OpponentReceivedHandEvent;
import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.oldevent.action.HeroFoldedEvent;
import pr.domain.poker.oldevent.action.HeroHoleCardsEvent;
import pr.domain.poker.oldevent.board.FlopEvent;
import pr.domain.poker.oldevent.board.RiverEvent;
import pr.domain.poker.oldevent.board.TurnEvent;
import pr.domain.poker.oldevent.seat.SeatTakenByOpponentEvent;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.pokerstars.OldPokerStarsWindowReader.ReadResult;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowStateReader;

public class OldPokerStarsTableObserver {
	
	private final OldPokerStarsWindowReader windowReader;
	
	private final PokerStarsLogReader logReader;
	
	private final OpponentAccountRepository opponentAccountRepository;

	@Inject
	OldPokerStarsTableObserver(PokerStarsWindowStateReader windowReader, PokerStarsLogReader logReader, OpponentAccountRepository opponentAccountRepository) {
		this.windowReader = null;
		this.logReader = logReader;
		this.opponentAccountRepository = opponentAccountRepository;
	}
	
	public List<PokerTableEvent> readTable(Image image, PokerTable table) {
		List<PokerTableEvent> tableEvents = new LinkedList<>();
		
		HoleCards currentHoleCards = table.getGameState().getHoleCards();
		BoardCards currentBoardCards = table.getGameState().getBoardCards();
		
		boolean newHand = checkForNewHand(image, table, tableEvents);
		
//		logReader.updateTable(table, tableEvents, newHand);
		
		List<Seat> opponentsInHand = windowReader.readOpponentsInHand(image, table);
		ReadResult<BoardCards> boardCardsResult = windowReader.readBoardCards(image, table.getGameState().getBoardCards());
		ReadResult<HoleCards> holeCardsResult = windowReader.readHoleCards(image, table);

		HoleCards newHoleCards = holeCardsResult.getResult();
		BoardCards newBoardCards = boardCardsResult.getResult();
		
		analyzeOpponentsInHand(opponentsInHand, table, tableEvents);

		if (!holeCardsResult.isUpdated() && !boardCardsResult.isUpdated()) {
			return tableEvents;
		}
		
		newHoleCards = analyzeHoleCardChanges(table, tableEvents, currentHoleCards, newHoleCards);
		analyzeBoardCardChanges(boardCardsResult, table, tableEvents, currentBoardCards, newBoardCards);
		
		GameState gameState = new GameState(newHoleCards, newBoardCards);
		table.setGameState(gameState);
		
		return tableEvents;
	}
	
	private void analyzeOpponentsInHand(List<Seat> opponentsInHand, PokerTable table, List<PokerTableEvent> tableEvents) {
		// All opponents in the hand not already known are recognized as new opponents.
		for (Seat seat : opponentsInHand) {
			if (table.getOpponent(seat) == null && table.getPlayerSeat() != seat) {
				Opponent opponent = table.addOpponent(seat.name(), seat);
				opponentAccountRepository.refreshRanges(opponent);
				opponent.setInHand(true);
				SeatTakenByOpponentEvent seatTakenByOpponentEvent = new SeatTakenByOpponentEvent(table, opponent);
				tableEvents.add(seatTakenByOpponentEvent);
			}
		}

		// The in-hand status for all opponents is updated.
		for (Opponent opponent : table.getOpponents()) {
			boolean inHand = opponentsInHand.contains(opponent.getSeat());
			
			if (opponent.isInHand() != inHand && inHand) {
				OpponentReceivedHandEvent event = new OpponentReceivedHandEvent(opponent, table);
				tableEvents.add(event);
			} else if (opponent.isInHand() != inHand && !inHand) {
				OpponentFoldedHandEvent event = new OpponentFoldedHandEvent(opponent, table);
				tableEvents.add(event);
			}
			
			opponent.setInHand(inHand);
		}
	}

	private boolean checkForNewHand(Image image, PokerTable table, List<PokerTableEvent> tableEvents) {
		if (windowReader.isNewHand(image, table)) {
			tableEvents.add(new NewGameEvent(table));
			table.newGame();
			return true;
		}
		
		return false;
	}

	private void analyzeBoardCardChanges(
			ReadResult<BoardCards> boardCardsResult, 
			PokerTable table,
			List<PokerTableEvent> tableEvents, 
			BoardCards currentBoardCards,
			BoardCards newBoardCards) {
		
		if (boardCardsResult.isUpdated()) {
			if (!flopsEqual(currentBoardCards, newBoardCards) && newBoardCards.getFlop() != null) {
				tableEvents.add(new FlopEvent(table));
			}
			
			if (currentBoardCards.getTurn() != newBoardCards.getTurn() && newBoardCards.getTurn() != null) {
				tableEvents.add(new TurnEvent(table));
			}
			
			if (currentBoardCards.getRiver() != newBoardCards.getRiver() && newBoardCards.getRiver() != null) {
				tableEvents.add(new RiverEvent(table));
			}
		}
		
	}

	private boolean flopsEqual(BoardCards currentBoardCards, BoardCards newBoardCards) {
		if (currentBoardCards.getFlop() == null && newBoardCards.getFlop() == null 
				|| (currentBoardCards.getFlop() != null && currentBoardCards.getFlop().equals(newBoardCards.getFlop()))) {
			
			return true;
		}

		return false;
	}
	
	private HoleCards analyzeHoleCardChanges(
			PokerTable table,
			List<PokerTableEvent> tableEvents, 
			HoleCards currentHoleCards,
			HoleCards newHoleCards) {
		
		HoleCards result = newHoleCards;
		
		boolean newGame = false;
		for (PokerTableEvent tableEvent : tableEvents) {
			if (tableEvent instanceof NewGameEvent) {
				newGame = true;
			}
		}
		
		if (heroFolded(currentHoleCards, newHoleCards)) {
			result = currentHoleCards;
			result = result.fold();
			
			tableEvents.add(new HeroFoldedEvent(table));
		} else if (noHoleCardsReceivedSinceLastFold(currentHoleCards, newHoleCards, newGame)) {
			result = currentHoleCards;
		} else if (newCardsDealt(currentHoleCards, newHoleCards)) {
			tableEvents.add(new HeroHoleCardsEvent(table));
		}
		
		return result;
	}

	private boolean newCardsDealt(HoleCards currentHoleCards, HoleCards newHoleCards) {
		return currentHoleCards == null && newHoleCards != null;
	}
	
	private boolean heroFolded(HoleCards currentHoleCards, HoleCards newHoleCards) {
		return currentHoleCards != null && !currentHoleCards.isFolded() && newHoleCards == null;
	}
	
	private boolean noHoleCardsReceivedSinceLastFold(HoleCards currentHoleCards, HoleCards newHoleCards, boolean newGame) {
		return currentHoleCards != null && currentHoleCards.isFolded() && newHoleCards == null && !newGame;
	}
	
}
