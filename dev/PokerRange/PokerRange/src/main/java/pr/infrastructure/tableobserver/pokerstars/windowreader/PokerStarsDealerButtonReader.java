package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import pr.application.exception.InitializationException;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;
import pr.util.FileType;
import pr.util.ImageUtil;
import pr.util.Optional;

public class PokerStarsDealerButtonReader {

	public static final String DEALER_BUTTON_IMAGE_NAME = "dealerbutton" + FileType.PNG.extension();
	private static Image dealerButtonImage;
	
	public PokerStarsDealerButtonReader() {
		if (dealerButtonImage == null) {
			try {
				dealerButtonImage = ImageUtil.loadFxImage(getClass().getResource(DEALER_BUTTON_IMAGE_NAME));
			} catch (IOException e) {
				throw new InitializationException(e);
			}
		}
	}
	
	public Optional<Seat> readDealerPosition(Image image, TableSize tableSize) {
		for (Seat seat : tableSize.getSeats()) {
			if (isDealer(image, tableSize, seat)) {
				return Optional.of(seat);
			}
		}
		
		return Optional.empty();
	}

	private boolean isDealer(Image image, TableSize tableSize, Seat seat) {
		Position position = PokerStarsTableData.getDealerPosition(tableSize, seat);
		WritableImage buttonImage = new WritableImage(image.getPixelReader(), position.getX(), position.getY(),
				PokerStarsTableData.DEALER_BUTTON_WIDTH, PokerStarsTableData.DEALER_BUTTON_HEIGHT);
		
		return ImageUtil.compareImage(buttonImage, dealerButtonImage);
	}

}
