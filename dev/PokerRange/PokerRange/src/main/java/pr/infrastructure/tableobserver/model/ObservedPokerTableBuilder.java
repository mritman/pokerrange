package pr.infrastructure.tableobserver.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import lombok.Getter;
import pr.domain.poker.BoardCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;

public class ObservedPokerTableBuilder {

	@Getter	private String name;
	@Getter private PokerVenue venue;
	@Getter private TableSize size;
	@Getter	private UUID handId;
	@Getter private List<ObservedPlayer> players;
	@Getter private BoardCards boardCards;
	@Getter private Seat dealerPosition;


	public ObservedPokerTableBuilder() {
		// Intentionally empty.
	}
	
	public ObservedPokerTableBuilder(ObservedPokerTable table) {
		name = table.getName();
		venue = table.getVenue();
		size = table.getSize();
		handId = table.getHandId();
		players = table.getPlayers();
		boardCards = table.getBoardCards();
		dealerPosition = table.getDealer();
	}
	
	public ObservedPokerTableBuilder setName(String name) {
		this.name = name;
		return this;
	}
	
	public ObservedPokerTableBuilder setVenue(PokerVenue venue) {
		this.venue = venue;
		return this;
	}

	public ObservedPokerTableBuilder setSize(TableSize size) {
		this.size = size;
		return this;
	}
	
	public ObservedPokerTableBuilder setHandId(UUID handId) {
		this.handId = handId;
		return this;
	}
	
	public ObservedPokerTableBuilder setPlayers(List<ObservedPlayer> players) {
		this.players = players;
		return this;
	}
	
	public ObservedPokerTableBuilder setBoardCards(BoardCards boardCards) {
		this.boardCards = boardCards;
		return this;
	}
	
	public ObservedPokerTableBuilder setDealerPosition(Seat dealerPosition) {
		this.dealerPosition = dealerPosition;
		return this;
	}
	
	public ObservedPokerTable build() {
		if (players != null && !players.isEmpty()) {
			List<ObservedPlayer> sortedPlayers = new ArrayList<>(players);
			Collections.sort(sortedPlayers, new Comparator<ObservedPlayer>() {
				@Override
				public int compare(ObservedPlayer player1, ObservedPlayer player2) {
					return player1.getSeat().compareTo(player2.getSeat());
				}
			});
			this.players = sortedPlayers;
		}
		
		return new ObservedPokerTable(name, venue, size, players, handId, boardCards, dealerPosition);
	}

}
