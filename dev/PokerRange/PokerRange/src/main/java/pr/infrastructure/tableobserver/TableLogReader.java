package pr.infrastructure.tableobserver;

import java.util.List;

import pr.domain.poker.event.PokerEvent;

public interface TableLogReader {

	/**
	 * Gets the log events since the last call to this method. In case it is the
	 * first call, all events since the start of monitoring will be returned.
	 * 
	 * @param tableName
	 *            the name of the table to get the log events for
	 * @return the events
	 * 
	 */
	List<PokerEvent> getEvents(String tableName);
	
}
