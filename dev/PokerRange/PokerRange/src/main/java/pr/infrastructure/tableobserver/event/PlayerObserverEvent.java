package pr.infrastructure.tableobserver.event;


import com.google.common.base.Objects;

import lombok.Getter;

public abstract class PlayerObserverEvent implements ObserverEvent {

	@Getter	private final String name;
	
	protected PlayerObserverEvent(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("name", name).toString();
	}

}
