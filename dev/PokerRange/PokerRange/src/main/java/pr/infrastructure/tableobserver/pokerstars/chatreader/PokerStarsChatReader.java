package pr.infrastructure.tableobserver.pokerstars.chatreader;

import java.util.List;

import javax.inject.Inject;

import pr.domain.poker.PokerVenue;
import pr.domain.poker.event.PokerEvent;
import pr.infrastructure.tableobserver.PokerVenueHookService;
import pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser.PokerStarsChatlineParser;

public class PokerStarsChatReader {
	
	private PokerVenueHookService hookService;
	private PokerStarsDuplicateChatlineRemover duplicateChatlineRemover;
	private PokerStarsChatlineParser chatLineParser;

	@Inject
	public PokerStarsChatReader(PokerVenueHookService hookService,
			PokerStarsDuplicateChatlineRemover duplicateChatLineRemover,
			PokerStarsChatlineParser chatLineParser) {
		
		this.hookService = hookService;
		this.duplicateChatlineRemover = duplicateChatLineRemover;
		this.chatLineParser = chatLineParser;
	}
	
	public List<PokerEvent> getEvents() {
		return chatLineParser.parse(getLines());
	}
	
	private List<String> getLines() {
		List<List<String>> lineBatches = hookService.getChatLines(PokerVenue.POKERSTARS);
		return duplicateChatlineRemover.getLines(lineBatches);
	}
	
}