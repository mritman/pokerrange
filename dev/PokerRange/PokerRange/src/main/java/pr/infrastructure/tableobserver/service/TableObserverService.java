package pr.infrastructure.tableobserver.service;

import java.util.Set;

import pr.domain.poker.table.PokerTableKey;
import pr.util.Service;

public abstract class TableObserverService extends Thread implements Service {

	/**
	 * Returns the list of poker table windows that are open.
	 * 
	 * @return
	 */
	public abstract Set<PokerTableKey> getTables();
	
	/**
	 * Sets the table to be monitored by the service. This table needs to be an
	 * element in the list returned by {@link #getTables()} or an
	 * {@link IllegalArgumentException} will be thrown.
	 * 
	 * @param table
	 *            the table to select
	 */
	public abstract void selectTable(PokerTableKey table);
	
	/**
	 * The table that is being monitored by the service.
	 * 
	 * @return the selected table
	 */
	public abstract PokerTableKey getSelectedTable();
	
	public abstract void addTableListener(TableListener tableListener);

}
