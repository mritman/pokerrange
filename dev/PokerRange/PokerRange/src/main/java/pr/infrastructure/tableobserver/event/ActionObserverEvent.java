package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;

public class ActionObserverEvent implements ObserverEvent {

	@Getter	private final Seat seat;
	@Getter private final ActionType actionType;
	
	public ActionObserverEvent(Seat seat, ActionType actionType) {
		this.seat = seat;
		this.actionType = actionType;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seat", seat)
				.add("actionType", actionType)
				.toString();
	}
	
}
