package pr.infrastructure.tableobserver;

import java.util.List;

import javafx.scene.image.Image;
import pr.domain.poker.event.PokerEvent;

public interface TableObserver {

	/**
	 * Observes a poker table for game events.
	 * 
	 * @param image
	 *            an image of the poker table window contents
	 * 
	 * @throws ImageProcessingException
	 *             if the reader was unable to extract the required information
	 *             from the table
	 */
	public List<PokerEvent> readTable(Image image, String tableName)	throws ImageProcessingException;

}
