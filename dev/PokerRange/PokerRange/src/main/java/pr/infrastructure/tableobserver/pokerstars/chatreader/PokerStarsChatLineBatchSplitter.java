package pr.infrastructure.tableobserver.pokerstars.chatreader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pr.util.IteratorFuser;
import pr.util.IteratorFuser.FusedEntry;

public class PokerStarsChatLineBatchSplitter {
	
	/**
	 * Splits batches that contains duplicate portions of the chat into separate batches. 
	 */
	public static List<List<String>> splitBatchesWithDuplicates(List<List<String>> batches) {
		List<List<String>> splitBatches = new ArrayList<>();
		
		for (List<String> batch : batches) {
			List<List<String>> newBatches = split(batch);
			splitBatches.addAll(newBatches);
		}
		
		return splitBatches;
	}

	private static List<List<String>> split(List<String> batch) {
		List<List<String>> splitBatches = new ArrayList<>();
		List<String> currentBatch = new ArrayList<>();
		int index = 0;
		
		while (index != batch.size()) {
			boolean match = getLineMatch(currentBatch, batch.subList(index, batch.size()));
			
			if (!match) {
				currentBatch.add(batch.get(index));
				index++;
			} else {
				splitBatches.add(currentBatch);
				currentBatch = new ArrayList<>();
			}
		}
		
		if (!currentBatch.isEmpty()) {
			splitBatches.add(currentBatch);
		}
		
		return splitBatches;
	}
	
	private static boolean getLineMatch(List<String> batchOne, List<String> batchTwo) {
		if (batchOne.isEmpty() || batchTwo.isEmpty()) return false;
		
		int startIndex = batchOne.indexOf(batchTwo.get(0));
		if (startIndex == -1) return false;
		
		Iterator<FusedEntry<String, String>> fusedIterator = IteratorFuser.fuse(batchOne.subList(startIndex, batchOne.size()), batchTwo);
		while (fusedIterator.hasNext()) {
			FusedEntry<String,String> entry = fusedIterator.next();
			
			// It is only considered a match in case batchOne from the startIndex is fully contained in batchTwo.
			if (!entry.getB().isPresent()) return false;
			if (!entry.getA().isPresent()) break;
			if (!entry.getA().equals(entry.getB())) return false; 
		}

		return true;
	}
	
}
