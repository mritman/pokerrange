package pr.infrastructure.jna.windows;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.WORD;
import com.sun.jna.platform.win32.WinGDI;

public interface WinGDIPr extends WinGDI {

	public static DWORD SRCCOPY = new DWORD(13369376L); //0x00CC0020
	
	public static final int BI_RGB = 0;
	
	public static final  int DIB_RGB_COLORS = 0;

	public static class BITMAP extends Structure {

		public long bmType;
		public long bmWidth;
		public long bmHeight;
		public long bmWidthBytes;
		public WORD bmPlanes;
		public WORD bmBitsPixel;
		public Pointer bmBits;
		
		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "bmType", "bmWidth",
					"bmHeight", "bmWidthBytes", "bmPlanes", "bmBitsPixel",
					"bmBits" });
		}
		
	}
}
