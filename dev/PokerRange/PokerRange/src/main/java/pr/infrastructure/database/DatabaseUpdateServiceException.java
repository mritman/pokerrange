package pr.infrastructure.database;

public class DatabaseUpdateServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public DatabaseUpdateServiceException() {
		super();
	}
	
	public DatabaseUpdateServiceException(String message) {
		super(message);
	}
	
	public DatabaseUpdateServiceException(String message, Throwable e) {
		super(message, e);
	}

}
