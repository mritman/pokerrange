package pr.infrastructure.persistence.jpa;

import java.util.Properties;

import pr.application.preferences.FilePreferences;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;
import pr.domain.poker.player.OpponentAccountRepository;

import com.google.common.base.Strings;
import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.persist.jpa.JpaPersistModule;

public class JpaRepositoryModule extends PrivateModule {

	public static final String PERSISTENCE_UNIT_NAME = "org.pokerrange";
	
	@Override
	protected void configure() {
		binder().requireExplicitBindings();

		install(createJpaPersistModule());
		
		bind(OpponentAccountRepository.class).to(JpaOpponentAccountRepository.class).in(Singleton.class);
		bind(JpaPersistServiceManager.class).asEagerSingleton();
		
		expose(OpponentAccountRepository.class);
		expose(JpaPersistServiceManager.class);
	}
	
    /**
	 * This method creates the JpaPersistModule with user specified values for
	 * the database URL, user and password properties if they exist.
	 * 
	 * This is a dirty solution because it creates a JavaPreferences object to
	 * retrieve the property values instead of getting this from Guice's DI
	 * context.
	 * 
	 * @return the JpaPersistModule with the user specified values for the
	 *         database, user and password properties if they exist
	 */
	private static JpaPersistModule createJpaPersistModule() {
		JpaPersistModule jpaPersistModule = new JpaPersistModule(PERSISTENCE_UNIT_NAME);
		
		Preferences preferences = new FilePreferences();
		Properties properties = new Properties();

		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_URL))) {
			properties.setProperty("javax.persistence.jdbc.url", preferences.get(PreferenceName.DATABASE_URL));
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_USER))) {
			properties.setProperty("javax.persistence.jdbc.user", preferences.get(PreferenceName.DATABASE_USER));
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.DATABASE_PASSWORD))) {
			properties.setProperty("javax.persistence.jdbc.password", preferences.get(PreferenceName.DATABASE_PASSWORD));
		}
		
		jpaPersistModule.properties(properties);
		
		return jpaPersistModule;
	}

}
