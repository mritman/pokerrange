package pr.infrastructure.persistence.jpa;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.util.Service;

import com.google.inject.persist.PersistService;

public class JpaPersistServiceManager implements Service {

	private final Logger logger = LoggerFactory.getLogger(JpaPersistServiceManager.class);
	
	private final PersistService persistService;
	
	private final boolean started;
	
	@Inject
	public JpaPersistServiceManager(PersistService persistService) {
		this.persistService = persistService;
		persistService.start();
		started = true;
		logger.debug("Persist service started.");
	}
	
	@Override
	public void startService() {
		// The persist service is already initialized in this class' constructor
		// to ensure it is started before any EntityManagers
		// need to be injected. Nothing is here done at startup. This
		// service instance is only used to stop the persist service at
		// shutdown.
	}

	@Override
	public void stopService() {
		persistService.stop();
		logger.debug("Persist service stopped.");
	}

	@Override
	public boolean isStarted() {
		return started;
	}

}
