package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.BoardCards;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.PokerTableKey;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.event.ActionObserverEvent;
import pr.infrastructure.tableobserver.event.FlopObserverEvent;
import pr.infrastructure.tableobserver.event.HoleCardsObserverEvent;
import pr.infrastructure.tableobserver.event.NewDealerObserverEvent;
import pr.infrastructure.tableobserver.event.NewGameObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.event.RiverObserverEvent;
import pr.infrastructure.tableobserver.event.SeatsInHandObserverEvent;
import pr.infrastructure.tableobserver.event.TurnObserverEvent;
import pr.infrastructure.tableobserver.model.ObservedPlayer;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsEventReorderer.Result;
import pr.util.Optional;
import pr.util.Utils;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class PokerStarsWindowChangeAnalyzer {
	
	private static final Logger logger = LoggerFactory.getLogger(PokerStarsWindowChangeAnalyzer.class);
	
	private final PokerStarsEventReorderer eventReorderer;
	
	private final Map<PokerTableKey, Boolean> bigBlindPosted = new HashMap<>();
	
	@Inject
	public PokerStarsWindowChangeAnalyzer(PokerStarsEventReorderer eventReorderer) {
		this.eventReorderer = eventReorderer;
	}
	
	public List<ObserverEvent> analyzeChanges(Optional<ObservedPokerTable> previous, ObservedPokerTable current) {
		Preconditions.checkNotNull("current", current);
		List<ObserverEvent> events = new ArrayList<>();
		
		//checkForNewHand(previous, current, events);
		checkForHoleCards(previous, current, events);
		//analyzeBoardCardChanges(previous, current, events);
		//analyzeActions(previous, current, events);
		
		return reorderIfNecessary(current, events);
	}

	private void checkForHoleCards(Optional<ObservedPokerTable> previous,
			ObservedPokerTable current, List<ObserverEvent> events) {

		for (ObservedPlayer player : current.getPlayers()) {
			if (newHoleCards(previous, player)) {
				events.add(new HoleCardsObserverEvent(player.getSeat(), player.getHoleCards().get()));
			}
		}
	}

	private boolean newHoleCards(Optional<ObservedPokerTable> previous, ObservedPlayer player) {
		if (!player.getHoleCards().isPresent()) return false; 
		if (!previous.isPresent()) 	return true;
		
		Optional<ObservedPlayer> previousPlayer = previous.get().getPlayer(player.getSeat());
		if (!previousPlayer.isPresent()) return true;
		
		if (!previousPlayer.get().getHoleCards().equals(player.getHoleCards())) {
			return true;
		}
		
		return false;
	}

	private void analyzeActions(
			Optional<ObservedPokerTable> previous, 
			ObservedPokerTable current,
			List<ObserverEvent> events) {
		
		if (!previous.isPresent()) return;
		
		List<ObservedPlayer> rotatedPlayers = rotatePastLastPlayerThatActed(current.getKey(), current.getPlayers());
		for (ObservedPlayer currentPlayer : rotatedPlayers) {
			Optional<ObservedPlayer> previousPlayer = previous.get().getPlayer(currentPlayer.getSeat());
		
			if (ActionType.POST_BIG_BLIND.equals(currentPlayer.getLastAction().orElse(null))) {
				bigBlindPosted.put(current.getKey(), Boolean.TRUE);
			}
			
			if (currentPlayer.getLastAction().isPresent()
					&& (!previousPlayer.isPresent() || !currentPlayer.getLastAction().equals(
									previousPlayer.get().getLastAction()))) {
				
				if (isFirstActionAfterBigBlind(current.getKey(), currentPlayer.getLastAction().get())) {
					events.add(new SeatsInHandObserverEvent(current.getSeatsInHand()));
					bigBlindPosted.put(current.getKey(), Boolean.FALSE);
				}
				
				events.add(new ActionObserverEvent(currentPlayer.getSeat(), currentPlayer.getLastAction().get()));
			}
		}
	}

	private List<ObservedPlayer> rotatePastLastPlayerThatActed(PokerTableKey tableKey, List<ObservedPlayer> players) {
		final Optional<Seat> nextSeatToAct = eventReorderer.getNextSeatToAct(tableKey);

		if (!nextSeatToAct.isPresent()) {
			return rotateToSmallBlindIfPresent(players);
		}
		
		List<ObservedPlayer> rotatedPlayers = new ArrayList<>(players);
		
		Utils.rotate(rotatedPlayers, new Predicate<ObservedPlayer>() {
			public boolean apply(ObservedPlayer player) {
				return player.getSeat() == nextSeatToAct.get();
			}
		});
		
		return rotatedPlayers;
	}

	private List<ObservedPlayer> rotateToSmallBlindIfPresent(List<ObservedPlayer> players) {
		final com.google.common.base.Optional<ObservedPlayer> smallBlind = Iterables.tryFind(players, new Predicate<ObservedPlayer>() {
			@Override
			public boolean apply(ObservedPlayer player) {
				return ActionType.POST_SMALL_BLIND.equals(player.getLastAction().orElse(null));
			}
		});
		
		List<ObservedPlayer> rotatedPlayers = new ArrayList<>(players);
		
		if (smallBlind.isPresent()) {
			
			Utils.rotate(rotatedPlayers, new Predicate<ObservedPlayer>() {
				public boolean apply(ObservedPlayer player) {
					return player.getSeat() == smallBlind.get().getSeat();
				}
			});
		}
		
		return rotatedPlayers;
	}

	private boolean isFirstActionAfterBigBlind(PokerTableKey key, ActionType actionType) {
		Boolean isBigBlindPosted = bigBlindPosted.get(key);
		boolean isBlindAction = actionType.isPostSmallBlind() || actionType.isPostBigBlind();
		return !isBlindAction && Optional.ofNullable(isBigBlindPosted).orElse(false);
	}

	private void analyzeBoardCardChanges(
			Optional<ObservedPokerTable> previous,
			ObservedPokerTable current,
			List<ObserverEvent> events) {
		
		if (!previous.isPresent()) {
			newBoardCards(current.getBoardCards(), events);
			return;
		}
		
		BoardCards previousBoardCards = previous.get().getBoardCards();
		BoardCards currentBoardCards = current.getBoardCards();
		
		if (previousBoardCards.equals(currentBoardCards)) {
			return;
		}
		
		if (!flopsEqual(previousBoardCards, currentBoardCards) && currentBoardCards.hasFlop()) {
			events.add(new FlopObserverEvent(currentBoardCards));
		}
		
		if (previousBoardCards.getTurn() != currentBoardCards.getTurn() && currentBoardCards.getTurn() != null) {
			events.add(new TurnObserverEvent(currentBoardCards));
		}
		
		if (previousBoardCards.getRiver() != currentBoardCards.getRiver() && currentBoardCards.getRiver() != null) {
			events.add(new RiverObserverEvent(currentBoardCards));
		}
	}
	
	private void newBoardCards(BoardCards boardCards, List<ObserverEvent> events) {
		if (boardCards.hasFlop()) {
			events.add(new FlopObserverEvent(boardCards));
		}
		if (boardCards.hasTurn()) {
			events.add(new TurnObserverEvent(boardCards));
		}
		if (boardCards.hasRiver()) {
			events.add(new RiverObserverEvent(boardCards));
		}
	}

	private boolean flopsEqual(BoardCards currentBoardCards, BoardCards newBoardCards) {
		if (currentBoardCards.getFlop() == null && newBoardCards.getFlop() == null 
				|| (currentBoardCards.getFlop() != null && currentBoardCards.getFlop().equals(newBoardCards.getFlop()))) {
			
			return true;
		}

		return false;
	}

	private boolean checkForNewHand(Optional<ObservedPokerTable> previous, 
			ObservedPokerTable current,
			List<ObserverEvent> events) {
		
		if (previous.isPresent() && previous.get().getDealer() != current.getDealer()) {
			events.add(new NewGameObserverEvent(current.getName()));
			events.add(new NewDealerObserverEvent(current.getDealer()));
			bigBlindPosted.put(current.getKey(), Boolean.FALSE);
			eventReorderer.newGame(current.getKey());
			
			return true;
		}

		return false;
	}

	private List<ObserverEvent> reorderIfNecessary(ObservedPokerTable table, List<ObserverEvent> events) {
		//TODO remove this logging, it was for debugging.
		
		Result result = eventReorderer.reorderIfNecessary(table.getKey(), table.getDealer(), events);
		
		switch (result.getType()) {
			case FAILED_TO_REORDER:
				logger.error("Failed to reorder events: " + events);
				break;
			case REORDERED:
				logger.debug("Succesfully reordered events: " + events);
				break;
			default:
				break;
		}
		
		return result.getEvents();
	}
	
}
