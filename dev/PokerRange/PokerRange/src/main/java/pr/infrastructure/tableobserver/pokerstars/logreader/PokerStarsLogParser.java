package pr.infrastructure.tableobserver.pokerstars.logreader;

import java.util.ArrayList;
import java.util.List;

import pr.domain.poker.event.PlayerSitsOutEvent;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.TableSizeEvent;
import pr.domain.poker.event.seat.SeatLeftEvent;
import pr.domain.poker.event.seat.SeatOccupiedEvent;
import pr.domain.poker.event.seat.SeatTakenEvent;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.util.Regex;

/**
 * This class parses the information from log lines from a PokerStars log file.
 * 
 * @author M.Ritman
 */
public class PokerStarsLogParser {

	public List<PokerEvent> getEvents(List<String> logLines) {
		List<PokerEvent> events = new ArrayList<>();

		addTableSize(logLines, events);
		addOpponentsThatLeft(logLines, events);
		addOpponentsPresent(logLines, events);
		addOpponentsThatJoined(logLines, events);
		addOpponentsSittingOut(logLines, events);
		
		return events;
	}

	private static void addTableSize(List<String> logLines, List<PokerEvent> events) {
		for (String line : logLines) {
			if (isTableSizeLine(line)) {
				TableSize size = getSizeFromTableSizeLine(line);
				TableSizeEvent tableSizeEvent = new TableSizeEvent(size);
				events.add(tableSizeEvent);
			}
		}
	}

	private static void addOpponentsThatLeft(List<String> logLines, List<PokerEvent> events) {
		for (String line : logLines) {
			if (isLeftTableLine(line)) {
				String opponentName = getNameFromTableLeftLine(line);
				SeatLeftEvent event = new SeatLeftEvent(opponentName);
				events.add(event);
			}
		}
	}
	
	private static void addOpponentsPresent(List<String> logLines, List<PokerEvent> events) {
		for (String line : logLines) {
			if (isSeatLine(line)) {
				SeatOccupiedEvent event = getSeatOccupiedEvent(line);
				
				if (!containsPlayerLeftEvent(events, event.getName())) {
					events.add(event);
				}
			}
		}
	}
	
	private static void addOpponentsThatJoined(List<String> logLines, List<PokerEvent> events) {
		for (String line : logLines) {
			if (isJoinedTableLine(line)) {
				String name = getNameFromJoinedTableLine(line);
				Seat seat = getSeatFromJoinedTableLine(line);

				SeatTakenEvent event = new SeatTakenEvent(seat, name);
				events.add(event);
			}
		}
	}
	
	private static void addOpponentsSittingOut(List<String> logLines, List<PokerEvent> events) {
		for (String line : logLines) {
			if (isSitsOutLine(line)) {
				String name = getNameFromSitsOutLine(line);
				events.add(new PlayerSitsOutEvent(name));
			}
		}
	}
	
	private static boolean containsPlayerLeftEvent(List<PokerEvent> events, String name) {
		for (PokerEvent event : events) {
			if (event instanceof SeatLeftEvent && ((SeatLeftEvent) event).getName().equals(name)) {
				return true;
			}
		}
		
		return false;
	}
	
	private static boolean isSeatLine(String line) {
		String regex = "^Seat \\d: .* \\(.* in chips\\)";
		return null != Regex.find(regex, line);
	}

	private static boolean isJoinedTableLine(String line) {
		String regex = "joins the table at seat";
		return null != Regex.find(regex, line);
	}
	
	private static String getNameFromJoinedTableLine(String line) {
		String regex = "(?<=^)(.*)(?=\\sjoins)";
		return Regex.find(regex, line);
	}
	
	private static Seat getSeatFromJoinedTableLine(String line) {
		String regex = "(?<=#)(.*)(?=\\s)";
		String seatNumber = Regex.find(regex, line);
		return Seat.parseSeat(seatNumber);
	}
	
	private static boolean isLeftTableLine(String line) {
		String regex = "^.* leaves the table";
		return null != Regex.find(regex, line);
	}
	
	private static boolean isSitsOutLine(String line) {
		String regex = "^.*: sits out";
		return null != Regex.find(regex, line);
	}
	
	private static boolean isTableSizeLine(String line) {
		String regex = "Table '.*' .*-max Seat .* is the button";
		return null != Regex.find(regex, line);
	}
	
	private static String getNameFromSitsOutLine(String line) {
		String regex = "^[^:]+";
		return Regex.find(regex, line);
	}
	
	private static String getNameFromTableLeftLine(String line) {
		String regex = "^[^\\s]+";
		return Regex.find(regex, line);
	}

	private static TableSize getSizeFromTableSizeLine(String line) {
		String regex = "(?<= )(\\d{1,2})(?=-max)";
		String size = Regex.find(regex, line);
		return TableSize.fromString(size);
	}
	
	private static SeatOccupiedEvent getSeatOccupiedEvent(String line) {
		Seat seat = getSeat(line);
		String name = getName(line);
		return new SeatOccupiedEvent(seat, name);
	}
	
	private static String getName(String line) {
		String regex = "(?<=Seat \\d{1,2}:\\s)(.*?)(?=\\s*\\()";
		return Regex.find(regex, line);
	}
	
	private static Seat getSeat(String line) {
		String regex = "(?<=^Seat )(\\d{1,2})(?=:)";
		
		String seatNumber = Regex.find(regex, line);
		
		return Seat.parseSeat(seatNumber);
	}

}
