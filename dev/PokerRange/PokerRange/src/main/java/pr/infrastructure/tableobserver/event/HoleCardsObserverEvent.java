package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.HoleCards;
import pr.domain.poker.table.Seat;

public class HoleCardsObserverEvent implements ObserverEvent {

	@Getter private final Seat seat;
	@Getter private final HoleCards holeCards;
	
	public HoleCardsObserverEvent(Seat seat, HoleCards holeCards) {
		super();
		this.seat = seat;
		this.holeCards = holeCards;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seat", seat)
				.add("holeCards", holeCards)
				.toString();
	}
	
}
