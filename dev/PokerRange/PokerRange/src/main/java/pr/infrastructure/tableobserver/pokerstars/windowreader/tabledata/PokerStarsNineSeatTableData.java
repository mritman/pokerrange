package pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata;

import lombok.Getter;
import pr.domain.poker.table.Seat;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.HoleCardsPosition;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;

public class PokerStarsNineSeatTableData {
	
	public enum NineSeatHeroHoleCardsPosition implements HoleCardsPosition {
		POS1(492, 28),  // 500, 58
		POS2(625, 95), // 633, 125
		POS3(667, 216), // 675, 246
		POS4(561, 334), // 569, 367
		POS5(360, 352), // 368, 382
		POS6(162, 335), // 809, 594 !
		POS7(59, 217),  // 67, 247
		POS8(96, 102),  // 104, 132
		POS9(232, 27);  // 240, 57
		
		@Getter private Position card1;
		@Getter private Position card2;
		
		private NineSeatHeroHoleCardsPosition(int x, int y) {
			card1 = new Position(x + PokerStarsTableData.CARD_INSET_LEFT, y + PokerStarsTableData.CARD_INSET_TOP);
			card2 = new Position(card1.getX() + PokerStarsTableData.HOLECARDS_CARD2_OFFSET_LEFT, card1.getY() + PokerStarsTableData.HOLECARDS_CARD2_OFFSET_TOP);
		}
		
		protected static HoleCardsPosition getHoleCardsPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1;
			case SEAT2: return POS2;
			case SEAT3: return POS3;
			case SEAT4: return POS4;
			case SEAT5: return POS5;
			case SEAT6: return POS6;
			case SEAT7: return POS7;
			case SEAT8: return POS8;
			case SEAT9: return POS9;
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
	public enum NineSeatActionPosition {
		POS1(512, 59),
		POS2(648, 133),
		POS3(686, 249),
		POS4(582, 365),
		POS5(381, 382),
		POS6(182, 365),
		POS7(80, 249),
		POS8(118, 133),
		POS9(252, 59);
		
		@Getter
		private Position position;
		
		private NineSeatActionPosition(int x, int y) {
			position = new Position(x, y);
		}
		
		public static Position getPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1.getPosition();
			case SEAT2: return POS2.getPosition();
			case SEAT3: return POS3.getPosition();
			case SEAT4: return POS4.getPosition();
			case SEAT5: return POS5.getPosition();
			case SEAT6: return POS6.getPosition();
			case SEAT7: return POS7.getPosition();
			case SEAT8: return POS8.getPosition();
			case SEAT9: return POS9.getPosition();
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}
	
	public enum NineSeatOpponentHoleCardsPosition {
		POS1(526, 121),
		POS2(606, 159),
		POS3(631, 249),
		POS4(572, 311),
		POS5(391, 328),
		POS6(209, 310),
		POS7(158, 241),
		POS8(182, 162),
		POS9(265, 121);
		
		@Getter
		private Position position;
		
		private NineSeatOpponentHoleCardsPosition(int x, int y) {
			position = new Position(x, y);
		}

		public static Position getPosition(Seat seat) {
			switch (seat) {
			case SEAT1: return POS1.getPosition();
			case SEAT2: return POS2.getPosition();
			case SEAT3: return POS3.getPosition();
			case SEAT4: return POS4.getPosition();
			case SEAT5: return POS5.getPosition();
			case SEAT6: return POS6.getPosition();
			case SEAT7: return POS7.getPosition();
			case SEAT8: return POS8.getPosition();
			case SEAT9: return POS9.getPosition();
			default: throw new IllegalStateException("This should never be reached.");
			}
		}
	}

}
