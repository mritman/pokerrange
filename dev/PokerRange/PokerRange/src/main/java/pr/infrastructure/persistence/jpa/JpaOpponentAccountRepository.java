package pr.infrastructure.persistence.jpa;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.application.preferences.IntegerPreferencesListener;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccount;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.player.PlayerStatistics;
import pr.domain.poker.range.HandRange;
import pr.domain.poker.range.ranked.PokerStoveHoleCardsRanks;
import pr.domain.poker.range.ranked.RankedHandRange;
import pr.domain.poker.range.typed.TypedHandRange;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;
import pr.infrastructure.persistence.jpa.pokertracker.PokerTrackerRepository;
import pr.util.Optional;

import com.google.inject.persist.Transactional;

public class JpaOpponentAccountRepository implements OpponentAccountRepository {

	private static final Logger logger = LoggerFactory.getLogger(JpaOpponentAccountRepository.class);
	
	private final Provider<EntityManager> em;
	private final PokerTrackerRepository pokerTrackerRepository;
	
	private int minimumRequiredHandsForRanges = 0;
	
	@Inject
	public JpaOpponentAccountRepository(Provider<EntityManager> em, PokerTrackerRepository pokerTrackerRepository, Preferences preferences) {
		this.em = em;
		this.pokerTrackerRepository = pokerTrackerRepository;
		
		preferences.addListener(PreferenceName.MINIMUM_HANDS_REQUIRED_FOR_RANGES, new IntegerPreferencesListener() {
			@Override
			public void preferenceChanged(PreferenceName name, Optional<Integer> value) {
				minimumRequiredHandsForRanges = value.orElse(0);
				logger.info("Minimum required hands to include poker tracker ranges set to: {}.", minimumRequiredHandsForRanges);
			}
		});
	}
	
	@Override
	@Transactional
	public OpponentAccount getOpponentAccount(String name, PokerVenue venue) {
		try {
			return em.get().createNamedQuery("OpponentAccount.findByNameAndVenue", OpponentAccount.class)
					.setParameter("name", name)
					.setParameter("venue", venue)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	@Transactional
	public void saveOpponentAccount(OpponentAccount opponentAccount) {
		em.get().merge(opponentAccount);
	}
	
	@Override
	@Transactional
	public void addRange(Opponent opponent, TypedHandRange range) {
		String name = opponent.getName();
		PokerVenue venue = opponent.getTable().getVenue();
		OpponentAccount opponentAccount = getOpponentAccount(name, venue);
		
		if (opponentAccount == null) {
			opponentAccount = new OpponentAccount(name, venue);
		}
		
		opponentAccount.addRange(range);
		saveOpponentAccount(opponentAccount);
		
		refreshRanges(opponent);
	}

	@Override
	@Transactional
	public void deleteRange(Opponent opponent, TypedHandRange range) {
		OpponentAccount opponentAccount = getOpponentAccount(opponent.getName(), opponent.getTable().getVenue());
		
		if (opponentAccount != null) {
			opponentAccount.removeRange(range);
			saveOpponentAccount(opponentAccount);
		}
		
		refreshRanges(opponent);
	}
	
	@Override
	@Transactional
	public void addGlobalRange(TypedHandRange range) {
		OpponentAccount globalOpponentAccount = getGlobalOpponentAccount();
		globalOpponentAccount.addRange(range);
		saveOpponentAccount(globalOpponentAccount);
	}

	@Override
	@Transactional
	public Collection<TypedHandRange> getGlobalRanges() {
		return getGlobalOpponentAccount().getRanges();
	}

	@Override
	@Transactional
	public void deleteGlobalRange(TypedHandRange range) {
		OpponentAccount globalOpponentAccount = getGlobalOpponentAccount();
		globalOpponentAccount.removeRange(range);
		saveOpponentAccount(globalOpponentAccount);		
	}

	/**
	 * Sets the ranges property of the specified {@link Opponent} to the ranges
	 * stored for it's corresponding opponent account and player statistics from
	 * the PokerTracker database.
	 * 
	 * @param opponent
	 */
	@Override
	@Transactional
	public void refreshRanges(Opponent opponent) {
		List<HandRange> ranges = new LinkedList<>();
		
		OpponentAccount opponentAccount = getOpponentAccount(opponent.getName(), opponent.getTable().getVenue());
		if (opponentAccount != null) {
			ranges.addAll(opponentAccount.getRanges());
		}
		
		PlayerStatistics playerStatistics = pokerTrackerRepository.getOpponentStatistics(opponent.getName());
		if (playerStatistics != null) {
			opponent.setStatistics(playerStatistics);
			
			if (playerStatistics.getNumberOfHands() >= minimumRequiredHandsForRanges) {
				RankedHandRange preflopRaiseRange = new RankedHandRange(PokerTrackerRepository.PREFLOP_RAISE_POKER_STOVE_RANGE, playerStatistics.getPreflopRaise(), PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap());
				RankedHandRange preflopThreeBetRange = new RankedHandRange(PokerTrackerRepository.PREFLOP_3BET_POKER_STOVE_RANGE, playerStatistics.getPreflopThreeBet(), PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap());
				RankedHandRange preflopColdCallFirstRaiseRange = new RankedHandRange(PokerTrackerRepository.PREFLOP_COLD_CALL_FIRST_RAISE_POKER_STOVE_RANGE, playerStatistics.getPreflopColdCallFirstRaise(), PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap());
				
				ranges.add(preflopColdCallFirstRaiseRange);
				ranges.add(preflopRaiseRange);
				ranges.add(preflopThreeBetRange);
			}
		}
		
		opponent.setRanges(ranges);
	}

	@Override
	@Transactional
	public Opponent getOpponent(String name, PokerTable table, Seat seat) {
		Opponent opponent = table.addOpponent(name, seat);
		refreshRanges(opponent);
		return opponent;
	}

	private OpponentAccount getGlobalOpponentAccount() {
		return getOpponentAccount(
				OpponentAccountRepository.GLOBAL_RANGES_ACCOUNT_NAME,
				OpponentAccountRepository.GLOBAL_RANGES_ACCOUNT_VENUE);
	}
	
}
