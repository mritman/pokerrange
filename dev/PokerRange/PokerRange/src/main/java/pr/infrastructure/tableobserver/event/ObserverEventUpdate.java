package pr.infrastructure.tableobserver.event;

import java.util.List;

import lombok.Getter;
import pr.domain.poker.table.PokerTableKey;

import com.google.common.collect.ImmutableList;

public class ObserverEventUpdate {

	@Getter private final PokerTableKey key;
	@Getter private final List<ObserverEvent> events;
	
	public ObserverEventUpdate(PokerTableKey key, List<ObserverEvent> events) {
		this.key = key;
		this.events = ImmutableList.copyOf(events);
	}
	
}
