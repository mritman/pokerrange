package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.BoardCards;

public abstract class BoardCardsObserverEvent implements ObserverEvent {

	@Getter private final BoardCards boardCards;

	public BoardCardsObserverEvent(BoardCards boardCards) {
		this.boardCards = boardCards;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("boardCardsEvent", boardCards)
				.toString();
	}

}
