package pr.infrastructure.persistence.jpa.pokertracker;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.player.PlayerStatistics;

import com.google.inject.persist.Transactional;

public class JpaPokerTrackerRepository implements PokerTrackerRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(JpaPokerTrackerRepository.class);
	
	// select id_hand, id_player, player_name, id_limit, date_played, cnt_players, flg_p_face_raise, flg_p_3bet_def_opp, flg_p_4bet_def_opp, flg_p_3bet, flg_p_4bet, flg_p_3bet_opp, flg_p_4bet_opp, flg_p_open, flg_p_open_opp, cnt_p_raise, flg_p_first_raise, flg_p_limp, flg_p_fold, flg_p_ccall, flg_vpip from holdem_hand_player_statistics join player using (id_player) order by date_played desc limit 50;
	// select id_hand, id_player, player_name, id_limit, date_played, cnt_players, flg_p_face_raise, flg_p_3bet_def_opp, flg_p_4bet_def_opp from holdem_hand_player_statistics join player using (id_player) where flg_p_face_raise = false AND (flg_p_3bet_def_opp = true OR flg_p_4bet_def_opp = true) order by date_played desc limit 50;
	
	//select 100.0 * (SUM(CASE WHEN flg_p_first_raise THEN 1 ELSE 0 END)) / (SUM(CASE WHEN cnt_p_face_limpers > 0 THEN 1 ELSE 0 END)) AS preflopRaise from holdem_hand_player_statistics;
	//select table_schema, table_name, column_name from information_schema.columns where column_name like '%_limpers%';
	
	// TODO add flg_p_limp
	// TODO 
	private static final String GET_PLAYER_STATISTICS = 
			"SELECT " +
			"count(*) as numberOfHands, " +
			"100.0 * (SUM(CASE WHEN flg_vpip = TRUE THEN 1 ELSE 0 END)) / COUNT(*) AS vpip, " +
			"100.0 * (SUM(CASE WHEN flg_p_first_raise THEN 1 ELSE 0 END)) / (COUNT(*) - (SUM(CASE WHEN flg_p_face_raise = TRUE THEN 1 ELSE 0 END))) AS preflopRaise, " +
			"100.0 * (SUM(CASE WHEN flg_p_3bet = TRUE THEN 1 ELSE 0 END)) / (SUM(CASE WHEN flg_p_3bet_opp = TRUE THEN 1 ELSE 0 END)) AS preflopThreeBet, " +
			"100.0 * (SUM(CASE WHEN flg_p_ccall = TRUE THEN 1 ELSE 0 END)) / (SUM(CASE WHEN flg_p_3bet_opp = TRUE THEN 1 ELSE 0 END)) AS preflopColdCallFirstRaise " +
			"FROM player JOIN holdem_hand_player_statistics USING (id_player) " +
			"WHERE player_name = (?1);";
	
	private final Provider<EntityManager> entityManager;
	
	@Inject
	public JpaPokerTrackerRepository(Provider<EntityManager> entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public PlayerStatistics getOpponentStatistics(String name) {
		Object result = entityManager.get().createNativeQuery(GET_PLAYER_STATISTICS)
				.setParameter(1, name).getSingleResult();
		
		Object[] results = (Object[]) result;
		
		if (results[1] == null) {
			logger.debug("Found no statistics for {}.", name);
			return null;
		}
		
		BigInteger numberOfHands = (BigInteger) results[0];
		BigDecimal vpip = (BigDecimal) results[1];
		BigDecimal preflopRaise = (BigDecimal) results[2];
		BigDecimal preflopThreeBet = (BigDecimal) results[3];
		BigDecimal preflopColdCallFirstRaise = (BigDecimal) results[4];
		
		PlayerStatistics playerStatistics = new PlayerStatistics(numberOfHands.intValue(),
				vpip.doubleValue(), preflopRaise.doubleValue(),
				preflopThreeBet.doubleValue(),
				preflopColdCallFirstRaise.doubleValue());
		
		logger.debug("Found statistics for {}: {}", name, playerStatistics);
		
		return playerStatistics;
	}

}
