package pr.infrastructure.database;

public interface DatabaseUpdateService {

	boolean isUpdateRequired() throws DatabaseUpdateServiceException;
	
	void update() throws DatabaseUpdateServiceException;

}
