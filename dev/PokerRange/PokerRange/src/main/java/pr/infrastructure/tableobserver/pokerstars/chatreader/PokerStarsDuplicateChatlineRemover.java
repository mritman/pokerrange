package pr.infrastructure.tableobserver.pokerstars.chatreader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.FluentIterable;

public class PokerStarsDuplicateChatlineRemover {

	private List<String> previousLineBatch;
	
	public PokerStarsDuplicateChatlineRemover() {
		previousLineBatch = Collections.emptyList();
	}
	
	public List<String> getLines(List<List<String>> lineBatches) {
		List<List<String>>splitBatches = PokerStarsChatLineBatchSplitter.splitBatchesWithDuplicates(lineBatches);
		List<String> newLines = new ArrayList<>();

		for (List<String> nextLineBatch : splitBatches) {
			int index = 0;
			
			while (index != nextLineBatch.size()) {
				int linesToSkip = getLinesToSkip(previousLineBatch, nextLineBatch.subList(index, nextLineBatch.size()));
				if (linesToSkip + index == nextLineBatch.size()) break; 
				
				newLines.add(nextLineBatch.get(linesToSkip + index));
				
				index += linesToSkip + 1;
			}
			
			previousLineBatch = nextLineBatch;
		}
		
		return newLines;
	}

	private int getLinesToSkip(List<String> batchOne, List<String> batchTwo) {
		int linesToSkip = 0;
		int startIndex = batchOne.indexOf(batchTwo.get(0));
		
		if (startIndex == -1) return 0;
		
		Iterator<String> previousBatchIterator = FluentIterable.from(batchOne).skip(startIndex).iterator();
		Iterator<String> nextBatchIterator = batchTwo.iterator();
		
		while (previousBatchIterator.hasNext() && nextBatchIterator.hasNext()) {
			if (previousBatchIterator.next().equals(nextBatchIterator.next())) {
				linesToSkip++;
			} else {
				break;
			}
		}
		
		return linesToSkip;
	}

}
