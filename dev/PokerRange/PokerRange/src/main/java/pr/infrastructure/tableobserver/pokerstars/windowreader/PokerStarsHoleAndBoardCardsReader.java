package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.TableCardReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.HoleCardsPosition;
import pr.util.Optional;

public class PokerStarsHoleAndBoardCardsReader {

	private final TableCardReader tableCardReader;
	
	@Inject
	public PokerStarsHoleAndBoardCardsReader(TableCardReader tableCardReader) {
		this.tableCardReader = tableCardReader;
	}
	
	protected List<HoleCardsResult> readHoleCards(Image image, TableSize tableSize) {
		List<HoleCardsResult> result = new ArrayList<>();
		
		for (Seat seat : tableSize.getSeats()) {
			Optional<HoleCards> holeCards = readHoleCards(image, tableSize, seat);
			if (holeCards.isPresent()) {
				result.add(new HoleCardsResult(seat, holeCards.get()));
			}
		}
		
		return result;
	}
	
	protected Optional<HoleCards> readHoleCards(Image image, TableSize tableSize, Seat seat) {
		HoleCardsPosition position = PokerStarsTableData.getHoleCardsPosition(tableSize, seat);
		
		Card card1 = readCard(image, position.getCard1().getX(), position.getCard1().getY());
		Card card2 = readCard(image, position.getCard2().getX(), position.getCard2().getY());
		
		if (card1 != null && card2 != null) {
			return Optional.of(new HoleCards(card1, card2));
		}
		
		return Optional.empty();
	}

	/**
	 * Reads the board cards on the table. A read attempt can be unsuccesful,
	 * for example if an animation obscures part of the board cards. In this
	 * case the returned optional is empty.
	 * 
	 * @param image
	 *            a table window image
	 * @return the board cards if the read was succesful, empty otherwise
	 */
	protected Optional<BoardCards> readBoardCards(Image image) {
		BoardCards boardCards;
		Street street = readStreet(image);
		
		if (street == Street.PREFLOP) {
			return Optional.of(BoardCards.PREFLOP);
		}
		
		Card flop1, flop2, flop3, turn, river;
		turn = null;
		river = null;
		
		flop1 = readCard(image, PokerStarsTableData.CARD1_LOCATION.getX(), PokerStarsTableData.CARD1_LOCATION.getY());
		flop2 = readCard(image, PokerStarsTableData.CARD2_LOCATION.getX(), PokerStarsTableData.CARD2_LOCATION.getY());
		flop3 = readCard(image, PokerStarsTableData.CARD3_LOCATION.getX(), PokerStarsTableData.CARD3_LOCATION.getY());
		turn = readCard(image, PokerStarsTableData.CARD4_LOCATION.getX(), PokerStarsTableData.CARD4_LOCATION.getY());
		if (turn != null) {
			river = readCard(image, PokerStarsTableData.CARD5_LOCATION.getX(), PokerStarsTableData.CARD5_LOCATION.getY());	
		}
		
		try {
			 boardCards = new BoardCards(flop1, flop2, flop3, turn, river);
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
		
		if (boardCards.getStreet() == street) {
			return Optional.of(boardCards);
		}
		
		return Optional.empty();
	}
	
	private Street readStreet(Image image) {
		Color card1 = image.getPixelReader().getColor(PokerStarsTableData.CARD1_PIXEL.getX(), PokerStarsTableData.CARD1_PIXEL.getY());
		Color card2 = image.getPixelReader().getColor(PokerStarsTableData.CARD2_PIXEL.getX(), PokerStarsTableData.CARD2_PIXEL.getY());
		Color card3 = image.getPixelReader().getColor(PokerStarsTableData.CARD3_PIXEL.getX(), PokerStarsTableData.CARD3_PIXEL.getY());
		Color card4 = image.getPixelReader().getColor(PokerStarsTableData.CARD4_PIXEL.getX(), PokerStarsTableData.CARD4_PIXEL.getY());
		Color card5 = image.getPixelReader().getColor(PokerStarsTableData.CARD5_PIXEL.getX(), PokerStarsTableData.CARD5_PIXEL.getY());
		
		if (PokerStarsTableData.CARD_COLOR.equals(card5)) {
			return Street.RIVER;
		} else if (PokerStarsTableData.CARD_COLOR.equals(card4)) {
			return Street.TURN;
		} else if (PokerStarsTableData.CARD_COLOR.equals(card1)
				|| PokerStarsTableData.CARD_COLOR.equals(card2)
				|| PokerStarsTableData.CARD_COLOR.equals(card3)) {
			
			return Street.FLOP;
		} else {
			return Street.PREFLOP;
		}
	}
	
	private Card readCard(Image image, int x, int y) {
		Image cardImage = new WritableImage(image.getPixelReader(), x, y, tableCardReader.getWidth(), tableCardReader.getHeight());
		return tableCardReader.readCard(cardImage);
	}
	
	protected static class HoleCardsResult {
		@Getter private final Seat seat;
		@Getter private final HoleCards holeCards;
		
		public HoleCardsResult(Seat seat, HoleCards holeCards) {
			this.seat = seat;
			this.holeCards = holeCards;
		}
	}
	
}
