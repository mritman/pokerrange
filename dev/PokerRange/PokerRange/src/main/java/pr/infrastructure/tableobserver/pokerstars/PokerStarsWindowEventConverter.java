package pr.infrastructure.tableobserver.pokerstars;

import java.util.ArrayList;
import java.util.List;

import pr.domain.poker.event.HoleCardsEvent;
import pr.domain.poker.event.PokerEvent;
import pr.infrastructure.tableobserver.event.HoleCardsObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;

public class PokerStarsWindowEventConverter {

	public static List<PokerEvent> convert(List<ObserverEvent> windowEvents) {
		List<PokerEvent> events = new ArrayList<>();
		
		for (ObserverEvent event : windowEvents) {
			if (event instanceof HoleCardsObserverEvent) {
				HoleCardsObserverEvent holeCardsEvent = (HoleCardsObserverEvent) event;
				events.add(new HoleCardsEvent(holeCardsEvent.getSeat(), holeCardsEvent.getHoleCards()));
			}
		}
		
		return events;
	}

}
