package pr.infrastructure.persistence.jpa.pokertracker;

import java.util.Properties;

import javax.inject.Singleton;

import pr.application.preferences.FilePreferences;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;

import com.google.common.base.Strings;
import com.google.inject.PrivateModule;
import com.google.inject.persist.jpa.JpaPersistModule;

public class PokerTrackerRepositoryModule extends PrivateModule {

	public static final String POKER_TRACKER_PERSISTENCE_UNIT_NAME = "com.pokertracker";
	
	@Override
	protected void configure() {
		binder().requireExplicitBindings();
		install(getPokerTrackerPersistModule());

		bind(PokerTrackerRepository.class).to(JpaPokerTrackerRepository.class).in(Singleton.class);
		bind(PokerTrackerPersistServiceManager.class).asEagerSingleton();
		
		expose(PokerTrackerRepository.class);
		expose(PokerTrackerPersistServiceManager.class);
	}
	
	private static JpaPersistModule getPokerTrackerPersistModule() {
		JpaPersistModule jpaPersistModule = new JpaPersistModule(POKER_TRACKER_PERSISTENCE_UNIT_NAME);
		
		Preferences preferences = new FilePreferences();
		Properties properties = new Properties();

		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_URL))) {
			properties.setProperty("javax.persistence.jdbc.url", preferences.get(PreferenceName.POKER_TRACKER_DATABASE_URL));
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_USER))) {
			properties.setProperty("javax.persistence.jdbc.user", preferences.get(PreferenceName.POKER_TRACKER_DATABASE_USER));
		}
		if (!Strings.isNullOrEmpty(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_PASSWORD))) {
			properties.setProperty("javax.persistence.jdbc.password", preferences.get(PreferenceName.POKER_TRACKER_DATABASE_PASSWORD));
		}
		
		jpaPersistModule.properties(properties);
		
		return jpaPersistModule;
	}

}
