package pr.infrastructure.tableobserver.event;

import com.google.common.base.Objects;

import lombok.Getter;
import pr.domain.poker.table.Seat;

public class PlayerJoinedObserverEvent extends PlayerSeatObserverEvent {

	@Getter private final Seat seat;
	
	public PlayerJoinedObserverEvent(String name, Seat seat) {
		super(name);
		this.seat = seat;
	}
	
	@Override
	public PlayerSeatObserverEvent setSeat(Seat seat) {
		return new PlayerJoinedObserverEvent(getName(), seat);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("name", getName())
				.add("seat", seat)
				.toString();
	}

}
