package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;

import pr.application.exception.InitializationException;
import pr.domain.cards.Card;
import pr.infrastructure.tableobserver.TableCardReader;
import pr.util.ImageUtil;

public class PokerStarsCardReader implements TableCardReader {
	
	private static final String CARD_IMAGE_LOCATION = "deck/";
	private static final String CARD_IMAGE_EXSTENSION = ".bmp";
	
	private static final Map<Card, Image> cardImages = new HashMap<>();
	
	public PokerStarsCardReader() {
		if (cardImages.isEmpty()) {
			loadCards();
		}
	}
	
	public Card readCard(Image image) {
		
		for (Entry<Card, Image> entry : cardImages.entrySet()) {
			
			boolean equal = ImageUtil.compareImage(image, entry.getValue());
			
			if (equal) return entry.getKey();
		}
		
		return null;
	}
	
	@Override
	public int getWidth() {
		return (int) cardImages.values().iterator().next().getWidth();
	}

	@Override
	public int getHeight() {
		return (int) cardImages.values().iterator().next().getHeight();
	}

	private void loadCards() {
		for (Card card : Card.values()) {
			Image image = loadImage(card);
			cardImages.put(card, image);
		}
	}
	
	private Image loadImage(Card card) {
		BufferedImage bufferedImage = null;
		WritableImage image = null;
		
		URL url = getClass().getResource(CARD_IMAGE_LOCATION + card.toString() + CARD_IMAGE_EXSTENSION);
				
		try {
			bufferedImage = ImageIO.read(url);
		} catch (IOException e) {
			throw new InitializationException("Failed to load card image.", e);
		}

		image = new WritableImage(bufferedImage.getWidth(), bufferedImage.getHeight());
		SwingFXUtils.toFXImage(bufferedImage, image);

		return image;
	}

}
