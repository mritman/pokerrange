package pr.infrastructure.tableobserver.pokerstars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pr.application.preferences.IntegerPreferencesListener;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;
import pr.application.preferences.PreferencesListener;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.seat.SeatOccupiedEvent;
import pr.domain.poker.event.seat.SeatPropertyEvent;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.util.Optional;

import com.google.inject.Inject;

public class PokerStarsLogSeatAdjuster {

	private String heroName;
	private Map<TableSize, Seat> preferredSeats;
	
	@Inject
	public PokerStarsLogSeatAdjuster(Preferences preferences) {
		this.preferredSeats = new HashMap<>();
		
		observeAccountNamePreference(preferences);
		observePreferredSeatPreferences(preferences);
	}
	
	/**
	 * In case the user has selected a preferred seat in the PokerStars client
	 * the seats from the log will not correspond with the seats in the
	 * PokerStars client window. If this is the case this method will adjust the
	 * seat results so that they match the seats of the client window.
	 * 
	 * @param tableSize
	 *            the table's size
	 * @param observerEvents
	 *            the events to adjust
	 * @result observerEvents the adjusted observer events if necessary, the
	 *         original observer events otherwise
	 */
	public List<PokerEvent> adjust(List<PokerEvent> observerEvents, TableSize tableSize) {
		if (observerEvents == null || observerEvents.isEmpty()) return observerEvents;
		
		int seatOffset = calculateSeatOffset(observerEvents, tableSize);
		if (seatOffset == 0) {
			return observerEvents;
		}

		List<PokerEvent> adjustedEvents = new ArrayList<>();
		
		for (PokerEvent event : observerEvents) {
			if (event instanceof SeatPropertyEvent) {
				SeatPropertyEvent seatEvent = (SeatPropertyEvent) event;
				
				int seatInteger = seatEvent.getSeat().toInt();
				seatInteger += seatOffset;

				if (seatInteger > tableSize.size()) {
					seatInteger %= tableSize.size();
				}

				adjustedEvents.add(seatEvent.setSeat(Seat.fromInt(seatInteger)));				
			} else {
				adjustedEvents.add(event);
			}
		}

		return adjustedEvents;
	}

	private int calculateSeatOffset(List<PokerEvent> events, TableSize tableSize) {
		int seatOffset = 0;
		
		for (PokerEvent event : events) {
			if (event instanceof SeatOccupiedEvent) {
				SeatOccupiedEvent seatOccupiedEvent = (SeatOccupiedEvent) event;
				
				if (seatOccupiedEvent.getName().equals(heroName) && seatOccupiedEvent.getSeat() != preferredSeats.get(tableSize)) {
					seatOffset = preferredSeats.get(tableSize).toInt() - seatOccupiedEvent.getSeat().toInt();
					if (seatOffset < 0) {
						seatOffset += tableSize.size();
					}
					
					break;
				}
			}
		}
		
		return seatOffset;
	}
	
	private void observeAccountNamePreference(Preferences preferences) {
		heroName = preferences.get(PreferenceName.POKERSTARS_ACCOUNT_NAME);
		
		preferences.addListener(PreferenceName.POKERSTARS_ACCOUNT_NAME, new PreferencesListener() {
			@Override
			public void preferenceChanged(PreferenceName name, String value) {
				heroName = value;
			}
		});
	}
	
	private void observePreferredSeatPreferences(final Preferences preferences) {
		Optional<Integer> size6 = preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_6);
		Optional<Integer> size10 = preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_10);
		if (size6.isPresent()) {
			preferredSeats.put(TableSize.SIX, Seat.fromInt(size6.get()));
		}
		if (size10.isPresent()) {
			preferredSeats.put(TableSize.SIX, Seat.fromInt(size10.get()));
		}
		
		preferences.addListener(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_6, new IntegerPreferencesListener() {
			@Override
			public void preferenceChanged(PreferenceName name, Optional<Integer> value) {
				Optional<Integer> size6 = preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_6);
				if (size6.isPresent()) {
					preferredSeats.put(TableSize.SIX, Seat.fromInt(size6.get()));
				} else {
					preferredSeats.remove(TableSize.SIX);
				}
			}
		});
		
		preferences.addListener(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_10, new IntegerPreferencesListener() {
			@Override
			public void preferenceChanged(PreferenceName name, Optional<Integer> value) {
				Optional<Integer> size10 = preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_10);
				if (size10.isPresent()) {
					preferredSeats.put(TableSize.TEN, Seat.fromInt(size10.get()));
				} else {
					preferredSeats.remove(TableSize.TEN);
				}
			}
		});
	}
}
