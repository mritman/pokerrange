package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;
import pr.util.FileType;
import pr.util.ImageUtil;
import pr.util.Optional;

import com.google.common.base.Objects;

public class PokerStarsActionReader {
	
	private static final String ACTION_IMAGE_LOCATION = "action/";
	
	private static final Map<ActionType, Image> actionImages = new HashMap<>();
	
	public PokerStarsActionReader() {
		if (actionImages.isEmpty()) {
			loadActions();
		}
	}
	
	/**
	 * Reads the actions from all active players at the table.
	 * 
	 * @param image a table window image
	 * @return the actions made by each seat on the table if any
	 */
	public List<ActionResult> readActions(Image image, TableSize tableSize) {
		List<ActionResult> actions = new ArrayList<>();
		
		for (Seat seat : tableSize.getSeats()) {
			Optional<ActionType> action = readAction(image, tableSize, seat);
			
			if (action.isPresent()) {
				actions.add(new ActionResult(seat, action.get()));
			}
		}
		
		return actions;
	}
	
	/**
	 * @param image a table window image
	 * @param tableSize the table size
	 * @param seat the seat number
	 * @return the {@link ActionType} at the specified seat, null otherwise
	 */
	public Optional<ActionType> readAction(Image image, TableSize tableSize, Seat seat) {
		Image actionImage = getActionImage(image, tableSize, seat);
		return readAction(actionImage);
	}

	/**
	 * @param actionImage an image that represents an action
	 * @return the optional {@link ActionType} that the image represents
	 */
	public Optional<ActionType> readAction(Image actionImage) {
		for (Entry<ActionType, Image> entry : actionImages.entrySet()) {
			if (ImageUtil.compareImage(actionImage, entry.getValue())) {
				return Optional.of(entry.getKey());
			}
		}
		
		return Optional.empty();
	}

	private Image getActionImage(Image image, TableSize tableSize, Seat seat) {
		Position position = PokerStarsTableData.getActionPosition(tableSize, seat);
		return new WritableImage(image.getPixelReader(), position.getX(), position.getY(),
				PokerStarsTableData.ACTION_WIDTH, PokerStarsTableData.ACTION_HEIGHT);
	}
	
	private void loadActions() {
		for (ActionType actionType : ActionType.values()) {
			Image image = loadImage(actionType);
			actionImages.put(actionType, image);
		}
	}
	
	private Image loadImage(ActionType action) {
		URL url = getClass().getResource(ACTION_IMAGE_LOCATION + action.name().toLowerCase() + FileType.PNG.extension());
		
		try {
			return ImageUtil.loadFxImage(url);
		} catch (IOException e) {
			throw new InitializationException(e);
		}
	}
	
	public static class ActionResult {
		@Getter private final Seat seat;
		@Getter private final ActionType actionType;
		
		public ActionResult(Seat seat, ActionType actionType) {
			this.seat = seat;
			this.actionType = actionType;
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
					.add("seat", seat)
					.add("actionType", actionType)
					.toString();
		}
	}
	
}
