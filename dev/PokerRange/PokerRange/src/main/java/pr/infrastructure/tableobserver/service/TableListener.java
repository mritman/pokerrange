package pr.infrastructure.tableobserver.service;

import pr.domain.poker.event.PokerEventUpdate;

public interface TableListener {

	
	/**
	 * This method is called by {@link TableObserverService} when the list of
	 * tables has changed.
	 * 
	 * @param tables the list of poker table windows.
	 */
	void tablesUpdated(TablesUpdate tablesUpdate);
	
	/**
	 * This method is called by {@link TableObserverService} when it's selected
	 * tabel's state has changed.
	 * 
	 * @param table
	 */
	void tableStateChanged(PokerEventUpdate tableUpdate);
	
}
