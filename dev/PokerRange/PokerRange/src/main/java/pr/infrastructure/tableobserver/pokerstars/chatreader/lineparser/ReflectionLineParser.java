package pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import pr.domain.poker.event.PokerEvent;
import pr.util.CurrencyParser;
import pr.util.Optional;
import pr.util.Regex;

public class ReflectionLineParser {

	public static Optional<PokerEvent> parse(String line, LineTemplate template) {
		if (!Regex.matches(template.getLinePattern(), line)) {
			return Optional.empty();
		}
		
		List<Object> arguments = new ArrayList<>();
		for (Entry<String, Class<?>> entry : template.getPatternMap().entrySet()) {
			if (entry.getValue().isAssignableFrom(String.class)) {
				String string = Regex.find(entry.getKey(), line);
				arguments.add(string);
			} else {
				Money money = getMoney(entry.getKey(), line);
				arguments.add(money);
			}
		}
		
		try {
			Constructor<? extends PokerEvent> constructor = template.getEventClass().getConstructor(template.getPatternMap().values().toArray(new Class<?>[0]));
			PokerEvent event = constructor.newInstance(arguments.toArray(new Object[0]));
			return Optional.<PokerEvent>of(event);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private static Money getMoney(String pattern, String line) {
		String amountString = Regex.find(pattern, line);
		CurrencyUnit currency = CurrencyParser.fromSymbol(amountString.charAt(0));
		Double amount = Double.valueOf(amountString.substring(1));
		return Money.of(currency, amount);
	}
	
}
