package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;

import javafx.scene.image.Image;
import javafx.scene.image.WritablePixelFormat;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.util.FileType;
import pr.util.ImageUtil;
import pr.util.MessageDigestFactory;
import pr.util.MessageDigestFactory.MessageDigestAlgorithm;

public class PokerStarsHandIdReader {
	
	protected static final String EMPTY_HAND_ID_IMAGE_NAME = "emptyhandid" + FileType.PNG.extension();
	
	@Getter
	private static final String emptyHandId;

	static {
		try {
			URL url = PokerStarsHandIdReader.class.getResource(EMPTY_HAND_ID_IMAGE_NAME);
			Image image = ImageUtil.loadFxImage(url);
			emptyHandId = staticReadHandId(image);
		} catch (IOException e) {
			throw new InitializationException(e);
		}
	}
	
	public String readHandId(Image image) {
		return staticReadHandId(image);
	}
	
	private static String staticReadHandId(Image image) {
		int nrOfPixels = PokerStarsTableData.HAND_ID_WIDTH * PokerStarsTableData.HAND_ID_HEIGHT;
		
		// Number of pixel times the number of bytes (alpha, blue, green, red) needed per pixel.
		byte[] pixelBytes = new byte[nrOfPixels * 4];
		
		image.getPixelReader().getPixels(
				PokerStarsTableData.HAND_ID_X,
				PokerStarsTableData.HAND_ID_Y,
				PokerStarsTableData.HAND_ID_WIDTH,
				PokerStarsTableData.HAND_ID_HEIGHT,
				WritablePixelFormat.getByteBgraInstance(), pixelBytes, 0,
				(int) PokerStarsTableData.HAND_ID_WIDTH * 4);
		
		MessageDigest md = MessageDigestFactory.getMessageDigest(MessageDigestAlgorithm.MD5);
		md.update(pixelBytes);
		byte[] hashBytes = md.digest();
	
		return bytesArrayToHexString(hashBytes);
	}
	
	private static String bytesArrayToHexString(byte[] hashBytes) {
		// http://www.rgagnon.com/javadetails/java-0596.html
		String hexes = "0123456789ABCDEF";
		
	    final StringBuilder hex = new StringBuilder( 2 * hashBytes.length );
	    for ( final byte b : hashBytes ) {
	      hex.append(hexes.charAt((b & 0xF0) >> 4)).append(hexes.charAt((b & 0x0F)));
	    }
	    
		return hex.toString();
	}
}
