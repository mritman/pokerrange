package pr.infrastructure.tableobserver.service.windows;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javafx.scene.image.Image;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.PokerEventUpdate;
import pr.domain.poker.table.PokerTableKey;
import pr.infrastructure.jna.windows.WindowCapturer;
import pr.infrastructure.tableobserver.ImageProcessingException;
import pr.infrastructure.tableobserver.TableObserverProvider;
import pr.infrastructure.tableobserver.service.TableListener;
import pr.infrastructure.tableobserver.service.TableObserverService;
import pr.infrastructure.tableobserver.service.TablesUpdate;
import pr.infrastructure.tableobserver.service.windows.PokerTableWindowEnumProc.TableWindowResult;
import pr.infrastructure.tableobserver.util.PokerTableTitleToName;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;

public class JnaTableObserverService extends TableObserverService {
	
	private static final Logger logger = LoggerFactory.getLogger(JnaTableObserverService.class);

	private static final String THREAD_NAME = "JnaTableObserverService";
	
	/**
	 * The frequency in milliseconds of table information updates.
	 */
	private static final long UPDATE_INTERVAL = TimeUnit.SECONDS.toMillis(1);
	
	private final TableObserverProvider tableObserverProvider;
	
	private final PokerTableWindowEnumProc windowEnumProc;
	
	private final WindowCapturer windowCapturer;

	private final Set<PokerTableKey> tables;
	
	private final Map<PokerTableKey, HWND> handles;
	private final Map<Integer, PokerTableKey> windowIdToTable;
	
	private PokerTableKey selectedTable;

	private final List<TableListener> listeners;
	
	private boolean started;

	@Inject
	public JnaTableObserverService(TableObserverProvider tableReaderFactory, PokerTableWindowEnumProc windowEnumProc, WindowCapturer windowCapturer) {
		super.setName(THREAD_NAME);
		this.tableObserverProvider = tableReaderFactory;
		this.windowEnumProc = windowEnumProc;
		this.windowCapturer = windowCapturer;
		
		tables = new HashSet<>();
		windowIdToTable = new HashMap<>();
		handles = new HashMap<>();
		listeners = new LinkedList<>();
	}
	
	@Override
	synchronized public Set<PokerTableKey> getTables() {
		return Collections.unmodifiableSet(tables);
	}
	
	@Override
	synchronized public void selectTable(PokerTableKey table) {
		if (table == null) {
			selectedTable = null;
			return;
		}
		
		for (PokerTableKey existingTable : tables) {
			if (existingTable.equals(table)) {
				selectedTable = existingTable;
				return;
			}
		}
		
		throw new IllegalArgumentException("The specified table does not exist in the list of tables.");
	}
	
	synchronized public PokerTableKey getSelectedTable() {
		return selectedTable;
	}

	/**
	 *  This method is synchronized to allow callers from multiple threads to add listeners safely.
	 */
	@Override
	synchronized public void addTableListener(TableListener listener) {
		listeners.add(listener);
	}
	

	@Override
	public void startService() {
		start();
		started = true;
	}
	
	@Override
	public void stopService() {
		interrupt();
	}
	
	@Override
	public void run() {
		logger.debug("{} thread started.", Thread.currentThread().getName());
		
		while (!Thread.interrupted()) {
			updateTables();

			updateSelectedTable();
			
			try {
				Thread.sleep(UPDATE_INTERVAL);
			} catch (InterruptedException e) {
				logger.debug("{} will now exit.", Thread.currentThread().getName());
				break;
			}
		}
	}
	
	synchronized private void updateSelectedTable() {
		if (selectedTable == null) return;
		
		HWND hwnd = handles.get(selectedTable);
		
		Image image = windowCapturer.capture(hwnd);
		if (image == null) {
			return;
		}
		
		List<PokerEvent> tableEvents = Collections.emptyList();
		try {
			tableEvents = tableObserverProvider.getReader(selectedTable.getVenue()).readTable(image, selectedTable.getName());
		} catch (ImageProcessingException e) {
			// Failure to read the image. This will be automatically be retried
			// during the next update attempt so nothing needs to be done here.
			logger.trace("Failed to read the selected table.");
		}
		
		if (!tableEvents.isEmpty()) {
			PokerEventUpdate eventUpdate = new PokerEventUpdate(selectedTable, tableEvents);
			notifyListeners(eventUpdate);
		}
	}
	
	private void updateTables() {
		Set<TableWindowResult> result = getWindowHandles();
		
		Set<PokerTableKey> tablesClosed = removeClosedTables(result);
		Set<PokerTableKey> tablesFound = addNewTables(result);
		
		if (!tablesClosed.isEmpty() || !tablesFound.isEmpty()) {
			notifyListeners(new TablesUpdate(tablesFound, tablesClosed));
		}
	}

	/** 
	 * Remove tables that no longer exist.
	 * 
	 * @param result a list of open poker tables.
	 */
	private Set<PokerTableKey> removeClosedTables(Set<TableWindowResult> result) {
		Set<PokerTableKey> closedTables = new HashSet<>();
		
		Set<Integer> remainingIds = new HashSet<>();
		for (TableWindowResult res : result) {
			remainingIds.add(res.getWindowId());
		}
		
		Set<Integer> idsToRemove = Sets.difference(windowIdToTable.keySet(), remainingIds);
		idsToRemove = ImmutableSet.copyOf(idsToRemove);
		
		for (Integer id : idsToRemove) {
			PokerTableKey tableToRemove = windowIdToTable.remove(id);
			tables.remove(tableToRemove);
			handles.remove(tableToRemove);
			closedTables.add(tableToRemove);
		}
		
		synchronized(this) {
			if (!tables.contains(selectedTable)) {
				selectedTable = null;
			}
		}
		
		return closedTables;
	}

	private Set<PokerTableKey> addNewTables(Set<TableWindowResult> result) {
		Set<PokerTableKey> newTables = new HashSet<>();
		
		for (TableWindowResult res : result) {
			PokerTableKey tableKey = getPokerTableKey(res);
			
			if (handles.get(tableKey) == null) {
				windowIdToTable.put(res.getWindowId(), tableKey);
				tables.add(tableKey);
				handles.put(tableKey, res.getHandle());
				newTables.add(tableKey);
			}
		}

		return newTables;
	}

	private PokerTableKey getPokerTableKey(TableWindowResult res) {
		String name = PokerTableTitleToName.getName(res.getTitle(), res.getVenue());
		return new PokerTableKey(name, res.getVenue());
	}
	
	private Set<TableWindowResult> getWindowHandles() {
		windowEnumProc.reset();
		
		User32.INSTANCE.EnumWindows(windowEnumProc, null);
		
		return windowEnumProc.getHandles(); 
	}

	/**
	 * Synchronize on the instance of this class to make sure that during this
	 * loop no listeners can be added. This relies on
	 * {@link #addTableListener(TableListener)} to be a synchronized method.
	 * 
	 * @param tables
	 *            the set of poker tables corresponding to open poker table
	 *            windows.
	 */
	private void notifyListeners(TablesUpdate tablesUpdate) {
		synchronized(this) {
			for (TableListener listener : listeners) {
				listener.tablesUpdated(tablesUpdate);
			}
		}
	}
	
	/**
	 * Synchronize on the instance of this class to make sure that during this
	 * loop no listeners can be added. This relies on
	 * {@link #addTableListener(TableListener)} to be a synchronized
	 * method.
	 * 
	 * @param table the table of which the state has changed.
	 */
	private void notifyListeners(PokerEventUpdate eventUpdate) {
		
		synchronized(this) {
			for (TableListener listener : listeners) {
				listener.tableStateChanged(eventUpdate);
			}
		}
		
	}

	@Override
	public boolean isStarted() {
		return started;
	}
	
}
