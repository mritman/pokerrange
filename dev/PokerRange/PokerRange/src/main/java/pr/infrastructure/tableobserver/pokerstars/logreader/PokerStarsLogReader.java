package pr.infrastructure.tableobserver.pokerstars.logreader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Inject;

import pr.domain.poker.event.PokerEvent;
import pr.infrastructure.tableobserver.TableLogReader;
import pr.infrastructure.tableobserver.pokerstars.logreader.PokerStarsLogReaderThread.LogReaderCallBack;


/**
 * TODO Extract generic log reader code to an abstract super class that can be used by log readers for other venues. 
 */
public class PokerStarsLogReader implements TableLogReader {

	private final PokerStarsLogParser logParser;
	
	private final Map<String, BlockingQueue<PokerEvent>> events = new ConcurrentHashMap<>();
	
	@Inject
	public PokerStarsLogReader(
			PokerStarsLogReaderThread logReaderThread,
			PokerStarsLogParser logParser) {
		
		this.logParser = logParser;
		
		logReaderThread.setLogReaderCallBack(createLogReaderCallBack());
		logReaderThread.start();
	}

	@Override
	public List<PokerEvent> getEvents(String tableName) {
		BlockingQueue<PokerEvent> tableEvents = events.get(tableName);
		
		if (tableEvents != null && !tableEvents.isEmpty()) {
			ArrayList<PokerEvent> newTableEvents = new ArrayList<>();
			tableEvents.drainTo(newTableEvents);
			return newTableEvents;
		}
		
		return Collections.emptyList();
	}

	private LogReaderCallBack createLogReaderCallBack() {
		return new LogReaderCallBack() {
			
			@Override
			public void call(String tableName, List<String> logLines) {
				List<PokerEvent> observerEvents = logParser.getEvents(logLines);
				
				BlockingQueue<PokerEvent> existingEvents = events.get(tableName);
				if (existingEvents == null) {
					existingEvents = new LinkedBlockingQueue<>();
					events.put(tableName, existingEvents);
				}
				
				existingEvents.addAll(observerEvents);
			}

		};
	}
	
}
