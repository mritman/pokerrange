package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import pr.application.exception.InitializationException;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;
import pr.util.ImageUtil;

public class PokerStarsOpponentHoleCardsReader {

	private static final String OPPONENT_HOLECARDS_IMAGE_LOCATION = "OpponentHoleCards.bmp";
	
	private final Image opponentHoleCardsImage;
	
	public PokerStarsOpponentHoleCardsReader() {
		URL url = getClass().getResource(OPPONENT_HOLECARDS_IMAGE_LOCATION);
			
		try {
			opponentHoleCardsImage = ImageUtil.loadFxImage(url);
		} catch (IOException e) {
			throw new InitializationException(e);
		}
	}
	
	public List<Seat> readOpponentsInHand(Image image, TableSize tableSize) {
		List<Seat> opponentsInHand = new ArrayList<>();
		
		for (Seat seat : tableSize.getSeats()) {
			if (areHoleCardsPresent(image, tableSize, seat)) {
				opponentsInHand.add(seat);
			}
		}
		
		return opponentsInHand;
	}
	
	public boolean areHoleCardsPresent(Image image, TableSize tableSize, Seat seat) {
		Position position = PokerStarsTableData.getOpponentHoleCardsPosition(tableSize, seat);
		
		WritableImage holeCardsImage = new WritableImage(
				image.getPixelReader(), position.getX(), position.getY(),
				(int) opponentHoleCardsImage.getWidth(),
				(int) opponentHoleCardsImage.getHeight()
				);
		
		return areHoleCardsPresent(holeCardsImage);
	}
	
	public boolean areHoleCardsPresent(Image holeCardsImage) {
		return ImageUtil.compareImage(opponentHoleCardsImage, holeCardsImage);
	}

}
