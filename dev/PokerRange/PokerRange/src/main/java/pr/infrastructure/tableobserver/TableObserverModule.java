package pr.infrastructure.tableobserver;

import pr.infrastructure.jna.windows.WindowCapturer;
import pr.infrastructure.tableobserver.pokerstars.PokerStarsTableObserverModule;
import pr.infrastructure.tableobserver.service.TableObserverService;
import pr.infrastructure.tableobserver.service.windows.JnaTableObserverService;
import pr.infrastructure.tableobserver.service.windows.PokerTableWindowEnumProc;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class TableObserverModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new PokerStarsTableObserverModule());
		
		bind(TableObserverProvider.class);
		bind(PokerTableWindowEnumProc.class);
		bind(WindowCapturer.class);
		bind(TableObserverService.class).to(JnaTableObserverService.class).in(Singleton.class);
	}

}
