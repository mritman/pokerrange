package pr.infrastructure.tableobserver;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.PokerVenue;
import pr.infrastructure.socket.SocketStringReader;
import pr.infrastructure.system.ProcessEnumerator;
import pr.infrastructure.system.ProcessInfo;
import pr.util.Optional;
import pr.util.StreamLogger;

public class PokerVenueHookService {
	private static final Logger log = LoggerFactory.getLogger(PokerVenueHookService.class);

	private static final String POKER_VENUE_HOOK_INSTALLER_NAME = "PokerVenueHookInstaller";
	private static final String POKER_VENUE_HOOK_INSTALLER_EXE = "hook/PokerVenueHookInstaller.exe";

	private static final Duration POLL_INTERVAL = new Duration(TimeUnit.SECONDS.toMillis(30));
	private static final String READER_THREAD_POSTFIX = " Chat Reader Thread";
	
	private final Map<PokerVenue, SocketStringReader> readers;
	private final ProcessEnumerator processEnumerator;
	private final Map<PokerVenue, Instant> lastCheck;

	@Inject
	public PokerVenueHookService(ProcessEnumerator processEnumerator) {
		readers = new HashMap<>();
		lastCheck = new HashMap<>();
		this.processEnumerator = processEnumerator;
	}

	public List<List<String>> getChatLines(PokerVenue venue) {
		Optional<SocketStringReader> reader = getReader(venue);
		
		if (!reader.isPresent()) { 
			return Collections.emptyList();
		}
		
		if (!reader.get().isAlive()) {
			readers.remove(reader);
			return Collections.emptyList();
		}

		return reader.get().getLines();
	}
	
	private Optional<SocketStringReader> getReader(PokerVenue venue) {
		SocketStringReader reader = readers.get(venue);
		if (reader != null) {
			return Optional.of(reader);
		}
		
		Interval interval = new Interval(lastCheck.get(venue), (Instant) null);
		
		if (lastCheck.get(venue) == null || interval.toDuration().isLongerThan(POLL_INTERVAL)) {
			lastCheck.put(venue, interval.getEnd().toInstant());
			return initializeReader(venue);
		}
		
		return Optional.empty();
	}
	
	private Optional<SocketStringReader> initializeReader(PokerVenue venue) {
		SocketStringReader reader = readers.get(venue);

		if (reader != null) return Optional.of(reader); 
		
		Optional<ProcessInfo> processInfo = processEnumerator.getProcess(venue.getExecutableName());
		if (!processInfo.isPresent()) {
			return Optional.empty();
		}

		reader = new SocketStringReader(venue.getDisplayableName() + READER_THREAD_POSTFIX);
		reader.start();
		
		boolean hookInstalled = installHook(venue, reader.getPort(), processInfo.get().getId());
		
		if (!hookInstalled) {
			reader.close();
			return Optional.empty();
		}
		
		readers.put(venue, reader);
		return Optional.of(reader);
	}
	
	private boolean installHook(PokerVenue venue, int port, int processId) {
		try {
			File workingDirectory = new File("hook/");
			Process process = new ProcessBuilder(POKER_VENUE_HOOK_INSTALLER_EXE,
					Integer.toString(port), 
					Integer.toString(processId))
					.redirectErrorStream(true)
					.directory(workingDirectory)
					.start();

			new StreamLogger(process.getInputStream(), POKER_VENUE_HOOK_INSTALLER_NAME, log).start();
			
			int exitValue = process.waitFor();
			if (exitValue == 0) {
				return true;
			}
			
			log.debug("Failed to install {} hook. Exit value: {}", venue.getDisplayableName(), exitValue);
			return  false;
		} catch (IOException | InterruptedException e) {
			log.error("Failed to install {} hook.", venue.getDisplayableName(), e);
			return false;
		}
	}

}
