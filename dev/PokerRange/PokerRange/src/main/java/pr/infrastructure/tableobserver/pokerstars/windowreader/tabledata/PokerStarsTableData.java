package pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata;

import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import javafx.scene.paint.Color;
import lombok.Getter;

public final class PokerStarsTableData {
	
	private PokerStarsTableData() {};
	
	public static final Color CARD_COLOR = Color.WHITE;
	
	protected static final int BOARDCARDS_X = 268; // was: 276, on laptop: 277
	protected static final int BOARDCARDS_Y = 181; // was: 211, on laptop: 217
	protected static final int BOARDCARD_SPACING = 54; // The number of pixels between the top-left pixel of two consecutive board cards.
	
	protected static final int CARD_INSET_LEFT = 2;
	protected static final int CARD_INSET_TOP = 5;
	
	protected static final int CARD_COLOR_PIXEL_INSET_LEFT = 48;
	protected static final int CARD_COLOR_PIXEL_INSET_TOP = 30;
    
	public static final Position CARD1_LOCATION = new Position(BOARDCARDS_X + CARD_INSET_LEFT, BOARDCARDS_Y + CARD_INSET_TOP);
	public static final Position CARD2_LOCATION = new Position(BOARDCARDS_X + CARD_INSET_LEFT + BOARDCARD_SPACING * 1, BOARDCARDS_Y + CARD_INSET_TOP);
	public static final Position CARD3_LOCATION = new Position(BOARDCARDS_X + CARD_INSET_LEFT + BOARDCARD_SPACING * 2, BOARDCARDS_Y + CARD_INSET_TOP);
	public static final Position CARD4_LOCATION = new Position(BOARDCARDS_X + CARD_INSET_LEFT + BOARDCARD_SPACING * 3, BOARDCARDS_Y + CARD_INSET_TOP);
	public static final Position CARD5_LOCATION = new Position(BOARDCARDS_X + CARD_INSET_LEFT + BOARDCARD_SPACING * 4, BOARDCARDS_Y + CARD_INSET_TOP);

	public static final Position CARD1_PIXEL = new Position(CARD1_LOCATION.getX() + CARD_COLOR_PIXEL_INSET_LEFT - CARD_INSET_LEFT, CARD1_LOCATION.getY() + CARD_COLOR_PIXEL_INSET_TOP - CARD_INSET_TOP);
	public static final Position CARD2_PIXEL = new Position(CARD2_LOCATION.getX() + CARD_COLOR_PIXEL_INSET_LEFT - CARD_INSET_LEFT, CARD2_LOCATION.getY() + CARD_COLOR_PIXEL_INSET_TOP - CARD_INSET_TOP);
	public static final Position CARD3_PIXEL = new Position(CARD3_LOCATION.getX() + CARD_COLOR_PIXEL_INSET_LEFT - CARD_INSET_LEFT, CARD3_LOCATION.getY() + CARD_COLOR_PIXEL_INSET_TOP - CARD_INSET_TOP);
	public static final Position CARD4_PIXEL = new Position(CARD4_LOCATION.getX() + CARD_COLOR_PIXEL_INSET_LEFT - CARD_INSET_LEFT, CARD4_LOCATION.getY() + CARD_COLOR_PIXEL_INSET_TOP - CARD_INSET_TOP);
	public static final Position CARD5_PIXEL = new Position(CARD5_LOCATION.getX() + CARD_COLOR_PIXEL_INSET_LEFT - CARD_INSET_LEFT, CARD5_LOCATION.getY() + CARD_COLOR_PIXEL_INSET_TOP - CARD_INSET_TOP);

	public static final int HAND_ID_X = 0;
	public static final int HAND_ID_Y = 0;
	public static final int HAND_ID_WIDTH = 123;
	public static final int HAND_ID_HEIGHT = 20;
	
	public static final int ACTION_WIDTH = 17;
	public static final int ACTION_HEIGHT = 1;
	
	public static final int DEALER_BUTTON_WIDTH = 5;
	public static final int DEALER_BUTTON_HEIGHT = 1;
	
	protected static final int HOLECARDS_CARD2_OFFSET_LEFT = 15;
	protected static final int HOLECARDS_CARD2_OFFSET_TOP = 4;
	
	public interface HoleCardsPosition {
		public Position getCard1();
		public Position getCard2();
	}
	
	public static class Position {
		@Getter private final int x;
		@Getter private final int y;
		
		Position(int x, int y) {
			this.x = x;
			this.y = y;
		}
	};
	
	public static HoleCardsPosition getHoleCardsPosition(TableSize size, Seat seat) {
		switch (size) {
		case SIX: return PokerStarsSixSeatTableData.SixSeatHoleCardsPosition.getHoleCardsPosition(seat);
		case NINE: return PokerStarsNineSeatTableData.NineSeatHeroHoleCardsPosition.getHoleCardsPosition(seat);
		default: throw new IllegalStateException("This should never be reached.");
		}
	}
	
	public static Position getDealerPosition(TableSize size, Seat seat) {
		switch (size) {
		case SIX: return PokerStarsSixSeatTableData.SixSeatDealerPosition.getPosition(seat);
		default: throw new IllegalStateException("This should never be reached.");
		}
	}
	
	public static Position getOpponentHoleCardsPosition(TableSize size, Seat seat) {
		switch (size) {
		case SIX: return PokerStarsSixSeatTableData.SixSeatOpponentHoleCardsPosition.getPosition(seat);
		case NINE:return PokerStarsNineSeatTableData.NineSeatOpponentHoleCardsPosition.getPosition(seat);
		default: throw new IllegalStateException("This should never be reached.");
		}
	}
	
	public static Position getActionPosition(TableSize size, Seat seat) {
		switch (size) {
		case SIX: return PokerStarsSixSeatTableData.SixSeatActionPosition.getPosition(seat);
		case NINE: return PokerStarsNineSeatTableData.NineSeatActionPosition.getPosition(seat);
		default: throw new IllegalStateException("This should never be reached.");	
		}
	}
	
}
