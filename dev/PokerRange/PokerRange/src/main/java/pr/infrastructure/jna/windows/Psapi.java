package pr.infrastructure.jna.windows;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public interface Psapi extends StdCallLibrary {

	Psapi INSTANCE = (Psapi) Native.loadLibrary("Psapi", Psapi.class,
			W32APIOptions.DEFAULT_OPTIONS);

	int MAX_RUNNING_PROCESSES = 4096;

	DWORD MAX_IMAGE_FILE_NAME_LENGTH = new DWORD(256);

	/**
	 * Retrieves the process identifier for each process object in the system.
	 * 
	 * @param pProcessIds
	 *            A pointer to an array that receives the list of process
	 *            identifiers.
	 * @param cb
	 *            The size of the pProcessIds array, in bytes.
	 * @param pBytesReturned
	 *            The number of bytes returned in the pProcessIds array.
	 * @return If the function succeeds, the return value is nonzero. If the
	 *         function fails, the return value is zero.
	 */
	boolean EnumProcesses(DWORD[] pProcessIds, DWORD cb, DWORD[] pBytesReturned);

	/**
	 * Retrieves the name of the executable file for the specified process.
	 * 
	 * @param hProcess
	 *            A handle to the process. The handle must have the
	 *            PROCESS_QUERY_INFORMATION or PROCESS_QUERY_LIMITED_INFORMATION
	 *            access right. For more information, see Process Security and
	 *            Access Rights. Windows Server 2003 and Windows XP: The handle
	 *            must have the PROCESS_QUERY_INFORMATION access right.
	 * @param lpImageFileName
	 *            A pointer to a buffer that receives the full path to the
	 *            executable file.
	 * @param nSize
	 *            The size of the lpImageFileName buffer, in characters.
	 * @return If the function succeeds, the return value specifies the length
	 *         of the string copied to the buffer.
	 */
	int GetProcessImageFileName(HANDLE hProcess, char[] lpImageFileName, DWORD nSize);

	/**
	 * Retrieves the base name of the specified module.
	 * 
	 * @param hProcess
	 *            A handle to the process that contains the module. The handle
	 *            must have the PROCESS_QUERY_INFORMATION and PROCESS_VM_READ
	 *            access rights. For more information, see Process Security and
	 *            Access Rights.
	 * @param hModule
	 *            A handle to the module. If this parameter is NULL, this
	 *            function returns the name of the file used to create the
	 *            calling process.
	 * @param lpBaseName
	 *            A pointer to the buffer that receives the base name of the
	 *            module. If the base name is longer than maximum number of
	 *            characters specified by the nSize parameter, the base name is
	 *            truncated.
	 * @param nSize
	 *            The size of the lpBaseName buffer, in characters.
	 * @return the function succeeds, the return value specifies the length of
	 *          the string copied to the buffer, in characters. If the function
	 *          fails, the return value is zero. To get extended error
	 *          information, call GetLastError.
	 */
	int GetModuleBaseName(HANDLE hProcess, HMODULE hModule, char[] lpBaseName, int nSize);

}
