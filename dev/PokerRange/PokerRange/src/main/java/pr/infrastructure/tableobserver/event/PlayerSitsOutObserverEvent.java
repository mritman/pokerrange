package pr.infrastructure.tableobserver.event;


public class PlayerSitsOutObserverEvent extends PlayerObserverEvent  {

	public PlayerSitsOutObserverEvent(String name) {
		super(name);
	}

}
