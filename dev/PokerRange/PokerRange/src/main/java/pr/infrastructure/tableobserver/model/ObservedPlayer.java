package pr.infrastructure.tableobserver.model;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import lombok.Getter;
import pr.domain.poker.HoleCards;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.util.Optional;

public class ObservedPlayer {

	@Getter	private final Seat seat;
	@Getter	private final Optional<ActionType> lastAction;
	@Getter private final Optional<HoleCards> holeCards;
	
	public ObservedPlayer(Seat seat, ActionType lastAction, HoleCards holeCards) {
		Preconditions.checkNotNull("seat", seat);
		
		this.seat = seat;
		this.lastAction = Optional.ofNullable(lastAction);
		this.holeCards = Optional.ofNullable(holeCards);
	}
	
	public boolean isInHand() {
		return !lastAction.isPresent() || lastAction.get() != ActionType.FOLD;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seat", seat)
				.add("lastAction", lastAction)
				.add("holeCards", holeCards)
				.toString();
	}

	public ObservedPlayer setLastAction(ActionType actionType) {
		return new ObservedPlayerBuilder(this).setLastAction(actionType).build();
	}

	public ObservedPlayer setHoleCards(HoleCards holeCards) {
		return new ObservedPlayerBuilder(this).setHoleCards(holeCards).build();
	}
	
}
