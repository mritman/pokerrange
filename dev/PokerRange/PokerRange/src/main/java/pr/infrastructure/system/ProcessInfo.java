package pr.infrastructure.system;


import com.google.common.base.Objects;

import lombok.Getter;

public class ProcessInfo {
	@Getter private final int id;
	@Getter private final String name;

	public ProcessInfo(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("id", id)
				.add("name", name)
				.toString();
	}

}
