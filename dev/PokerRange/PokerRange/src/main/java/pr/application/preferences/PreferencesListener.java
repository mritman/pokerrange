package pr.application.preferences;

import pr.application.preferences.Preferences.PreferenceName;

public interface PreferencesListener {

	void preferenceChanged(PreferenceName name, String value);
	
}
