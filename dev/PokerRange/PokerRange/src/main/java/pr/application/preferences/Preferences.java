package pr.application.preferences;

import pr.util.Optional;


public interface Preferences {

	enum PreferenceName {
		DATABASE_URL,
		DATABASE_USER,
		DATABASE_PASSWORD,
		POKER_TRACKER_DATABASE_URL,
		POKER_TRACKER_DATABASE_USER, 
		POKER_TRACKER_DATABASE_PASSWORD,
		POKERSTARS_ACCOUNT_NAME,
		POKERSTARS_HAND_HISTORY_DIRECTORY, 
		POKERSTARS_PREFERRED_SEAT_SIZE_6,
		POKERSTARS_PREFERRED_SEAT_SIZE_10,
		MINIMUM_HANDS_REQUIRED_FOR_RANGES
	}
	
	String get(PreferenceName name);
	
	Optional<Integer> getInteger(PreferenceName name);
	
	void set(PreferenceName name, String value);
	
	void set(PreferenceName name, Optional<Integer> value);
	
	boolean addListener(PreferenceName name, PreferencesListener listener);

	boolean addListener(PreferenceName name, IntegerPreferencesListener listener);

	boolean removeListener(PreferenceName name, PreferencesListener listener);

	boolean removeListener(PreferenceName name, IntegerPreferencesListener listener);
	
}
