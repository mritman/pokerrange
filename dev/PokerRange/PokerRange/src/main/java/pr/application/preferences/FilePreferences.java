package pr.application.preferences;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.util.Optional;

import com.google.common.base.Strings;

public class FilePreferences extends AbstractPreferences {

	private static final Logger log = LoggerFactory.getLogger(FilePreferences.class);
	
	private static final String PREFERENCES_FILE = "preferences.properties";
	private final Properties properties;
	
	public FilePreferences() {
		properties = new Properties();
		
		if (Files.exists(Paths.get(PREFERENCES_FILE))) {
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(PREFERENCES_FILE), StandardCharsets.UTF_8)) {
				properties.load(reader);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}
	}
	
	@Override
	public String get(PreferenceName name) {
		return properties.getProperty(name.name());
	}

	@Override
	public Optional<Integer> getInteger(PreferenceName name) {
		String stringValue = get(name);
		return fromString(stringValue);
	}

	@Override
	public synchronized void set(PreferenceName name, String value) {
		boolean changed = setString(name, value);
		
		if (changed && getListeners().get(name) != null) {
			for (PreferencesListener listener : getListeners().get(name)) {
				listener.preferenceChanged(name, value);
			}
		}
	}
	
	@Override
	public synchronized void set(PreferenceName name, Optional<Integer> value) {
		String string = value.isPresent() ? String.valueOf(value.get()) : null;
		
		boolean changed = setString(name, string);
		
		if (changed && getIntegerlisteners().get(name) != null) {
			for (IntegerPreferencesListener listener : getIntegerlisteners().get(name)) {
				listener.preferenceChanged(name, value);
			}
		}
	}

	private boolean setString(PreferenceName name, String value) {
		String currentValue = properties.getProperty(name.name());
		
		boolean oneValueIsNull = currentValue == null ^ value == null;
		boolean bothNotNullAndNotEqual = currentValue != null && value != null && !currentValue.equals(value);
		boolean changed = oneValueIsNull || bothNotNullAndNotEqual;
		
		if (!Strings.isNullOrEmpty(value)) {
			properties.setProperty(name.name(), value);
		} else {
			properties.remove(name.name());
		}
		
		if (changed) {
			save();
		}
		
		return changed;
	}
	
	private void save() {
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(PREFERENCES_FILE), StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
			properties.store(writer, null);
		} catch (IOException e) {
			log.warn("Failed to store preferences.", e);
		}
	}

}
