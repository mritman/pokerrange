package pr.application.preferences;

import pr.application.preferences.Preferences.PreferenceName;
import pr.util.Optional;

public interface IntegerPreferencesListener {

	void preferenceChanged(PreferenceName name, Optional<Integer> value);
	
}
