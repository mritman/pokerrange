package pr.application.exception;

public class InitializationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8569435750985310081L;

	public InitializationException() {
		super();
	}
	
	public InitializationException(String message) {
		super(message);
	}
	
	public InitializationException(Throwable cause) {
		super(cause);
	}
	
	public InitializationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
