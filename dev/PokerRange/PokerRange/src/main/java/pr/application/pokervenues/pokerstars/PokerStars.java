package pr.application.pokervenues.pokerstars;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

@Qualifier
@Retention(value=RetentionPolicy.RUNTIME)
@Target({FIELD, PARAMETER, METHOD})
@Documented
public @interface PokerStars {}
