package pr.application.preferences;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pr.util.Optional;
import lombok.AccessLevel;
import lombok.Getter;

public abstract class AbstractPreferences implements Preferences {

	@Getter(AccessLevel.PROTECTED)
	private final Map<PreferenceName, List<PreferencesListener>> listeners = new HashMap<>();
	
	@Getter(AccessLevel.PROTECTED)
	private final Map<PreferenceName, List<IntegerPreferencesListener>> integerlisteners = new HashMap<>();
	
	@Override
	public synchronized boolean addListener(PreferenceName name, PreferencesListener listener) {
		return addListener(listeners, name, listener);
	}
	
	@Override
	public synchronized boolean addListener(PreferenceName name, IntegerPreferencesListener listener) {
		return addListener(integerlisteners, name, listener);
	}

	@Override
	public boolean removeListener(PreferenceName name, PreferencesListener listener) {
		return removeListener(listeners, name, listener);
	}
	
	@Override
	public boolean removeListener(PreferenceName name, IntegerPreferencesListener listener) {
		return removeListener(integerlisteners, name, listener);
	}
	
	protected Optional<Integer> fromString(String stringValue) {
		int value;
		
		if (stringValue == null) {
			return Optional.empty();
		}
		
		try {
			value = Integer.parseInt(stringValue);
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
		
		return Optional.of(value);
	}
	
	private <T> boolean addListener(Map<PreferenceName, List<T>> listeners, PreferenceName name, T listener) {
		List<T> list = listeners.get(name);
		
		if (list == null) {
			list = new LinkedList<>();
			listeners.put(name, list);
		}
		
		if (!list.contains(listener)) {
			return list.add(listener);
		}
		
		return false;
	}
	
	private <T> boolean removeListener(Map<PreferenceName, List<T>> listeners, PreferenceName name, T listener) {
		List<T> list = listeners.get(name);
		
		if (list != null) {
			return list.remove(listener);
		}
		
		return false;
	}

}
