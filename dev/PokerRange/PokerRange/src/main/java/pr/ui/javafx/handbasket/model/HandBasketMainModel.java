package pr.ui.javafx.handbasket.model;

import pr.domain.gamestate.GameState;
import pr.domain.handbasket.HandBasketManager;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.Getter;

public class HandBasketMainModel {

	@Getter	private HandBasketStageModel valueBasketStageModel;
	@Getter	private HandBasketStageModel strengthBasketStageModel;
	
	private SimpleBooleanProperty autoFoldProperty;
	
	public HandBasketMainModel(HandBasketStageModel valueBasketStageModel, HandBasketStageModel strengthBasketStageModel) {
		this.valueBasketStageModel = valueBasketStageModel;
		this.strengthBasketStageModel = strengthBasketStageModel;
		
		autoFoldProperty = new SimpleBooleanProperty();
		autoFoldProperty.bindBidirectional(valueBasketStageModel.autoFoldProperty());
		autoFoldProperty.bindBidirectional(strengthBasketStageModel.autoFoldProperty());
	}
	
	public SimpleBooleanProperty autoFoldProperty() {
		return autoFoldProperty;
	}
	
	public void update(HandBasketManager valueBasketManager, HandBasketManager strengthBasketManager, GameState gameState) {
		valueBasketStageModel.update(valueBasketManager, gameState);
		strengthBasketStageModel.update(strengthBasketManager, gameState);
	}
	
	public void reset() {
		valueBasketStageModel.reset();
		strengthBasketStageModel.reset();
	}

}
