package pr.ui.javafx.handbasket.presenter;

import java.io.IOException;
import java.text.NumberFormat;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import lombok.Getter;
import lombok.Setter;
import pr.application.exception.InitializationException;
import pr.ui.javafx.handbasket.model.HandBasketModel;
import pr.ui.javafx.handbasket.model.HandBasketStageModel;
import pr.ui.javafx.handbasket.view.HandBasketPane;

public class HandBasketPresenter {

	@Getter
	@FXML 
	private Node view;
	
	@FXML
	@Getter
	private HandBasketPane handBasketPane;

	@FXML private Button clearButton;
	@FXML private Label basketName;
	@FXML private Label basketSize;
	
	@Getter
	private HandBasketModel model;
	
	@Getter
	@Setter
	private HandBasketStageModel handBasketStageModel;
	
	public HandBasketPresenter() {
		model = new HandBasketModel();
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HandBasketView.fxml"));
		fxmlLoader.setController(this);
		
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new InitializationException(e);
		}
		
		view = fxmlLoader.getRoot();
	}

	@FXML
	public void initialize() {
		handBasketPane.setPresenter(this);
		handBasketPane.setModel(model);
		model.getBasketElementViews().addListener(new ListChangeListener<Node>() {

			@Override
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends Node> c) {
				String percentageString = NumberFormat.getPercentInstance().format(model.getHandBasket().getPercentage());
				String label = String.format("%,d - %s", model.getHandBasket().getHandsCount(), percentageString);
				basketSize.setText(label);
			}
			
		});
		
		model.handBasketProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				basketName.setText(model.getHandBasket().toString());
			}
			
		});
		
		clearButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (handBasketStageModel != null) {
					handBasketStageModel.clearHandBasket(model);
				}
			}
			
		});
	}
	
}
