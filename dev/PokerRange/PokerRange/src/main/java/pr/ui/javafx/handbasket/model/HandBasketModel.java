package pr.ui.javafx.handbasket.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import lombok.Setter;
import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.HandBasket;
import pr.ui.javafx.handbasket.SelectionAndDragHandler;
import pr.ui.javafx.handbasket.basketelement.BasketElementView;
import pr.ui.javafx.handbasket.basketelement.BasketElementViewComparator;
import pr.ui.javafx.handbasket.basketelement.BasketElementViewFactory;

public class HandBasketModel {

	private ReadOnlyListWrapper<Node> basketElementControlsProperty;
	
	private ReadOnlyObjectWrapper<HandBasket> handBasketProperty;
	
	@Setter
	private SelectionAndDragHandler selectionAndDragHandler;
	
	public HandBasketModel() {
		basketElementControlsProperty = new ReadOnlyListWrapper<>(this, "basketElementControls", FXCollections.<Node>observableArrayList());
		handBasketProperty = new ReadOnlyObjectWrapper<>(this, "handBasket");
	}
	
	public void addBasketElementView(BasketElementView basketElementView) {
		if (basketElementView.getModel().getCurrentHandBasketModel() == this) return;

		// Remove the basketElementModel from the HandBasketModel it currently in.
		basketElementView.getModel().getCurrentHandBasketModel().removeBasketElementView(basketElementView);
		basketElementView.getModel().setCurrentHandBasketModel(this);
		
		// Add the basketElementModel to the list of HandBasketModels of this HandBasketModel.
		getHandBasket().add(basketElementView.getModel().getBasketElement());
		getBasketElementViews().add(basketElementView);

		FXCollections.sort(getBasketElementViews(), BasketElementViewComparator.getInstance());
	}
	
	public void removeBasketElementView(BasketElementView basketElementView) {
		getHandBasket().remove(basketElementView.getModel().getBasketElement());
		getBasketElementViews().remove(basketElementView);
	}
	
	public HandBasket getHandBasket() {
		return handBasketProperty.get();
	}
	
	public void setHandBasket(HandBasket handBasket) {
		handBasketProperty.set(handBasket);
		
		updateBasketElementControls();
	}
	
	public ReadOnlyObjectProperty<HandBasket> handBasketProperty() {
		return handBasketProperty;
	}
	
	public ObservableList<Node> getBasketElementViews() {
		return basketElementControlsProperty.get();
	}

	public ReadOnlyListProperty<Node> basketElementControlsProperty() {
		return basketElementControlsProperty.getReadOnlyProperty();
	}
	
	private void updateBasketElementControls() {
		List<Node> basketElementViews = new LinkedList<>();
		Set<BasketElement> basketElements = getHandBasket().getBasketElements();
		
		for (BasketElement basketElement : basketElements) {
			BasketElementView basketElementView = BasketElementViewFactory.create(basketElement, this);
			basketElementViews.add(basketElementView);
			
			if (selectionAndDragHandler != null) {
				selectionAndDragHandler.register(basketElementView);
			}
		}
		
		basketElementControlsProperty.setAll(basketElementViews);
	}
	
}
