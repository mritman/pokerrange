package pr.ui.javafx.handbasket.view;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import lombok.Getter;
import lombok.Setter;
import pr.ui.javafx.handbasket.basketelement.BasketElementView;
import pr.ui.javafx.handbasket.model.HandBasketModel;
import pr.ui.javafx.handbasket.presenter.HandBasketPresenter;

public class HandBasketPane extends Pane {
	
	private final static int ROWS = 4;
	private final static int MIN_COLS = 5;
	private static final double LEFT_PADDING = 5;
	private static final int BOTTOM_PADDING = 5;
	private static final double SPACING = 4;
	
	@Getter
	private DoubleProperty spacingProperty;
	
	@Getter
	private HandBasketModel model;
	
	@Getter
	@Setter
	private HandBasketPresenter presenter;
	
	public HandBasketPane() {
		spacingProperty = new SimpleDoubleProperty(SPACING);

		setMinWidth(Pane.USE_PREF_SIZE);
		setMinHeight(Pane.USE_PREF_SIZE);
		
		setStyle("-fx-border-color: lightsteelblue; -fx-border-width: 0px 0px 1px 1px;");
	}
	
	public void setModel(HandBasketModel handBasketModel) {
		model = handBasketModel;
		new SimpleListProperty<>(getChildren()).bindContent(model.getBasketElementViews());
	}
	
	@Override
	public void layoutChildren() {
		super.layoutChildren();

		int count = 0;
		double space = getSpacing();
		double size = BasketElementView.SIZE;
		int columns = getColumns();
		int rows = getRows();
		double startHeight = ((ROWS-1) * size) + ((ROWS) * space) + BOTTOM_PADDING;
		
		for (Node child : getChildren()) {
			int row = count / columns;
			int column = count % columns;
			
			// Draw from right to left, bottom to top.
			if (row+1 == rows) {
				int remaining = getChildren().size() - count;
				int colsRemaining = columns < remaining ? columns : remaining;			
				column = (colsRemaining-1);				
			} else {
				column = (columns-1) - column;
			}
			
			double x = (column * size) + (column * space) + LEFT_PADDING; 
			double y = startHeight - (space + (row * size) + (row * space)) - BOTTOM_PADDING;
			
			count++;
			
			child.setLayoutX(x);
			child.setLayoutY(y);
		}
		
	}
	
	@Override
	protected double computePrefWidth(double height) {
		int columns = getColumns();
		return columns * BasketElementView.SIZE + (columns - 1) * getSpacing() + LEFT_PADDING;
	}
	
	@Override
	protected double computePrefHeight(double width) {
		return ROWS * BasketElementView.SIZE + ((ROWS - 1) * getSpacing()) + BOTTOM_PADDING; 
	}
	
	public void setSpacing(double value) {
		spacingProperty.set(value);
	}
	
	public double getSpacing() {
		return spacingProperty.get();
	}
	
	private int getColumns() {
		double size = getChildren().size();
		int columns = (int) Math.ceil(size / ROWS);
		
		if (columns < MIN_COLS) {
			return MIN_COLS;
		} else {
			return columns;
		}
	}
	
	private int getRows() {
		double size = getChildren().size();
		
		return (int) Math.ceil(size / getColumns());
	}

}
