package pr.ui.javafx.handbasket.model;

import java.util.LinkedList;
import java.util.Map;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.Node;
import lombok.Getter;
import pr.domain.gamestate.GameState;
import pr.domain.handbasket.HandBasketKey;
import pr.domain.handbasket.HandBasketManager;
import pr.domain.poker.hand.Hand;
import pr.domain.poker.hand.HandValue;
import pr.domain.poker.hand.TwoPair;
import pr.ui.javafx.handbasket.SelectionAndDragHandler;
import pr.ui.javafx.handbasket.basketelement.BasketElementView;
import pr.ui.javafx.handbasket.presenter.HandBasketPresenter;

public class HandBasketStageModel {
	private ReadOnlyObjectWrapper<HandBasketManager> handBasketManagerProperty;
	private ReadOnlyObjectWrapper<GameState> gameStateProperty;
	private SimpleBooleanProperty autoFoldProperty;
	private SimpleStringProperty boardStatusProperty;
	
	private SimpleMapProperty<HandBasketKey, HandBasketPresenter> handBasketPresentersProperty;
	
	@Getter
	private final SelectionAndDragHandler selectionAndDragHandler;
	
	public HandBasketStageModel(HandBasketManager handBasketManager) {
		
		handBasketManagerProperty = new ReadOnlyObjectWrapper<HandBasketManager>(handBasketManager) {
			@Override
			public void set(HandBasketManager handBasketManager) {
				super.set(handBasketManager);
				fireValueChangedEvent();
			}
		};

		gameStateProperty = new ReadOnlyObjectWrapper<>();
		boardStatusProperty = new SimpleStringProperty();
		autoFoldProperty = new SimpleBooleanProperty(false);
		autoFoldProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				handBasketManagerProperty.get().setAutoFold(autoFoldProperty.get());
			}
		});
		
		ObservableMap<HandBasketKey, HandBasketPresenter> observableMap = FXCollections.observableHashMap();
		handBasketPresentersProperty = new SimpleMapProperty<>(observableMap);
		
        for (HandBasketKey handBasketKey : handBasketManager.getHandBasketKeys()) {
        	HandBasketPresenter handBasketPresenter = new HandBasketPresenter();
        	handBasketPresenter.setHandBasketStageModel(this);
        	getHandBasketPresenters().put(handBasketKey, handBasketPresenter);
        }
        
        selectionAndDragHandler = new SelectionAndDragHandler(getHandBasketPresenters().get(handBasketManager.getWillFoldKey()).getModel());
        
        for (HandBasketPresenter handBasketPresenter : getHandBasketPresenters().values()) {
        	handBasketPresenter.getModel().setSelectionAndDragHandler(selectionAndDragHandler);
        	selectionAndDragHandler.register(handBasketPresenter);
        }
	}
	
	public HandBasketManager getHandBasketManager() {
		return handBasketManagerProperty.get();
	}

	public GameState getGameState() {
		return gameStateProperty.get();
	}
	
	public SimpleBooleanProperty autoFoldProperty() {
		return autoFoldProperty;
	}
	
	public SimpleStringProperty boardStatusProperty() {
		return boardStatusProperty;
	}
	
	public Map<HandBasketKey, HandBasketPresenter> getHandBasketPresenters() {
		return handBasketPresentersProperty.get();
	}
	
	public SimpleMapProperty<HandBasketKey, HandBasketPresenter> handBasketPresentersProperty() {
		return handBasketPresentersProperty;
	}
	
	public ReadOnlyObjectProperty<HandBasketManager> handBasketManagerProperty() {
		return handBasketManagerProperty;
	}
	
	public void reset() {
		gameStateProperty.set(null);
		selectionAndDragHandler.reset();
	}

	public void update(HandBasketManager handBasketManager, GameState gameState) {
		handBasketManagerProperty.set(handBasketManager);
		
		gameStateProperty.set(gameState);
		
		if (boardMakesHighHand()) return;
		
		for (HandBasketKey key : handBasketManager.getHandBasketKeys()) {
			handBasketPresentersProperty.get(key).getModel().setHandBasket(handBasketManager.getHandBasketMap().get(key));
		}
	}
	
	public void clearHandBasket(HandBasketModel handBasketModel) {
		HandBasketKey willFoldKey = handBasketManagerProperty.get().getWillFoldKey();
		HandBasketModel foldHandBasketModel = getHandBasketPresenters().get(willFoldKey).getModel();
		
		// A new temporary list is required to prevent ConcurrentModificationExceptions.
		LinkedList<Node> list = new LinkedList<>(handBasketModel.getBasketElementViews());
		
		for (Node node : list) {
			BasketElementView basketElementView = (BasketElementView) node;
			HandBasketModel sourceHandBasketModel = basketElementView.getModel().getCurrentHandBasketModel();
			HandBasketModel targetHandBasketModel;
			
			if (sourceHandBasketModel.equals(foldHandBasketModel)) {
				targetHandBasketModel = foldHandBasketModel;
			} else {
				targetHandBasketModel = basketElementView.getModel().getOriginalHandBasketModel();
			}
			
			targetHandBasketModel.addBasketElementView(basketElementView);
		}
	}
	
	private boolean boardMakesHighHand() {
		Hand hand = getGameState().getBoardCardCombinations().getHighestHand();
		boolean alert = false;
		
		if (hand != null && hand.getValue() != HandValue.HIGH_CARD
				&& hand.getValue() != HandValue.PAIR) {
			alert = true;
		} else if (hand != null && hand.getValue() == HandValue.TWO_PAIR) {
			TwoPair twoPair = (TwoPair) hand;
			if (twoPair.getType() == TwoPair.Type.TWO_BOARD_PAIR) {
				alert = true;
			}
		}
		
		if (alert) {
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("The board makes a ");
			sb.append(hand.getValue().toString());
			sb.append(": ");
			sb.append(hand.toString());
			
			boardStatusProperty.set(sb.toString());
			
			return true;
		} else {
			boardStatusProperty.set("");
			
			return false;
		}
	}

}
