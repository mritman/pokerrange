package pr.ui.javafx.handrange;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import lombok.Getter;
import pr.application.ApplicationConstants;
import pr.application.exception.InitializationException;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.range.HandRange;
import pr.domain.poker.range.typed.HandTypeRange;
import pr.domain.poker.range.typed.TypedHandRange;
import pr.domain.poker.range.typed.types.HandType;
import pr.domain.poker.range.typed.types.HandTypeEnum;
import pr.infrastructure.persistence.jpa.pokertracker.PokerTrackerRepository;
import pr.ui.javafx.handrange.NewHandRangeDialog.NewHandRangeArguments;
import pr.util.Utils;

public class HandRangePresenter {
	
	private static final int COMPACT_VIEW_COMBOX_BOX_MIN_WIDTH = 120;

	@Getter
	@FXML
	private HBox view;

	private VBox editView;
	
	private Stage editStage;
	
	@Getter
	@FXML 
	private ComboBox<HandRange> rangesComboBox;
	
	@FXML private Button saveButton;
	@FXML private Button deleteButton;
	@FXML private Button newButton;
	
	@FXML private GridPane handRangeGridPane;

	@Getter private HandRange handRange;
	
	@FXML private Label numberOfHandsLabel;
	@FXML private Label vpipLabel;
	@FXML private Label preflopRaiseLabel;
	@FXML private Label preflopThreeBetLabel;
	@FXML private Label preflopColdCallFirstRaiseLabel;
	
	private final OpponentAccountRepository opponentAccountRepository;
	private final Opponent opponent;
	
	private Map<HandTypeEnum, HandRangeTextField> inputTextFields;
	private Collection<TextField> outputTextFields;
	private Map<HandTypeEnum, StringProperty> outputTextProperties;
	
	private boolean updateDisabled = false;
	
	public interface RangeInvalidationListener {
		void invalidated(HandRangePresenter handRangePresenter);
	};
	
	private List<RangeInvalidationListener> listeners;

	public HandRangePresenter(Opponent opponent, OpponentAccountRepository opponentAccountRepository) {
		this.opponentAccountRepository = opponentAccountRepository;
		this.opponent = opponent;
		listeners = new LinkedList<>();
		
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HandRangeView.fxml"));
        fxmlLoader.setController(this);
        
        handRange = HandRange.EMPTY_HAND_RANGE;
        inputTextFields = new HashMap<>();
        outputTextFields = new LinkedList<>();
        outputTextProperties = new HashMap<>();
                
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
        editView = fxmlLoader.getRoot();
        
        createView();
    }
	
	public void update() {
		updatePlayerStatistics();
		
		if (handRange instanceof TypedHandRange) {
			TypedHandRange typedHandRange = (TypedHandRange) handRange;
			
			for (HandTypeEnum handTypeEnum : HandTypeEnum.values()) {
				int size = typedHandRange.getSize(handTypeEnum);
				double percentage = typedHandRange.getPercentage(handTypeEnum);
				
				if (size == 0) {
					outputTextProperties.get(handTypeEnum).set("");
				} else {
					outputTextProperties.get(handTypeEnum).set(size + " (" + Utils.roundTwoDecimalsNew(percentage) + "%)");
				}
			}
		}
	}
	
	private void updatePlayerStatistics() {
		int numberOfHands = opponent.getStatistics().getNumberOfHands();
		String vpip = Math.round(opponent.getStatistics().getVpip()) + "%";
		String preflopRaise = Math.round(opponent.getStatistics().getPreflopRaise()) + "%";
		String preflopThreeBet = Math.round(opponent.getStatistics().getPreflopThreeBet()) + "%";
		String preflopColdCallFirstRaise = Math.round(opponent.getStatistics().getPreflopColdCallFirstRaise()) + "%";
		
		numberOfHandsLabel.setText(String.valueOf(numberOfHands));
		vpipLabel.setText(vpip);
		preflopRaiseLabel.setText(preflopRaise);
		preflopThreeBetLabel.setText(preflopThreeBet);
		preflopColdCallFirstRaiseLabel.setText(preflopColdCallFirstRaise);
	}
	
	public boolean addListener(RangeInvalidationListener listener) {
		return listeners.add(listener);
	}
	
	public boolean removeListener(RangeInvalidationListener listener) {
		return listeners.remove(listener);
	}
	
	@FXML
	private void initialize() {
		initializeRangeComboBox();
		initializeHandRangeGridPane();
		updatePlayerStatistics();
        selectRange();
	}

	private void createView() {
		view = new HBox(10);
		
		ComboBox<HandRange> compactViewRangeComboBox = new ComboBox<>();
		compactViewRangeComboBox.setFocusTraversable(false);
		
		compactViewRangeComboBox.setButtonCell(getRangesComboBoxCellFactory().call(null));
		compactViewRangeComboBox.setCellFactory(getRangesComboBoxCellFactory());
		compactViewRangeComboBox.setMinSize(COMPACT_VIEW_COMBOX_BOX_MIN_WIDTH, rangesComboBox.getMinHeight());
		compactViewRangeComboBox.itemsProperty().bind(rangesComboBox.itemsProperty());
		compactViewRangeComboBox.getSelectionModel().select(rangesComboBox.getSelectionModel().getSelectedItem());
		compactViewRangeComboBox.setSelectionModel(rangesComboBox.getSelectionModel());
		
		Button rangeEditButton = new Button("Edit ranges");
		
		editStage = new Stage();
		editStage.setTitle(ApplicationConstants.APPLICATION_TITLE + " - Range editor");
		editStage.setScene(new Scene(editView));
		
		rangeEditButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if (editStage.getOwner() == null) {
					editStage.initModality(Modality.WINDOW_MODAL);
					editStage.initOwner(view.getScene().getWindow());
				}
				
				if (!editStage.isShowing()) {
					editStage.show();
				} else {
					editStage.requestFocus();
					editStage.toFront();
				}
				
			}

		});
		
		view.getChildren().addAll(compactViewRangeComboBox, rangeEditButton);
	}

	private void initializeRangeComboBox() {
		Collection<HandRange> ranges = new LinkedList<>();
		ranges.addAll(opponent.getRanges().values());
		ranges.addAll(opponentAccountRepository.getGlobalRanges());

		ObservableList<HandRange> items = FXCollections.observableArrayList(ranges);
		Collections.sort(items, getHandRangeComparator());
		
		rangesComboBox.setButtonCell(getRangesComboBoxCellFactory().call(null));
		rangesComboBox.setCellFactory(getRangesComboBoxCellFactory());
		rangesComboBox.setItems(items);
		rangesComboBox.getSelectionModel().select(0);
		
		rangesComboBox.valueProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				selectRange();
			}
			
		});
		
		selectDefaultRangeIfPresent();
	}
	
	private void selectDefaultRangeIfPresent() {
		Iterable<HandRange> filter = Iterables.filter(rangesComboBox.getItems(), new Predicate<HandRange>() {
			@Override
			public boolean apply(HandRange handRange) {
				return PokerTrackerRepository.PREFLOP_COLD_CALL_FIRST_RAISE_POKER_STOVE_RANGE.equals(handRange.getName());
			}
		});
		
		if (filter.iterator().hasNext()) {
			HandRange handRange = filter.iterator().next();
			rangesComboBox.getSelectionModel().select(handRange);
		}
	}

	private Callback<ListView<HandRange>, ListCell<HandRange>> getRangesComboBoxCellFactory() {
		return new Callback<ListView<HandRange>, ListCell<HandRange>>() {

			@Override
			public ListCell<HandRange> call(ListView<HandRange> listView) {
				return new ListCell<HandRange>() {
					
					@Override
					protected void updateItem(HandRange handRange, boolean empty) {
						super.updateItem(handRange, empty);

						if (!empty && handRange != null) {
							String text = handRange.getName();
							
							if (handRange instanceof TypedHandRange && isGlobalRange((TypedHandRange) handRange)) {
								text += "*";
							}
							
							setText(text);
						} else {
							setText("");
						}
					}
				};
			}
			
		};
	}

	private Comparator<HandRange> getHandRangeComparator() {
		return new Comparator<HandRange>() {

			@Override
			public int compare(HandRange o1, HandRange o2) {
				if (o1 instanceof TypedHandRange && o2 instanceof TypedHandRange) {
					TypedHandRange thr1 = (TypedHandRange) o1;
					TypedHandRange thr2 = (TypedHandRange) o2;
					boolean isGlobalRange1 = isGlobalRange(thr1);
					boolean isGlobalRange2 = isGlobalRange(thr2);
					
					if (!Objects.equal(isGlobalRange1, isGlobalRange2)) {
						return Boolean.compare(isGlobalRange1, isGlobalRange2);
					}
				}
				
				return o1.getName().compareTo(o2.getName());
			}
			
		};
	}

	private void initializeHandRangeGridPane() {
		int row = 0;
		
		for (HandTypeEnum handTypeEnum : HandTypeEnum.values()) {
			Label label = new Label(handTypeEnum.toString());
			final HandRangeTextField inputField = new HandRangeTextField();
			TextField outputField = new TextField();
			outputTextFields.add(outputField);
			
			inputField.handRangeProperty().addListener(new InvalidationListener() {
				
				@Override
				public void invalidated(Observable observable) {
					if (!(handRange instanceof TypedHandRange)) {
						throw new IllegalStateException();
					}
					
					TypedHandRange typedHandRange = (TypedHandRange) handRange;
					
					HandType handType = inputField.getHandType();
					
					if (inputField.getHandRange() != null
							&& inputField.getHandRange().getLower() != null
							&& inputField.getHandRange().getUpper() != null) {
						
						HandTypeRange handTypeRange = new HandTypeRange(inputField.getHandRange().getLower(), inputField.getHandRange().getUpper());
						
						typedHandRange.setHandTypeRange(handType.getName(), handTypeRange);
					} else {
						typedHandRange.removeHandTypeRange(handType.getName());
					}
					
					fireStateChanged();
				}
				
			});
			
			outputField.setEditable(false);
			outputField.setFocusTraversable(false);
			
			handRangeGridPane.add(label, 0, row);
			
			if (handTypeEnum != HandTypeEnum.OTHER) {
				handRangeGridPane.add(inputField, 1, row);
			}

			handRangeGridPane.add(outputField, 2, row);
			
			inputTextFields.put(handTypeEnum, inputField);
			StringProperty stringProperty = new SimpleStringProperty();
			outputField.textProperty().bind(stringProperty);
			outputTextProperties.put(handTypeEnum, stringProperty);
			
			row++;
		}
	}
	
	private void selectRange() {
		if (rangesComboBox.getSelectionModel().getSelectedItem() == null) {
			handRange = HandRange.EMPTY_HAND_RANGE;
			disableTypedHandRangeFields();
			return;
		}
		
		handRange = rangesComboBox.getSelectionModel().getSelectedItem();
		
		// To prevent a stateChanged event being fired for each input field,
		// updating is disabled before updating the fields and re-enabled after.
		// Then fireStateChanged() is called to finally trigger a single update.
		if (handRange instanceof TypedHandRange) {
			enableTypedHandRangeFields();
			
			TypedHandRange typedHandRange = (TypedHandRange) handRange;
			
			updateDisabled = true;
			
			for (HandTypeEnum handTypeEnum : HandTypeEnum.values()) {
				inputTextFields.get(handTypeEnum).setHandType(typedHandRange.findHandType(handTypeEnum));
			}
			
			updateDisabled = false;
		} else {
			disableTypedHandRangeFields();
		}
		
		fireStateChanged();
	}
	
	@FXML
	private void newRange() {
		NewHandRangeArguments newRangeArguments = new NewHandRangeDialog("Please enter the name of the range",
				"Please enter the name of the range", "", view
						.getScene().getWindow()).show();
		
		if (newRangeArguments == null) {
			return;
		}
		
		TypedHandRange newRange = new TypedHandRange(newRangeArguments.getName());
		
		updateRepositoryAndView(newRange, newRangeArguments.isGlobalRange());
	}
	
	@FXML
	private void saveRange() {
		if (rangesComboBox.getSelectionModel().getSelectedItem() == null) return;
		if (!(handRange instanceof TypedHandRange)) return;
		
		TypedHandRange typedHandRange = (TypedHandRange) handRange;
		
		updateRepositoryAndView(typedHandRange, isGlobalRange(typedHandRange));
	}
	
	@FXML
	private void deleteRange() {
		if (rangesComboBox.getItems().isEmpty()) return;
		if (!(handRange instanceof TypedHandRange)) return;

		TypedHandRange typedHandRange = (TypedHandRange) handRange;
		
		if (isGlobalRange(typedHandRange)) {
			opponentAccountRepository.deleteGlobalRange(typedHandRange);
		} else {
			opponentAccountRepository.deleteRange(opponent, typedHandRange);
		}
		
		rangesComboBox.getItems().remove(handRange);
		
		if (rangesComboBox.getItems().isEmpty()) {
			selectRange();
		}
	}

	private void updateRepositoryAndView(TypedHandRange typedHandRange, boolean globalRange) {
		if (globalRange) {
			opponentAccountRepository.addGlobalRange(typedHandRange);
		} else {
			opponentAccountRepository.addRange(opponent, typedHandRange);
		}
		
		rangesComboBox.getItems().remove(typedHandRange);
		rangesComboBox.getItems().add(typedHandRange);
		
		Collections.sort(rangesComboBox.getItems(), getHandRangeComparator());
		
		if (rangesComboBox.getSelectionModel().getSelectedItem() == null
				|| !rangesComboBox.getSelectionModel().getSelectedItem().equals(typedHandRange)) {
			
			rangesComboBox.getSelectionModel().select(typedHandRange);
		}
	}
	
	private void fireStateChanged() {
		for (RangeInvalidationListener listener : listeners) {
			if (!updateDisabled) {
				listener.invalidated(this);
			}
		}
	}
	
	private void disableTypedHandRangeFields() {
		setFieldsDisabled(true);		
	}

	private void enableTypedHandRangeFields() {
		setFieldsDisabled(false);
	}
	
	private void setFieldsDisabled(boolean disabeld) {
		deleteButton.setDisable(disabeld);
		saveButton.setDisable(disabeld);
		
		for (HandRangeTextField textField : inputTextFields.values()) {
			textField.setDisable(disabeld);
		}
		
		for (TextField textField : outputTextFields) {
			textField.setDisable(disabeld);
		}
	}
	
	private boolean isGlobalRange(TypedHandRange typedHandRange) {
		return opponentAccountRepository.getGlobalRanges().contains(typedHandRange);
	}

}
