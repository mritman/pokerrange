package pr.ui.javafx.handbasket;

import javafx.beans.binding.DoubleBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import pr.domain.handbasket.handvaluebaskets.ValueBasketEnum;
import pr.ui.javafx.handbasket.model.HandBasketModel;
import pr.ui.javafx.util.MouseOverPieChart;

public class ValueBasketPieChart extends MouseOverPieChart {
	private final ObservableList<PieChart.Data> data = FXCollections.observableArrayList(
			new PieChart.Data("WP", 0), 
			new PieChart.Data("P", 0),
			new PieChart.Data("2P+", 0),
			new PieChart.Data("D", 0),
			new PieChart.Data("WF", 0));
	
	public ValueBasketPieChart() {
		super();
		
		getData().addAll(data);
		
    	String path = getClass().getResource("ValueBasketPieChartStyle.css").toExternalForm();
    	getStylesheets().add(path);
	}
	
	public void bind(ValueBasketEnum handBasketEnum, final HandBasketModel handBasketModel) {
		PieChart.Data pieChartData = getData(handBasketEnum);

		pieChartData.pieValueProperty().bind(new DoubleBinding() {

			{
				super.bind(handBasketModel.getBasketElementViews());
			}
			
			@Override
			protected double computeValue() {
				if (handBasketModel.getHandBasket() == null) {
					return 0;
				} else {
					return handBasketModel.getHandBasket().getHandsCount();
				}
			}

		});
		
	}
	
	private PieChart.Data getData(ValueBasketEnum handBasketEnum) {
		switch (handBasketEnum) {
		case WEAK_PAIR:
			return data.get(0);
		case PAIR:
			return data.get(1);
		case TWO_PAIR_PLUS:
			return data.get(2);
		case DRAW:
			return data.get(3);
		case WILL_FOLD:
			return data.get(4);
		default:
			// This should never happen.
			throw new IllegalStateException();
		}
	}
}
