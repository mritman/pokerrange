package pr.ui.javafx.handbasket.basketelement;

import pr.domain.handbasket.handcollection.HandCollection;

public class HandCollectionView extends BasketElementView {
	
	public HandCollectionView(HandCollectionModel handCollectionModel) {
		super(handCollectionModel);
		
		// Style has to be set here because setColor changes it. If it wouldn't be changed here it could be set in the superclass.
		setColor(handCollectionModel.getHandCollection());
		setStyle(getRegularStyle());
		
		if (handCollectionModel.getHandCollection().isComboDraw()) {
			getComboIndicator().setVisible(true);
		}
		
	}

	private void setColor(HandCollection handCollection) {
		String color = "-fx-background-color: ";
		
		if (handCollection.isSuited()) {
			color += "tomato;";
		} else if (handCollection.hasSuited()) {
			color += "#81C2D5;";
		} else {
			color += "silver;";
		}
		
		setRegularStyle(getRegularStyle() + color);
	}

}
