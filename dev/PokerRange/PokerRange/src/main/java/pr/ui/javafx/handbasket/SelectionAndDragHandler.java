package pr.ui.javafx.handbasket;

import java.util.List;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import pr.ui.javafx.handbasket.basketelement.BasketElementView;
import pr.ui.javafx.handbasket.model.HandBasketModel;
import pr.ui.javafx.handbasket.presenter.HandBasketPresenter;
import pr.ui.javafx.handbasket.view.HandBasketPane;
import pr.ui.util.Selection;

public class SelectionAndDragHandler {
	private Selection<BasketElementView> selection = new Selection<>();
	
	private HandBasketModel foldHandBasketModel;
	
	private EventHandler<MouseEvent> mouseClickedHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {
			if (event.getEventType() == MouseEvent.MOUSE_CLICKED && event.getSource() instanceof BasketElementView) {
				if (event.getButton() == MouseButton.PRIMARY) {
					handleLeftClick(event);
				} else if (event.getButton() == MouseButton.SECONDARY) {
					handleRightClick(event);
				}
			} else if (isNonSelectionOrDragMouseClick(event)) {
				selection.clearSelection();
			}
		}

	};
	
	private boolean isNonSelectionOrDragMouseClick(MouseEvent event) {
		boolean modifiersActive = event.isControlDown() && event.isShiftDown();  
		boolean sourceIsBackground = (event.getSource() instanceof HandBasketPane || event.getSource() instanceof GridPane);
		
		return event.getEventType() == MouseEvent.MOUSE_CLICKED
				&& !modifiersActive
				&& sourceIsBackground;
	}
	
	private EventHandler<MouseEvent> dragDetectedHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {
			Node source = (Node) event.getSource();
			
			if (source instanceof BasketElementView && selection.isEmpty()) {
				selection.addSelection((BasketElementView) source);
			}
			
			Dragboard dragBoard = source.startDragAndDrop(TransferMode.MOVE);
			
			// TODO change this, its necessary for a drag of this type, but the contents are meaningless.
			ClipboardContent content = new ClipboardContent();
			content.putString("Some string");
			dragBoard.setContent(content);
			
			event.consume();
		}
		
	};
	
	private EventHandler<DragEvent> dragOverHandler = new EventHandler<DragEvent>() {

		@Override
		public void handle(DragEvent event) {
			Node sourceBasket = ((Node) event.getGestureSource()).getParent();
			Node eventSource = (Node) event.getSource();
			
			if (eventSource.equals(event.getGestureSource())
					&& eventSource.equals(sourceBasket)
					&& eventSource.getParent().equals(sourceBasket)) {
				event.acceptTransferModes(TransferMode.MOVE);
			}
				
			event.consume();
		}
		
	};
	
	private EventHandler<DragEvent> dragDroppedHandler = new EventHandler<DragEvent>() {

		@Override
		public void handle(DragEvent event) {
			HandBasketModel targetHandBasket;
			
			if (event.getSource() instanceof BasketElementView) {
				BasketElementView targetBasketElementView = (BasketElementView) event.getGestureTarget();
				targetHandBasket = targetBasketElementView.getModel().getCurrentHandBasketModel();
			} else if (event.getSource() instanceof HandBasketPane) {
				HandBasketPane handBasketPane = (HandBasketPane) event.getSource();
				targetHandBasket = handBasketPane.getModel();
			} else {
				throw new ClassCastException();
			}
			
			if (event.getGestureSource() instanceof BasketElementView && !selection.contains((BasketElementView) event.getGestureSource())) {
				BasketElementView basketElementView = (BasketElementView) event.getGestureSource();
				targetHandBasket.addBasketElementView(basketElementView);
			} else {
				for (BasketElementView basketElementView : selection.getSelection()) {
					targetHandBasket.addBasketElementView(basketElementView);
				}
			}
			
			event.consume();
		}

	};

	public SelectionAndDragHandler(HandBasketModel foldHandBasketModel) {
		this.foldHandBasketModel = foldHandBasketModel;
	}

	public void register(BasketElementView basketElementView) {
		basketElementView.setOnMouseClicked(mouseClickedHandler);
		basketElementView.setOnDragDetected(dragDetectedHandler);
		basketElementView.setOnDragOver(dragOverHandler);
		basketElementView.setOnDragDropped(dragDroppedHandler);
	}
	
	public void register(HandBasketPresenter handBasketPresenter) {
		handBasketPresenter.getHandBasketPane().setOnMouseClicked(mouseClickedHandler);
		handBasketPresenter.getHandBasketPane().setOnDragDetected(dragDetectedHandler);
		handBasketPresenter.getHandBasketPane().setOnDragOver(dragOverHandler);
		handBasketPresenter.getHandBasketPane().setOnDragDropped(dragDroppedHandler);
	}
	
	public void register(GridPane basketPane) {
		basketPane.setOnMouseClicked(mouseClickedHandler);
	}
	
	public void reset() {
		selection.clearSelection();
	}
	
	private void handleShiftClick(BasketElementView basketElementView) {
		BasketElementView lastSelectedBasketElement = selection.getLastSelectedItem();
		
		HandBasketModel currentHandBasketModel = basketElementView.getModel().getCurrentHandBasketModel();
		
		if (lastSelectedBasketElement != null
				&& lastSelectedBasketElement.getModel()
						.getCurrentHandBasketModel() == currentHandBasketModel) {

			ObservableList<Node> list = currentHandBasketModel.getBasketElementViews(); 
			
			int fromIndex = list.indexOf(lastSelectedBasketElement);
			int toIndex = list.indexOf(basketElementView);

			if (fromIndex > toIndex) {
				int tmp = toIndex;
				toIndex = fromIndex;
				fromIndex = tmp;
			} else if (fromIndex < toIndex) {
				fromIndex++;
				toIndex++;
			}
			
			List<Node> subList = list.subList(fromIndex, toIndex);
			for (Node node : subList) {
				selection.addSelection((BasketElementView) node);
			}
		} else {
			selection.toggleSelection(basketElementView);
		}
	}
	
	private void handleLeftClick(MouseEvent event) {
		BasketElementView source = (BasketElementView) event.getSource();

		if (!event.isControlDown() && !event.isShiftDown()) {
			selection.setSelection(source);
		} else if (event.isShiftDown()) {
			handleShiftClick(source);
		} else if (event.isControlDown()) {
			selection.toggleSelection(source);
		} 
	}
	
	private void handleRightClick(MouseEvent event) {
		BasketElementView source = (BasketElementView) event.getSource();
		
		if (selection.contains(source)) {
			toggleFoldSelection();
		} else {
			toggleFold(source);
		}
	}
	
	/**
	 * Folds the selection if it is not folded, unfolds it otherwise.
	 */
	private void toggleFoldSelection() {
		List<BasketElementView> selectedBasketElementViews = selection.getSelection();
		
		for (BasketElementView basketElementView : selectedBasketElementViews) {
			toggleFold(basketElementView);
		}
	}
	
	private void toggleFold(BasketElementView basketElementView) {
		HandBasketModel originalHandBasketModel = basketElementView.getModel().getOriginalHandBasketModel();
		HandBasketModel currentHandBasketModel = basketElementView.getModel().getCurrentHandBasketModel();
		
		if (currentHandBasketModel.equals(foldHandBasketModel)) {
			foldHandBasketModel.addBasketElementView(basketElementView);
		} else {
			originalHandBasketModel.addBasketElementView(basketElementView);
		}
	}

}
