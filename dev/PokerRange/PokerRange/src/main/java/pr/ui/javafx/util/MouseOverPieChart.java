package pr.ui.javafx.util;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.scene.chart.PieChart;
import javafx.scene.input.MouseEvent;

public class MouseOverPieChart extends PieChart {

	public MouseOverPieChart() {
		setLabelsVisible(false);
		
		setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				setLabelsVisible(true);
			}
			
		});
		
		setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				setLabelsVisible(false);
			}
			
		});
		
		final InvalidationListener pieValueListener = new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				DoubleProperty value = (DoubleProperty) observable;
				PieChart.Data slice = (PieChart.Data) value.getBean();
				slice.getNode().setVisible(value.get() != 0);
			}
		};
		
		getData().addListener(new ListChangeListener<PieChart.Data>() {

			@Override
			public void onChanged(ListChangeListener.Change<? extends PieChart.Data> c) {
				while (c.next()) {
					for (PieChart.Data data : c.getAddedSubList()) {
						data.pieValueProperty().addListener(pieValueListener);
						data.getNode().setVisible(false);
					}
				}
			}

		});

	}
}
