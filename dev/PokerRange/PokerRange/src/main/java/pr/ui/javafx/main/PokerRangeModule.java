package pr.ui.javafx.main;

import javafx.stage.Stage;
import pr.application.preferences.FilePreferences;
import pr.application.preferences.Preferences;
import pr.ui.javafx.stage.StagePresenter;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class PokerRangeModule extends AbstractModule {

	private final Stage stage;
	
	public PokerRangeModule(Stage stage) {
		this.stage = stage;
	}
	
	@Override
	protected void configure() {
		binder().requireExplicitBindings();

		bind(ServiceManager.class).asEagerSingleton();
		bind(Preferences.class).to(FilePreferences.class).in(Singleton.class);
		bind(Stage.class).toInstance(stage);
		bind(StagePresenter.class);
	}

}
