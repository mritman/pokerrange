package pr.ui.javafx.handbasket.basketelement;

import java.util.Comparator;

import javafx.scene.Node;

import pr.domain.handbasket.handcollection.BasketElementComparator;

public class BasketElementViewComparator implements Comparator<Node> {

	private static final BasketElementViewComparator basketElementControlComparator = new BasketElementViewComparator();
	
	public static BasketElementViewComparator getInstance() {
		return basketElementControlComparator;
	}
	
	/**
	 * Both arguments should be an instance of BasketElementControl.
	 * 
	 * throws ClassCastException if either o1 or o2 is not an instance of BasketElementControl
	 */
	@Override
	public int compare(Node o1, Node o2) {
		return compareBasketElementControls((BasketElementView) o1, (BasketElementView) o2);	
	}

	private int compareBasketElementControls(BasketElementView o1,
			BasketElementView o2) {
		
		return BasketElementComparator.getInstance().compare(o1.getModel().getBasketElement(), o2.getModel().getBasketElement()); 
	}

}
