package pr.ui.javafx.table;

import java.util.Comparator;

import javafx.scene.control.Tab;
import lombok.Getter;
import pr.domain.poker.player.Opponent;
import pr.ui.javafx.PrStyle;
import pr.ui.javafx.table.opponent.OpponentPresenter;

public class OpponentTab extends Tab {

	@Getter
	private final Opponent opponent;
	
	@Getter
	private final OpponentPresenter presenter;
	
	public OpponentTab(Opponent opponent, OpponentPresenter presenter) {
		super(opponent.getSeat().toInt() + ". " + opponent.getName());
		this.opponent = opponent;
		this.presenter = presenter;
		
		setContent(presenter.getView());
	}
	
	public void updateTitle() {
		setText(opponent.getSeat().toInt() + ". " + opponent.getName());
	}
	
	public void setOpponentInHandStyle(boolean selected) {
		if (selected) {
			getStyleClass().remove(PrStyle.OPPONENT_TAB_INACTIVE_CLASS);
			getStyleClass().add(PrStyle.OPPONENT_TAB_CLASS);
		} else {
			getStyleClass().remove(PrStyle.OPPONENT_TAB_CLASS);
			getStyleClass().add(PrStyle.OPPONENT_TAB_INACTIVE_CLASS);
		}
	}
	
	public static final Comparator<OpponentTab> SORT_BY_SEAT = new Comparator<OpponentTab>() {

		@Override
		public int compare(OpponentTab o1, OpponentTab o2) {
			return Integer.compare(o1.getOpponent().getSeat().toInt(), o2.getOpponent().getSeat().toInt());
		}
		
	};
	
}
