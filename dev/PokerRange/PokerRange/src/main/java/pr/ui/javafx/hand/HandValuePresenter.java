package pr.ui.javafx.hand;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Popup;
import pr.application.exception.InitializationException;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.HandManager;
import pr.domain.poker.hand.HandValue;

public class HandValuePresenter implements Initializable {

	private HandValue[] singleHandValues = {
			HandValue.STRAIGHT_FLUSH,
			HandValue.FOUR_OF_A_KIND,
			HandValue.FULL_HOUSE,
			HandValue.FLUSH,
			HandValue.STRAIGHT,
			HandValue.THREE_OF_A_KIND,
			HandValue.TWO_PAIR,
			HandValue.PAIR,
			HandValue.HIGH_CARD,
			HandValue.FLUSH_DRAW,
			HandValue.STRAIGHT_DRAW
	};
	
	private HandManager handManager;
	
	@FXML private Node view;
	@FXML private GridPane gridPane;
	
	private Popup holeCardsPopup;
	
	private Map<HandValue, TextField> textFields = new HashMap<>();
	
	public HandValuePresenter() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HandValueView.fxml"));
        fxmlLoader.setController(this);
                
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
        view = fxmlLoader.getRoot();
        
        holeCardsPopup = createPopup();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		int row = 0;
		
		for (HandValue handValue : singleHandValues) {
			createTextField(row, handValue);
			row++;
		}

	}
	
	public Node getView() {
		return view;
	}

	public void update(HandManager handManager) {
		this.handManager = handManager;
		
		for (HandValue handValue : singleHandValues) {
			TextField textField = textFields.get(handValue);
			Set<Combinations> combinations = handManager.getCombinations(handValue);
			
			int size = combinations.size();
			double percentage = handManager.percentage(handValue);
			
			updateOutputField(textField, size, percentage);
		}

	}
	
	private void createTextField(int row, HandValue handValue) {
		final TextField textField = new TextField();
		textField.setEditable(false);
		textField.setFocusTraversable(false);
		textFields.put(handValue, textField);
		textField.setUserData(handValue);
		gridPane.add(textField, 1, row);
		
		createPopupListener(textField);
	}
	
	
	private Popup createPopup() {
		Popup popup = new Popup();
		popup.setAutoHide(true);
		popup.setAutoFix(true);
		
		return popup;
	}
	
	private void createPopupListener(final TextField textField) {
		textField.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				holeCardsPopup.getContent().clear();
				HandValue handValue = (HandValue) textField.getUserData();
				
				Set<Combinations> holeCardsSet = handManager.getCombinations(handValue);
				
				if (holeCardsSet.isEmpty() || event.getButton() != MouseButton.PRIMARY) {
					return;
				}
				
				ListView<String> listView = new ListView<>();
				
				for (Combinations combinations : holeCardsSet) {
					StringBuilder item = new StringBuilder();
					item.append(combinations.toString());
					
					if (combinations.getHighestHand().getValueMap().size() > 0) {
						item.append(" \t: " + combinations.getHand(handValue).toString());
					}
					
					listView.getItems().add(item.toString());
				}
				
				holeCardsPopup.getContent().add(listView);
				holeCardsPopup.show(textField, event.getScreenX(), event.getScreenY());
			}
		});
	}

	private void updateOutputField(TextField textField, int size, double percentage) {
		if (size == 0) {
			textField.setText("");
		} else {
			textField.setText(size + " (" + percentage + "%)");
		}
	}
}
