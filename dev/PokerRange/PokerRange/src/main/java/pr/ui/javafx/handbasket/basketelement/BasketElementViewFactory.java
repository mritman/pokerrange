package pr.ui.javafx.handbasket.basketelement;

import pr.domain.handbasket.BasketElement;
import pr.domain.handbasket.handcollection.HandCollection;
import pr.domain.handbasket.handcollection.HandCollectionGroup;
import pr.ui.javafx.handbasket.model.HandBasketModel;

public final class BasketElementViewFactory {

	private BasketElementViewFactory() {};
	
	public static BasketElementView create(BasketElement basketElement, HandBasketModel handBasketModel) {
		BasketElementView basketElementControl = null;
		
		if (basketElement instanceof HandCollection) {
			HandCollection handCollection = (HandCollection) basketElement;
			HandCollectionModel handCollectionModel = new HandCollectionModel(handCollection, handBasketModel);
			basketElementControl = new HandCollectionView(handCollectionModel);
		} else if (basketElement instanceof HandCollectionGroup) {
			HandCollectionGroup handCollectionGroup = (HandCollectionGroup) basketElement;
			HandCollectionGroupModel handCollectionGroupModel = new HandCollectionGroupModel(handCollectionGroup, handBasketModel);
			basketElementControl =  new HandCollectionGroupView(handCollectionGroupModel);
		} else {
			throw new ClassCastException();
		}
		
		return basketElementControl;
	}

}
