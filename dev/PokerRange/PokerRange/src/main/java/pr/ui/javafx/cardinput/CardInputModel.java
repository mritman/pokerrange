package pr.ui.javafx.cardinput;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.Street;

public class CardInputModel {
	public static final int MAX_NR_SELECTED_CARDS = 7;
	
	private static final int MAX_NR_BOARDCARDS = 5;
	private static final int MAX_NR_HOLECARDS = 2;
	
	private TreeSet<Card> holeCards = new TreeSet<Card>((Comparator<Card>) Card.CLUB_ACE);
	private TreeSet<Card> flop = new TreeSet<Card>((Comparator<Card>) Card.CLUB_ACE);
	private Card turn = null;
	private Card river = null;
	
	public void reset() {
		holeCards.clear();
		flop.clear();
		turn = null;
		river = null;
	}
	
	public boolean addHoleCard(Card card) {
		if (holeCards.size() < MAX_NR_HOLECARDS) {
			holeCards.add(card);
			return true;

		}
		
		return false;
	}
	
	public boolean addBoardCard(Card card) {
		if (boardCardsCount() < MAX_NR_BOARDCARDS) {
			if (flop.size() < 3) {
				flop.add(card);
			} else if (turn == null) {
				turn = card;
			} else if (river == null) {
				river = card;
			} else {
				//should never be reached
				throw new IllegalStateException("Should never be reached");
			}
			
			return true;
		}
		
		return false;
	}

	public boolean remove(Card card) {
		if (holeCards.remove(card)) {
			return true;
		} else if (flop.remove(card)) {
			return true;
		} else if (turn == card){
			turn = null;
			return true;
		} else if (river == card) {
			river = null;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns a new instance, identical to the {@link HoleCards} instance used in this
	 * class. A new instance if returned to guard the internal state of an
	 * instance of this class.
	 * 
	 * @return A new instance identical to the private {@link HoleCards} field
	 *         of an instance of this class.
	 */
	public HoleCards getHoleCards() {
		if (holeCards.size() == 2) {
			return new HoleCards(holeCards.first(), holeCards.last());
		}
		
		return null;
	}
	
	public BoardCards getBoardcards() {
		LinkedList<Card> cards = new LinkedList<Card>();
		
		if (flop.size() >= 3) { 
			cards.addAll(flop);
			
			if (turn != null) {
				cards.add(turn);
				
				if (river != null) {
					cards.add(river);
				}
			}
		} else {
			return new BoardCards();
		}
		
		return new BoardCards(cards.toArray(new Card[0]));
	}
	
	public Street getStreet() {
		Street street = Street.PREFLOP;
		
		if (flop.size() >= 3) {
			street = Street.FLOP;
			
			if (turn != null) {
				street = Street.TURN;
				
				if (river != null) {
					street = Street.RIVER;
				}
			}
		}
		
		return street;
	}
	
	private int boardCardsCount() {
		int count = flop.size();
		
		if (count == 3) {
			if (turn != null) {
				count++;
				
				if (river != null) {
					count++;
				}
			}
		}
		
		return count;
	}
	
	public int count() {
		int count = holeCards.size(); 

		if (flop.size() == 3) {
			count += flop.size();
			
			if (turn != null) {
				count++;
				
				if (river != null) {
					count++;
				}
			}
		}
		
		return count;
	}
}