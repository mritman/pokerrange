package pr.ui.javafx.util;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import pr.application.exception.InitializationException;

public class StringDialog extends AnchorPane {

	@FXML private Label messageLabel;
	@FXML private TextField textField;
	
	private Stage stage;
	private String result = null;
	
	public StringDialog(String title, String message, String defaultValue, Window owner) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("StringDialog.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new InitializationException(exception);
		}
		
		messageLabel.setText(message);
		textField.setText(defaultValue);
		Scene scene = new Scene(this);
		
		stage = new Stage();
		stage.setTitle(title);
		stage.setScene(scene);
		stage.setResizable(false);

		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(owner);
		stage.initStyle(StageStyle.UTILITY);
	}
	
	public String show() {
		stage.showAndWait();
		
		if (result == null || "".equals(result)) {
			return null;
		}
		
		return result;
	}
	
	public void onOk() {
		result = textField.getText();
		stage.close();
	}
	
	public void onCancel() {
		stage.close();
	}

}
