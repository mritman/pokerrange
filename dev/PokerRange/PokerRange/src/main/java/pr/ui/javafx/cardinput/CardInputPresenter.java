package pr.ui.javafx.cardinput;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.MouseButton;
import lombok.Getter;
import pr.domain.cards.Card;
import pr.domain.cards.Suit;
import pr.domain.gamestate.GameState;
import pr.domain.poker.Street;

public class CardInputPresenter {

	@Getter
	private CardInputView view;
	
	private Set<CardToggleButton> cardToggleButtons = new HashSet<CardToggleButton>();

	private CardInputModel cardInputModel = new CardInputModel();

	public interface CardInputListener {
		void invalidated();
	}
	
	private List<CardInputListener> listeners;
	
	public CardInputPresenter() {
		listeners = new LinkedList<>();
		
		view = new CardInputView();
		view.getResetButton().setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				manualInputProperty().set(true);
				reset();
			}
		});
		

        createToggleButtons();
    }
	
    public GameState getGameState() {
		return new GameState(cardInputModel.getHoleCards(), cardInputModel.getBoardcards());
	}

    public boolean addListener(CardInputListener listener) {
    	return listeners.add(listener);
    }
    
    public boolean removeListener(CardInputListener listener) {
    	return listeners.remove(listener);
    }
    
    public void setGameState(GameState gameState) {
    	cardInputModel.reset();
    	clearSelection();
    	
    	if (gameState.getHoleCards() != null) {
	    	cardInputModel.addHoleCard(gameState.getHoleCards().card1());
	    	cardInputModel.addHoleCard(gameState.getHoleCards().card2());
	    	
	    	CardToggleButton holeCard1Button = findButton(cardInputModel.getHoleCards().card1());
	    	CardToggleButton holeCard2Button = findButton(cardInputModel.getHoleCards().card2());
	    	holeCard1Button.rightClick(false);
	    	holeCard2Button.rightClick(false);
    	}
    	
    	if (gameState.getBoardCards().getFlop() != null) {
	    	for (Card card : gameState.getBoardCards().getFlop().getCards()) {
	    		cardInputModel.addBoardCard(card);
	    		findButton(card).leftClick(false);
	    	}
	    	
	    	Card turn = gameState.getBoardCards().getTurn();
	    	if (turn != null) {
	    		cardInputModel.addBoardCard(turn);
	    		findButton(turn).leftClick(false);
	    		
	    		Card river = gameState.getBoardCards().getRiver(); 
	    		if (river != null) {
	    			cardInputModel.addBoardCard(river);
	    			findButton(river).leftClick(false);
	    		}
	    	}
    	}
    }
    
    public BooleanProperty manualInputProperty() {
    	return getView().getManualInputCheckBox().selectedProperty();
    }
	
	private CardToggleButton findButton(Card card) {
		for (CardToggleButton button : cardToggleButtons) {
			if (button.getUserData() == card) return button;
		}
		
		return null;
	}
    
    @FXML
    private void buttonToggled(ActionEvent e) {
    	manualInputProperty().set(true);
    	
    	CardToggleButton toggleButton = (CardToggleButton) e.getSource();
		boolean stateChanged = false;
		
		Street streetBeforeChange = cardInputModel.getStreet();
		
		if (toggleButton.isSelected()) {
			stateChanged = handleButtonSelected(toggleButton, stateChanged);
		} else if (!toggleButton.isSelected()) {
			stateChanged = handleButtonDeselected(toggleButton, stateChanged);
		}
		
		if (streetBeforeChange != cardInputModel.getStreet()) {
			stateChanged = true;
		}
		
    	if (stateChanged) fireInvalidationEvent();
    }

	private boolean handleButtonSelected(CardToggleButton toggleButton, final boolean stateChanged) {
		boolean result = stateChanged;
		
		if (toggleButton.getMouseButton() == MouseButton.SECONDARY) {
			if (!cardInputModel.addHoleCard((Card) toggleButton.getUserData())) {
				toggleButton.setSelected(false);
			} else {
				result = true;
			}
		} else {
			if (!cardInputModel.addBoardCard((Card) toggleButton.getUserData())) {
				toggleButton.setSelected(false);
			}
		}
		
		if (cardInputModel.count() == CardInputModel.MAX_NR_SELECTED_CARDS) {
			disableButtons();
		}
		return result;
	}

	private boolean handleButtonDeselected(CardToggleButton toggleButton, boolean stateChanged) {
		boolean result = stateChanged;
		Card card = (Card) toggleButton.getUserData();
		
		boolean isHoleCard = false;
		if (cardInputModel.getHoleCards() != null) {
			isHoleCard = cardInputModel.getHoleCards().contains(card);
		}
		
		if (cardInputModel.remove(card)) {
			toggleButton.setStyle("");
			enableButtons();
			result = isHoleCard;
		}
		return result;
	}
    
    @FXML
    private void reset() {
    	clearSelection();
		
		fireInvalidationEvent();
    }
    
	private void clearSelection() {
		cardInputModel.reset();
		
		for (CardToggleButton button : cardToggleButtons) {
			button.setSelected(false);
			button.setDisable(false);
		}
	}
    
    private void createToggleButtons() {
    	int columnIndex = 0;
    	int rowIndex = 0;
    	
    	Card[] cards = Card.values();
    	Arrays.sort(cards, new ToggleButtonCardComparator());
    	
    	for (Card card : cards) {
    		CardToggleButton toggleButton = new CardToggleButton(card.toString());
    		toggleButton.setUserData(card);
    		toggleButton.setFocusTraversable(false);
			
			toggleButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					buttonToggled(event);
				}
				
			});
			
			cardToggleButtons.add(toggleButton);
			
    		view.getButtonGridPane().add(toggleButton, columnIndex, rowIndex);
    		
    		columnIndex++;
    		if (columnIndex == Suit.values().length) {
    			rowIndex++;
    			columnIndex = 0;
    		}
    	}
    }
    
    private void disableButtons() {
		for (CardToggleButton button : cardToggleButtons) {
			if (!button.isSelected()) {
				button.setDisable(true);
			}
		}
	}
	
	private void enableButtons() {
		for (CardToggleButton button : cardToggleButtons) {
			button.setDisable(false);
		}
	}
	
	private void fireInvalidationEvent() {
		for (CardInputListener listener : listeners) {
			listener.invalidated();
		}
	}
	
}
