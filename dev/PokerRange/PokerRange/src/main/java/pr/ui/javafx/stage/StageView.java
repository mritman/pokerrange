package pr.ui.javafx.stage;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.Callback;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.domain.poker.table.PokerTable;

public class StageView extends AnchorPane {

	@FXML private HBox top;
	@FXML private HBox mid;
	
	@FXML 
	@Getter
	private ComboBox<PokerTable> tableComboBox;
	
	@FXML
	@Getter
	private Button selectSeatButton;
	
	@FXML
	@Getter
	private ToggleButton showCardInputButton;
	
	@FXML
	@Getter
	private Button preferencesButton;
	
	private final StageModel model;

	public StageView(StageModel model) {
		this.model = model;
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("StageView.fxml"));
		fxmlLoader.setController(this);
		fxmlLoader.setRoot(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new InitializationException(exception);
		}
		
		top.getChildren().add(model.getGameStatePresenter().getView());
		
		tableComboBox.valueProperty().bindBidirectional(model.selectedTableProperty());
		tableComboBox.itemsProperty().bind(model.tablesProperty());
		
		initCardInputButton();
		initTableComboBox();
		
		model.tableViewProperty().addListener(new ChangeListener<Node>() {

			@Override
			public void changed(ObservableValue<? extends Node> observable,	Node oldValue, Node newValue) {
				mid.getChildren().remove(oldValue);
				
				if (newValue != null) {
					mid.getChildren().add(newValue);
					HBox.setHgrow(newValue, Priority.ALWAYS);
				}
			}
			
		});
		
		model.setSelectedTable(model.getManualTable());
	}

	private void initCardInputButton() {
		showCardInputButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
			
				@Override
				public void changed(
						ObservableValue<? extends Boolean> observable,
						Boolean oldValue, Boolean newValue) {

					if (newValue.booleanValue()) {
						showCardInputView();
					} else {
						hideCardInputView();
					}

				}

			});
	}
	
	private void hideCardInputView() {
		mid.getChildren().remove(model.getCardInputPresenter().getView());
		model.getStage().setScene(null);
		model.getStage().setScene(model.getScene());
	}

	private void showCardInputView() {
		mid.getChildren().add(0, model.getCardInputPresenter().getView());
		model.getStage().setScene(null);
		model.getStage().setScene(model.getScene());
	}
	
	private void initTableComboBox() {
		Callback<ListView<PokerTable>, ListCell<PokerTable>> cellFactory = new Callback<ListView<PokerTable>, ListCell<PokerTable>>() {
			
			@Override
			public ListCell<PokerTable> call(ListView<PokerTable> param) {
				
				return new ListCell<PokerTable>() {
					
					@Override
					protected void updateItem(PokerTable item, boolean empty) {
						super.updateItem(item, empty);
						
						if (!empty) {
							setText(item.getName());
						}
					}

				};
			}

		};

		tableComboBox.setButtonCell(cellFactory.call(null));
		tableComboBox.setCellFactory(cellFactory);
	}
	
}
