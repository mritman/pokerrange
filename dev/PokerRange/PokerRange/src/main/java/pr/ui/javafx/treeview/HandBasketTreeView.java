package pr.ui.javafx.treeview;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

public class HandBasketTreeView extends TreeView<Node> {

	private static final int PREF_WIDTH = 800;

	public HandBasketTreeView() {
		setPrefWidth(PREF_WIDTH);
		
		setShowRoot(false);
		
		TreeItem<Node> rootItem = new TreeItem<Node>(new Label("Root node"));
		
		setRoot(rootItem);
		
		this.getStylesheets().add(getClass().getResource("HandBasketTreeView.css").toExternalForm());
		
		setFocusTraversable(false);
	}
	
}
