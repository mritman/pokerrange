package pr.ui.javafx.preferences;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.application.preferences.Preferences;
import pr.application.preferences.Preferences.PreferenceName;
import pr.ui.javafx.util.IntegerTextField;
import pr.util.Optional;

public class PreferencesPresenter {

	@Getter	private VBox view;
	
	@FXML private Button saveButton;
	
	@FXML private TextField databaseUrlTextField;
	@FXML private TextField databaseUserTextField;
	@FXML private TextField databasePasswordField;
	
	@FXML private TextField pokerTrackerDatabaseUrlTextField;
	@FXML private TextField pokerTrackerDatabaseUserTextField;
	@FXML private TextField pokerTrackerDatabasePasswordField;
	@FXML private IntegerTextField pokerTrackerMinimumRequiredHandsTextField;

	@FXML private ChoiceBox<Integer> pokerStarsPreferredSeatSize6;
	@FXML private ChoiceBox<Integer> pokerStarsPreferredSeatSize10;
	
	@FXML
	private TextField pokerStarsAccountName;
	
	@FXML
	private TextField pokerStarsHandHistoryDirectory;
	
	@FXML
	private Button selectPokerStarsHandHistoryDirectoryButton;
	
	private final Preferences preferences;
	
	public PreferencesPresenter(Preferences preferences) {
		this.preferences = preferences;
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PreferencesView.fxml"));
        fxmlLoader.setController(this);
                
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
        view = fxmlLoader.getRoot();
        
        listenToSaveButton();
        listenToPokerStarsHandHistoryDirectorySelectButton();
        
        pokerStarsPreferredSeatSize6.getItems().setAll(null, 1, 2, 3, 4, 5, 6);
        pokerStarsPreferredSeatSize10.getItems().setAll(null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        databaseUrlTextField.setText(preferences.get(PreferenceName.DATABASE_URL));
        databaseUserTextField.setText(preferences.get(PreferenceName.DATABASE_USER));
        databasePasswordField.setText(preferences.get(PreferenceName.DATABASE_PASSWORD));  
        
        pokerTrackerDatabaseUrlTextField.setText(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_URL));
        pokerTrackerDatabaseUserTextField.setText(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_USER));
    	pokerTrackerDatabasePasswordField.setText(preferences.get(PreferenceName.POKER_TRACKER_DATABASE_PASSWORD));
    	pokerTrackerMinimumRequiredHandsTextField.setText(preferences.get(PreferenceName.MINIMUM_HANDS_REQUIRED_FOR_RANGES));
        
        pokerStarsAccountName.setText(preferences.get(PreferenceName.POKERSTARS_ACCOUNT_NAME));
        pokerStarsHandHistoryDirectory.setText(preferences.get(PreferenceName.POKERSTARS_HAND_HISTORY_DIRECTORY));
        
        pokerStarsPreferredSeatSize6.setValue(preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_6).orElse(null));
        pokerStarsPreferredSeatSize10.setValue(preferences.getInteger(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_10).orElse(null));
	}

	private void listenToPokerStarsHandHistoryDirectorySelectButton() {
		selectPokerStarsHandHistoryDirectoryButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser directoryChooser = new DirectoryChooser();
				directoryChooser.setTitle("Please select the folder where the PokerStars hand history is saved...");
				File file = directoryChooser.showDialog(selectPokerStarsHandHistoryDirectoryButton.getScene().getWindow());
				
				if (file != null) {
					pokerStarsHandHistoryDirectory.setText(file.toString());
				}
			}
        	
		});
	}

	private void listenToSaveButton() {
		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				preferences.set(PreferenceName.DATABASE_URL, databaseUrlTextField.getText());
				preferences.set(PreferenceName.DATABASE_USER, databaseUserTextField.getText());
				preferences.set(PreferenceName.DATABASE_PASSWORD, databasePasswordField.getText());
				
				preferences.set(PreferenceName.POKER_TRACKER_DATABASE_URL, pokerTrackerDatabaseUrlTextField.getText());
				preferences.set(PreferenceName.POKER_TRACKER_DATABASE_USER, pokerTrackerDatabaseUserTextField.getText());
				preferences.set(PreferenceName.POKER_TRACKER_DATABASE_PASSWORD, pokerTrackerDatabasePasswordField.getText());
				preferences.set(PreferenceName.MINIMUM_HANDS_REQUIRED_FOR_RANGES, pokerTrackerMinimumRequiredHandsTextField.getValue());
				
				preferences.set(PreferenceName.POKERSTARS_ACCOUNT_NAME, pokerStarsAccountName.getText());
				preferences.set(PreferenceName.POKERSTARS_HAND_HISTORY_DIRECTORY, pokerStarsHandHistoryDirectory.getText());
				
				Optional<Integer> pokerStarsSize6 = Optional.ofNullable(pokerStarsPreferredSeatSize6.getValue());
				Optional<Integer> pokerStarsSize10 = Optional.ofNullable(pokerStarsPreferredSeatSize10.getValue());
				preferences.set(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_6, pokerStarsSize6);
				preferences.set(PreferenceName.POKERSTARS_PREFERRED_SEAT_SIZE_10, pokerStarsSize10);
				
				Stage stage = (Stage) saveButton.getScene().getWindow();
				stage.close();
			}
			
		});
	}
	
}
