package pr.ui.javafx.table.opponent;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import lombok.Getter;
import pr.ui.javafx.treeview.HandBasketTreeView;

public class OpponentView extends HBox {

	@Getter private final Button expandAllButton = new Button("Expand all");
	@Getter private final Button collapseAllButton = new Button("Collapse all");
	
	protected OpponentView(OpponentModel model) {
        super(20);
        
        setPadding(new Insets(20));
        
        HBox handRangeView = model.getHandRangePresenter().getView();
        HandBasketTreeView handBasketTreeView = model.getHandBasketTreePresenter().getView();
        
        HBox hBox = new HBox(20);
        hBox.getChildren().add(handRangeView);
        hBox.getChildren().add(expandAllButton);
        hBox.getChildren().add(collapseAllButton);
        
        VBox vBox = new VBox(20);
        vBox.getChildren().add(hBox);
        vBox.getChildren().add(handBasketTreeView);
        
        VBox.setVgrow(handBasketTreeView, Priority.ALWAYS);
        
        getChildren().add(vBox);
        getChildren().add(model.getHandBasketMainPresenter().getView());
	}

}
