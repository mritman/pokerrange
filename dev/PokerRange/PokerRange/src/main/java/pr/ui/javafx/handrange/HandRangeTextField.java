package pr.ui.javafx.handrange;

import java.awt.Toolkit;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.IndexRange;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import pr.domain.cards.Rank;
import pr.domain.poker.range.typed.HandTypeRange;
import pr.domain.poker.range.typed.types.HandType;

public class HandRangeTextField extends TextField {

	private ReadOnlyObjectWrapper<HandType> handTypeProperty;
	private ReadOnlyObjectWrapper<HandTypeRange> handRangeProperty;
	
	public HandRangeTextField() {
		handTypeProperty = new ReadOnlyObjectWrapper<>();
		handRangeProperty = new ReadOnlyObjectWrapper<>();
		bindTextAndHandRangeProperty();
		
		// These listeners will update the handRange when an event occurs that
		// indicates a final value for the hand range has been entered.
		focusedProperty().addListener(createFocusListener());
		addEventHandler(ActionEvent.ACTION, createOnActionEventListener());
	}

	@Override
	public void replaceText(int start, int end, String text) {
    	
    	String newText = getNewText(start, end, text);
    	
        if (formatIsValid(newText)) {
            super.replaceText(start, end, text.toUpperCase());
        } else {
        	Toolkit.getDefaultToolkit().beep();
        }
    }
 
    @Override
    public void replaceSelection(String text) {
    	IndexRange selectionRange = getSelection();
    	
    	String newText = getNewText(selectionRange.getStart(), selectionRange.getEnd(), text);
    	
        if (formatIsValid(newText)) {
            super.replaceSelection(text.toUpperCase());
        } else {
        	Toolkit.getDefaultToolkit().beep();
        }
    }
    
    public ReadOnlyObjectProperty<HandType> handTypeProperty() {
    	return handTypeProperty.getReadOnlyProperty();
    }
    
    public HandType getHandType() {
    	return handTypeProperty.get();
    }
    
    public void setHandType(HandType handType) {
    	handTypeProperty.set(handType);
    	
    	HandTypeRange handRange = null;
    	if (handType != null && handType.isEnabled()) {
    		handRange = new HandTypeRange(handType.getLowestRank(), handType.getHighestRank());
    	} else {
    		handRange = null;
    	}
    		
    	setHandRange(handRange);
    }
    
    public ReadOnlyObjectProperty<HandTypeRange> handRangeProperty() {
    	return handRangeProperty.getReadOnlyProperty();
    }
    
    public HandTypeRange getHandRange() {
    	return handRangeProperty.get();
    }
    
    public void setHandRange(HandTypeRange handRange) {
    	handRangeProperty.set(handRange);
    }
    
	private void bindTextAndHandRangeProperty() {
		StringConverter<HandTypeRange> converter = new HandStringConverter();
		textProperty().bindBidirectional(handRangeProperty, converter);
	}

    private final ChangeListener<? super Boolean> createFocusListener() {
    	return new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				
				if (!newValue) {
					formatText();
				}
			}
    		
		};
	}

	private final EventHandler<ActionEvent> createOnActionEventListener() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				formatText();
				selectAll();
			}
			
		};
	}

	private void formatText() {
		// getRankChars is called here without checking if rankFormatIsValid()
		// would return true because it should be impossible for the content of
		// the text field to be invalid because of the checks being made in
		// replaceText() and replaceSelection().
		Character[] rankChars = getRankChars(getText());
		
		Rank[] ranks = getRanks(rankChars);
		ranks[0] = limitToRange(ranks[0]);
		ranks[1] = limitToRange(ranks[1]);
		
		StringBuilder text = new StringBuilder();
		
		if (ranks[0] != null) {
			text.append(ranks[0].strValue());
			
			
			if (ranks[1] != null) {
				text.append("-" + ranks[1].strValue());
			} else {
				text.append("-" + ranks[0].strValue());
			}
		}

		setText(text.toString());
	}
    
    private String getNewText(int start, int end, String newText) {
    	StringBuilder currentText = new StringBuilder(getText());
    	
    	return currentText.replace(start, end, newText).toString();
    }
	
	private boolean formatIsValid(String text) {
		boolean formatValid = false;
		
		if (stringFormatIsValid(text)) {
			Character[] ranks = getRankChars(text);
			
			if (rankFormatIsValid(ranks[0], ranks[1])) {
				formatValid = true;
			}
		}
		
		return formatValid;
	}
	
	private boolean stringFormatIsValid(String text) {
		boolean formatValid = false;

		if (text == null || text.equals("")) {
			formatValid = true;
		} else if (text.matches("^[2-9tjkqaTJKQA]-{0,1}[2-9tjkqaTJKQA]{0,1}$")) {
			formatValid = true;
		}

		return formatValid;
	}
	
	private boolean rankFormatIsValid(Character lower, Character upper) {
		boolean valid = true;
		
		if (lower != null && !Rank.isRank(lower)) {
			valid = false;
		}
		
		if (upper != null && !Rank.isRank(upper)) {
			valid = false;
		}
		
		if (valid
				&& lower != null
				&& upper != null
				&& Rank.parseRank(lower).intValue() > Rank.parseRank(upper)
						.intValue()) {

			valid = false;
		}
		
		return valid;
	}

	private boolean hasDash(String text) {
		return text.indexOf('-') != -1;
	}
	
	private Rank limitToRange(Rank rank) {
		Rank newRank = null;
		
		if (rank == null) {
			newRank = null;
		} else if (getHandType() == null) {
			newRank = rank;
		} else if (rank.intValue() < getHandType().getDefaultLowestRank().intValue()) {
			newRank = getHandType().getDefaultLowestRank();
		} else if (rank.intValue() > getHandType().getDefaultHighestRank().intValue()) {
			newRank = getHandType().getDefaultHighestRank();
		} else {
			newRank = rank;
		}
		
		return newRank;
	}
	
	/**
	 * The {@code text} that is passed to this method is expected to be valid,
	 * in other words {@link #stringFormatIsValid(String)} returns true on it.
	 * 
	 * @param text
	 *            the string representation of a range of ranks
	 * @return an array that contains the Character representation of the lower and upper rank, their value can also be null
	 */
	private Character[] getRankChars(String text) {
		Character[] ranks = new Character[2];
		
		if (text == null || text.equals("")) {
			return ranks;
		}
		
		ranks[0] = text.charAt(0);
		
		if (hasDash(text) && text.length() == 3) {
			ranks[1] = text.charAt(2);
		} else if (!hasDash(text) && text.length() == 2) {
			ranks[1] = text.charAt(1);
		}
		
		return ranks;
	}
	
	private Rank[] getRanks(Character[] chars) {
		Rank[] ranks = new Rank[2];
		
		if (chars[0] != null && Rank.isRank(chars[0])) {
			ranks[0] = Rank.parseRank(chars[0]);
		}
		
		if (chars[1] != null && Rank.isRank(chars[1])) {
			ranks[1] = Rank.parseRank(chars[1]);
		}
		
		return ranks;
	}
	
	private Rank[] getRanks(String string) {
		if (!formatIsValid(string)) {
			throw new IllegalArgumentException("Can not convert the specified argument to ranks.");
		}
		
		Character[] rankChars = getRankChars(string);
		return getRanks(rankChars);
	}
	
	private class HandStringConverter extends StringConverter<HandTypeRange> {

		@Override
		public HandTypeRange fromString(String string) {
			HandTypeRange handRange;
			
			if (string == null || string.isEmpty()) {
				handRange = null;
			} else if (formatIsValid(string) && getRanks(string)[0] != null && getRanks(string)[1] != null) {
				Rank[] ranks = getRanks(string);
				handRange = new HandTypeRange(ranks[0], ranks[1]);
			} else {
				handRange = handRangeProperty.get();
			}

			return handRange;
		}

		@Override
		public String toString(HandTypeRange handRange) {
			if (handRange == null || handRange.getLower() == null || handRange.getUpper() == null) {
				return "";
			}
			
			return handRange.getLower().strValue() + "-" + handRange.getUpper().strValue();
		}
		
	}

}
