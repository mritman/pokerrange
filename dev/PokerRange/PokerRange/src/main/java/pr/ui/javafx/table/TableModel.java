package pr.ui.javafx.table;

import java.util.LinkedList;

import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import pr.domain.poker.table.PokerTable;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class TableModel {

	@Getter
	private final PokerTable table;
	
	private ObservableList<OpponentTab> opponentTabs;
	
	public TableModel(PokerTable table) {
		this.table = table;
		opponentTabs = FXCollections.observableList(new LinkedList<OpponentTab>());
	}
	
	public ReadOnlyListWrapper<OpponentTab> getOpponentTabs() {
		return new ReadOnlyListWrapper<>(opponentTabs);
	}

	public OpponentTab getOpponentTabByOpponentName(String opponentName) {
		return Iterables.find(opponentTabs, new FindByNamePredicate(opponentName));
	}
	
	public void addOpponentTab(OpponentTab opponentTab) {
		opponentTabs.add(opponentTab);
		sortOpponentTabs();
	}
	
	public void removeOpponentTab(String opponentName) {
		OpponentTab opponentTab = getOpponentTabByOpponentName(opponentName);
		opponentTabs.remove(opponentTab);
	}
	
	public void sortOpponentTabs() {
		FXCollections.sort(opponentTabs, OpponentTab.SORT_BY_SEAT);
	}
	
	private static class FindByNamePredicate implements Predicate<OpponentTab>{
		
		private final String name;
		
		public FindByNamePredicate(String name) {
			this.name = name;
		}
		
		@Override
		public boolean apply(OpponentTab opponentTab) {
			return opponentTab.getOpponent().getName().equals(name);
		}
	
	};

}
