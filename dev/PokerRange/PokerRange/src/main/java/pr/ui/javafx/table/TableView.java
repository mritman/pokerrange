package pr.ui.javafx.table;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import lombok.Getter;
import pr.application.exception.InitializationException;

public class TableView extends AnchorPane {
	
	@FXML
	@Getter
	private AnchorPane view;
	
	@FXML
	@Getter
	private TabPane tabPane;
	
	public TableView() {
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TableView.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
	}
}
