package pr.ui.javafx.stage;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.application.preferences.Preferences;
import pr.domain.poker.oldevent.PokerTableUpdate;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.table.PokerTable;
import pr.infrastructure.tableobserver.service.TableObserverService;
import pr.ui.javafx.cardinput.CardInputPresenter.CardInputListener;
import pr.ui.javafx.seatselection.SeatSelectionPane;
import pr.ui.javafx.util.StageUtil;

public class StagePresenter {
	
	private static final Logger logger = LoggerFactory.getLogger(StagePresenter.class);

	private StageModel model;
	private StageView view;
	
	private TableObserverService tableObserverService;

	private ChangeListener<PokerTable> selectedTableListener;
	
	@Inject
	public StagePresenter(Stage stage, TableObserverService tableObserverService, Preferences preferences, OpponentAccountRepository opponentAccountRepository) {
		this.tableObserverService = tableObserverService;
		initTableObserverServiceListener();

		// Temporarily added at request of user. Remove when the regular window size has been improved.
		stage.setHeight(250.0);
		stage.setWidth(475.0);
		
		model = new StageModel(stage, "PR", preferences, opponentAccountRepository);
		view = new StageView(model);
		model.setRoot(view);
		
		createSelectedTableListener();
		listenToTableAndSeatSelectionButton();
		listenToPreferencesButton();

		model.selectedTableProperty().addListener(selectedTableListener);
		
		listenToManualCardInput();
	}

	public void showStage() {
		model.showStage();
	}

	private void initTableObserverServiceListener() {
//		tableObserverService.addTableListener(new TableListener() {
//			
//			@Override
//			public void tablesUpdated(final Set<PokerTable> tables) {
//				Platform.runLater(new Runnable() {
//
//					@Override
//					public void run() {
//						StagePresenter.this.tablesUpdated(tables);
//					}
//					
//				});
//				
//			}
//			
//			@Override
//			public void tableStateChanged(final PokerTableUpdate tableUpdate) {
//				Platform.runLater(new Runnable() {
//					
//					@Override
//					public void run() {
//						StagePresenter.this.tableUpdated(tableUpdate);
//					}
//					
//				});
//			}
//		});
	}
	
	private void createSelectedTableListener() {
		 selectedTableListener = new ChangeListener<PokerTable>() {

			@Override
			public void changed(
					ObservableValue<? extends PokerTable> observable,
					PokerTable oldTable, PokerTable newTable) {

				if (newTable != null) {
					logger.info("{} selected.", newTable.getName());
					if (newTable == model.getManualTable()) {
						tableObserverService.selectTable(null);
					} else {
						tableObserverService.selectTable(newTable.getKey());
					}
					
					model.getCardInputPresenter().manualInputProperty().set(newTable.isManualInput());
					model.getCardInputPresenter().setGameState(newTable.getGameState());
					model.setSelectedTable(newTable);
					model.getGameStatePresenter().update(newTable.getGameState());
				} else {
					logger.info("Selection cleared.");
				}
				
			}
		};
	}
	
	private void listenToTableAndSeatSelectionButton() {
		final SeatSelectionPane seatSelectionPane = new SeatSelectionPane();
		
		view.getSelectSeatButton().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				seatSelectionPane.show(model.getStage(), model.getSelectedTable());
			}

		});
	}
	
	private final void listenToManualCardInput() {
		model.getCardInputPresenter().manualInputProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				boolean manualInput = model.getCardInputPresenter().manualInputProperty().get();
				model.getSelectedTable().setManualInput(manualInput);
			}

		});
		
		model.getCardInputPresenter().addListener(new CardInputListener() {
			
			@Override
			public void invalidated() {
				model.getSelectedTable().setGameState(model.getCardInputPresenter().getGameState());
				model.getGameStatePresenter().update(model.getSelectedTable().getGameState());
				model.getTablePresenter().update(null);
			}

		});
	}

	private void listenToPreferencesButton() {
		view.getPreferencesButton().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				StageUtil.show(model.getPreferencesPresenter().getView(), view.getPreferencesButton().getScene().getWindow());
			}
			
		});
	}
	
	private void tablesUpdated(Set<PokerTable> tables) {
		ObservableList<PokerTable> items = model.getTables();
		int sizeBefore = items.size();
		
		PokerTable valueBefore = model.getSelectedTable();
		
		model.selectedTableProperty().removeListener(selectedTableListener);
		
		HashSet<Object> tableSet = new HashSet<>();
		tableSet.addAll(tables);
		tableSet.add(model.getManualTable());
		
		items.retainAll(tableSet);
		
		for (PokerTable table : tables) {
			if (!items.contains(table)) {
				items.add(table);
			}
		}
		
		sortTables(items);

		if (items.contains(valueBefore) && model.getSelectedTable() != valueBefore) {
			// IF the value has changed when it shouldn't, reset the selection
			// to its original value and only afterwards re-add the listener.
			model.setSelectedTable(valueBefore);
			model.selectedTableProperty().addListener(selectedTableListener);
		} else if (!items.contains(valueBefore) && !items.isEmpty()) {
			// If the selected value has changed because it has been removed
			// from the selectable items re-add the listener and then select a
			// new item.
			model.setSelectedTable(null);
			model.selectedTableProperty().addListener(selectedTableListener);
			PokerTable pokerTable = items.get(0);
			model.setSelectedTable(pokerTable);
		} else {
			model.selectedTableProperty().addListener(selectedTableListener);
		}

		// If the only table before the update was the manual table, select
		// the newly added table automatically.
		if (sizeBefore == 1 && items.size() > 1) {
			int manualTableIndex = items.indexOf(model.getManualTable());
			int selectIndex = manualTableIndex == 0 ? 1 : 0;
			
			model.setSelectedTable(items.get(selectIndex));
		}
	}

	private void sortTables(ObservableList<PokerTable> items) {
		FXCollections.sort(items, new Comparator<PokerTable>() {
			@Override
			public int compare(PokerTable o1, PokerTable o2) {
				if (StageModel.MANUAL_TABLE_NAME.equals(o1.getName())) {
					return 1;
				} else if (StageModel.MANUAL_TABLE_NAME.equals(o2.getName())) {
					return -1;
				}
				
				return o1.getName().compareTo(o2.getName());
			}
		});
	}
	
	private void tableUpdated(PokerTableUpdate tableUpdate) {
		logger.debug(tableUpdate.toString());
		
		logger.info("Table updated: {}", tableUpdate.getTable().getGameState().toString());
		
		model.getCardInputPresenter().setGameState(tableUpdate.getTable().getGameState());
		model.getGameStatePresenter().update(tableUpdate.getTable().getGameState());
		model.getTablePresenter().update(tableUpdate);
	}

}
