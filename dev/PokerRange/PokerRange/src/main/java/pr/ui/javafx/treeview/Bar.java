package pr.ui.javafx.treeview;

import java.text.DecimalFormat;
import java.text.Format;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Bar extends HBox {

	private static final int DEFAULT_WIDTH = 300;

	private static final int DEFAULT_HEIGHT = 15;
	
	private static final int MAX_WIDTH = 500;

	private static final int SPACING = 5;

	private final DoubleProperty valueProperty;
	
	private final DoubleProperty maxValueProperty;
	
	private final Rectangle valueBar;
	
	private Label valueLabel;
	
	private Label label;
	
	private Format valueLabelFormat = new DecimalFormat("#.#");
	
	public Bar(double value, double maxValue, Format valueFormat, String label) {
		this(DEFAULT_WIDTH, DEFAULT_HEIGHT, value, maxValue, valueFormat, label);
	}
	
	public Bar(double width, double height, double value, double maxValue, Format valueFormat, String title) {
		super(SPACING);
		
		if (valueFormat != null) {
			this.valueLabelFormat = valueFormat;
		}
		
		valueProperty = new SimpleDoubleProperty();
		maxValueProperty = new SimpleDoubleProperty(maxValue);
		label = new Label(title);
		valueLabel = new Label();
		valueBar = new Rectangle();
		
		setMaxHeight(height);
		
		initLabel();
		initValueLabel();
		initBar();
		
		getChildren().add(label);
		getChildren().add(valueLabel);
		getChildren().add(valueBar);
		
		maxValueProperty.set(maxValue);
		valueProperty.set(value);
		
		setPrefHeight(height);
		setPrefWidth(width);
	}

	private final DoubleBinding initBar() {
		DoubleBinding widthBinding = new DoubleBinding() {

			{
				super.bind(valueProperty, maxValueProperty, widthProperty());
			}
			
			@Override
			protected double computeValue() {
				double percentage = valueProperty.get() / maxValueProperty().get();
				
				return percentage * Math.min(widthProperty().get(), MAX_WIDTH);
			}
			
		};
		
		valueBar.widthProperty().bind(widthBinding);
		valueBar.heightProperty().bind(this.heightProperty());
		valueBar.setFill(Color.LIGHTBLUE);
		return widthBinding;
	}

	private final void initValueLabel() {
		StringBinding labelBinding = new StringBinding() {
			
			{
				super.bind(valueProperty);
			}
			
			@Override
			protected String computeValue() {
				return valueLabelFormat.format(valueProperty.get());
			}
		};
		
		valueLabel.textProperty().bind(labelBinding);
		valueLabel.prefHeightProperty().bind(this.heightProperty());
		
		// TODO clean up static sizing code
		valueLabel.setPrefWidth(60);
		valueLabel.setMinWidth(USE_PREF_SIZE);
		valueLabel.setAlignment(Pos.CENTER_RIGHT);
	}

	private void initLabel() {
		label.prefHeightProperty().bind(this.heightProperty());
		label.setMinWidth(USE_PREF_SIZE);
		
		// TODO clean up static sizing code
		label.setPrefWidth(100);
	}
	
	public double getValue() {
		return valueProperty.get();
	}
	
	public void setValue(double value) {
		valueProperty.set(value);
	}
	
	public DoubleProperty valueProperty() {
		return valueProperty;
	}
	
	public double getMaxValue() {
		return maxValueProperty.get();
	}
	
	public void setMaxValue(double maxValue) {
		maxValueProperty.set(maxValue);
	}
	
	public DoubleProperty maxValueProperty() {
		return maxValueProperty;
	}
	
	public void setColor(Color color) {
		valueBar.setFill(color);
	}
	
	public Label getLabel() {
		return label;
	}
	
}