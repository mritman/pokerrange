package pr.ui.javafx.handbasket.basketelement;

import javafx.beans.property.ReadOnlyObjectWrapper;
import pr.domain.handbasket.handcollection.HandCollection;
import pr.ui.javafx.handbasket.model.HandBasketModel;

public class HandCollectionModel extends BasketElementModel {

	private ReadOnlyObjectWrapper<HandCollection> handCollectionProperty;
	
	public HandCollectionModel(HandCollection handCollection, HandBasketModel handBasketModel) {
		super(handCollection, handBasketModel);
		
		handCollectionProperty = new ReadOnlyObjectWrapper<>(handCollection);
	}
	
	public HandCollection getHandCollection() {
		return handCollectionProperty.get();
	}

}
