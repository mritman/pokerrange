package pr.ui.javafx.stage;

import java.util.ArrayList;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ListProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Getter;
import pr.application.preferences.Preferences;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.PokerTableFactory;
import pr.domain.poker.table.Seat;
import pr.ui.javafx.cardinput.CardInputPresenter;
import pr.ui.javafx.gamestate.GameStatePresenter;
import pr.ui.javafx.preferences.PreferencesPresenter;
import pr.ui.javafx.table.TablePresenter;

public class StageModel {

	private static final String DEFAULT_OPPONENT_NAME = "Opponent 1";

	static final String MANUAL_TABLE_NAME = "Manual table";

	@Getter
	private PokerTable manualTable;
	
	private ListProperty<PokerTable> tablesProperty;
	private ObjectProperty<PokerTable> selectedTableProperty;
	private MapProperty<PokerTable, TablePresenter> tablePresentersProperty;
	private ObjectProperty<Node> tableViewProperty;
	
	private final Stage stage;
	private Scene scene;
	
	private final CardInputPresenter cardInputPresenter;
	private final GameStatePresenter gameStatePresenter;
	private final PreferencesPresenter preferencesPresenter;
	
	public StageModel(Stage stage, String title, Preferences preferences, final OpponentAccountRepository opponentAccountRepository) {
		this.stage = stage;
		stage.setTitle(title);
		
		cardInputPresenter = new CardInputPresenter();
		gameStatePresenter = new GameStatePresenter();
		preferencesPresenter = new PreferencesPresenter(preferences);
		
		tablesProperty = new SimpleListProperty<>(FXCollections.observableList(new ArrayList<PokerTable>()));
		selectedTableProperty = new SimpleObjectProperty<>();
		tablePresentersProperty = new SimpleMapProperty<>(FXCollections.<PokerTable, TablePresenter>observableHashMap());
		tableViewProperty = new SimpleObjectProperty<>();
		
		tablesProperty.addListener(new ListChangeListener<PokerTable>() {

			@Override
			public void onChanged(ListChangeListener.Change<? extends PokerTable> change) {

				while(change.next()) {
					for (PokerTable newTable : change.getAddedSubList()) {
						tablePresentersProperty.put(newTable, new TablePresenter(newTable, opponentAccountRepository));
					}
	
					for (PokerTable removedTable : change.getRemoved()) {
						tablePresentersProperty.remove(removedTable);
						if (getSelectedTable() == removedTable) {
							setSelectedTable(null);
						}
					}
				}
			}
			
		});
		
		selectedTableProperty.addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				PokerTable table = selectedTableProperty.get();
				
				if (table != null) {
					Node view = tablePresentersProperty.get(table).getView();
					tableViewProperty.set(view);
				}
			}

		});
		
		manualTable = PokerTableFactory.createTable(MANUAL_TABLE_NAME, PokerVenue.MANUAL);
		opponentAccountRepository.getOpponent(DEFAULT_OPPONENT_NAME, manualTable, Seat.SEAT1);
		getTables().add(manualTable);
	}
	
	/**
	 * @return the {@link TablePresenter} of which the view is currently visible.
	 */
	protected TablePresenter getTablePresenter() {
		return tablePresentersProperty.get(getSelectedTable());
	}
	
	protected ObservableList<PokerTable> getTables() {
		return tablesProperty.get();
	}
	
	protected ListProperty<PokerTable> tablesProperty() {
		return tablesProperty;
	}
	
	protected PokerTable getSelectedTable() {
		return selectedTableProperty.get();
	}
	
	protected void setSelectedTable(PokerTable table) {
		selectedTableProperty.set(table);
	}
	
	protected ObjectProperty<PokerTable> selectedTableProperty() {
		return selectedTableProperty;
	}
	
	protected ObjectProperty<Node> tableViewProperty() {
		return tableViewProperty;
	}
	
	protected void showStage() {
		if (!stage.isShowing()) {
			stage.show();
		}
	}
	
	protected void setRoot(Parent root) {
		scene = new Scene(root);
		scene.getStylesheets().add("/pr/ui/javafx/style.css");
		stage.setScene(scene);
	}
	
	protected CardInputPresenter getCardInputPresenter() {
		return cardInputPresenter;
	}
	
	protected GameStatePresenter getGameStatePresenter() {
		return gameStatePresenter;
	}
	
	protected PreferencesPresenter getPreferencesPresenter() {
		return preferencesPresenter;
	}
	
	protected Stage getStage() {
		return stage;
	}
	
	protected Scene getScene() {
		return scene;
	}
}
