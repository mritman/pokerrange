package pr.ui.util;

import java.util.List;
import java.util.Set;
import java.util.Stack;

public class Selection<T extends Selectable> {

	private Stack<T> selection = new Stack<T>();
	
	public void setSelection(T element) {
		clearSelection();
		addSelection(element);
	}
	
	public void setLastSelection(T element) {
		if (!contains(element)) {
			addSelection(element);
		} else {
			selection.remove(element);
			selection.push(element);
		}
	}
	
	public void addSelection(T element) {
		if (!contains(element)) {
			selection.push(element);
			element.setSelected(true);
		}
	}
	
	public void addSelection(Set<T> elements) {
		for (T element : elements) {
			addSelection(element);
		}
	}
	
	public void addSelection(List<T> elements) {
		for (T element : elements) {
			addSelection(element);
		}
	}
	
	public void removeSelection(T element) {
		selection.remove(element);
		element.setSelected(false);
	}
	
	public void clearSelection() {
		for (T selectedElement : selection) {
			selectedElement.setSelected(false);
		}
		
		selection.clear();
	}
	
	public List<T> getSelection() {
		return selection.subList(0, selection.size());
	}

	public void toggleSelection(T element) {
		if (selection.contains(element)) {
			removeSelection(element);
		} else {
			addSelection(element);
		}
	}

	public boolean contains(T element) {
		return selection.contains(element);
	}
	
	public T getLastSelectedItem() {
		if (!selection.isEmpty()) {
			return selection.peek();
		} else {
			return null; 
		}
	}
	
	public boolean isMultipleSelection() {
		if (selection.size() > 1) {
			return true;
		}
		
		return false;
	}
	
	public boolean isEmpty() {
		return selection.isEmpty();
	}

}
