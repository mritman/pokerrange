package pr.ui.javafx.seatselection;

import java.util.LinkedList;
import java.util.List;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import lombok.Getter;
import pr.domain.poker.table.PokerTable;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;

public abstract class AbstractTablePane extends AnchorPane {
	static final Paint REGULAR_STROKE = new Color(0.2, 0.2, 0.2, 1.0);
	static final Paint SELECTED_STROKE = Color.ANTIQUEWHITE;

	private final String name;
	private final TableSize size;
	
	private final ReadOnlyObjectWrapper<PokerTable> tableProperty;
	private final ReadOnlyObjectWrapper<Seat> selectedSeatProperty;
	
	@Getter private final List<Circle> seats;
	
	protected AbstractTablePane(String name, TableSize size) {
		this.name = name;
		this.size = size;
		
		tableProperty = new ReadOnlyObjectWrapper<>();
        selectedSeatProperty = new ReadOnlyObjectWrapper<>();
		seats = new LinkedList<>();
	}
	
	public PokerTable getTable() {
		return tableProperty.get();
	}
	
	protected void setTable(PokerTable table) {
		table.setSize(size);
		tableProperty.set(table);
		select(seats.get(getTable().getPlayerSeat().toInt() - 1));
	}
	
	public ReadOnlyObjectProperty<PokerTable> tableProperty() {
		return tableProperty.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<Seat> selectedSeatProperty() {
		return selectedSeatProperty.getReadOnlyProperty();
	}
	
	@FXML
	void seatSelected(MouseEvent event) {
		Circle selectedSeat = (Circle) event.getSource();
		getTable().setPlayerSeat((Seat) selectedSeat.getUserData());
		selectedSeatProperty.set(getTable().getPlayerSeat());
		select(selectedSeat);
	}
	
	private void select(Circle selectedSeat) {
		for (Circle seat : seats) {
			seat.setStroke(REGULAR_STROKE);
		}
		
		selectedSeat.setStroke(SELECTED_STROKE);
	}

	@Override
	public String toString() {
		return name;
	}
}
