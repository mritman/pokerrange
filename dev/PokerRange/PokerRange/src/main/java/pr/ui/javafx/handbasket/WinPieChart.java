package pr.ui.javafx.handbasket;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import pr.domain.gamestate.GameState;
import pr.domain.handbasket.HandBasketManager;
import pr.domain.poker.Combinations;
import pr.domain.poker.Street;
import pr.domain.poker.hand.comparator.HandComparator;
import pr.ui.javafx.util.MouseOverPieChart;

public class WinPieChart extends MouseOverPieChart {

	private ObservableList<PieChart.Data> data =
	           FXCollections.observableArrayList(           
	               new PieChart.Data("L", 0),
	               new PieChart.Data("D", 0),
	               new PieChart.Data("W", 0));
	
	private SimpleObjectProperty<HandBasketManager> handBasketManagerProperty;
	
	public WinPieChart() {
		super();
		getData().addAll(data);
		handBasketManagerProperty = new SimpleObjectProperty<>();
		InvalidationListener handBasketListener = new HandBasketListener();
		handBasketManagerProperty.addListener(handBasketListener);
		
		String path = getClass().getResource("RedYellowBlueChartStyle.css").toExternalForm();
		getStylesheets().add(path);
	}
	
	public SimpleObjectProperty<HandBasketManager> handBasketManagerProperty() {
		return handBasketManagerProperty;
	}
	
	private class HandBasketListener implements InvalidationListener {
		
		@Override
		public void invalidated(Observable observable) {
			HandBasketManager handBasketManager = handBasketManagerProperty.get();
			
			if (handBasketManager == null) return;
			
			GameState gameState = handBasketManager.getGameState();
			
			if (gameState.getStreet() == Street.PREFLOP || gameState.getHoleCards() == null) {
		    	data.get(0).pieValueProperty().set(0);
		    	data.get(1).pieValueProperty().set(0);
		    	data.get(2).pieValueProperty().set(0);
		    	return;
			}
			
	    	int handsLost = 0;
	    	int handsSplit = 0;
	    	int handsWon = 0;
	    	
	    	for (Combinations hand : handBasketManager.getHoleCards()) {
	    		if (hand.getHoleCards().isFolded()) {
	    			continue;
	    		}
	    		
	    		Combinations holeCardCombinations = gameState.getHoleCardCombinations().orElse(null);
	    		int cmp = HandComparator.compareByNonDrawHand(holeCardCombinations, hand);
	    		if (cmp > 0) {
	    			handsWon++;
	    		} else if (cmp == 0) {
	    			handsSplit++;
	    		} else {
	    			handsLost++;
	    		}
	    	}
			
	    	data.get(0).pieValueProperty().set(handsLost);
	    	data.get(1).pieValueProperty().set(handsSplit);
	    	data.get(2).pieValueProperty().set(handsWon);
		}
		
	}
	
}