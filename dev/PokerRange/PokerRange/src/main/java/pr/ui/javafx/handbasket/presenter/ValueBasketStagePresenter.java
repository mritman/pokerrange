package pr.ui.javafx.handbasket.presenter;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.domain.handbasket.HandBasketManager;
import pr.domain.handbasket.handvaluebaskets.ValueBasketEnum;
import pr.domain.handbasket.handvaluebaskets.ValueBasketFactory;
import pr.ui.javafx.handbasket.model.HandBasketStageModel;

public class ValueBasketStagePresenter {
	
	@Getter
	private HandBasketStageModel model;
	
	@Getter
	@FXML
	private GridPane view;
	
	private Label boardStatusLabel;
	
	public ValueBasketStagePresenter() {
		HandBasketManager handBasketManager = new HandBasketManager(new ValueBasketFactory(), ValueBasketEnum.WILL_FOLD);
        model = new HandBasketStageModel(handBasketManager);
		
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HandBasketStageView.fxml"));
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
	}

	@FXML
	protected void initialize() {
        boardStatusLabel = new Label();
        boardStatusLabel.setTextFill(Color.RED);
        boardStatusLabel.setFont(new Font("Cambria", 20));
        boardStatusLabel.setWrapText(true);
        view.add(boardStatusLabel, 1, 2);
        
        boardStatusLabel.textProperty().bind(model.boardStatusProperty());
        
        view.add(model.getHandBasketPresenters().get(ValueBasketEnum.WEAK_PAIR).getView(), 0, 0);
        view.add(model.getHandBasketPresenters().get(ValueBasketEnum.PAIR).getView(), 0, 1);
        view.add(model.getHandBasketPresenters().get(ValueBasketEnum.TWO_PAIR_PLUS).getView(), 0, 2);
        view.add(model.getHandBasketPresenters().get(ValueBasketEnum.DRAW).getView(), 1, 0);
        view.add(model.getHandBasketPresenters().get(ValueBasketEnum.WILL_FOLD).getView(), 1, 1);
        
        model.getSelectionAndDragHandler().register(view);
	}

}
