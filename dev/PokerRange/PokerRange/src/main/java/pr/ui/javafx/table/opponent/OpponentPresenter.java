package pr.ui.javafx.table.opponent;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import lombok.Getter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.gamestate.GameState;
import pr.domain.handbasket.HandBasketManager;
import pr.domain.poker.hand.HandManager;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.range.HandRange;
import pr.ui.javafx.handrange.HandRangePresenter;

public class OpponentPresenter {

	private static final Logger logger = LoggerFactory.getLogger(OpponentPresenter.class);
	
	@Getter
	private OpponentModel model;
	
	@Getter
	private OpponentView view;
	
	private HandManager handManager;
	private HandBasketManager valueBasketManager;
	private HandBasketManager strengthBasketManager;
	
	public OpponentPresenter(Opponent opponent, OpponentAccountRepository opponentAccountRepository) {
		model = new OpponentModel(opponent, opponentAccountRepository);
		view = new OpponentView(model);
		
		handManager = new HandManager();
		
		valueBasketManager = model.getHandBasketMainPresenter().getModel().getValueBasketStageModel().getHandBasketManager();
		strengthBasketManager = model.getHandBasketMainPresenter().getModel().getStrengthBasketStageModel().getHandBasketManager();
		
		model.getHandRangePresenter().addListener(new HandRangePresenter.RangeInvalidationListener() {
			
			@Override
			public void invalidated(HandRangePresenter handRangePresenter) {
				update();
			}

		});
		
		listenToExpandAllButton();
		listenToCollapseAllButton();
	}

	public void update() {
		logger.debug("Updating opponent view: {}", model.getOpponent().getName());
		
		GameState gameState = model.getOpponent().getTable().getGameState();
		HandRange handRange = model.getHandRangePresenter().getHandRange();

		handRange.calculateRange(gameState);
		model.getHandRangePresenter().update();
		
		handManager.calculateHits(handRange.getHandsInRange(), gameState.getBoardCards());
		
		valueBasketManager.update(handManager.getHoleCards(), gameState);
		strengthBasketManager.update(handManager.getHoleCards(), gameState);

		model.getHandBasketTreePresenter().update(valueBasketManager);
		model.getHandBasketMainPresenter().getModel().update(valueBasketManager, strengthBasketManager, gameState);
	}
	
	public void reset() {
		valueBasketManager.reset();
		model.getHandBasketMainPresenter().getModel().reset();
	}
	
	private void listenToExpandAllButton() {
		view.getExpandAllButton().setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				model.getHandBasketTreePresenter().expandAll();
			}
			
		});
	}
	
	private void listenToCollapseAllButton() {
		view.getCollapseAllButton().setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				model.getHandBasketTreePresenter().collapseAll();
			}
		});
	}
	
}
