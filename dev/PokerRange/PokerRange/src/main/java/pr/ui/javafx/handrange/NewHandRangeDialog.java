package pr.ui.javafx.handrange;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.Getter;
import pr.application.exception.InitializationException;

public class NewHandRangeDialog extends AnchorPane {

	@FXML private Label messageLabel;
	@FXML private TextField textField;
	@FXML private CheckBox globalRangeCheckBox;
	
	private Stage stage;
	private String result = null;

	public NewHandRangeDialog(String title, String message,	String defaultValue, Window owner) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NewHandRangeDialog.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new InitializationException(exception);
		}
		
		messageLabel.setText(message);
		textField.setText(defaultValue);
		Scene scene = new Scene(this);
		
		stage = new Stage();
		stage.setTitle(title);
		stage.setScene(scene);
		stage.setResizable(false);

		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(owner);
		stage.initStyle(StageStyle.UTILITY);
	}

	public NewHandRangeArguments show() {
		stage.showAndWait();
		
		if (result == null || "".equals(result)) {
			return null;
		}
		
		return new NewHandRangeArguments(result, globalRangeCheckBox.isSelected());
	}
	
	public void onOk() {
		result = textField.getText();
		stage.close();
	}
	
	public void onCancel() {
		stage.close();
	}
	
	public static final class NewHandRangeArguments {
		@Getter private final String name;
		@Getter private final boolean globalRange;
		
		public NewHandRangeArguments(String name, boolean globalRange) {
			this.name = name;
			this.globalRange = globalRange;
		}
	}
	
}
