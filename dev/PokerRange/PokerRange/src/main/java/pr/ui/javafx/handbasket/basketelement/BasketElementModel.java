package pr.ui.javafx.handbasket.basketelement;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import pr.domain.handbasket.BasketElement;
import pr.ui.javafx.handbasket.model.HandBasketModel;

public abstract class BasketElementModel {

	private ReadOnlyObjectWrapper<HandBasketModel> originalHandBasketModelProperty;
	private ReadOnlyObjectWrapper<HandBasketModel> currentHandBasketModelProperty;
	private ReadOnlyObjectWrapper<BasketElement> basketElementProperty;
	private ReadOnlyBooleanWrapper movedProperty;
	private ReadOnlyBooleanWrapper selectedProperty;
	
	
	public BasketElementModel(BasketElement basketElement, HandBasketModel handBasketModel) {
		originalHandBasketModelProperty = new ReadOnlyObjectWrapper<>(handBasketModel);
		currentHandBasketModelProperty = new ReadOnlyObjectWrapper<>(handBasketModel);
		basketElementProperty = new ReadOnlyObjectWrapper<>(basketElement);
		movedProperty = new ReadOnlyBooleanWrapper(false);
		selectedProperty = new ReadOnlyBooleanWrapper(false);
		
		BooleanBinding movedBinding = new BooleanBinding() {

			{
				super.bind(originalHandBasketModelProperty);
				super.bind(currentHandBasketModelProperty);
			}
			
			@Override
			protected boolean computeValue() {
				return getOriginalHandBasketModel() != getCurrentHandBasketModel();
			}
			
		};
		
		movedProperty.bind(movedBinding);
	}

	public HandBasketModel getOriginalHandBasketModel() {
		return originalHandBasketModelProperty.get();
	}
	
	public void setOriginalHandBasketModel(HandBasketModel handBasketModel) {
		originalHandBasketModelProperty.set(handBasketModel);
	}
	
	public ReadOnlyObjectProperty<HandBasketModel> originalHandBasketModelProperty() {
		return originalHandBasketModelProperty.getReadOnlyProperty();
	}
	
	public HandBasketModel getCurrentHandBasketModel() {
		return currentHandBasketModelProperty.get();
	}
	
	public void setCurrentHandBasketModel(HandBasketModel handBasketModel) {
		currentHandBasketModelProperty.set(handBasketModel);
	}
	
	public ReadOnlyObjectProperty<HandBasketModel> currentHandBasketModelProperty() {
		return currentHandBasketModelProperty.getReadOnlyProperty();
	}
	
	public BasketElement getBasketElement() {
		return basketElementProperty.get();
	}
	
	public void setBasketElement(BasketElement basketElement) {
		basketElementProperty.set(basketElement);
	}
	
	public ReadOnlyObjectProperty<BasketElement> basketElementProperty() {
		return basketElementProperty.getReadOnlyProperty();
	}
	
	public boolean isMoved() {
		return movedProperty.get();
	}
	
	public void setMoved(boolean isMoved) {
		movedProperty.set(isMoved);
	}
	
	public ReadOnlyBooleanProperty movedProperty() {
		return movedProperty.getReadOnlyProperty();
	}
	
	public boolean isSelected() {
		return selectedProperty.get();
	}
	
	public void setSelected(boolean selected) {
		selectedProperty.set(selected);
	}
	
	public ReadOnlyBooleanProperty selectedProperty() {
		return selectedProperty;
	}

}
