package pr.ui.javafx.handbasket.basketelement;

import javafx.scene.paint.Color;
import pr.domain.poker.hand.HandValue;

public class HandCollectionGroupView extends BasketElementView {

	public HandCollectionGroupView(HandCollectionGroupModel handCollectionGroupModel) {
		super(handCollectionGroupModel);
		setColor();
		setStyle(getRegularStyle());
		getButton().setTextFill(Color.WHITE);
		
		if (handCollectionGroupModel.getHandCollectionGroup().getHandValue() == HandValue.FLUSH) {
			getButton().setText("F");
		} else if (handCollectionGroupModel.getHandCollectionGroup().getHandValue() == HandValue.FLUSH_DRAW) {
			getButton().setText("FD");
		}
		
	}
	
	private void setColor() {
		String color = "-fx-background-color: purple;";
		setRegularStyle(getRegularStyle() + color);
	}

}
