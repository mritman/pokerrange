package pr.ui.javafx.table;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.control.Tab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.oldevent.OpponentFoldedHandEvent;
import pr.domain.poker.oldevent.OpponentReceivedHandEvent;
import pr.domain.poker.oldevent.PokerTableEvent;
import pr.domain.poker.oldevent.PokerTableUpdate;
import pr.domain.poker.oldevent.seat.SeatLeftByOpponentEvent;
import pr.domain.poker.oldevent.seat.SeatTakenByOpponentEvent;
import pr.domain.poker.oldevent.seat.SeatsAdjustedEvent;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.table.PokerTable;
import pr.ui.javafx.table.opponent.OpponentPresenter;

public class TablePresenter {
	
	private static final Logger logger = LoggerFactory.getLogger(TablePresenter.class);

	private final TableModel model;
	
	private final TableView view;
	
	private final OpponentAccountRepository opponentAccountRepository;
	
	public TablePresenter(final PokerTable table, OpponentAccountRepository opponentAccountRepository) {
		this.opponentAccountRepository = opponentAccountRepository;
		this.model = new TableModel(table);
		
		view = new TableView();
		
		Bindings.bindContent(view.getTabPane().getTabs(), model.getOpponentTabs());
		
		for (Opponent opponent : table.getOpponents()) {
			addOpponentTab(opponent);
		}
		
		listenToOpponentSelection();
		
		update(null);
	}

	public Node getView() {
		return view;
	}
	
	/**
	 * Don't allow (and encourage) passing null for tableUpdate. 
	 */
	public final void update(PokerTableUpdate tableUpdate) {
		logger.debug("Updating table view: {}", model.getTable().getName());
		
		if (tableUpdate != null) {
			updateOpponents(tableUpdate);
		}
		
		// update the visible/selected opponent
		OpponentTab selectedTab = getSelectedTab();
		if (selectedTab != null) {
			selectedTab.getPresenter().update();
		}
	}
	
	private void updateOpponents(PokerTableUpdate tableUpdate) {
		for (PokerTableEvent event : tableUpdate.getEvents()) {
			
			if (event instanceof SeatLeftByOpponentEvent) {
				
				SeatLeftByOpponentEvent seatLeftEvent = (SeatLeftByOpponentEvent) event;
				model.removeOpponentTab(seatLeftEvent.getOpponent().getName());
				
			} else if (event instanceof SeatsAdjustedEvent) {
				
				reorderOpponentTabs((SeatsAdjustedEvent) event);
				
			} else if (event instanceof SeatTakenByOpponentEvent) {
				
				SeatTakenByOpponentEvent seatTakenEvent = (SeatTakenByOpponentEvent) event;
				addOpponentTab(seatTakenEvent.getOpponent());
				
			} else if (event instanceof OpponentReceivedHandEvent) {
				
				OpponentReceivedHandEvent opponentReceivedHandEvent = (OpponentReceivedHandEvent) event;
				OpponentTab tab = model.getOpponentTabByOpponentName(opponentReceivedHandEvent.getOpponent().getName());
				tab.setOpponentInHandStyle(true);
				
				selectFirstOpponentInHandIfNeeded();
			} else if (event instanceof OpponentFoldedHandEvent) {
				
				OpponentFoldedHandEvent opponentFoldedHandEvent = (OpponentFoldedHandEvent) event;
				OpponentTab tab = model.getOpponentTabByOpponentName(opponentFoldedHandEvent.getOpponent().getName());
				tab.setOpponentInHandStyle(false);
				
				selectFirstOpponentInHandIfNeeded();
			}
		}
	}
	

	private void reorderOpponentTabs(SeatsAdjustedEvent event) {
		model.sortOpponentTabs();
		for (OpponentTab tab : model.getOpponentTabs()) {
			tab.updateTitle();
		}
	}

	private void addOpponentTab(Opponent opponent) {
		if (opponent != null) {
			OpponentPresenter opponentPresenter = new OpponentPresenter(opponent, opponentAccountRepository);
			OpponentTab opponentTab = new OpponentTab(opponent, opponentPresenter);
			model.addOpponentTab(opponentTab);
		}
	}

	/**
	 * Selects the first opponent on the table that is in the hand if the
	 * currently selected opponent is not in the hand.
	 */
	private void selectFirstOpponentInHandIfNeeded() {
		if (!getSelectedTab().getOpponent().isInHand()) {
			for (Opponent opponent : model.getTable().getOpponents()) {
				
				if (opponent.isInHand()) {
					Tab tab = model.getOpponentTabByOpponentName(opponent.getName());
					view.getTabPane().getSelectionModel().select(tab);
					
					break;
				}
			}
		}
	}
	
	private void listenToOpponentSelection() {
		view.getTabPane().getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				OpponentTab selectedTab = getSelectedTab();
				
				if (selectedTab != null) {
					selectedTab.getPresenter().update();
				}
			}
			
		});
	}
	
	private OpponentTab getSelectedTab() {
		return (OpponentTab) view.getTabPane().getSelectionModel().getSelectedItem();
	}

}
