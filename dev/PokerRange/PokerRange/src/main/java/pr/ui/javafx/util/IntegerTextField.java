package pr.ui.javafx.util;

import pr.util.Optional;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import lombok.Getter;

public class IntegerTextField extends TextField {
	
	@Getter
	private final IntegerProperty valueProperty = new SimpleIntegerProperty();
	
	public IntegerTextField() {
		Bindings.bindBidirectional(textProperty(), valueProperty, new NumberStringConverter());
	}
	
	@Override
	public void replaceText(int start, int end, String text) {
		if (text.matches("[0-9]*")) {
			super.replaceText(start, end, text);
		}
	}

	@Override
	public void replaceSelection(String text) {
		if (text.matches("[0-9]*")) {
			super.replaceSelection(text);
		}
	}

	public Optional<Integer> getValue() {
		return Optional.ofNullable(valueProperty.get());
	}

}
