package pr.ui.javafx.handbasket.basketelement;

import javafx.beans.property.ReadOnlyObjectWrapper;
import pr.domain.handbasket.handcollection.HandCollectionGroup;
import pr.ui.javafx.handbasket.model.HandBasketModel;

public class HandCollectionGroupModel extends BasketElementModel {
	private ReadOnlyObjectWrapper<HandCollectionGroup> handCollectionGroupProperty;
	
	public HandCollectionGroupModel(HandCollectionGroup handCollectionGroup, HandBasketModel handBasketModel) {
		super(handCollectionGroup, handBasketModel);
		
		handCollectionGroupProperty = new ReadOnlyObjectWrapper<>(handCollectionGroup);
	}
	
	public HandCollectionGroup getHandCollectionGroup() {
		return handCollectionGroupProperty.get();
	}
}
