package pr.ui.javafx.table.opponent;

import lombok.Getter;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.ui.javafx.handbasket.presenter.HandBasketMainPresenter;
import pr.ui.javafx.handbasket.presenter.StrengthBasketStagePresenter;
import pr.ui.javafx.handbasket.presenter.ValueBasketStagePresenter;
import pr.ui.javafx.handrange.HandRangePresenter;
import pr.ui.javafx.treeview.HandBasketTreePresenter;

public class OpponentModel {
	
	@Getter
	private final Opponent opponent;
	
	@Getter 
	private final HandRangePresenter handRangePresenter;
	
	@Getter	
	private final HandBasketMainPresenter handBasketMainPresenter;
	
	@Getter
	private final HandBasketTreePresenter handBasketTreePresenter;
	
	protected OpponentModel(Opponent opponent, OpponentAccountRepository opponentAccountRepository) {
		this.opponent = opponent;
		
		handRangePresenter = new HandRangePresenter(opponent, opponentAccountRepository);
		handBasketMainPresenter = new HandBasketMainPresenter(
				new ValueBasketStagePresenter(),
				new StrengthBasketStagePresenter());
		
		handBasketTreePresenter = new HandBasketTreePresenter();
	}
}
