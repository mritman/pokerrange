package pr.ui.javafx.handbasket.presenter;

import java.io.IOException;

import javafx.beans.property.SimpleMapProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TabPane;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.domain.handbasket.HandBasketKey;
import pr.domain.handbasket.handvaluebaskets.ValueBasketEnum;
import pr.domain.handbasket.strengthbaskets.StrengthBasketEnum;
import pr.ui.javafx.handbasket.StrengthBasketPieChart;
import pr.ui.javafx.handbasket.ValueBasketPieChart;
import pr.ui.javafx.handbasket.WinPieChart;
import pr.ui.javafx.handbasket.model.HandBasketMainModel;

public class HandBasketMainPresenter {

	@FXML 
	@Getter
	private Node view;
	
	@FXML private CheckBox autoFoldCheckBox;
	@FXML private ValueBasketPieChart valueBasketPieChart;
	@FXML private StrengthBasketPieChart strengthBasketPieChart;
	@FXML private WinPieChart winPieChart;
	
	@FXML private TabPane tabPane;
	
	@Getter
	private HandBasketMainModel model;

	public HandBasketMainPresenter(ValueBasketStagePresenter valueBasketStagePresenter, StrengthBasketStagePresenter strengthBasketStagePresenter) {
		model = new HandBasketMainModel(valueBasketStagePresenter.getModel(), strengthBasketStagePresenter.getModel());
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HandBasketMainView.fxml"));
		fxmlLoader.setController(this);
		
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new InitializationException(e);
		}
		
		view = fxmlLoader.getRoot();
	}
	
	@FXML
	public void initialize() {
		model.autoFoldProperty().bindBidirectional(autoFoldCheckBox.selectedProperty());
    	initializeValueBasketPieChart();
    	initializeStrengthBasketPieChart();
        winPieChart.handBasketManagerProperty().bind(model.getValueBasketStageModel().handBasketManagerProperty());
	}

	private void initializeValueBasketPieChart() {
		SimpleMapProperty<HandBasketKey, HandBasketPresenter> map = model.getValueBasketStageModel().handBasketPresentersProperty();
		
		valueBasketPieChart.bind(ValueBasketEnum.WEAK_PAIR, map.get(ValueBasketEnum.WEAK_PAIR).getModel());
		valueBasketPieChart.bind(ValueBasketEnum.PAIR, map.get(ValueBasketEnum.PAIR).getModel());
		valueBasketPieChart.bind(ValueBasketEnum.TWO_PAIR_PLUS, map.get(ValueBasketEnum.TWO_PAIR_PLUS).getModel());
		valueBasketPieChart.bind(ValueBasketEnum.DRAW, map.get(ValueBasketEnum.DRAW).getModel());
		valueBasketPieChart.bind(ValueBasketEnum.WILL_FOLD, map.get(ValueBasketEnum.WILL_FOLD).getModel());
	}
	
	private void initializeStrengthBasketPieChart() {
		SimpleMapProperty<HandBasketKey, HandBasketPresenter> map = model.getStrengthBasketStageModel().handBasketPresentersProperty();
		
		strengthBasketPieChart.bind(StrengthBasketEnum.WILL_FOLD, map.get(StrengthBasketEnum.WILL_FOLD).getModel());
		strengthBasketPieChart.bind(StrengthBasketEnum.MIGHT_FOLD, map.get(StrengthBasketEnum.MIGHT_FOLD).getModel());
		strengthBasketPieChart.bind(StrengthBasketEnum.WONT_FOLD, map.get(StrengthBasketEnum.WONT_FOLD).getModel());
	}
	
}
