package pr.ui.javafx.cardinput;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class CardToggleButton extends ToggleButton {

	private static final String STYLESHEET_PATH = "CardToggleButton.css";

	private ObjectProperty<MouseButton> mouseButtonProperty = new SimpleObjectProperty<MouseButton>();

	public CardToggleButton() {
		super();
		initialize();
	}

	public CardToggleButton(String text) {
		super(text);
		initialize();
	}

	public ObjectProperty<MouseButton> mouseButtonProperty() {
		return mouseButtonProperty;
	}

	public final void setMouseButton(MouseButton mouseButton) {
		mouseButtonProperty.set(mouseButton);
	}

	public MouseButton getMouseButton() {
		return mouseButtonProperty.get();
	}

	public void leftClick() {
		leftClick(true);
	}
	
	public void rightClick() {
		rightClick(true);
	}
	
	public void leftClick(boolean fire) {
		setMouseButton(MouseButton.PRIMARY);
		if (fire) {
			fire();
		} else {
			setSelected(!selectedProperty().get());
		}
	}

	public void rightClick(boolean fire) {
		setMouseButton(MouseButton.SECONDARY);
		if (fire) {
			fire();
		} else {
			setSelected(!selectedProperty().get());
		}
	}

	private void initialize() {
		this.setTextOverrun(OverrunStyle.CLIP);
		this.setPrefHeight(40);
		this.setPrefWidth(47);
		this.getStylesheets().add(getClass().getResource(STYLESHEET_PATH).toExternalForm());

		this.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				ToggleButton toggleButton = (ToggleButton) event.getSource();
				toggleButton.arm();
				setMouseButton(event.getButton());
			}

		});

		selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				updateStyle(newValue);
			}

		});
	}

	private void updateStyle(boolean selected) {
		if (selected) {
			if (getMouseButton() == MouseButton.PRIMARY) {
				getStyleClass().add("BoardCardToggleButton");
			} else if (getMouseButton() == MouseButton.SECONDARY) {
				getStyleClass().add("HoleCardToggleButton");
			}
		} else {
			this.getParent().requestLayout();
			
			getStyleClass().remove("BoardCardToggleButton");
			getStyleClass().remove("HoleCardToggleButton");
		}
	}

}
