package pr.ui.javafx.handbasket.basketelement;

import java.util.Iterator;
import java.util.TreeSet;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.Setter;
import pr.domain.handbasket.BasketElement;
import pr.domain.poker.Combinations;
import pr.domain.poker.hand.Hand;
import pr.domain.poker.hand.HandValue;
import pr.ui.util.Selectable;

public abstract class BasketElementView extends StackPane implements Selectable {
	
	public final static double SIZE = 32;
	
	@Getter
	private final BasketElementModel model;

	@Getter(AccessLevel.PROTECTED) private AnchorPane anchorPane;
	@Getter(AccessLevel.PROTECTED) private Circle movedIndicator;
	@Getter(AccessLevel.PROTECTED) private StackPane comboIndicator;
	@Getter(AccessLevel.PROTECTED) private Text count;
	@Getter(AccessLevel.PROTECTED) private Button button;

	@Getter(AccessLevel.PROTECTED)
	@Setter(AccessLevel.PROTECTED)
	private String regularStyle = "-fx-border-width: 0px;";
	
	@Getter(AccessLevel.PROTECTED) 
	private String selectedStyle = regularStyle + 
			"-fx-border-color: navy;" +
			"-fx-border-width: 1px;" +
			"-fx-border-insets: -1";
	
	public BasketElementView(BasketElementModel basketElementModel) {
		this.model = basketElementModel;
		
		setStyle(regularStyle);
		setSnapToPixel(true);

		addSelectedListener();
		addMovedListener();
		
		createAnchorPane();
		createMovedIndicator();
		createCountText();
		createComboIndicator();
		createButton();
		
		createMouseEventHandler();
	}
	
	@Override
	public boolean isSelected() {
		return model.isSelected();
	}

	@Override
	public void setSelected(boolean selected) {
		model.setSelected(selected);
	}
	
	private void addSelectedListener() {
		model.selectedProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				if (model.isSelected()) {
					setStyle(selectedStyle);
				} else {
					setStyle(regularStyle);
				}
			}

		});
	}
	
	private void addMovedListener() {
		model.movedProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				if (model.isMoved()) {
					movedIndicator.setVisible(true);
				} else {
					movedIndicator.setVisible(false);
				}
			}

		});
	}
	
	private void createAnchorPane() {
		anchorPane = new AnchorPane();
		this.getChildren().add(anchorPane);
	}

	private void createComboIndicator() {
		comboIndicator = new StackPane();
		Circle circle = new Circle(4);
		circle.setStrokeWidth(0);
		circle.setFill(Color.DIMGRAY);
		comboIndicator.getChildren().add(circle);
		
		Text text = new Text("c");
		text.setFont(new Font("Century Gothic", 11));
		text.setFill(Color.WHITE);
		text.setFontSmoothingType(FontSmoothingType.LCD);
		text.setStrokeWidth(0);
		text.setBoundsType(TextBoundsType.VISUAL);
		
		comboIndicator.getChildren().add(text);
		
		anchorPane.getChildren().add(comboIndicator);
		AnchorPane.setBottomAnchor(comboIndicator, 1d);
		AnchorPane.setLeftAnchor(comboIndicator, 1d);
		
		comboIndicator.setVisible(false);
	}

	private void createCountText() {
		count = new Text(Integer.toString(model.getBasketElement().getHandCount()));
		count.setFill(Color.WHITE);
		count.setFont(new Font("Gulim Bold", 11));
		count.setBoundsType(TextBoundsType.VISUAL);
		count.setTextAlignment(TextAlignment.RIGHT);
		count.setFontSmoothingType(FontSmoothingType.LCD);
		
		anchorPane.getChildren().add(count);
		AnchorPane.setRightAnchor(count, 0d);
		AnchorPane.setBottomAnchor(count, 1d);
		
	}

	private void createButton() {
		button = new Button(model.getBasketElement().getName());
		button.setStyle("-fx-padding: 0; -fx-size: 30; -fx-background-color: null;");
		button.setPrefSize(SIZE, SIZE);
		button.setTooltip(getTooltipText(model.getBasketElement()));
		button.setFont(new Font("System Bold", 13));
		
		this.getChildren().add(button);
	}

	private void createMovedIndicator() {
		movedIndicator = new Circle(4);
		movedIndicator.setFill(Color.GOLD);
		movedIndicator.setStrokeWidth(0);
		movedIndicator.setVisible(false);
		
		anchorPane.getChildren().add(movedIndicator);
		AnchorPane.setTopAnchor(movedIndicator, 1d);
		AnchorPane.setLeftAnchor(movedIndicator, 1d);
	}
	
	private Tooltip getTooltipText(BasketElement basketElement) {
		StringBuilder stringBuilder = new StringBuilder();
		boolean first = true;
		
		TreeSet<Combinations> combinations = basketElement.getCombinations();
		
		if (combinations == null || combinations.isEmpty()) {
			return null; 
		}

		Iterator<Hand> itr = combinations.first().getHands().iterator();
		
		while (itr.hasNext()) {
			Hand hand = itr.next();
			
			// Do not show NO_MADE_HAND if the there are hands with other values.
			if (hand.getValue() == HandValue.HIGH_CARD && !first) {
				continue;
			}

			if (first) {
				first = false;
			} else {
				stringBuilder.append(" - ");
			}
			
			stringBuilder.append(hand.getValue().toString());
		}
		
		stringBuilder.append(" - " + combinations.first().getHighestHand().toString());
		
		return new Tooltip(stringBuilder.toString());
	}
	
	// TODO this is a hack, implement a cleaner solution for dealing with events on this node or any of its children.
	private void createMouseEventHandler() {
		MouseEventHandler eventHandler = new MouseEventHandler();
		addEventFilter(MouseEvent.ANY, eventHandler);
	}
	
	private class MouseEventHandler implements EventHandler<MouseEvent> {
		
		@Override
		public void handle(MouseEvent event) {
			
			if (mousePressed(event)) {
				event.consume();	
				getOnMousePressed().handle(event);
			} else if (mouseClicked(event)) {
				event.consume();	
				getOnMouseClicked().handle(event);
			} else if (mouseReleased(event)) {
				event.consume();	
				getOnMouseReleased().handle(event);
			} else if (mouseDragged(event)) {
				event.consume();	
				getOnMouseDragged().handle(event);
			} else if (mouseDragDetected(event)) {
				event.consume();	
				getOnDragDetected().handle(event);
			}
			
		}

		private boolean mousePressed(MouseEvent event) {
			return MouseEvent.MOUSE_PRESSED == event.getEventType() && getOnMousePressed() != null;
		}
		
		private boolean mouseClicked(MouseEvent event) {
			return MouseEvent.MOUSE_CLICKED == event.getEventType() && getOnMouseClicked() != null;
		}
		
		private boolean mouseReleased(MouseEvent event) {
			return MouseEvent.MOUSE_RELEASED == event.getEventType() && getOnMouseReleased() != null;
		}

		private boolean mouseDragged(MouseEvent event) {
			return MouseEvent.MOUSE_DRAGGED == event.getEventType() && getOnMouseDragged() != null;
		}
		
		private boolean mouseDragDetected(MouseEvent event) {
			return MouseEvent.DRAG_DETECTED == event.getEventType() && getOnDragDetected() != null;
		}
	}
	
}
