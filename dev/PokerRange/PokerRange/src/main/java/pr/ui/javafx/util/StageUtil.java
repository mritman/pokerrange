package pr.ui.javafx.util;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public final class StageUtil {

	private StageUtil() {};
	
	/**
	 * Shows the specified node in a new stage.
	 * 
	 * If an owner is specified then the stage will be a modal stage of the
	 * specified owner.
	 * 
	 * @param content
	 *            the node to show in the stage
	 * @param owner
	 */
	public static Stage show(Node content, Window owner) {
		return show(content, owner, false);
	}
	
	/**
	 * Shows the specified node in a stage that stops the current thread until
	 * the stage has been closed.
	 * 
	 * If an owner is specified then the stage will be a modal stage of the
	 * specified owner.
	 * 
	 * @param content
	 *            the node to show in the stage
	 * @param owner
	 */
	public static void showAndWait(Node content, Window owner) {
		show(content, owner, true);
	}
	
	public static void showLater(final Node content) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				show(content, null, false);
			}
			
		});
	}
	
	private static Stage show(final Node content, Window owner, boolean wait) {
		final AnchorPane parent = new AnchorPane();
		
		parent.getChildren().add(content);
		
		Scene scene = new Scene(parent);
		
		Stage stage = new Stage();
		stage.setScene(scene);

		if (owner != null) {
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(owner);
		}

		if (wait) {
			stage.showAndWait();
		} else {
			stage.show();
		}
		
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				parent.getChildren().remove(content);
			}
			
		});
		
		return stage;
	}

}
