package pr.ui.javafx.cardinput;

import java.io.Serializable;
import java.util.Comparator;

import pr.domain.cards.Card;

public class ToggleButtonCardComparator implements Comparator<Card>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public int compare(Card card1, Card card2) {
		if (card1.rank().intValue() > card2.rank().intValue()) {
			return -1;
		} else if (card1.rank().intValue() < card2.rank().intValue()) {
			return 1;
		} else if (card1.suit().intValue() > card2.suit().intValue()) {
			return 1;
		} else if (card1.suit().intValue() < card2.suit().intValue()) {
			return -1;
		} else {
			return 0;
		}
	}

}
