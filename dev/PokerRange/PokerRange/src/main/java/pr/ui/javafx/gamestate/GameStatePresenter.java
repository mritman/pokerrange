package pr.ui.javafx.gamestate;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import lombok.Getter;
import pr.application.exception.InitializationException;
import pr.domain.gamestate.GameState;
import pr.domain.poker.hand.Hand;
import pr.domain.poker.hand.HandValue;

public class GameStatePresenter {
	
	@Getter
	@FXML 
	private Node view;
	
	@FXML private TextField boardCardsTextField;
	@FXML private TextField boardCardsResultField;
	@FXML private TextField playerHandTextField;
	@FXML private TextField playerResultTextField;
	
	@FXML private ImageView statusImageView;
	
	public GameStatePresenter() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GameStateView.fxml"));
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
        view = fxmlLoader.getRoot();
        
        statusImageView.setVisible(false);
	}
	
	public void update(GameState gameState) {
		
		if (gameState.getHoleCards() != null) {
			playerHandTextField.setText(gameState.getHoleCards().card1() + " " + gameState.getHoleCards().card2());
			playerResultTextField.setText(gameState.getHoleCardCombinations().get().getHighestHand().getValue().toString());
		} else {
			playerHandTextField.setText("");
			playerResultTextField.setText("");
		}
		
		if (gameState.getBoardCards() != null && gameState.getBoardCards().getFlop() != null) {
			String boardCardsString = "";
			
			boardCardsString += gameState.getBoardCards().getFlop().toString();
			if (gameState.getBoardCards().getTurn() != null) boardCardsString += " " + gameState.getBoardCards().getTurn();
			if (gameState.getBoardCards().getRiver() != null) boardCardsString += " " + gameState.getBoardCards().getRiver();
			
			boardCardsTextField.setText(boardCardsString);
			HandValue boardCardsValue = gameState.getBoardCardCombinations().getHighestHand().getValue();
			String boardCardsValueString = boardCardsValue == null ? "" : boardCardsValue.toString();
			boardCardsResultField.setText(boardCardsValueString);
		} else {
			boardCardsTextField.setText("");
			boardCardsResultField.setText("");
		}
		
		Hand boardHand = gameState.getBoardCardCombinations().getHighestHand();
		boolean showAlert = boardHand != null && boardHand.getValue().intValue() > HandValue.PAIR.intValue();
		statusImageView.setVisible(showAlert);
	}
}
