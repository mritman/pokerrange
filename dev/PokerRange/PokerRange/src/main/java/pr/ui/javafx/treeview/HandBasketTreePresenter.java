package pr.ui.javafx.treeview;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import lombok.Getter;
import pr.domain.handbasket.HandBasket;
import pr.domain.handbasket.HandBasketKey;
import pr.domain.handbasket.HandBasketManager;
import pr.domain.handbasket.HandBasketSubset;
import pr.domain.handbasket.HandBasketSubsetKey;
import pr.domain.handbasket.handvaluebaskets.ValueBasketEnum;
import pr.domain.handbasket.handvaluebaskets.ValueHandBasket;
import pr.ui.javafx.handbasket.presenter.HandBasketPresenter;

// TODO cleanup, this is just prototype code
public class HandBasketTreePresenter {

	@Getter
	private HandBasketTreeView view;
	
	public HandBasketTreePresenter() {
		view = new HandBasketTreeView();
	}

	public void update(HandBasketManager valueBasketManager) {
		List<TreeItem<Node>> treeItems = new LinkedList<TreeItem<Node>>();
		
		Map<ValueBasketEnum, Color> map = new HashMap<>();
		map.put(ValueBasketEnum.TWO_PAIR_PLUS, Color.RED);
		map.put(ValueBasketEnum.PAIR, Color.SALMON);
		map.put(ValueBasketEnum.WEAK_PAIR, Color.BURLYWOOD);
		map.put(ValueBasketEnum.DRAW, Color.GOLD);
		map.put(ValueBasketEnum.WILL_FOLD, Color.LIME);
		
		List<HandBasketKey> keys = new LinkedList<HandBasketKey>();
		keys.addAll(Arrays.asList(valueBasketManager.getHandBasketKeys()));
		Collections.reverse(keys);
		
		for (HandBasketKey key : keys) {
			HandBasket handBasket = valueBasketManager.getHandBasketMap().get(key);
			if (handBasket.getPercentage() == 0) {
				continue;
			}

			
			Bar bar = new Bar(handBasket.getPercentage(), 1, NumberFormat.getPercentInstance(), handBasket.getName());
			
			// TODO clean this up, make it a property on Bar for example.
			Color color = map.get(ValueBasketEnum.valueOf(key.getName()));
					
			bar.getLabel().setStyle("-fx-font-weight: bold;");
			bar.setColor(color);
			
			TreeItem<Node> treeItem = new TreeItem<Node>(bar);
			treeItems.add(treeItem);
			
			List<TreeItem<Node>> childNodes = createChildNodes((ValueHandBasket) handBasket, color);
			
			treeItem.getChildren().addAll(childNodes);
		}
		
		view.getRoot().getChildren().setAll(treeItems);
		
	}

	/**
	 * Creates subsets.
	 */
	private List<TreeItem<Node>> createChildNodes(ValueHandBasket handBasket, Color color) {
		List<TreeItem<Node>> children = new LinkedList<>();
		
		Map<HandBasketSubsetKey, HandBasketSubset> subsets = handBasket.getSubsets();
		
		int maxSize = 0;
		
		for (HandBasketSubset subset : subsets.values()) {
			maxSize = Math.max(subset.numberOfHands(), maxSize);
		}
		
		List<Entry<HandBasketSubsetKey, HandBasketSubset>> reversedSubset = new LinkedList<>();
		for (Entry<HandBasketSubsetKey, HandBasketSubset> entry : subsets.entrySet()) {
			reversedSubset.add(entry);
		}
		Collections.reverse(reversedSubset);
		
		for (Map.Entry<HandBasketSubsetKey, HandBasketSubset> entry : reversedSubset) {
			if (entry.getValue().size() == 0) continue;
			
			Bar bar = new Bar(entry.getValue().numberOfHands(), maxSize, NumberFormat.getIntegerInstance(), entry.getKey().getName());
			bar.setColor(color);
			
			TreeItem<Node> treeItem = new TreeItem<Node>(bar);
			children.add(treeItem);
			
			createChildNodes(treeItem, entry.getKey(), entry.getValue());
		}
		
		return children;
	}

	/**
	 * Creates BasketElements. 
	 */
	private void createChildNodes(TreeItem<Node> treeItem, HandBasketSubsetKey key, HandBasketSubset subset) {
		HandBasketPresenter handBasketPresenter = new HandBasketPresenter();
		handBasketPresenter.getModel().setHandBasket(new HandBasket(key.getName(), subset.getBasketElements()));
		
		FlowPane flowPane = new FlowPane(5, 5) {
			
			@Override
			protected double computePrefHeight(double forWidth) {
				double size = getChildren().size();
				
				double rows = size / 14;
				rows = Math.ceil(rows);
				
				return rows * 32 + ((rows - 1) * getHgap());
			}
			
		};
		flowPane.setPadding(new Insets(0, 0, 0, 170));
		flowPane.getChildren().addAll(handBasketPresenter.getModel().getBasketElementViews());
		flowPane.setPrefHeight(FlowPane.USE_COMPUTED_SIZE);
		
		TreeItem<Node> newTreeItem = new TreeItem<Node>(flowPane);
		treeItem.getChildren().add(newTreeItem);
	}

	public void expandAll() {
		setExpanded(view.getRoot(), true);
	}
	
	public void collapseAll() {
		setExpanded(view.getRoot(), false);
	}
	
	public void setExpanded(TreeItem<Node> root, boolean expand) {
		for (TreeItem<Node> child : root.getChildren()) {
			child.setExpanded(expand);
			setExpanded(child, expand);
		}
	}
	
}
