package pr.ui.javafx.main;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import pr.infrastructure.persistence.jpa.JpaPersistServiceManager;
import pr.infrastructure.persistence.jpa.pokertracker.PokerTrackerPersistServiceManager;
import pr.infrastructure.tableobserver.service.TableObserverService;
import pr.util.Service;

public class ServiceManager {
	
	private List<Service> services = new LinkedList<>();
	
	@Inject
	public ServiceManager(
			PokerTrackerPersistServiceManager pokerTrackerPersistServiceManager,
			JpaPersistServiceManager jpaPersistServiceManager,
			TableObserverService tableObserverService) {
		
		services.add(pokerTrackerPersistServiceManager);
		services.add(jpaPersistServiceManager);
		services.add(tableObserverService);
	}
	
	public void start() {
		for (Service service : services) {
			service.startService();
		}
	}
	
	public void stop() {
		for (Service service : services) {
			service.stopService();
		}
	}

}
