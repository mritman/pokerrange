package pr.ui.javafx.handbasket;

import javafx.beans.binding.DoubleBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import pr.domain.handbasket.strengthbaskets.StrengthBasketEnum;
import pr.ui.javafx.handbasket.model.HandBasketModel;
import pr.ui.javafx.util.MouseOverPieChart;

public class StrengthBasketPieChart extends MouseOverPieChart {
	private final ObservableList<PieChart.Data> data = FXCollections.observableArrayList(
			new PieChart.Data("NF", 0),
			new PieChart.Data("MF", 0),
			new PieChart.Data("WF", 0));
	
	public StrengthBasketPieChart() {
		super();
		
		getData().addAll(data);
		
    	String path = getClass().getResource("RedYellowBlueChartStyle.css").toExternalForm();
    	getStylesheets().add(path);
	}
	
	public void bind(StrengthBasketEnum strengthBasketEnum, final HandBasketModel handBasketModel) {
		PieChart.Data pieChartData = getData(strengthBasketEnum);

		pieChartData.pieValueProperty().bind(new DoubleBinding() {

			{
				super.bind(handBasketModel.getBasketElementViews());
			}
			
			@Override
			protected double computeValue() {
				if (handBasketModel.getHandBasket() == null) {
					return 0;
				} else {
					return handBasketModel.getHandBasket().getHandsCount();
				}
			}

		});
		
	}
	
	private PieChart.Data getData(StrengthBasketEnum strengthBasketEnum) {
		switch (strengthBasketEnum) {
		case WONT_FOLD:
			return data.get(0);
		case MIGHT_FOLD:
			return data.get(1);
		case WILL_FOLD:
			return data.get(2);
		default:
			// This should never happen.
			throw new IllegalStateException();
		}
	}
}
