package pr.ui.javafx.cardinput;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.GridPaneBuilder;
import javafx.scene.layout.VBox;
import lombok.Getter;

public class CardInputView extends VBox {
	private static final int SPACING = 10;
	private static final int BUTTON_GAP = 2;
	
	@Getter
	private GridPane buttonGridPane;
	@Getter
	private Button resetButton;
	@Getter
	private CheckBox manualInputCheckBox;

	public CardInputView() {
		setSpacing(SPACING);
		setPrefSize(USE_COMPUTED_SIZE, USE_COMPUTED_SIZE);
		setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
		setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
		
		buttonGridPane = GridPaneBuilder.create().hgap(BUTTON_GAP).vgap(BUTTON_GAP).build();
		buttonGridPane.setPrefSize(USE_COMPUTED_SIZE, USE_COMPUTED_SIZE);
		buttonGridPane.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
		buttonGridPane.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
		
		getChildren().add(buttonGridPane);
		
		resetButton = new Button("Reset");
		resetButton.setPrefSize(75, 25);
		resetButton.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
		resetButton.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
		
		getChildren().add(resetButton);

		manualInputCheckBox = new CheckBox("Manual input");
		
		getChildren().add(manualInputCheckBox);
	}
}
