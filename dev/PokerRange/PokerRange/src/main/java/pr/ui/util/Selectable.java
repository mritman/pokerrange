package pr.ui.util;

public interface Selectable {
	boolean isSelected();
	void setSelected(boolean selected);
}
