package pr.ui.javafx.main;

import javafx.application.Application;
import javafx.stage.Stage;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.application.preferences.FilePreferences;
import pr.infrastructure.database.LiquibaseDatabaseUpdateService;
import pr.infrastructure.persistence.jpa.JpaRepositoryModule;
import pr.infrastructure.persistence.jpa.pokertracker.PokerTrackerRepositoryModule;
import pr.infrastructure.tableobserver.TableObserverModule;
import pr.ui.javafx.stage.StagePresenter;
import pr.util.DefaultExceptionHandler;

import com.google.inject.Guice;
import com.google.inject.Injector;
 
public class PrApplication extends Application {
	
	private static final Logger logger = LoggerFactory.getLogger(PrApplication.class);
	
	private ServiceManager serviceManager;
	
	@Override
	public void init() {
		logger.debug("JavaFX version: {}", com.sun.javafx.runtime.VersionInfo.getRuntimeVersion());
	}
	
	@Override
    public void start(Stage stage) throws Exception {
		try {
			LiquibaseDatabaseUpdateService databasUpdateService = new LiquibaseDatabaseUpdateService(new FilePreferences());
			databasUpdateService.update();
			databasUpdateService.close();
			
			Injector injector = Guice.createInjector(
					new PokerTrackerRepositoryModule(),
					new JpaRepositoryModule(),
					new TableObserverModule(),
					new PokerRangeModule(stage));
			
			serviceManager = injector.getInstance(ServiceManager.class);
			serviceManager.start();
			
			injector.getInstance(StagePresenter.class).showStage();
		} catch (Throwable e) {
			// For some reason logging using the uncaught exception handler does
			// not work when building the jar with the Maven JavaFX plugin.
			logger.error("Uncaught exception", e);
			JOptionPane.showMessageDialog(null, "Uncaught exception: " + e.getMessage());
			System.exit(1);
		}
    }

	@Override
	public void stop() {
		serviceManager.stop();
	}
    
    public static void main(String[] args) {
    	Thread.setDefaultUncaughtExceptionHandler(DefaultExceptionHandler.getInstance());
        launch(args);
    }
    
}