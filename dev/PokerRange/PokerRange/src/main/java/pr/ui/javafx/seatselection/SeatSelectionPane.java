package pr.ui.javafx.seatselection;

import java.util.HashMap;
import java.util.Map;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import pr.domain.poker.table.PokerTable;

public class SeatSelectionPane extends StackPane {
	
	private final Scene seatSelectionScene = new Scene(this);
	
	private ComboBox<String> tableSelection; 
	
	private SixSeatTable sixSeatTable = new SixSeatTable();
	private NineSeatTable nineSeatTable = new NineSeatTable();
	
	private ObjectProperty<PokerTable> tableProperty;
	
	private Map<String, AbstractTablePane> tableMap;
	
	public SeatSelectionPane() {
		tableMap = new HashMap<>();
		tableMap.put(sixSeatTable.toString(), sixSeatTable);
		tableMap.put(nineSeatTable.toString(), nineSeatTable);
		
		tableProperty = new SimpleObjectProperty<>();
		tableProperty.addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable arg1) {
				tableMap.get(tableSelection.getValue()).setTable(tableProperty.get());
			}
			
		});
		
		tableSelection = new ComboBox<>();
		tableSelection.getItems().add(sixSeatTable.toString());
		tableSelection.getItems().add(nineSeatTable.toString());
		tableSelection.setValue(sixSeatTable.toString());
		tableSelection.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> selectedItem,
					String oldValue, String newValue) {

				changeTable(tableMap.get(newValue));
			}

		});
		
		this.getChildren().add(sixSeatTable);
		this.getChildren().add(tableSelection);
		StackPane.setAlignment(tableSelection, Pos.TOP_LEFT);
		StackPane.setMargin(tableSelection, new Insets(10.0, 0, 0, 10.0));
		
		// Make sure that the selected item in the combo box corresponds to the table being shown.
	}
	
	public void show(Window owner, PokerTable table) {
		tableProperty.set(table);
		
		Stage stage = new Stage();
		stage.setScene(seatSelectionScene);
		stage.setResizable(false);

		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(owner);
		stage.show();
	}

	private void changeTable(AbstractTablePane tablePane) {
		tablePane.setTable(tableProperty.get());
		getChildren().remove(0);
		getChildren().add(0, tablePane);
	}

}
