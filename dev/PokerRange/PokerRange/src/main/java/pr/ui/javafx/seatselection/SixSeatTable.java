package pr.ui.javafx.seatselection;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.shape.Circle;
import pr.application.exception.InitializationException;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;

public class SixSeatTable extends AbstractTablePane implements Initializable {
	
	@FXML
	private Circle seat1, seat2, seat3, seat4, seat5, seat6;
	
	protected SixSeatTable() {
		super("6 seat table", TableSize.SIX);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SixSeatTable.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		seat1.setUserData(Seat.SEAT1);
		seat2.setUserData(Seat.SEAT2);
		seat3.setUserData(Seat.SEAT3);
		seat4.setUserData(Seat.SEAT4);
		seat5.setUserData(Seat.SEAT5);
		seat6.setUserData(Seat.SEAT6);
		
		Collections.addAll(getSeats(), seat1, seat2, seat3, seat4, seat5, seat6);
	}
	
}
