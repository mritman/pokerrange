package pr.util;

import java.util.LinkedList;

public class BoundedFifoList<E> extends LinkedList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -814531323442789914L;
	
	private int maxSize;
	
	public BoundedFifoList(int maxSize) {
		if (maxSize <= 0) {
			throw new IllegalArgumentException("Argument 'maxSize' has to be positive number but was: " + maxSize);
		}
		
		this.maxSize = maxSize;
	}

	@Override
	public boolean add(E e) {
		if (size() == maxSize) {
			removeFirst();
		}
		
		return super.add(e);
	}
	
}
