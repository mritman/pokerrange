package pr.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public final class Utils {
	
	private Utils() {};
	
	public static String roundTwoDecimalsNew(double d) {
		NumberFormat nf = DecimalFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		return nf.format(d);
	}
	
	public static <T> void rotate(List<T> list, T element) {
		Preconditions.checkArgument(list.contains(element));
		
		int index = list.indexOf(element);
		
		Collections.rotate(list, -index);
	}
	
	public static <T> void rotate(List<T> list, Predicate<T> predicate) {
		T element = Iterables.find(list, predicate);
		rotate(list, element);
	}
	
	public static <T> void rotatePast(List<T> list, T element) {
		rotate(list, element);
		Collections.rotate(list, -1);
	}

	public static <T> void rotatePast(List<T> list, Predicate<T> predicate) {
		T element = Iterables.find(list, predicate);
		rotatePast(list, element);
	}

}
