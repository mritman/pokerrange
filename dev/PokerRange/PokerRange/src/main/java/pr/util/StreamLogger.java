package pr.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;

public class StreamLogger extends Thread {
	private InputStream is;
	private Logger log;
	private String linePrefix;

	public StreamLogger(InputStream is, String linePrefix, Logger log) {
		this.is = is;
		this.linePrefix = linePrefix;
		this.log = log;
	}

	@Override
	public void run() {
		try (InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);) {
			
			String line = null;
			while ((line = br.readLine()) != null) {
				log.info(linePrefix + ": " + line);
			}
		} catch (IOException e) {
			log.error("Failed to read the stream.", e);
		}
	}
}
