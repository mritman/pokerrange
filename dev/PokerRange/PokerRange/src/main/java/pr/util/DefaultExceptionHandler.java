package pr.util;

import java.lang.Thread.UncaughtExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DefaultExceptionHandler implements UncaughtExceptionHandler {

	private static final DefaultExceptionHandler instance = new DefaultExceptionHandler();
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
	
	private DefaultExceptionHandler() {
		// Singleton.
	}
	
	public static DefaultExceptionHandler getInstance() {
		return instance;
	}
	
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		logger.error("Uncaught exception", e);
	}

}
