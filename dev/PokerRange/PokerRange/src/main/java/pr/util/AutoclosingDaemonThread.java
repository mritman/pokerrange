package pr.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * During construction of an instance of this class a shutdown hook is created
 * that interrupts this thread and waits for it to die. This allows the thread
 * instance to call its clean up methods.
 */
public abstract class AutoclosingDaemonThread extends Thread {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());

	public AutoclosingDaemonThread(String name) {
		super(name);
		setDaemon(true);
		
		Runtime.getRuntime().addShutdownHook(new Thread(name + " Shutdown Hook") {
			
			@Override
			public void run() {
				try {
					AutoclosingDaemonThread.this.interrupt();
					AutoclosingDaemonThread.this.join();
				} catch (InterruptedException e) {
							log.warn(
									"{} thread was interuppted while waiting for {}",
									Thread.currentThread().getName(),
									AutoclosingDaemonThread.this.getName(), e);
				}
			}
			
		});
	}
}
