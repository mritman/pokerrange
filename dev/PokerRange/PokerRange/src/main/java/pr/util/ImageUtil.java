package pr.util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public final class ImageUtil {
	
	private ImageUtil() {};
	
	public static boolean compareImage(Image image1, Image image2) {
		PixelReader pixelReader1 = image1.getPixelReader();
		PixelReader pixelReader2 = image2.getPixelReader();
		
		if (image1.getWidth() != image2.getWidth() || image1.getHeight() != image2.getHeight()) {
			return false;
		}
		
		for (int x = 0; x < image1.getWidth(); x++) {
			for (int y = 0; y < image1.getHeight(); y++) {
				if (pixelReader1.getArgb(x, y) != pixelReader2.getArgb(x, y)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static Image loadFxImage(URL url) throws IOException {
		BufferedImage bufferedImage = null;
		WritableImage image = null;
		
		bufferedImage = ImageIO.read(url);

		image = new WritableImage(bufferedImage.getWidth(), bufferedImage.getHeight());
		SwingFXUtils.toFXImage(bufferedImage, image);

		return image;
	}

}
