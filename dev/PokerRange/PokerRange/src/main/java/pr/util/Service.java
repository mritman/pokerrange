package pr.util;

public interface Service {

	/**
	 * Starts the service. This method blocks until the service has completely
	 * started.
	 */
	void startService();

	/**
	 * Stops the service. This method blocks until the service has completely
	 * shut down.
	 */
	void stopService();
	
	/**
	 * @return true if the service was started.
	 */
	boolean isStarted();

}
