package pr.util;

import java.util.NoSuchElementException;

public class Optional<T> {
    private final static Optional<?> EMPTY = new Optional<>();

    private final T value;
    private final boolean present;

    private Optional() {
        this.value = null;
        this.present = false;
    }
    
    private Optional(T value) {
    	this.value = value;
    	this.present = value != null;
    }

    @SuppressWarnings("unchecked")
	public static <T> Optional<T> empty() {
        return (Optional<T>) EMPTY;
    }
    
    public static <T> Optional<T> of(T value) {
    	if (value == null) throw new NullPointerException();
    	return new Optional<T>(value);
    }
    
    public static <T> Optional<T> ofNullable(T value) {
    	return new Optional<T>(value);
    }

    public T get() {
        if (!present)
            throw new NoSuchElementException();
        return value;
    }

    public boolean isPresent() {
        return present;
    }

    public T orElse(T other) {
        return present ? value : other;
    }

    public<V extends Throwable> T orElseThrow(Class<V> exceptionClass) throws V {
        if (present)
            return value;
        else
            try {
                throw exceptionClass.newInstance();
            }
            catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalStateException("Unexpected exception: " + e, e);
            }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Optional<?> optional = (Optional<?>) o;

        if (present != optional.present) return false;
        if (value != null ? !value.equals(optional.value) : optional.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (present ? 1 : 0);
        return result;
    }
    
    @Override
    public String toString() {
    	if (present) {
    		return value.toString();
    	}
    	
    	return "empty";
    }
}