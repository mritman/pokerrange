package pr.util;

import java.util.Map;

import org.joda.money.CurrencyUnit;

import com.google.common.collect.ImmutableMap;

public final class CurrencyParser {
	
	private CurrencyParser() {}
	
	private static final Map<Character, CurrencyUnit> symbolToCurrency = ImmutableMap
			.of('$', CurrencyUnit.USD, 
					'€', CurrencyUnit.EUR, 
					'¥', CurrencyUnit.JPY, 
					'£', CurrencyUnit.GBP);
	
	public static CurrencyUnit fromSymbol(char symbol) {
		return symbolToCurrency.get(symbol);
	}

}
