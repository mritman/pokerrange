package pr.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lombok.Getter;

import com.google.common.base.Preconditions;

public final class MessageDigestFactory {

	private MessageDigestFactory() {
		// can't be instantiated
	}
	
	public enum MessageDigestAlgorithm {

		MD2("MD2"), MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256"), SHA384("SHA-384"), SHA512(
				"SHA-512");

		@Getter
		private final String algorithm;

		private MessageDigestAlgorithm(final String algorithm) {
			this.algorithm = algorithm;
		}

	}
	
	public static MessageDigest getMessageDigest(final MessageDigestAlgorithm algorithm) {
		Preconditions.checkNotNull(algorithm);

		try {
			return MessageDigest.getInstance(algorithm.getAlgorithm());
		} catch (NoSuchAlgorithmException e) {
			// Should never happen
			throw new IllegalStateException(e);
		}
	}

}

