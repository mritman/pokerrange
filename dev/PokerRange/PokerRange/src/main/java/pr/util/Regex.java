package pr.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
	
	public static String find(String regex, String line) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		
		if (matcher.find()) {
			return matcher.group();
		}
		
		return null;
	}
	
	public static boolean matches(String regex, String line) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		return matcher.find();
	}
	
}
