package pr.util;

import java.util.Iterator;

import com.google.common.collect.AbstractIterator;

import lombok.Getter;

public class IteratorFuser {
	
	public static <A, B> Iterator<FusedEntry<A, B>> fuse(Iterable<A> iterableA, Iterable<B> iterableB) {
		final Iterator<A> iteratorA = iterableA.iterator();
		final Iterator<B> iteratorB = iterableB.iterator();
		
		return new AbstractIterator<IteratorFuser.FusedEntry<A,B>>() {
			@Override
			protected FusedEntry<A, B> computeNext() {
				Optional<A> a = iteratorA.hasNext() ? Optional.of(iteratorA.next()) : Optional.<A>empty();
				Optional<B> b = iteratorB.hasNext() ? Optional.of(iteratorB.next()) : Optional.<B>empty();
				
				if (!a.isPresent() && !b.isPresent()) {
					return endOfData();
				}
				
				return new FusedEntry<A, B>(a, b);
			}
		};
	}
	
	public static class FusedEntry<A, B> {
		@Getter private final Optional<A> a;
		@Getter private final Optional<B> b;
		
		public FusedEntry(A a, B b) {
			this.a = Optional.ofNullable(a);
			this.b = Optional.ofNullable(b);
		}
		
		public FusedEntry(Optional<A> a, Optional<B> b) {
			this.a = a;
			this.b = b;
		}
	};

}
