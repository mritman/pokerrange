package pr.util;

public enum FileType {
	PNG("png");

	private final String value;
	private final String extension;
	
	private FileType(String value) {
		this.value = value;
		this.extension = "." + value;
	}
	
	/**
	 * @return the filetype extension including the preceding '.' character.
	 */
	public String extension() {
		return extension;
	}
	
	/**
	 * Returns the file type extension without the preceding '.' character.
	 */
	public String toString() {
		return value;
	}

}
