package liquibase.ext.logging.slf4j;

import liquibase.logging.core.AbstractLogger;
import lombok.Getter;
import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jLogger extends AbstractLogger {
    
	@Getter
    @Setter
	private int priority = 1;

    private Logger logger;

    @Override
    public void setName(String name) {
        this.logger = LoggerFactory.getLogger(name);
    }

    @Override
    public void setLogLevel(String logLevel, String logFile) {
        // Intentionally empty.
    }

    @Override
    public void severe(String message) {
        this.logger.error(message);
    }

    @Override
    public void severe(String message, Throwable throwable) {
        this.logger.error(message, throwable);
    }

    @Override
    public void warning(String message) {
        this.logger.warn(message);
    }

    @Override
    public void warning(String message, Throwable throwable) {
        this.logger.warn(message, throwable);
    }

    @Override
    public void info(String message) {
        this.logger.info(message);
    }

    @Override
    public void info(String message, Throwable throwable) {
        this.logger.info(message, throwable);
    }

    @Override
    public void debug(String message) {
        this.logger.debug(message);
    }

    @Override
    public void debug(String message, Throwable throwable) {
        this.logger.debug(message, throwable);
    }

}
