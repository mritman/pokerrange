package pr.ui.javafx.handrange;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Rank;
import pr.domain.poker.range.typed.types.HandType;
import pr.domain.poker.range.typed.types.Pair;

public class HandRangeTextFieldTest {

	@Test
	public void testHandTypeChanges() {
		HandRangeTextField handRangeTextField = new HandRangeTextField();
		
		// Test initial state;
		Assert.assertEquals("", handRangeTextField.getText());
		Assert.assertEquals(null, handRangeTextField.getHandRange());

		// Test if the text value is correctly updated when the HandType property changes value.
		HandType handType = new Pair();
		handRangeTextField.setHandType(handType);
		
		Assert.assertEquals(handType.getLowestRank().strValue() + "-" + handType.getHighestRank().strValue(), handRangeTextField.getText());
		Assert.assertEquals(handType.getLowestRank(), handRangeTextField.getHandRange().getLower());
		Assert.assertEquals(handType.getHighestRank(), handRangeTextField.getHandRange().getUpper());
		
		// Test if the tex value is correctly updated when the HandType property
		// is set, but not changed, but has different values for its lower and
		// upper ranks.
		handType.setLowestRank(Rank.THREE);
		handType.setHighestRank(Rank.KING);
		handRangeTextField.setHandType(handType);
		
		Assert.assertEquals(handType.getLowestRank().strValue() + "-" + handType.getHighestRank().strValue(), handRangeTextField.getText());
		Assert.assertEquals(handType.getLowestRank(), handRangeTextField.getHandRange().getLower());
		Assert.assertEquals(handType.getHighestRank(), handRangeTextField.getHandRange().getUpper());
		
		// Test if setting the handType to null resets the HandRangeTextField to its initial state.
		handRangeTextField.setHandType(null);
		Assert.assertEquals("", handRangeTextField.getText());
		Assert.assertEquals(null, handRangeTextField.getHandRange());
	}
	
	@Test
	public void testTextChanges() {
		HandRangeTextField handRangeTextField = new HandRangeTextField();
		
		// Test if the ranks are updated when the text is set.
		handRangeTextField.setText("2-A");		
		Assert.assertEquals(Rank.TWO, handRangeTextField.getHandRange().getLower());
		Assert.assertEquals(Rank.ACE, handRangeTextField.getHandRange().getUpper());
		
		// Test that the ranks are not updated while the current text is not a valid range.
		handRangeTextField.setText("2-");
		Assert.assertEquals(Rank.TWO, handRangeTextField.getHandRange().getLower());
		Assert.assertEquals(Rank.ACE, handRangeTextField.getHandRange().getUpper());
	}
	
}
