package pr.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.util.IteratorFuser.FusedEntry;


public class IteratorFuserTest {

	@Test
	public void testFusion() {
		List<Integer> listA = Arrays.asList(1, 2, 3, 4);
		List<String> listB = Arrays.asList("A", "B", "C");
		
		Iterator<FusedEntry<Integer, String>> fusedIterator = IteratorFuser.fuse(listA, listB);
		
		FusedEntry<Integer,String> entry = fusedIterator.next();
		Assert.assertEquals(1, (int) entry.getA().get());
		Assert.assertEquals("A", entry.getB().get());
		
		entry = fusedIterator.next();
		Assert.assertEquals(2, (int) entry.getA().get());
		Assert.assertEquals("B", entry.getB().get());
		
		entry = fusedIterator.next();
		Assert.assertEquals(3, (int) entry.getA().get());
		Assert.assertEquals("C", entry.getB().get());
		
		entry = fusedIterator.next();
		Assert.assertEquals(4, (int) entry.getA().get());
		Assert.assertFalse(entry.getB().isPresent());
		
		Assert.assertFalse(fusedIterator.hasNext());
	}
	
}
