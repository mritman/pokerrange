package pr.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

public class UtilsTest {

	@Test
	public void testRotate() {
		List<Integer> list = Lists.newArrayList(1, 2, 3, 4);
		List<Integer> expected = Lists.newArrayList(1, 2, 3, 4);
		Utils.rotate(list, Integer.valueOf(1));
		Assert.assertEquals(expected, list);
		
		list = Lists.newArrayList(1, 2, 3, 4);
		expected = Lists.newArrayList(4, 1, 2, 3);
		Utils.rotate(list, Integer.valueOf(4));
		Assert.assertEquals(expected, list);
		
		list = Lists.newArrayList(1, 2, 3, 4);
		expected = Lists.newArrayList(2, 3, 4, 1);
		Utils.rotate(list, Integer.valueOf(2));
		Assert.assertEquals(expected, list);
	}
	
	@Test
	public void testRotatePredicate() {
		List<Integer> list = Lists.newArrayList(1, 2, 3, 4);
		List<Integer> expected = Lists.newArrayList(2, 3, 4, 1);
		
		Utils.rotate(list, new Predicate<Integer>() {
			public boolean apply(Integer integer) {
				return integer == Integer.valueOf(2);
			}
		});
		
		Assert.assertEquals(expected, list);
	}
	
	@Test
	public void testRotatePast() {
		List<Integer> list = Lists.newArrayList(1, 2, 3, 4);
		List<Integer> expected = Lists.newArrayList(3, 4, 1, 2);
		Utils.rotatePast(list, Integer.valueOf(2));
		Assert.assertEquals(expected, list);
	}
	
	@Test
	public void rotatePastPredicate() {
		List<Integer> list = Lists.newArrayList(1, 2, 3, 4);
		List<Integer> expected = Lists.newArrayList(3, 4, 1, 2);
		
		Utils.rotatePast(list, new Predicate<Integer>() {
			public boolean apply(Integer integer) {
				return integer == Integer.valueOf(2);
			}
		});
		
		Assert.assertEquals(expected, list);
	}
	
}
