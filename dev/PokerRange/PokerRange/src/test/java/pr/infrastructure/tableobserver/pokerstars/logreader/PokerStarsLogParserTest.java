package pr.infrastructure.tableobserver.pokerstars.logreader;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pr.domain.poker.event.PlayerSitsOutEvent;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.TableSizeEvent;
import pr.domain.poker.table.TableSize;

public class PokerStarsLogParserTest {

	private PokerStarsLogParser logParser;
	
	@Before
	public void before() {
		logParser = new PokerStarsLogParser();
	}
	
	@Test
	public void testSitsOutResult() {
		List<String> lines = Arrays.asList("PlayerName: sits out");
		List<PokerEvent> events = logParser.getEvents(lines);
		
		Assert.assertEquals(1, events.size());
		Assert.assertTrue(events.get(0) instanceof PlayerSitsOutEvent);
		PlayerSitsOutEvent event = (PlayerSitsOutEvent) events.get(0);
		Assert.assertEquals("PlayerName", event.getName());
	}
	
	@Test
	public void testTableSize() {
		List<String> lines = Arrays.asList("Table 'Deikoon II' 9-max Seat #4 is the button");
		List<PokerEvent> events = logParser.getEvents(lines);
		
		Assert.assertEquals(1, events.size());
		Assert.assertTrue(events.get(0) instanceof TableSizeEvent);
		TableSizeEvent event = (TableSizeEvent) events.get(0);
		Assert.assertEquals(TableSize.NINE, event.getSize());
	}

}
