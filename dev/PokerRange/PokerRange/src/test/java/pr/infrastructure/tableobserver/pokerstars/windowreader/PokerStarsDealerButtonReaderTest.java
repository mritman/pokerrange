package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;

import javafx.scene.image.Image;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsDealerButtonReader;
import pr.util.ImageUtil;
import pr.util.Optional;

public class PokerStarsDealerButtonReaderTest {

	private final PokerStarsDealerButtonReader dealerButtonReader;
	
	public PokerStarsDealerButtonReaderTest() {
		 dealerButtonReader = new PokerStarsDealerButtonReader();
	}
	
	@Test
	public void testReadDealerButtonSize6() throws IOException {
		Image image1 = getImage("capture5.png");
		Image image2 = getImage("capture1.png");
		Image image3 = getImage("capture3.png");
		Image image4 = getImage("capture6.png");
		Image image5 = getImage("capture2.png");
		Image image6 = getImage("capture7.png");
		
		Optional<Seat> dealerPosition1 = dealerButtonReader.readDealerPosition(image1, TableSize.SIX);
		Optional<Seat> dealerPosition2 = dealerButtonReader.readDealerPosition(image2, TableSize.SIX);
		Optional<Seat> dealerPosition3 = dealerButtonReader.readDealerPosition(image3, TableSize.SIX);
		Optional<Seat> dealerPosition4 = dealerButtonReader.readDealerPosition(image4, TableSize.SIX);
		Optional<Seat> dealerPosition5 = dealerButtonReader.readDealerPosition(image5, TableSize.SIX);
		Optional<Seat> dealerPosition6 = dealerButtonReader.readDealerPosition(image6, TableSize.SIX);
		
		Assert.assertEquals(Seat.SEAT1, dealerPosition1.get());
		Assert.assertEquals(Seat.SEAT2, dealerPosition2.get());
		Assert.assertEquals(Seat.SEAT3, dealerPosition3.get());
		Assert.assertEquals(Seat.SEAT4, dealerPosition4.get());
		Assert.assertEquals(Seat.SEAT5, dealerPosition5.get());
		Assert.assertEquals(Seat.SEAT6, dealerPosition6.get());
	}
	
	private Image getImage(String fileName) throws IOException {
		URL url = getClass().getResource(fileName);
		return ImageUtil.loadFxImage(url);
	}

}
