package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javafx.scene.image.Image;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsOpponentHoleCardsReader;
import pr.util.ImageUtil;

public class PokerStarsOpponentHoleCardsReaderTest {

	private PokerStarsOpponentHoleCardsReader holeCardsReader = new PokerStarsOpponentHoleCardsReader();
	
	@Test
	public void testReadActionsSize6() throws IOException {
		Image image = getImage("capture5.png");
		List<Seat> seats = holeCardsReader.readOpponentsInHand(image, TableSize.SIX);
		Assert.assertEquals(5, seats.size());
		List<Seat> expectedSeats = Arrays.asList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3, Seat.SEAT4, Seat.SEAT6);
		Assert.assertEquals(expectedSeats, seats);
	}
	
	private Image getImage(String fileName) throws IOException {
		URL url = getClass().getResource(fileName);
		return ImageUtil.loadFxImage(url);
	}
}
