package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.event.ActionObserverEvent;
import pr.infrastructure.tableobserver.event.HoleCardsObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.model.ObservedPlayer;
import pr.infrastructure.tableobserver.model.ObservedPlayerBuilder;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.infrastructure.tableobserver.model.ObservedPokerTableBuilder;
import pr.util.Optional;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class PokerStarsWindowChangeAnalyzerTest {

	private PokerStarsWindowChangeAnalyzer changeAnalyzer;
	
	private final Predicate<ObserverEvent> HOLECARDS_EVENT_PREDICATE = new Predicate<ObserverEvent>() {
		@Override
		public boolean apply(ObserverEvent input) {
			return input instanceof HoleCardsObserverEvent;
		}
	};
	
	@Before
	public void before() {
		changeAnalyzer = new PokerStarsWindowChangeAnalyzer(new PokerStarsEventReorderer());
	}
	
	@Test
	public void testNewHoleCards() {
		ObservedPlayer player = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).build();
		ObservedPokerTable previous = getTable(Seat.SEAT1, BoardCards.PREFLOP);
		ObservedPokerTable current = previous.setPlayers(Arrays.asList(player));
		
		List<ObserverEvent> changes = changeAnalyzer.analyzeChanges(Optional.<ObservedPokerTable>empty(), current);
		Assert.assertFalse(Iterables.tryFind(changes, HOLECARDS_EVENT_PREDICATE).isPresent());

		player = player.setHoleCards(new HoleCards(Card.CLUB_ACE, Card.CLUB_FIVE));
		current = current.setPlayers(Arrays.asList(player));
		changes = changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		Assert.assertTrue(Iterables.tryFind(changes, HOLECARDS_EVENT_PREDICATE).isPresent());
	}
	
	@Test
	public void testNoChanges() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.POST_SMALL_BLIND).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.POST_BIG_BLIND).build();
		ObservedPlayer seat4 = new ObservedPlayerBuilder().setSeat(Seat.SEAT4).build();
		ObservedPokerTable previous = getPreflopTable(seat1, seat2, seat3, seat4);
		
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.of(previous), previous);
		
		Assert.assertTrue(events.isEmpty());
	}
	
	@Test
	@Ignore
	public void testPostBlinds() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).build();
		ObservedPlayer seat4 = new ObservedPlayerBuilder().setSeat(Seat.SEAT4).build();
		ObservedPokerTable previous = getPreflopTable(seat1, seat2, seat3, seat4);
		
		seat2 = seat2.setLastAction(ActionType.POST_SMALL_BLIND);
		seat3 = seat3.setLastAction(ActionType.POST_BIG_BLIND);
		ObservedPokerTable current = previous.setPlayers(Lists.newArrayList(seat1, seat2, seat3, seat4));
		
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		Assert.assertEquals(2, events.size());
		assertActionObserverEvent(Seat.SEAT2, ActionType.POST_SMALL_BLIND, events.get(0));
		assertActionObserverEvent(Seat.SEAT3, ActionType.POST_BIG_BLIND, events.get(1));
	}
	
	@Test
	@Ignore
	public void testOpen() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.POST_SMALL_BLIND).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.POST_BIG_BLIND).build();
		ObservedPlayer seat4 = new ObservedPlayerBuilder().setSeat(Seat.SEAT4).build();
		ObservedPokerTable previous = getPreflopTable(seat1, seat2, seat3, seat4);
		
		seat4 = previous.getPlayer(Seat.SEAT4).get().setLastAction(ActionType.CALL);
		ObservedPokerTable current = previous.setPlayers(Lists.newArrayList(seat1, seat2, seat3, seat4));
		
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		Assert.assertEquals(2, events.size());
		assertActionObserverEvent(Seat.SEAT4, ActionType.CALL, events.get(1));
	}
	
	@Test
	public void unfinishedGameToNewGame() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).setLastAction(ActionType.POST_BIG_BLIND).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.RAISE).build();
		
		ObservedPokerTable previous = getPreflopTable();
		ObservedPokerTable current = getTable(Seat.SEAT1, BoardCards.PREFLOP, seat1, seat2);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat2 = seat2.setLastAction(ActionType.POST_BIG_BLIND);
		previous = current;
		current = getTable(Seat.SEAT2, BoardCards.PREFLOP, seat2);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
	}
	
	@Test
	public void sameActionBeforeAndAfterStreetChange() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.POST_SMALL_BLIND).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.POST_BIG_BLIND).build();
		ObservedPokerTable previous = getTable(Seat.SEAT6, BoardCards.PREFLOP);
		ObservedPokerTable current = getTable(Seat.SEAT1, BoardCards.PREFLOP, seat1, seat2, seat3);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat1 = seat1.setLastAction(ActionType.CALL);
		seat2 = seat2.setLastAction(ActionType.FOLD);
		seat3 = seat3.setLastAction(ActionType.CHECK);
		previous = current;
		current = getTable(Seat.SEAT1, BoardCards.PREFLOP, seat1, seat2, seat3);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat3 = seat3.setLastAction(ActionType.CHECK);
		seat1 = seat1.setLastAction(ActionType.BET);
		previous = current;
		current = getTable(Seat.SEAT1, BoardCards.fromString("3s 2c 4h"), seat3, seat1);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
	}
	
	@Test
	@Ignore
	public void testActionOrderAfterNewStreet() {
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.CHECK).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.BET).build();
		ObservedPlayer seat2_2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.CALL).build();
		ObservedPlayer seat2_3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.CHECK).build();
		ObservedPlayer seat3_2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.CHECK).build();
		
		ObservedPokerTable first = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js"), seat2, seat3);
		ObservedPokerTable second = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js"), seat2_2);
		ObservedPokerTable third = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js 6d"), seat2_3, seat3_2);
		
		changeAnalyzer.analyzeChanges(Optional.of(first), second);
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.of(second), third);
		
		System.out.println(events);
		Assert.assertEquals(3, events.size());
		assertActionObserverEvent(Seat.SEAT2, ActionType.CHECK, events.get(1));
		assertActionObserverEvent(Seat.SEAT3, ActionType.CHECK, events.get(2));
	}
	
	@Test
	@Ignore
	public void testNewGameOrder() {
		ObservedPlayer seat1 = new ObservedPlayerBuilder().setSeat(Seat.SEAT1).setLastAction(ActionType.POST_BIG_BLIND).build();
		ObservedPlayer seat2 = new ObservedPlayerBuilder().setSeat(Seat.SEAT2).setLastAction(ActionType.CALL).build();
		ObservedPlayer seat3 = new ObservedPlayerBuilder().setSeat(Seat.SEAT3).setLastAction(ActionType.RAISE).build();
		ObservedPlayer seat5 = new ObservedPlayerBuilder().setSeat(Seat.SEAT5).setLastAction(ActionType.FOLD).build();
		ObservedPlayer seat6 = new ObservedPlayerBuilder().setSeat(Seat.SEAT6).setLastAction(ActionType.POST_SMALL_BLIND).build();
		
		ObservedPokerTable previous = getTable(Seat.SEAT5, BoardCards.PREFLOP);
		ObservedPokerTable current = getTable(Seat.SEAT5, BoardCards.PREFLOP, seat1, seat2, seat3, seat5, seat6);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat6 = seat6.setLastAction(ActionType.RAISE);
		seat1 = seat1.setLastAction(ActionType.FOLD);
		seat2 = seat2.setLastAction(ActionType.RAISE);
		seat3 = seat3.setLastAction(ActionType.CALL);
		
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.PREFLOP, seat1, seat2, seat3, seat6);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat6 = seat6.setLastAction(ActionType.CALL);
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.PREFLOP, seat6);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat6 = seat6.setLastAction(ActionType.CHECK);
		seat2 = seat2.setLastAction(ActionType.CHECK);
		seat3 = seat3.setLastAction(ActionType.BET);

		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac"), seat2, seat3, seat6);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat6 = seat6.setLastAction(ActionType.FOLD);
		seat2 = seat2.setLastAction(ActionType.CALL);
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js"), seat2, seat6);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);

		seat2 = seat2.setLastAction(ActionType.CHECK);
		seat3 = seat3.setLastAction(ActionType.BET);
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js"), seat2, seat3);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat2 = seat2.setLastAction(ActionType.CALL);
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js 6d"), seat2);
		changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		seat2 = seat2.setLastAction(ActionType.CHECK);
		seat3 = seat3.setLastAction(ActionType.CHECK);
		previous = current;
		current = getTable(Seat.SEAT5, BoardCards.fromString("3d 6c Ac Js 6d"), seat2, seat3);
		List<ObserverEvent> events = changeAnalyzer.analyzeChanges(Optional.of(previous), current);
		
		Assert.assertEquals(2, events.size());
		assertActionObserverEvent(Seat.SEAT2, ActionType.CHECK, events.get(0));
		assertActionObserverEvent(Seat.SEAT3, ActionType.CHECK, events.get(1));
	}

	private void assertActionObserverEvent(Seat seat, ActionType actionType, ObserverEvent event) {
		Assert.assertTrue(event instanceof ActionObserverEvent);
		ActionObserverEvent actionEvent = (ActionObserverEvent) event;
		Assert.assertEquals(seat, actionEvent.getSeat());
		Assert.assertEquals(actionType, actionEvent.getActionType());
	}

	/**
	 * @return a six seat table, preflop, with seat 1 as the dealer.
	 */
	private ObservedPokerTable getPreflopTable(ObservedPlayer... players) {
		return getTable(Seat.SEAT1, BoardCards.PREFLOP, players);
	}
	
	private ObservedPokerTable getTable(Seat dealer, BoardCards boardCards, ObservedPlayer... players) {
		return new ObservedPokerTableBuilder()
			.setVenue(PokerVenue.POKERSTARS)
			.setSize(TableSize.SIX)
			.setDealerPosition(dealer)
			.setBoardCards(boardCards)
			.setPlayers(Arrays.asList(players))
			.build();		
	}
	
}
