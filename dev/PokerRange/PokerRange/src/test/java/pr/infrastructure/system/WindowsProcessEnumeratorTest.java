package pr.infrastructure.system;

import java.util.List;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

public class WindowsProcessEnumeratorTest {

	@Test
	public void getProcesses() throws Exception {
		Assume.assumeTrue(OperatingSystem.isWindows());
		
		ProcessEnumerator processEnumerator = new WindowsProcessEnumerator();
		List<ProcessInfo> processes = processEnumerator.getProcesses();
		
		Assert.assertTrue(!processes.isEmpty());
		Assert.assertTrue(processEnumerator.getProcess("taskhost.exe").isPresent());
	}

}
