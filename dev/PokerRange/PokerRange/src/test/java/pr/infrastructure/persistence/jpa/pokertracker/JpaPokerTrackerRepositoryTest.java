package pr.infrastructure.persistence.jpa.pokertracker;


import org.junit.Assert;
import org.junit.Test;

import pr.domain.poker.player.PlayerStatistics;


public class JpaPokerTrackerRepositoryTest extends PokerTrackerDatabaseTest {

	/**
	 * This should correspond to a player name in the PokerTracker database for
	 * which there is hand history data. This will test the mapping of values
	 * returned by the queries to java objects.
	 */
	private static final String PLAYER_NAME = "MRN2110";

	/**
	 * Test if the persistence unit is configured correctly, the entity manager
	 * can be created and the query can be executed. Results from the query are
	 * not tested.
	 */
	@Test
	public void getOpponentStatistics() {
		PokerTrackerRepository pokerTrackerRepository = new JpaPokerTrackerRepository(getEntityManagerProvider());
		System.out.println(pokerTrackerRepository.getOpponentStatistics(PLAYER_NAME));
	}
	
	@Test
	public void getOpponentStatisticsNullResult() {
		PokerTrackerRepository pokerTrackerRepository = new JpaPokerTrackerRepository(getEntityManagerProvider());
		PlayerStatistics playerStatistics = pokerTrackerRepository.getOpponentStatistics("User name that most likely does not exist");
		Assert.assertNull(playerStatistics);
	}
	
}
