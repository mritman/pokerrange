package pr.infrastructure.persistence.jpa;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import pr.application.preferences.Preferences;
import pr.domain.cards.Rank;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.player.Opponent;
import pr.domain.poker.player.OpponentAccount;
import pr.domain.poker.player.OpponentAccountRepository;
import pr.domain.poker.player.PlayerStatistics;
import pr.domain.poker.range.typed.HandTypeRange;
import pr.domain.poker.range.typed.TypedHandRange;
import pr.domain.poker.range.typed.types.HandTypeEnum;
import pr.domain.poker.table.PokerTableFactory;
import pr.domain.poker.table.Seat;
import pr.infrastructure.persistence.jpa.pokertracker.PokerTrackerRepository;

public class JpaOpponentAccountRepositoryTest extends JpaTest {
	
	private static final String TABLE_NAME = "title - title";
	private static final PokerVenue VENUE = PokerVenue.POKERSTARS;
	private static final String OPPONENT_NAME = "OpponentName";

	@Test
	public void testSaveAndGetOpponentAccount() {
		JpaOpponentAccountRepository repository = new JpaOpponentAccountRepository(
				getEntityManagerProvider(), mockPokerTrackerRepository(),
				Mockito.mock(Preferences.class)); 
		
		OpponentAccount opponentAccount = createOpponentAccount(repository, Collections.<TypedHandRange>emptyList());
		
		OpponentAccount retrievedOpponentAccount = repository.getOpponentAccount(opponentAccount.getName(), opponentAccount.getVenue());
		
		assertEquals(opponentAccount, retrievedOpponentAccount);
	}

	@Test
	public void testAddRange() {
		JpaOpponentAccountRepository repository = new JpaOpponentAccountRepository(
				getEntityManagerProvider(), mockPokerTrackerRepository(),
				Mockito.mock(Preferences.class)); 
		
		TypedHandRange range1 = createHandTypeRange("Range1");
		TypedHandRange range2 = createHandTypeRange("Range2");
		
		List<TypedHandRange> ranges = new LinkedList<>();
		ranges.add(range1);
		OpponentAccount opponentAccount = createOpponentAccount(repository, ranges);
		
		Opponent opponent = new Opponent(OPPONENT_NAME, PokerTableFactory.createTable(TABLE_NAME, VENUE), Seat.SEAT1);
		repository.addRange(opponent, range2);
		
		OpponentAccount updatedAccount = repository.getOpponentAccount(opponentAccount.getName(), opponentAccount.getVenue());
		
		Assert.assertEquals(2, updatedAccount.getRanges().size());
		Assert.assertTrue(updatedAccount.getRanges().contains(range1));
		Assert.assertTrue(updatedAccount.getRanges().contains(range2));
		
		Assert.assertEquals(2, opponent.getRanges().size());
		Assert.assertTrue(opponent.getRanges().values().contains(range1));
		Assert.assertTrue(opponent.getRanges().values().contains(range2));
	}
	
	@Test
	public void testGlobalRange() {
		JpaOpponentAccountRepository repository = new JpaOpponentAccountRepository(
				getEntityManagerProvider(), mockPokerTrackerRepository(),
				Mockito.mock(Preferences.class));
		
		TypedHandRange range = createHandTypeRange("Global range");
		List<TypedHandRange> ranges = new LinkedList<>();
		ranges.add(range);
		
		createOpponentAccount(repository,
				Collections.<TypedHandRange>emptyList(),
				OpponentAccountRepository.GLOBAL_RANGES_ACCOUNT_NAME,
				OpponentAccountRepository.GLOBAL_RANGES_ACCOUNT_VENUE);
		
		repository.addGlobalRange(range);
		
		Collection<TypedHandRange> globalRanges = repository.getGlobalRanges();
		
		Assert.assertEquals(1, globalRanges.size());
		Assert.assertTrue(globalRanges.contains(range));
		
		repository.deleteGlobalRange(range);
		
		globalRanges = repository.getGlobalRanges();
		
		Assert.assertTrue(globalRanges.isEmpty());
	}
	
	@Test
	public void testDeleteRange() {
		JpaOpponentAccountRepository repository = new JpaOpponentAccountRepository(
				getEntityManagerProvider(), mockPokerTrackerRepository(),
				Mockito.mock(Preferences.class));
		
		TypedHandRange range1 = createHandTypeRange("Range1");
		TypedHandRange range2 = createHandTypeRange("Range2");
		List<TypedHandRange> ranges = new LinkedList<>();
		Collections.addAll(ranges, range1, range2);
		OpponentAccount opponentAccount = createOpponentAccount(repository, ranges);
		
		Opponent opponent = new Opponent(OPPONENT_NAME, PokerTableFactory.createTable(TABLE_NAME, VENUE), Seat.SEAT1);
		repository.deleteRange(opponent, range2);
		
		OpponentAccount updatedAccount = repository.getOpponentAccount(opponentAccount.getName(), opponentAccount.getVenue());
		
		Assert.assertEquals(1, updatedAccount.getRanges().size());
		Assert.assertTrue(updatedAccount.getRanges().contains(range1));
		Assert.assertFalse(updatedAccount.getRanges().contains(range2));
		
		Assert.assertEquals(1, opponent.getRanges().size());
		Assert.assertTrue(opponent.getRanges().values().contains(range1));
		Assert.assertFalse(opponent.getRanges().values().contains(range2));
	}
	
	@Test
	public void getOpponent() {
		JpaOpponentAccountRepository repository = new JpaOpponentAccountRepository(
				getEntityManagerProvider(), mockPokerTrackerRepository(),
				Mockito.mock(Preferences.class));
		
		TypedHandRange range1 = createHandTypeRange("Range1");
		TypedHandRange range2 = createHandTypeRange("Range2");
		List<TypedHandRange> ranges = new LinkedList<>();
		Collections.addAll(ranges, range1, range2);
		OpponentAccount opponentAccount = createOpponentAccount(repository, ranges);
		
		Opponent opponent = repository.getOpponent(OPPONENT_NAME, PokerTableFactory.createTable(TABLE_NAME, VENUE), Seat.SEAT1);
		
		Assert.assertEquals(ranges.size(), opponentAccount.getRanges().size());
		Assert.assertEquals(opponentAccount.getRanges().size(), opponent.getRanges().size());
		for (TypedHandRange range : ranges) {
			Assert.assertTrue(opponent.getRanges().containsValue(range));
			Assert.assertTrue(opponentAccount.getRanges().contains(range));
		}
	}

	private OpponentAccount createOpponentAccount(JpaOpponentAccountRepository repository, Collection<TypedHandRange> ranges) {
		return createOpponentAccount(repository, ranges, OPPONENT_NAME, VENUE);
	}
	
	private OpponentAccount createOpponentAccount(JpaOpponentAccountRepository repository, Collection<TypedHandRange> ranges, String name, PokerVenue venue) {
		OpponentAccount opponentAccount = new OpponentAccount(name, venue);
		
		for (TypedHandRange range : ranges) {
			opponentAccount.addRange(range);
		}
		
		repository.saveOpponentAccount(opponentAccount);
		
		return opponentAccount;
	}

	private TypedHandRange createHandTypeRange(String name) {
		TypedHandRange handTypeRange = new TypedHandRange(name);
		
		HandTypeRange handTypeRange1 = new HandTypeRange(Rank.TWO, Rank.ACE);
		HandTypeRange handTypeRange2 = new HandTypeRange(Rank.THREE, Rank.KING);
		
		handTypeRange.setHandTypeRange(HandTypeEnum.ACE_X, handTypeRange1);
		handTypeRange.setHandTypeRange(HandTypeEnum.PAIR, handTypeRange2);
		
		return handTypeRange;
	}

	private void assertEquals(OpponentAccount expected, OpponentAccount actual) {
		Assert.assertEquals(expected, actual);
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getVenue(), actual.getVenue());
		
		Assert.assertEquals(expected.getRanges().size(), actual.getRanges().size());
		Iterator<TypedHandRange> actualIterator = actual.getRanges().iterator();
		for (TypedHandRange expectedRange : expected.getRanges()) {
			assertEquals(expectedRange, actualIterator.next());
		}
	}

	private void assertEquals(TypedHandRange expected, TypedHandRange actual) {
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getRange().size(), actual.getRange().size());
		
		Iterator<Entry<HandTypeEnum, HandTypeRange>> iterator = actual.getRange().entrySet().iterator();
		for (Entry<HandTypeEnum, HandTypeRange> entry : expected.getRange().entrySet()) {
			Entry<HandTypeEnum, HandTypeRange> next = iterator.next();
			Assert.assertEquals(entry.getKey(), next.getKey());
			assertEquals(entry.getValue(), next.getValue());
		}
	}

	private void assertEquals(HandTypeRange expected, HandTypeRange actual) {
		Assert.assertEquals(expected.getLower(), actual.getLower());
		Assert.assertEquals(expected.getUpper(), actual.getUpper());
	}
	
	private static PokerTrackerRepository mockPokerTrackerRepository() {
		return new PokerTrackerRepository() {
			
			@Override
			public PlayerStatistics getOpponentStatistics(String name) {
				return null;
			}

		};
	}
	
}
