package pr.infrastructure.persistence.jpa.pokertracker;

import java.util.Properties;

import javax.inject.Provider;
import javax.persistence.EntityManager;

import lombok.Getter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

public class PokerTrackerDatabaseTest {
	private static Injector injector;
	
	@Getter
	private static EntityManager entityManager;
	
	@Getter
	private static Provider<EntityManager> entityManagerProvider;
	
	@BeforeClass
	public static void beforeClass() {
		JpaPersistModule jpaPersistModule = new JpaPersistModule(PokerTrackerRepositoryModule.POKER_TRACKER_PERSISTENCE_UNIT_NAME);
		Properties properties = new Properties();
		properties.setProperty("javax.persistence.jdbc.password", "dbpass");
		
		jpaPersistModule.properties(properties);
		
		injector = Guice.createInjector(jpaPersistModule);
		injector.getInstance(PersistService.class).start();
		entityManager = injector.getInstance(EntityManager.class);
		entityManagerProvider = new Provider<EntityManager>() {
			@Override
			public EntityManager get() {
				return entityManager;
			}
		};
	}
	
	@AfterClass
	public static void afterClass() {
		injector.getInstance(PersistService.class).stop();
	}
	
	@Before
	public void before() {
		entityManager.getTransaction().begin();
	}
	
	@After
	public void after() {
		entityManager.getTransaction().rollback();
	}
}
