package pr.infrastructure.tableobserver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pr.application.exception.InitializationException;
import pr.application.preferences.FilePreferences;
import pr.application.preferences.Preferences;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.PokerEventUpdate;
import pr.domain.poker.table.PokerTableKey;
import pr.infrastructure.tableobserver.service.TableListener;
import pr.infrastructure.tableobserver.service.TableObserverService;
import pr.infrastructure.tableobserver.service.TablesUpdate;
import pr.ui.javafx.preferences.PreferencesPresenter;
import pr.ui.javafx.util.StageUtil;
import pr.util.Optional;

import com.google.common.base.Preconditions;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;

public class TableObserverTester extends Application {

	private TableObserverService observerService;
	
	private Parent view;
	
	@FXML private ComboBox<String> tableComboBox;
	@FXML private TextArea textArea;
	@FXML private Button preferencesButton;
	
	private final Map<String, PokerTableKey> nameToKey = new HashMap<>();

	private PreferencesPresenter preferencesPresenter;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		Injector injector = Guice.createInjector(new DependenciesModule(), new TableObserverModule());
		observerService = injector.getInstance(TableObserverService.class);
		Preferences preferences = injector.getInstance(Preferences.class);
		preferencesPresenter = new PreferencesPresenter(preferences);
		
		view = initView();
		registerWithObserverService();
		observerService.startService();
		
		stage.setScene(new Scene(view));
        stage.show();
	}

	@Override
	public void stop() {
		observerService.stopService();
	}
	
	private void registerWithObserverService() {
		tableComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg, String arg1, String arg2) {
				if (!Optional.ofNullable(arg1).equals(arg2) && arg2 != null) {
					observerService.selectTable(nameToKey.get(arg2));
				}
			}
		});
		
		observerService.addTableListener(new TableListener() {
			@Override
			public void tablesUpdated(final TablesUpdate tablesUpdate) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						updateTables(tablesUpdate);
					}
				});
			}

			@Override
			public void tableStateChanged(final PokerEventUpdate tableUpdate) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						for (PokerEvent event : tableUpdate.getEvents()) {
							textArea.appendText("\n" + event.toString());
						}						
					}
				});
			}
		});
	}
	
	private void updateTables(TablesUpdate tablesUpdate) {
		for (PokerTableKey tableKey : tablesUpdate.getTablesClosed()) {
			tableComboBox.getItems().remove(tableKey.getName());
			nameToKey.remove(tableKey.getName());
		}
		
		for (PokerTableKey tableKey : tablesUpdate.getTablesFound()) {
			tableComboBox.getItems().add(tableKey.getName());
			nameToKey.put(tableKey.getName(), tableKey);
		}
		
		boolean invalidSelection = tableComboBox.getValue() == null || !tableComboBox.getItems().contains(tableComboBox.getValue());
		if (invalidSelection && !tableComboBox.getItems().isEmpty()) {
			tableComboBox.getSelectionModel().select(0);
		}
	}
	
	private class DependenciesModule extends AbstractModule {
		@Override
		protected void configure() {
			bind(Preferences.class).to(FilePreferences.class).in(Singleton.class);
		}
	}
	
	private AnchorPane initView() {
		AnchorPane anchorPane = new AnchorPane();
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TableObserverTesterView.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(anchorPane);
        
        try {
            fxmlLoader.load();
            Preconditions.checkNotNull("tableComboBox", tableComboBox);
            Preconditions.checkNotNull("textArea", textArea);
            Preconditions.checkNotNull("preferencesButton", preferencesButton);
        } catch (IOException exception) {
            throw new InitializationException(exception);
        }
        
        listenToPreferencesButton();
        
        return anchorPane;
	}

	private void listenToPreferencesButton() {
		preferencesButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				StageUtil.show(preferencesPresenter.getView(), view.getScene().getWindow());
			}
			
		});
	}

}
