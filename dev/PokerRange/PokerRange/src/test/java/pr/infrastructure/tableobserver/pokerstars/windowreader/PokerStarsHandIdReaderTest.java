package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;

import javafx.scene.image.Image;

import org.junit.Assert;
import org.junit.Test;

import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsHandIdReader;
import pr.util.ImageUtil;

public class PokerStarsHandIdReaderTest {

	private final PokerStarsHandIdReader reader = new PokerStarsHandIdReader();
	
	@Test
	public void testEmptyHand() throws IOException {
		Image image = getImage("capture9.png");
		String handId = reader.readHandId(image);
		Assert.assertEquals(PokerStarsHandIdReader.getEmptyHandId(), handId);
	}
	
	private Image getImage(String fileName) throws IOException {
		URL url = getClass().getResource(fileName);
		return ImageUtil.loadFxImage(url);
	}
}
