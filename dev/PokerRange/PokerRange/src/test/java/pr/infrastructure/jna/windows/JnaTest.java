package pr.infrastructure.jna.windows;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pr.infrastructure.jna.windows.WindowCapturer;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;

public class JnaTest extends Application{
	
	public static final String POKER_STARS_TABLE_FRAME_CLASS = "PokerStarsTableFrameClass";
	public static final int MAX_WINDOW_CLASS_NAME_SIZE = 256;
	
	private static Image image;
	
	private static WindowCapturer windowCapturer = new WindowCapturer();
	
	static WNDENUMPROC windowEnumProc = new WNDENUMPROC() {

		@Override
		public boolean callback(HWND hWnd, Pointer lParam) {
			if (hWnd == null) return true; 
			
			int windowTextLength = User32.INSTANCE.GetWindowTextLength(hWnd);
			
			// +1 to include room for the terminating character.
			char[] windowText = new char[windowTextLength+1];
			int result = User32.INSTANCE.GetWindowText(hWnd, windowText, windowTextLength+1);
			if (result == 0) {
				return true;
			}

			String title = new String(windowText);
			
			boolean WindowVisible = User32.INSTANCE.IsWindowVisible(hWnd);
			int windowStyle = User32.INSTANCE.GetWindowLong(hWnd, WinUser.GWL_STYLE);

			boolean hasChildStyle = (windowStyle & WinUser.WS_CHILD) == 0 ? false : true;
			
			if (hasChildStyle || !WindowVisible) {
				return true;
			}
			
			HWND owner = User32.INSTANCE.GetWindow(hWnd, new DWORD(Long.valueOf(WinUser.GW_OWNER)));
			if (owner != null) {
				return true;
			}
			
			
			char[] className = new char[MAX_WINDOW_CLASS_NAME_SIZE]; 
			result = User32.INSTANCE.GetClassName(hWnd, className, MAX_WINDOW_CLASS_NAME_SIZE);
			if (result == 0) {
				System.err.println("Failed to get the window class name");
			}
			
			String windowClassName = new String(className).trim();
			if (!POKER_STARS_TABLE_FRAME_CLASS.equals(windowClassName)) {
				return true;
			}
			
			if (!title.isEmpty()) {
				System.out.println(title);
				
				image = windowCapturer.capture(hWnd);
			}
			
			return true;
		}

	};

	public static void main(String[] args) {
		User32.INSTANCE.EnumWindows(windowEnumProc, null);
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		ImageView imageView = new ImageView();
		AnchorPane anchorPane = new AnchorPane();
		anchorPane.getChildren().add(imageView);
		
        imageView.setImage(image);
		
        Scene scene = new Scene(anchorPane);
        Stage stage = new Stage();
        stage.setScene(scene);
        
        stage.sizeToScene();
        stage.show();
	}
	
}
