package pr.infrastructure.persistence.jpa;

import javax.inject.Provider;
import javax.persistence.EntityManager;

import lombok.Getter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import pr.test.TestConstants;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

public abstract class JpaTest {
	
	private static Injector injector;
	
	@Getter
	private static EntityManager entityManager;
	
	@Getter
	private static Provider<EntityManager> entityManagerProvider;
	
	@BeforeClass
	public static void beforeClass() {
		injector = Guice.createInjector(new JpaPersistModule(TestConstants.TEST_PERSISTENCE_UNIT_NAME));
		injector.getInstance(PersistService.class).start();
		entityManager = injector.getInstance(EntityManager.class);
		entityManagerProvider = new Provider<EntityManager>() {
			@Override
			public EntityManager get() {
				return entityManager;
			}
		};
	}
	
	@AfterClass
	public static void afterClass() {
		injector.getInstance(PersistService.class).stop();
	}
	
	
	@Before
	public void before() {
		entityManager.getTransaction().begin();
	}
	
	@After
	public void after() {
		entityManager.getTransaction().rollback();
	}
	
	
}
