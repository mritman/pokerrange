package pr.infrastructure.tableobserver.pokerstars.chatreader;

import java.util.Arrays;
import java.util.List;



import org.junit.Assert;
import org.junit.Test;


public class PokerStarsChatLineBatchSplitterTest {

	private static final List<String> batch = Arrays.asList(
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)",
			"Dealer: Starting new hand: #108157129472",
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)",
			"Dealer: Starting new hand: #108157129472",
			"Dealer: PokerNolifer posts small blind $0.01"
			);
	
	@Test
	public void testSplit() {
		List<List<String>> batches = PokerStarsChatLineBatchSplitter.splitBatchesWithDuplicates(Arrays.asList(batch));
		Assert.assertEquals(2, batches.size());
		Assert.assertEquals(2, batches.get(0).size());
		Assert.assertEquals(3, batches.get(1).size());
		Assert.assertEquals(batch.subList(0, 2), batches.get(0));		
		Assert.assertEquals(batch.subList(2, 5), batches.get(1));
	}

}
