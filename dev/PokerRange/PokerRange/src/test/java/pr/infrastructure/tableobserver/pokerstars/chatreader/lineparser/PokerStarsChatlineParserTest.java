package pr.infrastructure.tableobserver.pokerstars.chatreader.lineparser;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.Flop;
import pr.domain.poker.event.NewGameEvent;
import pr.domain.poker.event.PokerEvent;
import pr.domain.poker.event.action.BetEvent;
import pr.domain.poker.event.action.BigBlindEvent;
import pr.domain.poker.event.action.CallEvent;
import pr.domain.poker.event.action.CheckEvent;
import pr.domain.poker.event.action.FoldEvent;
import pr.domain.poker.event.action.RaiseEvent;
import pr.domain.poker.event.action.SmallBlindEvent;
import pr.domain.poker.event.board.FlopEvent;
import pr.domain.poker.event.board.RiverEvent;
import pr.domain.poker.event.board.TurnEvent;


public class PokerStarsChatlineParserTest {
	
	private PokerStarsChatlineParser parser;
	
	@Before
	public void before() {
		parser = new PokerStarsChatlineParser();
	}
	
	@Test
	public void testNewGameLine() {
		PokerEvent event = parser.parse("Dealer: Starting new hand: #108206985076").get();
		Assert.assertTrue(event instanceof NewGameEvent);
	}
	
	@Test
	public void testSmallBlind() {
		PokerEvent event = parser.parse("Dealer: valerakazak posts small blind $0.01").get();
		Assert.assertTrue(event instanceof SmallBlindEvent);
		SmallBlindEvent smallBlindEvent = (SmallBlindEvent) event;
		Assert.assertEquals("valerakazak", smallBlindEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.01), smallBlindEvent.getAmount());
	}
	
	@Test
	public void testBigBlind() {
		PokerEvent event = parser.parse("Dealer: lpglpg posts big blind $0.02").get();
		Assert.assertTrue(event instanceof BigBlindEvent);
		BigBlindEvent bigBlindEvent = (BigBlindEvent) event;
		Assert.assertEquals("lpglpg", bigBlindEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), bigBlindEvent.getAmount());
	}
	
	@Test
	public void testFold() {
		PokerEvent event = parser.parse("Dealer: SimaDen folds").get();
		Assert.assertTrue(event instanceof FoldEvent);
		FoldEvent foldChatEvent = (FoldEvent) event;
		Assert.assertEquals("SimaDen", foldChatEvent.getName());
	}
	
	@Test
	public void testRaise() {
		PokerEvent event = parser.parse("Dealer: satyi raises $0.02 to $0.04").get();
		Assert.assertTrue(event instanceof RaiseEvent);
		RaiseEvent raiseEvent = (RaiseEvent) event;
		Assert.assertEquals("satyi", raiseEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), raiseEvent.getFrom());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.04), raiseEvent.getTo());
	}
	
	@Test
	public void testRaiseAllIn() {
		PokerEvent event = parser.parse("Dealer: manjachka57 raises $0.32 to $0.60 and is all-in").get();
		Assert.assertTrue(event instanceof RaiseEvent);
		RaiseEvent raiseEvent = (RaiseEvent) event;
		Assert.assertEquals("manjachka57", raiseEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.32), raiseEvent.getFrom());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.60), raiseEvent.getTo());
	}
	
	@Test
	public void testCall() {
		PokerEvent event = parser.parse("Dealer: lpglpg calls $0.02").get();
		Assert.assertTrue(event instanceof CallEvent);
		CallEvent callEvent = (CallEvent) event;
		Assert.assertEquals("lpglpg", callEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), callEvent.getAmount());
	}
	
	@Test
	public void testCallAllIn() {
		PokerEvent event = parser.parse("Dealer: lpglpg calls $0.02 and is all-in").get();
		Assert.assertTrue(event instanceof CallEvent);
		CallEvent callEvent = (CallEvent) event;
		Assert.assertEquals("lpglpg", callEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), callEvent.getAmount());
	}

	@Test
	public void testCheck() {
		PokerEvent event = parser.parse("Dealer: lpglpg checks").get();
		Assert.assertTrue(event instanceof CheckEvent);
		CheckEvent checkEvent = (CheckEvent) event;
		Assert.assertEquals("lpglpg", checkEvent.getName());
	}
	
	@Test
	public void testBet() {
		PokerEvent event = parser.parse("Dealer: satyi bets $0.02").get();
		BetEvent betEvent = (BetEvent) event;
		Assert.assertEquals("satyi", betEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), betEvent.getAmount());
	}
	
	@Test
	public void testBetAllIn() {
		PokerEvent event = parser.parse("Dealer: satyi bets $0.02 and is all-in").get();
		BetEvent betEvent = (BetEvent) event;
		Assert.assertEquals("satyi", betEvent.getName());
		Assert.assertEquals(Money.of(CurrencyUnit.USD, 0.02), betEvent.getAmount());
	}
	
	@Test
	public void testFlop() {
		PokerEvent event = parser.parse("Dealer: Dealing Flop: [Qs 4c 8c]").get();
		Assert.assertTrue(event instanceof FlopEvent);
		FlopEvent foldChatEvent = (FlopEvent) event;
		Flop expectedFlop = Flop.fromString("Qs 4c 8c");
		Assert.assertEquals(expectedFlop, foldChatEvent.getFlop());
	}
	
	@Test
	public void testTurn() {
		PokerEvent event = parser.parse("Dealer: Dealing Turn: [Kd]").get();
		Assert.assertTrue(event instanceof TurnEvent);
		TurnEvent turnEvent = (TurnEvent) event;
		Assert.assertEquals(Card.DIAMOND_KING, turnEvent.getTurn());
	}
	
	@Test
	public void testRiver() {
		PokerEvent event = parser.parse("Dealer: Dealing River: [Qh]").get();
		Assert.assertTrue(event instanceof RiverEvent);
		RiverEvent turnEvent = (RiverEvent) event;
		Assert.assertEquals(Card.HEART_QUEEN, turnEvent.getRiver());
	}
	
	@Test
	public void testTimeout() {
		PokerEvent event = parser.parse("Dealer: Knevitz C. has timed out - hand is folded").get();
		Assert.assertTrue(event instanceof FoldEvent);
		FoldEvent foldEvent = (FoldEvent) event;
		Assert.assertEquals("Knevitz C.", foldEvent.getName());
	}

}
