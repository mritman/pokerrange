package pr.infrastructure.jna.windows;


import org.junit.Assert;
import org.junit.Test;

import com.sun.jna.platform.win32.WinDef.DWORD;

public class PsapiTest {

	@Test
	public void EnumProcesses() throws Exception {
		DWORD[] pProcessIds = new DWORD[Psapi.MAX_RUNNING_PROCESSES];
		DWORD cb = new DWORD(Psapi.MAX_RUNNING_PROCESSES);
		DWORD[] pBytesReturned = new DWORD[1];
		
		boolean success = Psapi.INSTANCE.EnumProcesses(pProcessIds, cb, pBytesReturned);
		Assert.assertTrue(success);
	}

}
