package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;

import javafx.scene.image.Image;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.model.ObservedPlayer;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsWindowStateReader;
import pr.util.ImageUtil;
import pr.util.Optional;

public class PokerStarsWindowStateReaderTest {

	private static final String TABLE_NAME = "Table1";
	
	private final PokerStarsWindowStateReader windowReader;
	
	public PokerStarsWindowStateReaderTest() {
		PokerStarsHoleAndBoardCardsReader holeAndBoardCardsReader = new PokerStarsHoleAndBoardCardsReader(
				new PokerStarsCardReader());
		PokerStarsActionReader actionReader = new PokerStarsActionReader();
		PokerStarsOpponentHoleCardsReader opponentHoleCardsReader = new PokerStarsOpponentHoleCardsReader();
		PokerStarsDealerButtonReader dealerButtonReader = new PokerStarsDealerButtonReader();
		
		windowReader = new PokerStarsWindowStateReader(holeAndBoardCardsReader,
				actionReader, opponentHoleCardsReader, dealerButtonReader);
	}
	
	@Test
	public void testSizeSix() throws IOException {
		Image image = getImage("capture8.png");
		
		ObservedPokerTable table = windowReader.read(image, TableSize.SIX, TABLE_NAME, PokerVenue.POKERSTARS).get();
		
		BoardCards expectedBoardCards = new BoardCards(Card.DIAMOND_NINE, Card.DIAMOND_TWO, Card.SPADE_QUEEN, null, null);
		
		Assert.assertEquals(TableSize.SIX, table.getSize());
		Assert.assertEquals(expectedBoardCards, table.getBoardCards());
		Assert.assertEquals(3, table.getPlayers().size());
		Assert.assertEquals(Seat.SEAT6, table.getDealer());
		assertEquals(Seat.SEAT4, ActionType.CALL, null, table.getPlayers().get(0));
		assertEquals(Seat.SEAT5, ActionType.CHECK, null, table.getPlayers().get(1));
		assertEquals(Seat.SEAT6, ActionType.FOLD, null, table.getPlayers().get(2));
	}
	
	private void assertEquals(Seat expectedSeat, 
			ActionType expectedAction,
			Optional<HoleCards> expectedHoleCards,
			ObservedPlayer player) {
		
		Assert.assertEquals(expectedSeat, player.getSeat());
		Assert.assertEquals(Optional.ofNullable(expectedAction), player.getLastAction());
		Assert.assertEquals(Optional.ofNullable(expectedHoleCards), player.getHoleCards());
	}

	private Image getImage(String fileName) throws IOException {
		URL url = getClass().getResource(fileName);
		return ImageUtil.loadFxImage(url);
	}

}
