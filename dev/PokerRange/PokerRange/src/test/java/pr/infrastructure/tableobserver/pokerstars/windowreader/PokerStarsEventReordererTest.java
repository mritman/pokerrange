package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.PokerVenue;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.game.GameException;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.event.ActionObserverEvent;
import pr.infrastructure.tableobserver.event.FlopObserverEvent;
import pr.infrastructure.tableobserver.event.NewDealerObserverEvent;
import pr.infrastructure.tableobserver.event.ObserverEvent;
import pr.infrastructure.tableobserver.event.SeatsInHandObserverEvent;
import pr.infrastructure.tableobserver.model.ObservedPokerTable;
import pr.infrastructure.tableobserver.model.ObservedPokerTableBuilder;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsEventReorderer;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsEventReorderer.Result;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsEventReorderer.ResultType;

import com.google.common.collect.Lists;

public class PokerStarsEventReordererTest {

	private final PokerStarsEventReorderer reorderer = new PokerStarsEventReorderer();
	
	@Test
	public void testReorder() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2);
		ObservedPokerTable table = createTable();
		
		SeatsInHandObserverEvent seatsInHandEvent = new SeatsInHandObserverEvent(new HashSet<>(seats));
		ActionObserverEvent callActionEvent = new ActionObserverEvent(Seat.SEAT2, ActionType.CALL);
		FlopObserverEvent flopEvent = new FlopObserverEvent(table.getBoardCards());
		ActionObserverEvent checkAction = new ActionObserverEvent(Seat.SEAT1, ActionType.CHECK);
		
		List<ObserverEvent> incorrectOrderEvents = Lists.newArrayList(seatsInHandEvent, callActionEvent, flopEvent, checkAction);
		List<ObserverEvent> expectedOrderEvents = Lists.newArrayList(seatsInHandEvent, callActionEvent, checkAction, flopEvent);
		
		Result result = reorderer.reorderIfNecessary(table.getKey(), table.getDealer(), incorrectOrderEvents);

		Assert.assertEquals(ResultType.REORDERED, result.getType());
		Assert.assertEquals(expectedOrderEvents, result.getEvents());
	}
	
	@Test
	public void testFromGameToNewGame() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2);
		ObservedPokerTable table = createTable();
		
		SeatsInHandObserverEvent seatsInHandEvent = new SeatsInHandObserverEvent(new HashSet<>(seats));
		ActionObserverEvent foldAction = new ActionObserverEvent(Seat.SEAT2, ActionType.FOLD);
		NewDealerObserverEvent newDealerEvent = new NewDealerObserverEvent(Seat.SEAT2);
		ActionObserverEvent foldAction2 = new ActionObserverEvent(Seat.SEAT1, ActionType.FOLD);
		
		
		List<ObserverEvent> events = Lists.newArrayList(seatsInHandEvent, foldAction, newDealerEvent, seatsInHandEvent, foldAction2);
		
		Result result = reorderer.reorderIfNecessary(table.getKey(), table.getDealer(), events);
		
		Assert.assertEquals(ResultType.NO_CHANGE_REQUIRED, result.getType());
		Assert.assertEquals(events, result.getEvents());
	}

	private ObservedPokerTable createTable() {
		BoardCards boardCards = new BoardCards(Lists.newArrayList(Card.CLUB_ACE, Card.CLUB_EIGHT, Card.CLUB_FIVE));
		
		ObservedPokerTable table = new ObservedPokerTableBuilder()
				.setBoardCards(boardCards).setDealerPosition(Seat.SEAT1)
				.setName("TableName").setSize(TableSize.SIX)
				.setHandId(UUID.randomUUID()).setVenue(PokerVenue.POKERSTARS)
				.build();
		return table;
	}
	
}
