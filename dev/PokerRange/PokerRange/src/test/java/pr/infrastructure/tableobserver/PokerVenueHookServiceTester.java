package pr.infrastructure.tableobserver;

import java.util.List;

import pr.domain.poker.PokerVenue;
import pr.infrastructure.system.WindowsProcessEnumerator;
import pr.infrastructure.tableobserver.pokerstars.chatreader.PokerStarsDuplicateChatlineRemover;


public class PokerVenueHookServiceTester {

	public void start() {
		PokerVenueHookService service = new PokerVenueHookService(new WindowsProcessEnumerator());
		PokerStarsDuplicateChatlineRemover duplicateChatlineRemover = new PokerStarsDuplicateChatlineRemover();
		
		while (true) {
			List<List<String>> lineBatches = service.getChatLines(PokerVenue.POKERSTARS);
			List<String> lines = duplicateChatlineRemover.getLines(lineBatches);
			
			for (String line : lines) {
				System.out.println(line);
			}
		}
	}

	public static void main(String[] args) {
		new PokerVenueHookServiceTester().start();
	}
}
