package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import javafx.scene.image.Image;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsActionReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsActionReader.ActionResult;
import pr.util.ImageUtil;

public class PokerStarsActionReaderTest {
	
	private PokerStarsActionReader actionReader = new PokerStarsActionReader();
	
	@Test
	public void testReadActionsSize6() throws IOException {
		Image image = getImage("capture5.png");
		List<ActionResult> actions = actionReader.readActions(image, TableSize.SIX);
		Assert.assertEquals(4, actions.size());
		assertEquals(Seat.SEAT2, ActionType.POST_SMALL_BLIND, actions.get(0));
	}
	
	private void assertEquals(Seat expectedSeat, ActionType expectedActionType, ActionResult action) {
		Assert.assertEquals(expectedSeat, action.getSeat());
		Assert.assertEquals(expectedActionType, action.getActionType());
	}

	@Test
	public void testPostSmallBlindSize6Seat1() throws IOException {
		Image image = getImage("capture7.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT1).get();
		Assert.assertEquals(ActionType.POST_SMALL_BLIND, actionType);
	}
	
	@Test
	public void testPostSmallBlindSize6Seat6() throws IOException {
		Image image = getImage("capture2.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT6).get();
		Assert.assertEquals(ActionType.POST_SMALL_BLIND, actionType);
	}
	
	@Test
	public void testPostBigBlindSize6Seat1() throws IOException {
		Image image = getImage("capture2.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT1).get();
		Assert.assertEquals(ActionType.POST_BIG_BLIND, actionType);
	}
	
	@Test
	public void testFoldSize6Seat3() throws IOException {
		Image image = getImage("capture2.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT3).get();
		Assert.assertEquals(ActionType.FOLD, actionType);
	}
	
	@Test
	public void testCheckSize6Seat4() throws IOException {
		Image image = getImage("capture3.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT4).get();
		Assert.assertEquals(ActionType.CHECK, actionType);
	}
	
	@Test
	public void testCheckSize6Seat5() throws IOException {
		Image image = getImage("capture3.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT5).get();
		Assert.assertEquals(ActionType.CHECK, actionType);
	}
	
	@Test
	public void testBetSize6Seat4() throws IOException {
		Image image = getImage("capture2.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT4).get();
		Assert.assertEquals(ActionType.BET, actionType);
	}
	
	@Test
	public void testCallSize6Seat3() throws IOException {
		Image image = getImage("capture3.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT3).get();
		Assert.assertEquals(ActionType.CALL, actionType);
	}
	
	@Test
	public void testCallSize6Seat4() throws IOException {
		Image image = getImage("capture8.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT4).get();
		Assert.assertEquals(ActionType.CALL, actionType);
	}
	
	@Test
	public void testRaiseSize6Seat4() throws IOException {
		Image image = getImage("capture5.png");
		ActionType actionType = actionReader.readAction(image, TableSize.SIX, Seat.SEAT4).get();
		Assert.assertEquals(ActionType.RAISE, actionType);
	}

	private Image getImage(String fileName) throws IOException {
		URL url = getClass().getResource(fileName);
		return ImageUtil.loadFxImage(url);
	}

}
