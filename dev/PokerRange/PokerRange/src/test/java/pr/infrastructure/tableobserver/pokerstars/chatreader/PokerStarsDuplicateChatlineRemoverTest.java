package pr.infrastructure.tableobserver.pokerstars.chatreader;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class PokerStarsDuplicateChatlineRemoverTest {

	private static final List<String> batch1 = Arrays.asList(
			"Dealer: Starting new hand: #108151944751",
			"Dealer: briarboy11 posts small blind $0.01");

	private static final List<String> batch2 = Arrays.asList(
			"Dealer: Starting new hand: #108151944751",
			"Dealer: briarboy11 posts small blind $0.01",
			"Dealer: MRN2110 posts big blind $0.02");
	
	private static final List<String> batch3 = Arrays.asList(
			"Dealer: briarboy11 posts small blind $0.01",
			"Dealer: MRN2110 posts big blind $0.02",
			"Dealer: Dealing Hole Cards");
	
	private static final List<String> batch4 = Arrays.asList(
			"Dealer: Uncalled bet ($0.01) returned to zi49"
			);
	
	private static final List<String> batch5 = Arrays.asList(
			"Dealer: Uncalled bet ($0.01) returned to zi49",
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)",
			"Dealer: Uncalled bet ($0.01) returned to zi49",
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)"
			);
	
	private static final List<String> batch6 = Arrays.asList(
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)",
			"Dealer: Starting new hand: #108157129472",
			"Dealer: Hand #108157294801: zi49 wins pot ($0.02)",
			"Dealer: Starting new hand: #108157129472",
			"Dealer: PokerNolifer posts small blind $0.01"
			);
	
	private PokerStarsDuplicateChatlineRemover chatReader;
	
	@Before
	public void before() {
		chatReader = new PokerStarsDuplicateChatlineRemover();
	}
	
	@Test
	public void testFirstBatch() {
		List<String> lines = chatReader.getLines(Arrays.asList(batch1));
		Assert.assertEquals(batch1, lines);
	}
	
	@Test
	public void testSecondBatch() {
		chatReader.getLines(Arrays.asList(batch1));
		List<String> lines = chatReader.getLines(Arrays.asList(batch2));

		Assert.assertEquals(1, lines.size());
		Assert.assertEquals(batch2.get(2), lines.get(0));
	}
	
	@Test
	public void testDuplicateBatch() {
		chatReader.getLines(Arrays.asList(batch1));
		List<String> lines = chatReader.getLines(Arrays.asList(batch1));

		Assert.assertTrue(lines.isEmpty());
	}
	
	@Test
	public void testNewDuplicateBatchStart() {
		chatReader.getLines(Arrays.asList(batch2));
		List<String> lines = chatReader.getLines(Arrays.asList(batch3));
		
		Assert.assertEquals(1, lines.size());
		Assert.assertEquals(batch3.get(2), lines.get(0));
	}

	@Test
	public void testNextBatchContainsDuplicates() {
		List<String> lines = chatReader.getLines(Arrays.asList(batch4, batch5));
		Assert.assertEquals(2, lines.size());
		Assert.assertEquals(batch4.get(0), lines.get(0));
		Assert.assertEquals(batch5.get(1), lines.get(1));
	}
	
	@Test
	public void testPreviousBatchContainsDuplicates() {
		List<String> lines = chatReader.getLines(Arrays.asList(batch5, batch6));
		Assert.assertEquals(4, lines.size());
		Assert.assertEquals(batch5.get(0), lines.get(0));
		Assert.assertEquals(batch5.get(1), lines.get(1));
		Assert.assertEquals(batch6.get(1), lines.get(2));
		Assert.assertEquals(batch6.get(4), lines.get(3));
	}

}
