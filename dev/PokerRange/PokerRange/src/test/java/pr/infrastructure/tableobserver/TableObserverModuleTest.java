package pr.infrastructure.tableobserver;

import org.junit.Test;
import org.mockito.Mockito;

import pr.application.preferences.Preferences;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;

public class TableObserverModuleTest {

	@Test
	public void testModuleConfig() {
		Guice.createInjector(new DependenciesModule(), new TableObserverModule());
	}
	
	private class DependenciesModule extends AbstractModule {
		@Override
		protected void configure() {
			bind(Preferences.class).toInstance(Mockito.mock(Preferences.class));
		}
	}
}
