package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Rank;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class StraightDrawTest {

	private static final int CARD_INDEX = 0;
	private static final int SD1_INDEX = 1;
	private static final int SD2_INDEX = 2;
	private static final int TYPE1_INDEX = 3;
	private static final int TYPE2_INDEX = 4;
	private static final int SIDE1_INDEX = 5;
	private static final int SIDE2_INDEX = 6;
	private static final int RANK1_HIGH_INDEX = 7;
	private static final int RANK1_LOW_INDEX = 8;
	private static final int RANK2_HIGH_INDEX = 9;
	private static final int RANK2_LOW_INDEX = 10;
	private static final int ACE_LOW_INDEX = 11;

	private static final String[][] straightDrawTests = {
			{ "2c 4c - 6s 7s 8s", "4c 6s 7s 8s", "",
				StraightDraw.Type.PLAYER.toString(), "",
				StraightDraw.Side.INSIDE.toString(), "",
				Rank.EIGHT.toString(), "", "", "" },
		
			// XXXX, 2 low
			{ "2s 3c - 4d 5h 8s", "2s 3c 4d 5h", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.OUTSIDE.toString(), "",
					Rank.SIX.toString(), Rank.FIVE.toString(), "", "" },
			
			// X_XXX
			{ "2s 4c - 5d 6h 9s", "2s 4c 5d 6h", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.INSIDE.toString(), "",
					Rank.SIX.toString(), "", "", "" },

			// XXX_X, board and player
			{ "Ts 3c - 4d 5h 7d 8s", "4d 5h 7d 8s", "3c 4d 5h 7d",
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Side.INSIDE.toString(),
					StraightDraw.Side.INSIDE.toString(), Rank.EIGHT.toString(),
					"", Rank.SEVEN.toString(), "" },

			// XX_XX, board and player
			{ "8c 3c - 4d 5h 7d 8s", "4d 5h 7d 8s", "4d 5h 7d 8c",
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Side.INSIDE.toString(),
					StraightDraw.Side.INSIDE.toString(), Rank.EIGHT.toString(),
					"", Rank.EIGHT.toString(), "" },

			{ "9c Tc - 4d 5h 7d 8s", "7d 8s 9c Tc", "4d 5h 7d 8s",
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Side.OUTSIDE.toString(),
					StraightDraw.Side.INSIDE.toString(), Rank.JACK.toString(),
					Rank.TEN.toString(), Rank.EIGHT.toString(), "" },

			// XXXX, board, ace low, inside
			{ "8s 9c - 2c 3d 4h As", "As 2c 3d 4h", "",
					StraightDraw.Type.BOARD.toString(), "",
					StraightDraw.Side.INSIDE.toString(), "",
					Rank.FIVE.toString(), "", "", "", "true" },

			// XXXX, ace low, inside
			{ "As 9c - 2c 3d 4h Ts", "As 2c 3d 4h", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.INSIDE.toString(), "",
					Rank.FIVE.toString(), "", "", "", "true" },

			// XXXX, ace high, inside
			{ "As 9c - 2c Jd Qh Ks", "Jd Qh Ks As", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.INSIDE.toString(), "",
					Rank.ACE.toString(), "", "", "" },

			{ "8s 9c - Jd Qh Ks Ac", "Jd Qh Ks Ac", "9c Jd Qh Ks",
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Side.INSIDE.toString(), 
					StraightDraw.Side.INSIDE.toString(),
					Rank.ACE.toString(), "", 
					Rank.KING.toString(), "" },

			{ "2s 3c - 5d 6h 7s 8c", "5d 6h 7s 8c", "3c 5d 6h 7s",
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Side.OUTSIDE.toString(),
					StraightDraw.Side.INSIDE.toString(),
					Rank.NINE.toString(),
					Rank.EIGHT.toString(),
					Rank.SEVEN.toString(), "" },
				
			// X_XXX_X
			{ "3s 7c - 2d 5h 6s 9d", "3s 5h 6s 7c 9d", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "", 
					Rank.NINE.toString(),
					Rank.SEVEN.toString(), "", "" },
			
			{ "As 3d - 4h 5s 6c", "3d 4h 5s 6c", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.OUTSIDE.toString(), "",
					Rank.SEVEN.toString(), Rank.SIX.toString(), "", "" },

			// X_XXX_X, ace low
			{ "As 3d - 4s 5h 7c", "As 3d 4s 5h 7c", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "",
					Rank.SEVEN.toString(), Rank.FIVE.toString(), "", "", "true" },
					
			// X_XXX_X
			{ "2s 4d - 5s 6h 8c", "2s 4d 5s 6h 8c", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "",
					Rank.EIGHT.toString(), Rank.SIX.toString(), "", "" },
			
			// X_XXX_X, ace high
			{ "8s Td - Js Qh Ac", "8s Td Js Qh Ac", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "",
					Rank.ACE.toString(), Rank.QUEEN.toString(), "", "" },

			// XXXX_X
			{ "9s Td - Js Qh Ac", "9s Td Js Qh Ac", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "",
					Rank.ACE.toString(), Rank.QUEEN.toString(), "", "" },
			
			// XXXX_X, inside (no double inside draw because an ace is a dead end)
			{ "As 2d - 3h 4s 6c", "2d 3h 4s 6c", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.INSIDE.toString(), "",
					Rank.SIX.toString(), "", "", "" },
					
			// XXXX_XX
			{ "9s Td - 8s Js Kh Ac", "8s 9s Td Js Kh Ac", "",
					StraightDraw.Type.PLAYER.toString(), "",
					StraightDraw.Side.DOUBLE_INSIDE.toString(), "",
					Rank.ACE.toString(), Rank.JACK.toString(), "", "" },

			// XXXX_XX, player double inside beating equal board inside
			{ "2s 3c - 4d 5h 7d 8s", "2s 3c 4d 5h 7d 8s", "4d 5h 7d 8s",
					StraightDraw.Type.PLAYER.toString(),
					StraightDraw.Type.BOARD.toString(),
					StraightDraw.Side.DOUBLE_INSIDE.toString(),
					StraightDraw.Side.INSIDE.toString(),
					Rank.EIGHT.toString(), Rank.FIVE.toString(),
					Rank.EIGHT.toString(), "" },
	};

	private static final String[] notStraightDrawTests = {
			"As Tc - ",
			"As Tc - Ac Td 2h",
			"2s 3c - 5d 7h 8s",
			"2s 3c - 5d 7h 8s",

			"2s 3c - 4d 5h As", "2s 3c - 4d 5h Js Qd Kc",
			"2s 3c - 4d 5h 6s 7d 8c" 
	};

	@Test
	public void testStraightDraw() throws DoesNotMakeHandException {
		for (String[] test : straightDrawTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);

			List<Card> sd1 = CardUtil.createCards(test[SD1_INDEX]);
			if (test.length < ACE_LOW_INDEX + 1
					|| !"true".equals(test[ACE_LOW_INDEX])) {
				Collections.sort(sd1, CardComparator.getInstance());
			}

			List<Card> sd2 = null;
			if (!"".equals(test[SD2_INDEX])) {
				sd2 = CardUtil.createCards(test[SD2_INDEX]);
				Collections.sort(sd2, CardComparator.getInstance());
			}

			List<Card> valueCards = new LinkedList<>(sd1);
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(sd1);
			Collections.sort(valueCards, CardComparator.getInstance());

			StraightDraw straightDraw = new StraightDraw(holeCards, boardCards);

			Assert.assertEquals(test[CARD_INDEX], HandValue.STRAIGHT_DRAW, straightDraw.getValue());

			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(sd1, straightDraw.getStraightDraw()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE1_INDEX], straightDraw.getType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[SIDE1_INDEX], straightDraw.getSide().toString());
			Assert.assertEquals(test[CARD_INDEX], test[RANK1_HIGH_INDEX], straightDraw.getHighRank().toString());
			if (!"".equals(test[RANK1_LOW_INDEX])) {
				Assert.assertEquals(test[CARD_INDEX], test[RANK1_LOW_INDEX], straightDraw.getLowRank().toString());
			} else {
				Assert.assertNull(test[CARD_INDEX], straightDraw.getLowRank());
			}

			if (sd2 != null) {
				Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(sd2, straightDraw.getSecondStraightDraw()));
				Assert.assertEquals(test[CARD_INDEX], test[TYPE2_INDEX], straightDraw.getSecondType().toString());
				Assert.assertEquals(test[CARD_INDEX], test[SIDE2_INDEX], straightDraw.getSecondSide().toString());
				Assert.assertEquals(test[CARD_INDEX], test[RANK2_HIGH_INDEX], straightDraw.getSecondHighRank().toString());
				if (!"".equals(test[RANK2_LOW_INDEX])) {
					Assert.assertEquals(test[CARD_INDEX], test[RANK2_LOW_INDEX], straightDraw.getSecondLowRank().toString());
				} else {
					Assert.assertNull(test[CARD_INDEX],	straightDraw.getSecondLowRank());
				}
			} else {
				Assert.assertNull(test[CARD_INDEX], straightDraw.getSecondStraightDraw());
				Assert.assertNull(test[CARD_INDEX],	straightDraw.getSecondType());
				Assert.assertNull(test[CARD_INDEX],	straightDraw.getSecondSide());
				Assert.assertNull(test[CARD_INDEX],	straightDraw.getSecondHighRank());
				Assert.assertNull(test[CARD_INDEX],	straightDraw.getSecondLowRank());
			}

			Assert.assertTrue(test[CARD_INDEX],	Iterables.elementsEqual(valueCards,	straightDraw.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX],	Iterables.elementsEqual(nonValueCards, straightDraw.getNonValueCards()));
		}
	}

	@Test
	public void testNotStraightDraw() {
		for (String test : notStraightDrawTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);

			try {
				new StraightDraw(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsStraightDraw() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 3s 4s 5s");
		
		new StraightDraw(null, boardCards);
	}
}
