package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class FlushTest {
	
	private static final int CARD_INDEX = 0;
	private static final int FLUSH_INDEX = 1;
	private static final int TYPE_INDEX = 2;
	
	private static final String[][] flushTests = {
		{"2s 3s - 5s 8s Ts", "2s 3s 5s 8s Ts", Flush.Type.PLAYER.toString()},
		{"2s 3s - 5s 8s Js Ah Ts", "3s 5s 8s Ts Js", Flush.Type.PLAYER.toString()},
		{"2s 3s - 5c 8c Jc Ac Tc", "5c 8c Jc Ac Tc", Flush.Type.BOARD.toString()},
	};
	
	private static final String[] notFlushTests = {
		"2s 3s - 5s 8s 9c",
		"2s 3s - 5s 8s 9c Td",
		"2s 3s - 5s 8s 9c Td Ah"
	};
	
	@Test
	public void testFlush() throws DoesNotMakeHandException {
		for (String[] test : flushTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> flushCards = CardUtil.createCards(test[FLUSH_INDEX]);
			Collections.sort(flushCards, CardComparator.getInstance());
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(flushCards);
			
			Flush flush = new Flush(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.FLUSH, flush.getValue());
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], flush.getType().toString());
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(flushCards, flush.getFlush()));
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(flushCards, flush.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, flush.getNonValueCards()));
		}
	}
	
	@Test
	public void testNotFlush() {
		for (String test : notFlushTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new Flush(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsFlush() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 4s 6s 8s Ts");
		new Flush(null, boardCards);
	}
}
