package pr.domain.poker.range.ranked;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Rank;
import pr.domain.poker.HoleCards;
import pr.domain.poker.range.ranked.HoleCardsRank.SuitedRequirement;

public class PokerStoveHoleCardsRanksTest {

	@Test
	public void getPokerStoveHandRangeFilter() {
		int count = 0;
		
		for (Set<HoleCards> values : PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap().values()) {
			count += values.size();
		}
		
		Assert.assertEquals(HoleCards.NR_HOLE_CARD_COMBINATIONS, count);
	}
	
	@Test
	public void testOrder() {
		Map<HoleCardsRank, Set<HoleCards>> map = PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap();
		List<String> stringOrder = PokerStoveHoleCardsRanks.getPokerStoveHoleCardsOrder();
		
		Iterator<HoleCardsRank> rankIterator = map.keySet().iterator();
		Iterator<String> stringIterator = stringOrder.iterator();
		
		Assert.assertEquals(map.keySet().size(), stringOrder.size());
		
		for (int i = 0; i < stringOrder.size(); i++) {
			HoleCardsRank rank = rankIterator.next();
			String string = stringIterator.next();
			
			Rank rank1 = Rank.parseRank(string.charAt(0));
			Rank rank2 = Rank.parseRank(string.charAt(1));
			SuitedRequirement suitedRequirement = SuitedRequirement.BOTH;
			if (string.length() == 3) {
				suitedRequirement = SuitedRequirement.fromChar(string.charAt(2));
			}
			
			Assert.assertEquals(i, rank.getOrdinal());
			Assert.assertEquals(rank1, rank.getRank1());
			Assert.assertEquals(rank2, rank.getRank2());
			Assert.assertEquals(suitedRequirement, rank.getSuited());
		}
	}
	
	@Test
	public void getPokerStoveHoleCardsOrder() {
		List<String> pokerStoveHoleCardsOrder = PokerStoveHoleCardsRanks.getPokerStoveHoleCardsOrder();
		Set<String> set = new TreeSet<>(pokerStoveHoleCardsOrder);
		Assert.assertEquals(RankedHandRange.NR_OF_RANKS, set.size());
	}
	
	@Test
	public void getPokerStoveHoleCardsRanks() {
		Set<HoleCardsRank> pokerStoveHoleCardsRanks = PokerStoveHoleCardsRanks.getPokerStoveHoleCardsRanks();
		RankedHandRangeTest.checkHoleCardsRankSet(pokerStoveHoleCardsRanks);
	}
	
}
