package pr.domain.poker.hand;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class HandTest {

	private static final int VALUE_INDEX = 0;
	private static final int NON_VALUE_INDEX = 1;
	private static final int KICKERS_INDEX = 2;
	
	private static class HandSubclass extends Hand {

		public HandSubclass(HoleCards holeCards, BoardCards boardCards) {
			super(holeCards, boardCards);
		}
		
		public void init(HandValue value, List<Card> valueCards, List<Card> nonValueCards) {
			super.init(value, valueCards, nonValueCards);
		}
		
	}
	
	private static final String[][] tests = {
		{"", 	  			"", 				""},
		{"2c 2d", 			"", 				""},
		{"2c 2d", 			"4s 6h 8c", 		"4s 6h 8c"},
		{"2c 2d", 			"4s 6h 8c Tc", 		"6h 8c Tc"},
		{"2c 2d", 			"4s 6h 8c Tc 4h", 	"6h 8c Tc"},
		{"2c 2d", 			"4s Tc 4h 6h 8c", 	"6h 8c Tc"},
		{"Th Jh Qh Kh Ah", 	"", 				""},
		{"Th Jh Qh Kh Ah", 	"2c 3s", 			""},
		{"Th Jh Qh Kh Ah", 	"2c 4s 6d", 		""},
		{"As Ac Ad Ah", 	"6d",		 		"6d"},
		{"As Ac Ad Ah", 	"2c 4s 6d 8c", 		"8c"},
		{"2s 2c 3d 3h", 	"6s 8c Td", 		"Td"},
		{"2s 2c 2d",	 	"6s 8c Td Qs", 		"Td Qs"},
	};
	
	@Test
	public void testKickers() {
		for (String[] test : tests) {
			Hand hand = new HandSubclass(null, null);
			
			List<Card> valueCards = CardUtil.createCards(test[VALUE_INDEX]);
			List<Card> nonValueCards = CardUtil.createCards(test[NON_VALUE_INDEX]);
			List<Card> kickers = CardUtil.createCards(test[KICKERS_INDEX]);
			
			hand.init(HandValue.STRAIGHT_FLUSH, valueCards, nonValueCards);
			
			Assert.assertTrue(test[VALUE_INDEX] + test[NON_VALUE_INDEX], Iterables.elementsEqual(kickers, hand.getKickers()));
		}
	}
	
}
