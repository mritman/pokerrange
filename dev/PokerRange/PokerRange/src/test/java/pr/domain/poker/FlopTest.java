package pr.domain.poker;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;

public class FlopTest {

	@Test
	public void getCardsOrder() {
		Flop flop = new Flop(Card.CLUB_ACE, Card.HEART_THREE, Card.SPADE_EIGHT);
		
		List<Card> cards = flop.getCards();
		Assert.assertEquals(Card.CLUB_ACE, cards.get(0));
		Assert.assertEquals(Card.HEART_THREE, cards.get(1));
		Assert.assertEquals(Card.SPADE_EIGHT, cards.get(2));
	}

}
