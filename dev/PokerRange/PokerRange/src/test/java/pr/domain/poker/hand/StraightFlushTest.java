package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class StraightFlushTest {
	
	private static final int CARD_INDEX = 0;
	private static final int STRAIGHT_FLUSH_INDEX = 1;
	private static final int TYPE_INDEX = 2;
	private static final int ROYAL_FLUSH_INDEX = 3;
	private static final int ACE_LOW_INDEX = 4;

	private static final String[][] straightFlushTests = {
		{"2s 3s - 4s 5s 6s", "2s 3s 4s 5s 6s", StraightFlush.Type.PLAYER.toString(), "false"},
		{"8s 9c - 2s 3s 4s 5s 6s", "2s 3s 4s 5s 6s", StraightFlush.Type.BOARD.toString(), "false"},
		{"8s 9s - 6s 7s Ts Js Qs", "8s 9s Ts Js Qs", StraightFlush.Type.PLAYER.toString(), "false"},
		
		{"8c 4c - Ts Js Qs Ks As", "Ts Js Qs Ks As", StraightFlush.Type.BOARD.toString(), "true"},
		{"As 4c - Ts Js Qs Ks 8c", "Ts Js Qs Ks As", StraightFlush.Type.PLAYER.toString(), "true"},
		
		{"As 2s - 3s 4s 5s Kc 8c", "As 2s 3s 4s 5s", StraightFlush.Type.PLAYER.toString(), "false", "true"},
		{"Ac 8d - As 2s 3s 4s 5s", "As 2s 3s 4s 5s", StraightFlush.Type.BOARD.toString(), "false", "true"},
	};
	
	private static final String[] notStraightFlushTests = {
		"3s As - 6s 7s 8s",
		"2s 3c - 5s 5d 7h",
		"2s 3c - 5s 5d 7h Td As",
		"2s 3s - 4d 5d 6d Td Ad",
	};
	
	@Test
	public void testStraightFlush() throws DoesNotMakeHandException {
		for (String[] test : straightFlushTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> straightFlushCards = CardUtil.createCards(test[STRAIGHT_FLUSH_INDEX]);
			if (test.length < 5 || !"true".equals(test[ACE_LOW_INDEX])) {
				Collections.sort(straightFlushCards, CardComparator.getInstance());
			}
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			boolean royalFlush = Boolean.parseBoolean(test[ROYAL_FLUSH_INDEX]);
			
			StraightFlush straightFlush = new StraightFlush(holeCards, boardCards);
			
			nonValueCards.removeAll(straightFlushCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.STRAIGHT_FLUSH, straightFlush.getValue());
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], straightFlush.getType().toString());
			Assert.assertEquals(test[CARD_INDEX], royalFlush, straightFlush.isRoyalFlush());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(straightFlushCards, straightFlush.getStraightFlush()));
			
			Collections.sort(straightFlushCards, CardComparator.getInstance());
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(straightFlushCards, straightFlush.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, straightFlush.getNonValueCards()));
		}
	}
	
	@Test
	public void testNotStraightFlush() {
		for (String test : notStraightFlushTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new StraightFlush(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsStraightFlush() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 3s 4s 5s 6s");
		
		new StraightFlush(null, boardCards);
	}
}
