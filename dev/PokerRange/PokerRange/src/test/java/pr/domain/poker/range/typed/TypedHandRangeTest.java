package pr.domain.poker.range.typed;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Rank;
import pr.domain.poker.range.typed.types.HandTypeEnum;

public class TypedHandRangeTest {

	@Test
	public void testEqualsAndHashCodeNames() {
		Map<HandTypeEnum, HandTypeRange> map = new HashMap<>();
		
		TypedHandRange range1 = new TypedHandRange("Range1", map);
		TypedHandRange range2 = new TypedHandRange("Range2", map);
		TypedHandRange range3 = new TypedHandRange("Range1", map);
		
		Assert.assertNotEquals(range1, range2);
		Assert.assertEquals(range1, range3);
		Assert.assertEquals(range1.hashCode(), range3.hashCode());
	}
	
	@Test
	public void testEqualsAndHashCodeRanges() {
		String rangeName = "Range1";
		
		HandTypeRange twoToAce = new HandTypeRange(Rank.TWO, Rank.ACE);
		HandTypeRange threeToAce = new HandTypeRange(Rank.THREE, Rank.ACE);
		
		Map<HandTypeEnum, HandTypeRange> map1 = new HashMap<>();
		Map<HandTypeEnum, HandTypeRange> map2 = new HashMap<>();
		
		map1.put(HandTypeEnum.PAIR, twoToAce);
		map1.put(HandTypeEnum.ACE_X, threeToAce);
		
		map2.put(HandTypeEnum.ACE_X, twoToAce);
		map2.put(HandTypeEnum.PAIR, threeToAce);
		
		TypedHandRange range1 = new TypedHandRange(rangeName, map1);
		TypedHandRange range2 = new TypedHandRange(rangeName, map2);
		TypedHandRange range3 = new TypedHandRange(rangeName, map1);
		
		Assert.assertNotEquals(range1, range2);
		Assert.assertEquals(range1, range3);
		Assert.assertEquals(range1.hashCode(), range3.hashCode());
	}

}
