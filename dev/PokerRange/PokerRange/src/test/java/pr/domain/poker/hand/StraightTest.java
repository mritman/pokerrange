package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class StraightTest {

	private static final int CARD_INDEX = 0;
	private static final int STRAIGHT1_INDEX = 1;
	private static final int STRAIGHT2_INDEX = 2;
	private static final int TYPE1_INDEX = 3;
	private static final int TYPE2_INDEX = 4;
	private static final int ACE_LOW_INDEX = 5;
	
	private static final String[][] straightTests = {
		{"2s 3c - 4d 5h 6s", "2s 3c 4d 5h 6s", "", Straight.Type.PLAYER.toString(), ""},
		{"Ts Tc - 4d 5h 6s 7c 8d", "4d 5h 6s 7c 8d", "", Straight.Type.BOARD.toString(), ""},
		
		{"3s Tc - 4d 5h 6s 7c 8d", "4d 5h 6s 7c 8d", "3s 4d 5h 6s 7c", Straight.Type.BOARD.toString(), Straight.Type.PLAYER.toString()},
		{"3s 8d - 3s 4d 5h 6s 7c", "4d 5h 6s 7c 8d", "3s 4d 5h 6s 7c", Straight.Type.PLAYER.toString(), Straight.Type.BOARD.toString()},
		
		{"2s 5d - As 3d 4h 6s 7c", "3d 4h 5d 6s 7c", "", Straight.Type.PLAYER.toString(), ""},
		{"2s 5d - As 3d 4h 6s 8c", "2s 3d 4h 5d 6s", "", Straight.Type.PLAYER.toString(), ""},
		{"2s 5d - As 3d 4h Ts 8c", "As 2s 3d 4h 5d", "", Straight.Type.PLAYER.toString(), "", "true"},
		{"Ts Jd - As 2d 3h 4s 5c", "As 2d 3h 4s 5c", "", Straight.Type.BOARD.toString(), "", "true"},

		{"8s 8d - Ts Jh Qc Kd As", "Ts Jh Qc Kd As", "", Straight.Type.BOARD.toString(), ""},
		{"8s Ad - Ts Jh Qc Kd 8s", "Ts Jh Qc Kd Ad", "", Straight.Type.PLAYER.toString(), ""},
	};
	
	private static final String[] notStraightTests = {
		"2s 4c - 6d 8h Ts",
		"2s 4c - 5d 6h 7s",
		"2s 4c - 5d 6h 7s 9c",
		"2s 4c - 5d 6h 7s 9c Td",
	};
	
	@Test
	public void testStraight() throws DoesNotMakeHandException {
		for (String[] test : straightTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> straight1 = CardUtil.createCards(test[STRAIGHT1_INDEX]);
			if (test.length < 6 || !"true".equals(test[ACE_LOW_INDEX])) {
				Collections.sort(straight1, CardComparator.getInstance());
			}
			
			List<Card> straight2 = null;
			if (!"".equals(test[STRAIGHT2_INDEX])) {
				straight2 = CardUtil.createCards(test[STRAIGHT2_INDEX]);
				Collections.sort(straight2, CardComparator.getInstance());
			}
			
			List<Card> valueCards = new LinkedList<>(straight1);
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(straight1);
			Collections.sort(valueCards, CardComparator.getInstance());
			
			Straight straight = new Straight(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.STRAIGHT, straight.getValue());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(straight1, straight.getStraight()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE1_INDEX], straight.getType().toString());
			
			if (straight2 != null) {
				Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(straight2, straight.getSecondStraight()));
				Assert.assertEquals(test[CARD_INDEX], test[TYPE2_INDEX], straight.getSecondStraightType().toString());
			}
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(valueCards, straight.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, straight.getNonValueCards()));
		}
	}
	
	@Test
	public void testNotStraight() {
		for (String test : notStraightTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new Straight(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsStraight() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 3c 4d 5h 6s");
		
		new Straight(null, boardCards);
	}
}
