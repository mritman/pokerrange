package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class TwoPairTest {

	private static final int CARD_INDEX = 0;
	private static final int FIRST_PAIR_INDEX = 1;
	private static final int SECOND_PAIR_INDEX = 2;
	private static final int THIRD_PAIR_INDEX = 3;
	private static final int TWO_PAIR_TYPE_INDEX = 4;
	private static final int TYPE1_INDEX = 5;
	private static final int TYPE2_INDEX = 6;
	private static final int TYPE3_INDEX = 7;
	private static final int FIRST_RANK_INDEX = 8;
	private static final int SECOND_RANK_INDEX = 9;
	private static final int THIRD_RANK_INDEX = 10;
	
	private static final String[][] validTwoPairs = {
			{ "2c 2d - 3s 3h Kc", "3s 3h", "2c 2d", "",
					TwoPair.Type.COMBINED_TWO_PAIR.toString(),
					Pair.Type.BOARD_PAIR.toString(),
					Pair.Type.POCKET_PAIR.toString(), "",
					Pair.RelativeRank.SECOND_PAIR.toString(),
					Pair.RelativeRank.UNDER_PAIR.toString(), "" },

			{ "2c 2d - 3s 3h Kc Kd", "Kc Kd", "3s 3h", "2c 2d",
					TwoPair.Type.TWO_BOARD_PAIR.toString(),
					Pair.Type.BOARD_PAIR.toString(),
					Pair.Type.BOARD_PAIR.toString(),
					Pair.Type.POCKET_PAIR.toString(),
					Pair.RelativeRank.TOP_PAIR.toString(),
					Pair.RelativeRank.SECOND_PAIR.toString(),
					Pair.RelativeRank.UNDER_PAIR.toString() },

			{ "2c 3d - 2s 3h Kc Kd Ac", "Kc Kd", "3d 3h", "2c 2s",
					TwoPair.Type.COMBINED_TWO_PAIR.toString(),
					Pair.Type.BOARD_PAIR.toString(),
					Pair.Type.COMBINED_PAIR.toString(),
					Pair.Type.COMBINED_PAIR.toString(),
					Pair.RelativeRank.SECOND_PAIR.toString(),
					Pair.RelativeRank.THIRD_PAIR.toString(),
					Pair.RelativeRank.FOURTH_PAIR.toString() },
	
			{ "Kc Ad - 2s 3h Qc Kd Ac", "Ac Ad", "Kc Kd", "",
				TwoPair.Type.TWO_PLAYER_PAIR.toString(),
				Pair.Type.COMBINED_PAIR.toString(),
				Pair.Type.COMBINED_PAIR.toString(), "",
				Pair.RelativeRank.TOP_PAIR.toString(),
				Pair.RelativeRank.SECOND_PAIR.toString(), "" }
	};
	
	@Test
	public void testTwoPair() throws DoesNotMakeHandException {
		for (String[] test : validTwoPairs) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> valueCards = new LinkedList<>();
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			List<Card> first = CardUtil.createCards(test[FIRST_PAIR_INDEX]);
			List<Card> second = CardUtil.createCards(test[SECOND_PAIR_INDEX]);
			Collections.sort(first, CardComparator.getInstance());
			Collections.sort(second, CardComparator.getInstance());
			
			List<Card> third = null;
			if (!test[THIRD_PAIR_INDEX].equals("")) {
				third = CardUtil.createCards(test[THIRD_PAIR_INDEX]);
				Collections.sort(third, CardComparator.getInstance());
			}

			valueCards.addAll(first);
			valueCards.addAll(second);
			nonValueCards.removeAll(first);
			nonValueCards.removeAll(second);
			Collections.sort(valueCards, CardComparator.getInstance());
			Collections.sort(nonValueCards, CardComparator.getInstance());
			
			TwoPair twoPair = new TwoPair(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.TWO_PAIR, twoPair.getValue());
			Assert.assertEquals(test[CARD_INDEX], test[TWO_PAIR_TYPE_INDEX], twoPair.getType().toString());

			// First pair.
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(first, twoPair.getFirstPair()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE1_INDEX], twoPair.getFirstPairType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[FIRST_RANK_INDEX], twoPair.getFirstPairRelativeRank().toString());
			
			// Second pair.
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(second, twoPair.getSecondPair()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE2_INDEX], twoPair.getSecondPairType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[SECOND_RANK_INDEX], twoPair.getSecondPairRelativeRank().toString());			

			// Third pair.
			if (third == null) {
				Assert.assertNull(test[CARD_INDEX], twoPair.getThirdPair());
				Assert.assertNull(test[CARD_INDEX], twoPair.getThirdPairType());
				Assert.assertNull(test[CARD_INDEX], twoPair.getThirdPairRank());
				Assert.assertNull(test[CARD_INDEX], twoPair.getThirdPairRelativeRank());
			} else {
				Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(third, twoPair.getThirdPair()));
				Assert.assertEquals(test[CARD_INDEX], test[TYPE3_INDEX], twoPair.getThirdPairType().toString());
				Assert.assertEquals(test[CARD_INDEX], test[THIRD_RANK_INDEX], twoPair.getThirdPairRelativeRank().toString());			
			}
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(valueCards, twoPair.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, twoPair.getNonValueCards()));
		}
	}
	
	@Test
	public void testBoardCardsTwoPair() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2c 2s 3s 3c 5d");
		
		new TwoPair(null, boardCards);
	}
}
