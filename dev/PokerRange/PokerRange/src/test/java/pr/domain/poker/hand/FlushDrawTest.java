package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.cards.Suit;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class FlushDrawTest {
	
	private static final int CARD_INDEX = 0;
	private static final int FLUSH_DRAW_INDEX = 1;
	private static final int SUIT_INDEX = 2;
	private static final int TYPE_INDEX = 3;
	private static final int NUT_FLUSH_DRAW_INDEX = 4;

	private static final String[][] flushDrawTests = {
		{"2s 3s - 6s 8s Tc", "2s 3s 6s 8s", Suit.SPADE.toString(), FlushDraw.Type.PLAYER.toString(), "false"},
		{"2s 3s - 6s 8c Ts", "2s 3s 6s Ts", Suit.SPADE.toString(), FlushDraw.Type.PLAYER.toString(), "false"},
		
		{"2s 3s - 6c 8c Tc 2c", "6c 8c Tc 2c", Suit.CLUB.toString(), FlushDraw.Type.BOARD.toString(), "false"},
		
		{"2s Ks - 6s 8c Tc As", "2s 6s Ks As", Suit.SPADE.toString(), FlushDraw.Type.PLAYER.toString(), "true"},
		
		{"2c Kc - Qs Ts Ks As", "Qs Ts Ks As", Suit.SPADE.toString(), FlushDraw.Type.BOARD.toString(), "false"},
	};
	
	private static final String[] notFlushDrawTests = {
		"2s 3s - 5s 8s Ts",
		"2s 3s - 5s 8d Td",
		"2s 3s - 5s 8s Td Jh Kc"
	};
	
	@Test
	public void testFlushDraw() throws DoesNotMakeHandException {
		for (String[] test : flushDrawTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> flushDrawCards = CardUtil.createCards(test[FLUSH_DRAW_INDEX]);
			Collections.sort(flushDrawCards, CardComparator.getInstance());
			
			boolean nutFlushDraw = Boolean.parseBoolean(test[NUT_FLUSH_DRAW_INDEX]);
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(flushDrawCards);
			
			FlushDraw flushDraw = new FlushDraw(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.FLUSH_DRAW, flushDraw.getValue());
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], flushDraw.getType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[SUIT_INDEX], flushDraw.getSuit().toString());
			Assert.assertEquals(test[CARD_INDEX], nutFlushDraw, flushDraw.isNutFlushDraw());
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(flushDrawCards, flushDraw.getFlushDraw()));
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(flushDrawCards, flushDraw.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, flushDraw.getNonValueCards()));
		}
	}
	
	@Test
	public void testNotFlushDraw() {
		for (String test : notFlushDrawTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new FlushDraw(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsFlushDraw() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 4s 5s 8s");
		
		new FlushDraw(null, boardCards);
	}
}
