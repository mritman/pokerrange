package pr.domain.poker.game;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.poker.Street;
import pr.domain.poker.action.Action;
import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.util.IteratorFuser;
import pr.util.IteratorFuser.FusedEntry;

import com.google.common.collect.Lists;

public class GameTest {

	@Test
	public void testNewGame() {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3, Seat.SEAT4);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);
		
		Assert.assertTrue(game.getStreet().isPreflop());
		Assert.assertEquals(Seat.SEAT4, game.getNextSeatToAct());
		Assert.assertEquals(seats, game.getSeatsInHand());
		
		List<Action> seat2Actions = game.getActions(Seat.SEAT2);
		Assert.assertEquals(1, seat2Actions.size());
		Assert.assertTrue(seat2Actions.get(0).getType().isPostSmallBlind());
		
		List<Action> seat3Actions = game.getActions(Seat.SEAT3);
		Assert.assertTrue(seat3Actions.get(0).getType().isPostBigBlind());
		
		Assert.assertTrue(game.getActions(Seat.SEAT1).isEmpty());
		Assert.assertTrue(game.getActions(Seat.SEAT4).isEmpty());
	}
	
	@Test
	public void testNewGameHeadsUp() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);
		
		Assert.assertTrue(game.getStreet().isPreflop());
		Assert.assertEquals(Seat.SEAT2, game.getNextSeatToAct());
		
		List<Action> seat1Actions = game.getActions(Seat.SEAT1);
		Assert.assertEquals(1, seat1Actions.size());
		Assert.assertTrue(seat1Actions.get(0).getType().isPostBigBlind());
		
		List<Action> seat2Actions = game.getActions(Seat.SEAT2);
		Assert.assertEquals(1, seat2Actions.size());
		Assert.assertTrue(seat2Actions.get(0).getType().isPostSmallBlind());
	}
	
	@Test
	public void testStreetTransitions() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);
		
		game.processEvent(Seat.SEAT2, ActionType.CALL);
		game.processEvent(Seat.SEAT1, ActionType.CHECK);
		
		Assert.assertTrue(game.getStreet().isFlop());
		Assert.assertEquals(Seat.SEAT2, game.getNextSeatToAct());
		
		game.processEvent(Seat.SEAT2, ActionType.CHECK);
		game.processEvent(Seat.SEAT1, ActionType.CHECK);
		
		Assert.assertTrue(game.getStreet().isTurn());
		Assert.assertEquals(Seat.SEAT2, game.getNextSeatToAct());
		
		game.processEvent(Seat.SEAT2, ActionType.CHECK);
		game.processEvent(Seat.SEAT1, ActionType.CHECK);
		
		Assert.assertTrue(game.getStreet().isRiver());
		Assert.assertEquals(Seat.SEAT2, game.getNextSeatToAct());
		
		game.processEvent(Seat.SEAT2, ActionType.CHECK);
		game.processEvent(Seat.SEAT1, ActionType.CHECK);
		
		Assert.assertTrue(game.isGameFinished());
	}
	
	@Test
	public void testOthersFold() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3, Seat.SEAT4);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);
		
		game.processEvent(Seat.SEAT4, ActionType.FOLD);
		game.processEvent(Seat.SEAT1, ActionType.FOLD);
		game.processEvent(Seat.SEAT2, ActionType.FOLD);
		
		Assert.assertTrue(game.isGameFinished());
		Assert.assertEquals(Street.PREFLOP, game.getStreet());
	}
	
	@Test
	public void testIncorrectActionOrder() {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3, Seat.SEAT4);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);

		try {
			game.processEvent(Seat.SEAT3, ActionType.CALL);
		} catch (GameException e) {
			Assert.assertEquals(GameException.INCORRECT_ACTION_ORDER.substring(0, 10), e.getMessage().substring(0, 10));
		}
	}
	
	@Test
	public void testPreflopBet() {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3, Seat.SEAT4);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT1);

		try {
			game.processEvent(Seat.SEAT4, ActionType.BET);
			Assert.fail();
		} catch (GameException e) {
			Assert.assertEquals(GameException.CAN_ONLY_BET_PREFLOP, e.getMessage());
		}
	}
	
	@Test
	public void test() throws GameException {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT2, Seat.SEAT3, Seat.SEAT5, Seat.SEAT6);
		Game game = new Game(new HashSet<>(seats), Seat.SEAT2);
		
		Iterator<FusedEntry<Seat, ActionType>> events = IteratorFuser.fuse(
				Lists.newArrayList(Seat.SEAT6, Seat.SEAT2, Seat.SEAT3, Seat.SEAT5, Seat.SEAT5),
				Lists.newArrayList(ActionType.FOLD, ActionType.CALL, ActionType.FOLD, ActionType.CHECK, ActionType.CHECK));
		
		while (events.hasNext()) {
			FusedEntry<Seat, ActionType> event = events.next();
			game.processEvent(event.getA().get(), event.getB().get());
		}
	}
	
}
