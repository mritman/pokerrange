package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class ThreeOfAKindTest {

	private static final int CARD_INDEX = 0;
	private static final int THREE_OF_A_KIND_INDEX = 1;
	private static final int TYPE_INDEX = 2;
	
	private static final String[][] threeOfAKindTests = {
		{"2s 3c - 3d 3h 5s", "3c 3d 3h", ThreeOfAKind.Type.PLAYER.toString()},
		{"3s 3c - 3d 8h 5s", "3s 3c 3d", ThreeOfAKind.Type.PLAYER.toString()},
		{"3s 3c - 3d 8h 5s Th", "3s 3c 3d", ThreeOfAKind.Type.PLAYER.toString()},
		{"3s 3c - 3d 8h 5s Th Qs", "3s 3c 3d", ThreeOfAKind.Type.PLAYER.toString()},
		
		{"2s 3c - 5d 5h 5s Th Qs", "5d 5h 5s", ThreeOfAKind.Type.BOARD.toString()},
		{"2s 3c - 5d Th Qs 5h 5s", "5d 5h 5s", ThreeOfAKind.Type.BOARD.toString()}
	};
	
	private static final String[] notThreeOfAKindTests = {
		"2s 3c - 3d 2h 5s",
		"2s 3c - 3d 2h 5s 5c",
		"3c 3s - 3d 3h 7c",
		"3c 3s - 3d 3h 7c 7s 7d",
		"5s Tc - 5d Th Ts 5h Ac",
		"2s 5c - 5d Th Ts 5h Tc",
		"2s Tc - 5d Th Ts 5h 5s",
		"2s Tc - 4s 4c 5h 5d 5s",
		"4c Tc - 2s 4c 5h 5d 5s",
		"4c 2c - 2s 4c 5h 5d 5s",
		"4c Tc - 2s 4c 4h 5d 5s",
	};
	
	@Test
	public void testThreeOfAKind() throws DoesNotMakeHandException {
		for (String[] test : threeOfAKindTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
		
			List<Card> first = CardUtil.createCards(test[THREE_OF_A_KIND_INDEX]);
			Collections.sort(first, CardComparator.getInstance());
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(first);
			
			ThreeOfAKind threeOfAKind = new ThreeOfAKind(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.THREE_OF_A_KIND, threeOfAKind.getValue());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(first, threeOfAKind.getCards()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], threeOfAKind.getType().toString());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(first, threeOfAKind.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, threeOfAKind.getNonValueCards()));
		}
	}
	
	@Test
	public void testNotThreeOfAKind() {
		for (String test : notThreeOfAKindTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new ThreeOfAKind(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsThreeOfAKind() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2c 2s 2d");
		
		new ThreeOfAKind(null, boardCards);
	}
}
