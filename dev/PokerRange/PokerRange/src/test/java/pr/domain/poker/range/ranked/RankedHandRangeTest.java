package pr.domain.poker.range.ranked;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Rank;
import pr.domain.gamestate.GameState;
import pr.domain.poker.HoleCards;
import pr.domain.poker.range.ranked.HoleCardsRank.SuitedRequirement;

import com.google.common.collect.ComparisonChain;

public class RankedHandRangeTest {

	@Test
	public void calculateRange() {
		double rangePercentage = 0.20;
		double expectedRangeSize = HoleCards.NR_HOLE_CARD_COMBINATIONS * rangePercentage;
		
		RankedHandRange rankedHandRange = new RankedHandRange("Range name",
				rangePercentage,
				PokerStoveHoleCardsRanks.getRankedPokerStoveHoleCardsMap());
		
		rankedHandRange.calculateRange(new GameState());
		
		int actualRangeSize = rankedHandRange.getHandsInRange().size();
		
		Assert.assertEquals(expectedRangeSize, actualRangeSize, HoleCardsRank.MAX_RANK_SIZE - 1);
	}
	
	protected static void checkHoleCardsRankSet(Set<HoleCardsRank> holeCardsRankSet) {
		Assert.assertEquals(RankedHandRange.NR_OF_RANKS, holeCardsRankSet.size());
		
		TreeSet<HoleCardsRank> set = createHoleCardsRankTreeSet();
		set.addAll(holeCardsRankSet);
		
		TreeSet<HoleCardsRank> allRanks = createAllRanks();
		
		allRanks.removeAll(set);
		
		Assert.assertTrue("The following ranks were not found: " + allRanks.toString(), allRanks.isEmpty());
	}

	private static TreeSet<HoleCardsRank> createAllRanks() {
		TreeSet<HoleCardsRank> allRanks = createHoleCardsRankTreeSet();
		int ordinal = 0;
				
		for (Rank rank1 : Rank.values()) {
			for (Rank rank2 : Rank.values()) {
				if (rank2.intValue() > rank1.intValue()) continue;
				
				if (rank1.intValue() == rank2.intValue()) {
					HoleCardsRank pair = new HoleCardsRank(rank1, rank2, SuitedRequirement.BOTH, ordinal++);
					allRanks.add(pair);
				} else {
					HoleCardsRank suited = new HoleCardsRank(rank1, rank2, SuitedRequirement.SUITED, ordinal++);
					HoleCardsRank offsuit = new HoleCardsRank(rank1, rank2, SuitedRequirement.OFFSUIT, ordinal++);
				
					allRanks.add(suited);
					allRanks.add(offsuit);
				}
			}
		}
		
		Assert.assertEquals(RankedHandRange.NR_OF_RANKS, allRanks.size());
		
		return allRanks;
	}
	
	private static TreeSet<HoleCardsRank> createHoleCardsRankTreeSet() {
		return new TreeSet<>(new Comparator<HoleCardsRank>() {

			@Override
			public int compare(HoleCardsRank o1, HoleCardsRank o2) {
				if (o1.equals(o2)) {
					return 0;
				}
				
				return ComparisonChain
						.start()
						.compare(o1.getRank1(), o2.getRank1())
						.compare(o1.getRank2(), o2.getRank2())
						.compare(o1.getSuited().ordinal(), o2.getSuited().ordinal())
						.result();
			}
		});
	}
	
}
