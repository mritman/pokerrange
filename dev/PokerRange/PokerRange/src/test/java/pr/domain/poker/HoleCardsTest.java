package pr.domain.poker;


import org.junit.Assert;
import org.junit.Test;

public class HoleCardsTest {

	@Test
	public void createAllHoleCards() {
		Assert.assertEquals(HoleCards.NR_HOLE_CARD_COMBINATIONS, HoleCards.createAllHoleCards().size());
	}

}
