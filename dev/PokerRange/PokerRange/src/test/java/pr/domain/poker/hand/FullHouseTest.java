package pr.domain.poker.hand;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class FullHouseTest {
	
	private static final int CARD_INDEX = 0;
	private static final int PAIR_INDEX = 1;
	private static final int THREE_OF_A_KIND_INDEX = 2;
	private static final int PAIR_TYPE_INDEX = 3;
	private static final int THREE_OF_A_KIND_TYPE_INDEX = 4;
	private static final int FULL_HOUSE_TYPE_INDEX = 5;
	
	private static String[][] fullHouseTests = {
		{"2s 2c - 4s 4c 4d", "2s 2c", "4s 4c 4d", Pair.Type.POCKET_PAIR.toString(), ThreeOfAKind.Type.BOARD.toString(), FullHouse.Type.PLAYER.toString()},
		{"2s 2c - 2d 4c 4d 6h 6s", "6h 6s", "2s 2c 2d", Pair.Type.BOARD_PAIR.toString(), ThreeOfAKind.Type.PLAYER.toString(), FullHouse.Type.PLAYER.toString()},
		{"2s 2c - 4s 4c 6d 6h 6s", "4s 4c", "6d 6h 6s", Pair.Type.BOARD_PAIR.toString(), ThreeOfAKind.Type.BOARD.toString(), FullHouse.Type.BOARD.toString()},
		{"2s 4s - Ts 4c 6d 6h 6s", "4s 4c", "6d 6h 6s", Pair.Type.COMBINED_PAIR.toString(), ThreeOfAKind.Type.BOARD.toString(), FullHouse.Type.PLAYER.toString()},
		{"4s 6s - As 4c 6d 6h Ts", "4s 4c", "6d 6h 6s", Pair.Type.COMBINED_PAIR.toString(), ThreeOfAKind.Type.PLAYER.toString(), FullHouse.Type.PLAYER.toString()},
	};
	
	private static String[] notFullHouseTests = {
		"2s 2c - 2d 2h 3s 3c 3d",
		"2s 2c - 2d 3h 3s 3c 3d",
		"2s 3c - 3d 3h 3s 4c 4d",
		"2s 2c - 3d 3h 3s 3c 4d"
	};
	
	@Test
	public void testFullHouse() throws DoesNotMakeHandException {
		for (String[] test : fullHouseTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> pairCards = CardUtil.createCards(test[PAIR_INDEX]);
			List<Card> threeOfAKindCards = CardUtil.createCards(test[THREE_OF_A_KIND_INDEX]);
			
			List<Card> valueCards = new LinkedList<>();
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			valueCards.addAll(pairCards);
			valueCards.addAll(threeOfAKindCards);
			nonValueCards.removeAll(pairCards);
			nonValueCards.removeAll(threeOfAKindCards);
			
			Collections.sort(pairCards, CardComparator.getInstance());
			Collections.sort(threeOfAKindCards, CardComparator.getInstance());
			Collections.sort(valueCards, CardComparator.getInstance());
			Collections.sort(nonValueCards, CardComparator.getInstance());
			
			FullHouse fullHouse = new FullHouse(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.FULL_HOUSE, fullHouse.getValue());
			Assert.assertEquals(test[CARD_INDEX], test[FULL_HOUSE_TYPE_INDEX], fullHouse.getType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[PAIR_TYPE_INDEX], fullHouse.getPairType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[THREE_OF_A_KIND_TYPE_INDEX], fullHouse.getThreeOfAKindType().toString());
			

			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(pairCards, fullHouse.getPair()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(threeOfAKindCards, fullHouse.getThreeOfAKind()));
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(valueCards, fullHouse.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, fullHouse.getNonValueCards()));
		}
	}
	
	
	@Test
	public void testNotStraight() {
		for (String test : notFullHouseTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);
			
			try {
				new FullHouse(holeCards, boardCards);
				Assert.fail(test);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
		}
	}
	
	@Test
	public void testBoardCardsFullHouse() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 2c 2d 3h 3s");
		
		new FullHouse(null, boardCards);
	}
}
