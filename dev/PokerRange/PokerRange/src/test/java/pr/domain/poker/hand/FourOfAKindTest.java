package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class FourOfAKindTest {
	private static final int CARD_INDEX = 0;
	private static final int FOUR_OF_A_KIND_INDEX = 1;
	private static final int TYPE_INDEX = 2;
	
	private static final String[][] fourOfAKindTests = {
		{"2s 2c - 2d 2h 4s", "2s 2c 2d 2h", FourOfAKind.Type.PLAYER.toString()},
		{"2s 3c - 2d 2h 4s 2c", "2s 2c 2d 2h", FourOfAKind.Type.PLAYER.toString()},
		{"2s 3c - 4d 4h 4s 6c 4d", "4d 4h 4s 4d", FourOfAKind.Type.BOARD.toString()},
		{"3s 3c - 4d 4h 4s 3d 4c", "4d 4h 4s 4c", FourOfAKind.Type.BOARD.toString()},
		{"4s 4c - 3d 3h 3s 4d 4h", "4d 4h 4s 4c", FourOfAKind.Type.PLAYER.toString()},
	};
	
	@Test
	public void testFourOfAKind() throws DoesNotMakeHandException {
		for (String[] test : fourOfAKindTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> fourOfAKindCards = CardUtil.createCards(test[FOUR_OF_A_KIND_INDEX]);
			Collections.sort(fourOfAKindCards, CardComparator.getInstance());
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(fourOfAKindCards);
			
			FourOfAKind fourOfAKind = new FourOfAKind(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.FOUR_OF_A_KIND, fourOfAKind.getValue());
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(fourOfAKindCards, fourOfAKind.getFourOfAKind()));
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], fourOfAKind.getType().toString());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(fourOfAKindCards, fourOfAKind.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, fourOfAKind.getNonValueCards()));
		}
	}
	
	@Test
	public void testBoardCardsFourOfAKind() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 2c 2d 2h");
		
		new FourOfAKind(null, boardCards);
	}
}
