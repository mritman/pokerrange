package pr.domain.poker.table;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Lists;

public class PokerTableTest {

	@Test
	public void testGetFirstSeatAfter() {
		List<Seat> seats = Lists.newArrayList(Seat.SEAT4, Seat.SEAT5, Seat.SEAT6);
		Seat seat = PokerTable.getFirstSeatAfter(seats, Seat.SEAT3);
		Assert.assertEquals(Seat.SEAT4, seat);
		
		seats = Lists.newArrayList(Seat.SEAT3, Seat.SEAT5, Seat.SEAT6);
		seat = PokerTable.getFirstSeatAfter(seats, Seat.SEAT3);
		Assert.assertEquals(Seat.SEAT5, seat);
		
		seats = Lists.newArrayList(Seat.SEAT1, Seat.SEAT2, Seat.SEAT3);
		seat = PokerTable.getFirstSeatAfter(seats, Seat.SEAT6);
		Assert.assertEquals(Seat.SEAT1, seat);
	}

}
