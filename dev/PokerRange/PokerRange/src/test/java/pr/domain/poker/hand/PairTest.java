package pr.domain.poker.hand;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.cards.CardComparator;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.hand.Pair.RelativeRank;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;

public class PairTest {

	private static final int CARD_INDEX = 0;
	private static final int PAIR_INDEX = 1;
	private static final int TYPE_INDEX = 2;
	private static final int REL_RANK_INDEX =3;

	private static final String[][] validPairs = {
		{"2c 2d - 3s 9h Kc", 		"2c 2d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.UNDER_PAIR.toString()},
		{"2c 2d - 3s 9h Kc 5d", 	"2c 2d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.UNDER_PAIR.toString()},
		{"2c 2d - 3s 9h Kc 5d 7s", 	"2c 2d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.UNDER_PAIR.toString()},
		
		{"2c 3d - 2s 9h Kc", 		"2c 2s", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.BOTTOM_PAIR.toString()},
		{"2c 3d - 2s 9h Kc 5d", 	"2c 2s", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.BOTTOM_PAIR.toString()},
		{"2c 3d - 2s 9h Kc 5d 7s", 	"2c 2s", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.BOTTOM_PAIR.toString()},

		{"3c 9d - 2s 9h Kc", 		"9d 9h", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.SECOND_PAIR.toString()},
		{"3c 9d - 2s 9h Kc 5d",		"9d 9h", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.SECOND_PAIR.toString()},
		{"3c 9d - 2s 9h Kc 5d 7s",	"9d 9h", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.SECOND_PAIR.toString()},

		{"3c 5c - 2s 9h Kc 5d",		"5c 5d", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.THIRD_PAIR.toString()},
		{"3c 5c - 2s 9h Kc 5d 4c",	"5c 5d", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.THIRD_PAIR.toString()},
		
		{"3c 4s - 2s 9h Kc 5d 4c",	"4s 4c", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.FOURTH_PAIR.toString()},
		
		{"3c Kd - 2s 9h Kc",		"Kd Kc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.TOP_PAIR.toString()},
		{"3c Kd - 2s 9h Kc 5d",		"Kd Kc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.TOP_PAIR.toString()},
		{"3c Kd - 2s 9h Kc 5d 7s",	"Kd Kc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.TOP_PAIR.toString()},
		
		{"Ac Ad - 2s 9h Kc",		"Ac Ad", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.OVER_PAIR.toString()},
		{"Ac Ad - 2s 9h Kc 5d",		"Ac Ad", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.OVER_PAIR.toString()},
		{"Ac Ad - 2s 9h Kc 5d 7s",	"Ac Ad", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.OVER_PAIR.toString()},
		
		{"3c 3d - 2s 9h Kc",	 	"3c 3d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.SECOND_AND_A_HALF_PAIR.toString()},
		{"3c 3d - 2s 9h Kc 5d", 	"3c 3d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.THIRD_AND_A_HALF_PAIR.toString()},
		{"3c 3d - 2s 9h Kc 5d 7s", 	"3c 3d", Pair.Type.POCKET_PAIR.toString(), 		Pair.RelativeRank.FOURTH_AND_A_HALF_PAIR.toString()},
		
		{"3c Qd - 2s 9h Qc",	 	"Qd Qc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.TOP_PAIR.toString()},
		{"3c Qd - 2s 9h Qc Ks",	 	"Qd Qc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.SECOND_PAIR.toString()},
		{"3c Qd - 2s 9h Qc Ks Ah", 	"Qd Qc", Pair.Type.COMBINED_PAIR.toString(), 	Pair.RelativeRank.THIRD_PAIR.toString()},
		
		// BOARD PAIRS
		{"2s 3c - 4d Qh Qs", 		"Qh Qs", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.TOP_PAIR.toString()},
		{"2s 3c - 4d Qh Qs Js",		"Qh Qs", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.TOP_PAIR.toString()},
		{"2s 3c - 4d Qh Qs Js Td",	"Qh Qs", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.TOP_PAIR.toString()},
		
		{"2s 3c - 4d Qh Qs Ks",		"Qh Qs", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.SECOND_PAIR.toString()},
		{"2s 3c - 4d Qh Qs Ks Ad",	"Qh Qs", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.THIRD_PAIR.toString()},

		{"2s 4c - 3d 3h 6s", 		"3d 3h", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.SECOND_PAIR.toString()},
		{"2s 4c - 3d 3h 6s 7h",		"3d 3h", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.THIRD_PAIR.toString()},
		{"2s 4c - 3d 3h 6s 7h 8s",	"3d 3h", Pair.Type.BOARD_PAIR.toString(), 		Pair.RelativeRank.FOURTH_PAIR.toString()},
		
	};
	
	private static final String[] notPairs = {
		"2s 3c - 4d 5h 6s",
		"5s Qd - 2h 3s 8d 4s",
		"Js Qd - 2h 3s 8d 4s 9d",
		"2s 3c - 2d 3h 6s",
		"2s 2c - 2d 3h 6s",
		"2s 2c - 2d 2h 6s 6c"
	};
	
	private static final String[][] relativeRankTests = {
		{" - 2c 2d Ks Kh", "2c 2d", "", Pair.RelativeRank.SECOND_PAIR.toString()},
		{" - 2c 3d Ks Kh", "2c 2d", "", Pair.RelativeRank.THIRD_PAIR.toString()},
		{" - 2c 2d Ks Kh", "Ks Kh", "", Pair.RelativeRank.TOP_PAIR.toString()},
		{" - 2c 2d Ks Kh", "Qd Qc", "", Pair.RelativeRank.FIRST_AND_A_HALF_PAIR.toString()},
	};
	
	@Test
	public void testPair() throws DoesNotMakeHandException {
		for (String[] test : validPairs) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			List<Card> pairCards = CardUtil.createCards(test[PAIR_INDEX]);
			Collections.sort(pairCards, CardComparator.getInstance());
			
			List<Card> nonValueCards = CardUtil.asList(holeCards, boardCards);
			nonValueCards.removeAll(pairCards);
			
			Pair pair = new Pair(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.PAIR, pair.getValue());
			
			Assert.assertTrue(test[CARD_INDEX], pairCards.containsAll(pair.getPair()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(pairCards, pair.getValueCards()));
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(nonValueCards, pair.getNonValueCards()));
			
			Assert.assertEquals(test[CARD_INDEX], test[TYPE_INDEX], pair.getType().toString());
			Assert.assertEquals(test[CARD_INDEX], test[REL_RANK_INDEX], pair.getRelativeRank().toString());
		}
	}

	@Test
	public void testNotPair() {
		for (String test : notPairs) {
			HoleCards holeCards = CardUtil.createHoleCards(test);
			BoardCards boardCards = CardUtil.createBoardCards(test);

			try {
				new Pair(holeCards, boardCards);
				Assert.fail("Should not make a pair: " + test);
			} catch (DoesNotMakeHandException e) {
				// Do nothing.
			}
		}
	}
	
	@Test
	public void testRelativeRank() {
		for (String[] test : relativeRankTests) {
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			List<Card> pair = CardUtil.createCards(test[PAIR_INDEX]);
			
			RelativeRank relativeRank = Pair.calculateRelativeRank(pair, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX] + " " + test[PAIR_INDEX], test[REL_RANK_INDEX], relativeRank.toString());
		}
	}
	
	@Test
	public void testBoardCardsPair() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 2c 3s 8d Th");
		
		new Pair(null, boardCards);
	}

}
