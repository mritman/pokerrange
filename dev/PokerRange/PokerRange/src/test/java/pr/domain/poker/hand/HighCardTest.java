package pr.domain.poker.hand;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pr.domain.cards.Card;
import pr.domain.poker.BoardCards;
import pr.domain.poker.HoleCards;
import pr.domain.poker.util.CardUtil;

import com.google.common.collect.Iterables;


public class HighCardTest {

	private static final int CARD_INDEX = 0;
	private static final int HIGH_CARD_INDEX = 1;
	
	private static final String[][] highCardTests = {
		{"2c 3d - 5s 7c 9d", 		"9d"},
		{"2c 3d - 8s Tc Qd 6h 4c",  "Qd"},
		{"2c 3d - 8s Tc Qd 6h Ac",  "Ac"},
		{"2c 3d - 4s 5c 7h Qd Kc",  "Kc"},
		{"2c 3c - 4c 5c 7h Qd Ks",  "Ks"},
		{"2c 3c - 6c 8c Ts Js Kd",  "Kd"},
	};
	
	private static final String[][] nonHighCardTests = {
		{"2c 3c - 4c 5c 7h Qd Ac"},
		{"2c 3c - 4c 5c 6h Qd Tc"},
		{"2c 3c - 4c 5c 6h Qd Ts"},
		{"2c 3c - 6c 8c Tc Js Kd"},
		{"2c 2s - 6d 8h Tc Js Kd"},
		{"2c 2s - 2d 8h Tc Js Kd"},
		{"2c 8s - 2d 8h Tc Js Kd"},
		{"Ac 2s - 3d 8h Tc Js Ad"},
	};
	
	@Test
	public void testHighCard() throws DoesNotMakeHandException {
		for (String[] test : highCardTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			Card highestCard = Card.fromString(test[HIGH_CARD_INDEX]);
			
			List<Card> cards = CardUtil.asList(holeCards, boardCards);
			HighCard highCard = new HighCard(holeCards, boardCards);
			
			Assert.assertEquals(test[CARD_INDEX], HandValue.HIGH_CARD, highCard.getValue());
			Assert.assertEquals(test[CARD_INDEX], highestCard, highCard.getHighCard());
			
			Assert.assertTrue(test[CARD_INDEX], Iterables.elementsEqual(cards, highCard.getNonValueCards()));
			Assert.assertTrue(test[CARD_INDEX], highCard.getValueCards().isEmpty());
		}
	}
	
	@Test
	public void testNonHighCard() {
		for (String[] test : nonHighCardTests) {
			HoleCards holeCards = CardUtil.createHoleCards(test[CARD_INDEX]);
			BoardCards boardCards = CardUtil.createBoardCards(test[CARD_INDEX]);
			
			try {
				new HighCard(holeCards, boardCards);
				Assert.fail(test[CARD_INDEX]);
			} catch (DoesNotMakeHandException e) {
				// Test successful, do nothing.
			}
			
		}
	}
	
	@Test
	public void testCalculateBoardCardsHighCard() throws DoesNotMakeHandException {
		BoardCards boardCards = BoardCards.fromString("2s 4c 6d 8h");
		
		new HighCard(null, boardCards);
	}
	
}
