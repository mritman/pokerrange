package pr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.google.common.base.Charsets;

public class RegexTester {

    public static void main(String[] args) {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in, Charsets.UTF_8));
        
        while (true) {
        	String regex = "";
        	System.out.println(regex);
        	
            System.out.println("Enter regex: ");
            Pattern pattern = null;
			try {
				pattern = Pattern.compile(console.readLine());
			} catch (IOException | PatternSyntaxException e) {
				continue;
			}
            
            Matcher matcher = 
            pattern.matcher("Dealer: valerakazak posts small blind $0.01");
            
            boolean found = false;
            while (matcher.find()) {
                System.out.format("I found the text" +
                    " \"%s\" starting at " +
                    "index %d and ending at index %d.%n",
                    matcher.group(),
                    matcher.start(),
                    matcher.end());
                found = true;
            }
            if(!found){
                System.out.format("No match found.%n");
            }
        }
    }
}
