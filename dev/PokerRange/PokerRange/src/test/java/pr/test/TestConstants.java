package pr.test;

public final class TestConstants {

	private TestConstants() {}
	
	public static final String TEST_PERSISTENCE_UNIT_NAME = "org.pokerrange.test";
	
}
