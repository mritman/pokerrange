package pr.infrastructure.tableobserver.pokerstars.windowreader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pr.domain.poker.action.ActionType;
import pr.domain.poker.table.Seat;
import pr.domain.poker.table.TableSize;
import pr.infrastructure.tableobserver.ValidationException;
import pr.infrastructure.tableobserver.pokerstars.windowreader.PokerStarsDealerButtonReader;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData;
import pr.infrastructure.tableobserver.pokerstars.windowreader.tabledata.PokerStarsTableData.Position;
import pr.util.FileType;
import pr.util.ImageUtil;

public class TableWindowResourceExtractor {

	private static final Logger logger = LoggerFactory.getLogger(TableWindowResourceExtractor.class);

	@Test
	public void extractHandId() throws IOException {
		URL capture = getClass().getResource("capture9.png");
		Image image = ImageUtil.loadFxImage(capture);
		
		WritableImage handIdImage = new WritableImage(
				image.getPixelReader(), 
				PokerStarsTableData.HAND_ID_X, 
				PokerStarsTableData.HAND_ID_Y,
				PokerStarsTableData.HAND_ID_WIDTH,
				PokerStarsTableData.HAND_ID_HEIGHT);
		
		saveImage(handIdImage, PokerStarsHandIdReader.EMPTY_HAND_ID_IMAGE_NAME);
	}
	
	@Test
	public void extractDealerButton() throws IOException, ValidationException {
		URL capture = getClass().getResource("capture1.png");
		Image image = ImageUtil.loadFxImage(capture);
		
		Position position = PokerStarsTableData.getDealerPosition(TableSize.SIX, Seat.SEAT2);
		
		WritableImage dealerButtonImage = new WritableImage(image.getPixelReader(), position.getX(), position.getY(),
				PokerStarsTableData.DEALER_BUTTON_WIDTH, PokerStarsTableData.DEALER_BUTTON_HEIGHT);
		
		Image savedImage = saveImage(dealerButtonImage, PokerStarsDealerButtonReader.DEALER_BUTTON_IMAGE_NAME);
		logger.info("Saved " + PokerStarsDealerButtonReader.DEALER_BUTTON_IMAGE_NAME + ".");
		
		assertEquals(savedImage, dealerButtonImage);
	}
	
	@Test
	public void extractActions() throws IOException, ValidationException {
		URL capture5 = getClass().getResource("capture5.png");
		URL capture6 = getClass().getResource("capture6.png");
		URL capture8 = getClass().getResource("capture8.png");
		Image image5 = ImageUtil.loadFxImage(capture5);
		Image image6 = ImageUtil.loadFxImage(capture6);
		Image image8 = ImageUtil.loadFxImage(capture8);
		Image action1 = extractAction(image5, TableSize.SIX, Seat.SEAT2, ActionType.POST_SMALL_BLIND);
		Image action2 = extractAction(image5, TableSize.SIX, Seat.SEAT3, ActionType.POST_BIG_BLIND);
		Image action3 = extractAction(image5, TableSize.SIX, Seat.SEAT4, ActionType.RAISE);
		Image action4 = extractAction(image5, TableSize.SIX, Seat.SEAT5, ActionType.FOLD);
		Image action5 = extractAction(image6, TableSize.SIX, Seat.SEAT4, ActionType.BET);
		Image action6 = extractAction(image8, TableSize.SIX, Seat.SEAT4, ActionType.CALL);
		Image action7 = extractAction(image8, TableSize.SIX, Seat.SEAT5, ActionType.CHECK);
		
		List<Image> actionImages = Arrays.asList(action1, action2, action3,
				action4, action5, action6, action7);
		assertAllUnique(actionImages);
		
		logger.info("Finished.");
	}

	private Image extractAction(Image image, TableSize tableSize, Seat seat, ActionType actionType) throws IOException, ValidationException {
		Position position = PokerStarsTableData.getActionPosition(tableSize, seat);
		WritableImage actionImage = new WritableImage(image.getPixelReader(), position.getX(), position.getY(),
				PokerStarsTableData.ACTION_WIDTH, PokerStarsTableData.ACTION_HEIGHT);
		
		Image savedActionImage = saveImage(actionImage, actionType.name().toLowerCase() + FileType.PNG.extension());
		logger.info("Saved image for actiontype: {}", actionType.name());
		assertEquals(actionImage, savedActionImage);
		
		return savedActionImage;
	}

	private WritableImage saveImage(Image image, String fileName) throws IOException {
		File file = new File(fileName);
		ImageIO.write(SwingFXUtils.fromFXImage(image, null),
                FileType.PNG.toString(), file);
		
		return SwingFXUtils.toFXImage(ImageIO.read(file), null);
	}
	
	private void assertEquals(Image image1, Image image2) throws ValidationException {
		if (!ImageUtil.compareImage(image1, image2)) {
			throw new ValidationException("Saved image was not equal to the original image.");
		}
	}

	private void assertAllUnique(List<Image> images) throws ValidationException {
		Set<Image> imageSet = new TreeSet<>(new Comparator<Image>() {
			@Override
			public int compare(Image image1, Image image2) {
				return ImageUtil.compareImage(image1, image2) ? 0 : 1;
			}
		});
		
		imageSet.addAll(images);
		
		if (images.size() != imageSet.size()) {
			throw new ValidationException("Not all images are unique");
		}
	}

}
